#include <iostream>
#include <math.h>
#include <random>
#include <chrono>
#include <functional>

//
//  This problem is coded in such a way that the inputs
//  given in the problem can be directly plugged into the
//  parameters.
//  Given inputs: eta = 0.01, gamma = 0.1, beta = 0
//  diffusion = 0.02, rate = 0.1 and Term = 10
//  Output: 0.37986
//
void Problem6(double eta, double gamma, double beta,
                double diffusion, double rate, double Term)
{
  const double& alpha = diffusion * diffusion / rate;
  const double& gamma_sq = gamma * gamma;
  const double& phi1 = std::sqrt(gamma_sq + 2 * alpha);
  const double& b = (gamma + phi1) / alpha;
  const double& a = (-gamma + phi1) / alpha;
  const double& phi2 = (eta - alpha * beta * 0.5) / (a + b);

  double A = 0.0, B = 0.0, yield = 0.0, Z = 0.0;
  for (double dt = 0.01; dt <= Term; dt = dt + 0.01) {
    B = 2 * (std::exp(phi1 * dt) - 1) /
        ((gamma + phi1) * (std::exp(phi1 * dt) - 1) + 2 * phi1);
    A = (2 / alpha) *
        (a * phi2 * std::log(a - B) + 
        (phi2 + 0.5 * beta) * b * std::log(1 + B / b) - 
        (0.5 * B * beta) - a * phi2 * std::log(a));
    Z = std::exp(A - rate * B);
    yield = -std::log(Z) / dt;
    printf("%0.2f, %0.5f, %0.5f\n", Term-dt, Z, yield);
  }
}

// Problem 8
template<typename T>
double integral(const std::function<T(T)>& f, T a, T b)
{
  if (a == 0 && b == 0) return 0.0;
  const double& dt = 0.01;
  const int& N = (b - a) / dt;

  // numerical integration formula
  // extended trapezoidal rule
  // Integral(a,b) of f(x) = h * [f0/2 + Sum(f1..fN-1) + fN/2]
  // where h = (b - a) / N;
  // here, b = tau; a = 0 => h = dt
  double sum = 0.5 * (f(a) + f(b));
  for (int i = 1; i < N; ++i)
    sum += f(i * dt);

  return sum * dt;
}

//
//  Constants used here are copied from the spreadsheet
//  They are used as the starting point just as it is
//  done in the spreadsheet
//
double vol_1(double)
{ return 0.0064306548; }

double vol_2(double tau)
{
  return -0.0035565431 + 
          tau * (-0.0005683999) +
          tau * tau * 0.0001181915 + 
          tau * tau * tau * (-0.0000035939);
}

double vol_3(double tau)
{
  return -0.0047506715 +
          tau * 0.0017541783 +
          tau * tau * (-0.0001415249) +
          tau * tau * tau * 0.0000031274;
}

double drift(double tau)
{
  // drift = M1 + M2 + M3
  return (vol_1(tau) * integral<double>(vol_1, 0.0, tau) +
          vol_2(tau) * integral<double>(vol_2, 0.0, tau) +
          vol_3(tau) * integral<double>(vol_3, 0.0, tau));
}

//
//  function to calculate the ZCB bond price
//  This will be 1 MC simulation
//
double ZCB(double T, double seed_now, double seed_next, double dtau)
{
  if (T == 0) return seed_now;

  // random number generator
  std::mt19937 gen(std::chrono::system_clock::now().time_since_epoch().count());
  // mean = 0.0, std_dev = 1.0
  std::normal_distribution<double> dist(0.0,1.0);
  const double& dt = 0.01;  // time step
  double sum_dr = seed_now;
  double prev_seed_now = seed_now;
  double prev_seed_next = seed_next;
  for (double d = 0.0; d <= T; d = d + dt) {
    seed_now = prev_seed_now + drift(T) * dt +
                ( vol_1(T) * dist(gen) + 
                  vol_2(T) * dist(gen) + 
                  vol_3(T) * dist(gen)) * std::sqrt(dt) +
                ((prev_seed_next - prev_seed_now) / dtau) * dt;
    sum_dr += seed_now;
    seed_next = prev_seed_next + drift(T + 0.5) * dt +
                ( vol_1(T + 0.5) * dist(gen) + 
                  vol_2(T + 0.5) * dist(gen) + 
                  vol_3(T + 0.5) * dist(gen)) * std::sqrt(dt) +
                ((prev_seed_next - prev_seed_now) / dtau) * dt;
    prev_seed_now = seed_now;
    prev_seed_next = seed_next;
  }

  return std::exp(-sum_dr * dt);
}

//
//  function to call ZCB function for N simulations
//  The constants used here are again taken from the
//  spreadsheet and are the initial starting points
//  term here is taken as 2 years and number of
//  simulations is 5000
//
int HJM()
{
  const int N = 5000; // number of simulations
  double sum = 0.0;
  for (int i = 1; i <= N; ++i) {
    sum += ZCB(2.0, 0.0434977186, 0.0440537918, 0.5);
    std::cout << i << ", " << sum / i << std::endl;
  }
  return 0;
}

int Test()
{
  return HJM();
  //  Problem6(0.1*0.1, 0.1, 0.0, 0.02, 0.1, 10);
  //  return 0;
}