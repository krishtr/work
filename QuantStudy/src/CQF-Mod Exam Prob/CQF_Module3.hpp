#include <iostream>
#include <vector>
#include <chrono>
#include <math.h>
#include <random>
#include <limits>

double N(double x)
{
  return 0.5 * std::erfc(-x * M_SQRT1_2);
}

// Module 3 Exam: Problem 6
/**
  Calculating the implied vol using Newton-Raphson method
  using the values given in the question, the answer
  obtained is ~ 18% (17.9948%)
*/
void Problem6()
{
  // Given inputs
  const double spot = 101.5, strike = 100.0, time = 1.0/3.0, rate = 0.08;
  const double call = 6.51;

  // starting value = 20%
  double vol = 0.2;

  // pre-calculate the constants
  const double d1_numerator_const = log(spot/strike) + rate * time;
  const double sqrt_time = sqrt(time);
  const double pv = std::exp(-rate * time);
  const double pi = M_PI;

  // error tolerance = 0.001%
  const double error = 0.001/100.0;
  double tolerance = 0.0;

  // x(n + 1) = x(n) - (f(x(n)) / f'(x(n)))
  double d1 = 0.0, d2 = 0.0, fx = 0.0, vega = 0.0;
  for(;;) {
    d1 = (d1_numerator_const + 0.5 * vol * vol * time) / (vol * sqrt_time);
    d2 = d1 - vol * sqrt_time;
    fx = spot * N(d1) - strike * pv * N(d2) - call; // f(x) = 0 form
    vega = sqrt_time * spot * (1.0 / sqrt(2.0 * pi)) * std::exp(-0.5 * d1 * d1);
    tolerance = fx / vega;
    if (std::abs(tolerance) < error)
      break;
    vol -= tolerance;
  }

  std::cout << "Starting vol: 0.2" << std::endl;
  std::cout << "Calculated vol: " << vol << std::endl;
}

double square( double d = 0.0 )
{
  return d * d;
}

// Module 3 Exam: Problem 7
/**
  Has a vector of iterations which is the number of times
  the simulation is run. From this we can see that when
  N increases the accuracy of the results starts to 
  improve. Hence large values on N gives more accurate
  results.
*/
void Problem7()
{
  const int N = 100000000; // no. of steps
  const int step = 1000000; // increment per step

  // random generator
  std::default_random_engine gen;
  // normal distribution function with mean=0, stddev=1
  std::normal_distribution<double> dist( 0.0, 1.0 );

  double expected_result = 1.0;  
  // run the simulation scenarios
  for( int n = step; n <= N; n = n + step ) {
    double result = 0.0;
    for( int i = 0; i < n; ++i ) {
      result += square( dist( gen ) );
    }
    result /= n;
    // (error, N) <- Can plot this in Excel
    std::cout << ( expected_result - result ) << "," << n << std::endl;
  }
}

double func(double d)
{
  return std::exp( -1 * d * d ) + ( ( std::exp(-1 / ( d * d ) ) ) / ( d * d ) );
}

// Module 3 Exam: Problem 8
void Problem8()
{
  // vector of attempts to see how the error reduces
  // as the number of attempts increase
  const double upper = 1.0;
  const double lower = 0.0;

  // iterations vector
  std::vector<int> 
  N_Vec = { 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000 };

  std::default_random_engine gen;
  std::uniform_real_distribution<double> dist( lower, upper );
  for( const auto& n : N_Vec ) {
    double result = 0.0;
    for ( int i = 0; i < n; ++i ) {
      result += func( dist( gen ) );
    }
    result /= n; // average
    std::cout << n << ": " << result << std::endl;
  }
}

int Test()
{
  Problem6();
}
