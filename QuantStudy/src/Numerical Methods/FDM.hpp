#include <iostream>
#include <tuple>
#include <vector>
#include <memory>

#include "Fixed Income/Project 2 - HJM/src/Matrix.h"
//#include "nr.h"
#include "nrutil.h"
#include "nrtypes.h"
#define NR_END 1

/* allocate a double vector with subscript range v[nl..nh] */
double *vector(long nl, long nh)
{
  double *v;

  v=(double *)malloc((size_t) ((nh-nl+1+NR_END)*sizeof(double)));
  if (!v) {
    std::cout << ("allocation failure in vector()") << std::endl;
    exit(1);
  }
  return v-nl+NR_END;
}



// Finite Difference method
void FDM(double vol,double T,double r,double K,int N, bool isCall)
{
  // inputs -> vol, T, r, K, N, isCall?
  const double payoff = 1.0;

  std::vector<double> S(N, 0.0);  // to store Spot prices for an asset step
  auto dt = 0.9 / (vol * vol * N * N);
  const auto Nt = int(T / dt) + 1;
  dt = T / Nt;
  const auto ds = 2 * K / N;

  std::vector<std::vector<double>> V (N,std::vector<double>(Nt,0.0));

  std::cout << "Initialising..." << std::endl;
  for (int i = 0; i < N; ++i) {
    S[i] = i * ds;
    V[i][0] = (S[i] > K) ? payoff : 0;
  }

  std::cout << "Calculating Values..." << std::endl;
  // calculate new values of V
  for (int k = 1; k < Nt; ++k) {    // column
    for (int j = 1; j < N - 1; ++j) {  // row
      const auto delta = (V[j+1][k-1] - V[j-1][k-1]) / (2 * ds);
      const auto gamma = (V[j+1][k-1] + V[j-1][k-1] - 2*V[j][k-1]) / (ds * ds);
      const auto theta = (-0.5 * vol * vol * S[j] * S[j] * gamma) 
                  - (r * S[j] * delta) + (r * V[j][k-1]);
      V[j][k] = V[j][k-1] - (theta * dt);
    }

    // boundary conditions
    V[N-1][k] = 2 * V[N-2][k] - V[N-3][k];  // linear extrapolation
    V[0][k] = V[0][k-1] * (1 - r * dt);
  }

  std::cout << "Display: " << std::endl;
  for (int i = 0; i < N; ++i) {
    std::cout << S[i] << "," << V[i][0] << "," << V[i][Nt-1] << std::endl;
  }
}

void FDMBinary(const std::tuple<double,double,double,double,int,int,bool,double>& inputs)
{
  // inputs -> vol, T, r, K, N, isCall?
  const auto volMin    = std::get<0>(inputs);  // volatility
  const auto T      = std::get<1>(inputs);  // Expiry
  const auto r      = std::get<2>(inputs);  // interest rate
  const auto K      = std::get<3>(inputs);  // strike
  const auto N      = std::get<4>(inputs);  // number of asset steps (y axis)
  const auto payoff = std::get<5>(inputs);  // binary payoff
  const bool flag   = std::get<6>(inputs);  // call/put?
  const auto volMax   = std::get<7>(inputs);  // call/put?
  auto vol = volMax;

  std::vector<double> S(N, 0.0);  // to store Spot prices for an asset step
  auto dt = 0.9 / (vol * vol * N * N);
  const auto Nt = int(T / dt) + 1;
  dt = T / Nt;
  const auto ds = 2 * K / N;

  std::vector<std::vector<double>> V (N,std::vector<double>(Nt,0.0));

  std::cout << "Initialising..." << std::endl;
  for (int i = 0; i < N; ++i) {
    S[i] = i * ds;
    if (flag)
      V[i][0] = (S[i] > K) ? payoff : 0;
    else
      V[i][0] = (S[i] < K) ? payoff : 0;
  }

  std::cout << "Calculating Values..." << std::endl;
  // calculate new values of V
  for (int k = 1; k < Nt; ++k) {    // column
    for (int j = 1; j < N - 1; ++j) {  // row
      const auto delta = (V[j+1][k-1] - V[j-1][k-1]) / (2 * ds);
      const auto gamma = (V[j+1][k-1] + V[j-1][k-1] - 2*V[j][k-1]) / (ds * ds);
      if (gamma > 0)
        vol = volMin;
      const auto theta = (-0.5 * vol * vol * S[j] * S[j] * gamma) 
                  - (r * S[j] * delta) + (r * V[j][k-1]);
      V[j][k] = V[j][k-1] - (theta * dt);
    }

    // boundary conditions
    V[N-1][k] = 2 * V[N-2][k] - V[N-3][k];  // linear extrapolation
    V[0][k] = V[0][k-1] * (1 - r * dt);
  }

  std::cout << "Display: " << std::endl;
  for (int i = 0; i < N; ++i) {
    std::cout << S[i] << "," << V[i][0] << "," << V[i][Nt-1] << std::endl;
  }
}
double N(double x)
{
  return 0.5 * std::erfc(-x * M_SQRT1_2);
}

double GetBSPrice(double S, double K, double r, double vol, double T)
{
  double d1 = (1 / (vol * sqrt(T))) * (log(S / K) + (r + 0.5 * vol * vol) * T);
  double d2 = d1 - vol * sqrt(T);
  return S * N(d1) - K * std::exp(-r * T) * N(d2);
}

double GetVanillaPayoff(double S, double K) 
{
  return std::max(S - K, 0.0);
}

double GetBinaryPayoff(double S, double K, double Payoff) 
{
  return (S > K) ? Payoff : 0.0;
}

double FDMBinaryHedge(double K, double K1, double K2, double x1, double x2)
{
  const double volMin = 0.17;
  const double volMax = 0.23;
  double vol = volMax;
  const double r = 0.05;
  const int N = 100;
  const double payoff = 1.0;
  const double T = 1.0;

  std::vector<double> S(N, 0.0);  // to store Spot prices for an asset step
  auto dt = 0.9 / double(vol * vol * N * N);
  const auto Nt = int(T / dt) + 1;
  dt = T / double(Nt);
  const auto ds = 2.0 * double(K) / double(N);

  std::vector<std::vector<double>> V (N,std::vector<double>(Nt,0.0));
  //std::cout << "Initialising..." << std::endl;
  for (int i = 0; i < N; ++i) {
    S[i] = i * ds;
    V[i][0] = GetBinaryPayoff(S[i],K,payoff) +
               x1 * GetVanillaPayoff(S[i],K1) +
               x2 * GetVanillaPayoff(S[i],K2);
  }

  //std::cout << "Calculating Values..." << std::endl;
  // calculate new values of V
  for (int k = 1; k < Nt; ++k) {    // column
    for (int j = 1; j < N - 1; ++j) {  // row
      const auto delta = (V[j+1][k-1] - V[j-1][k-1]) / (2.0 * double(ds));
      const auto gamma = (V[j+1][k-1] + V[j-1][k-1] - 2*V[j][k-1]) / double(ds * ds);
      if(gamma > 0)
        vol = volMin;
      const auto theta = (-0.5 * vol * vol * S[j] * S[j] * gamma) 
                  - (r * S[j] * delta) + (r * V[j][k-1]);
      V[j][k] = V[j][k-1] - (theta * dt);
    }

    // boundary conditions
    V[N-1][k] = 2 * V[N-2][k] - V[N-3][k];  // linear extrapolation
    V[0][k] = V[0][k-1] * (1 - r * dt);
  }

  double result = 0.0;
  //std::cout << "Display: " << std::endl;
  for (int i = 0; i < N; ++i) {
    std::cout << S[i] << "," 
              << V[i][0] << "," 
              << V[i][Nt-1] 
              << std::endl;
    if(S[i] == 100.0)
      result = V[i][Nt - 1];
  }

  return result;
}

double func(Vec_DP& ptest)
{
  const double K1 = 80.0;
  const double K2 = 120.0;
  const double K = 100.0;
  const double S = 100.0;
  const double r = 0.05;
  const double vol = 0.2;
  const double T = 1.0;
  const double spread = 0.2;
  const double C1 = GetBSPrice(S,K1,r,vol,T);
  const double C2 = GetBSPrice(S,K2,r,vol,T);
  //std::cout << "C1: " << C1 << std::endl;
  //std::cout << "C2: " << C2 << std::endl;
  const double bid1 = C1 - spread / 2;
  const double ask1 = C1 + spread / 2;
  const double bid2 = C2 - spread / 2;
  const double ask2 = C2 + spread / 2;

  double mktPrice1 = (ptest[0] > 0) ? ask1 : bid1;
  double mktPrice2 = (ptest[1] > 0) ? ask2 : bid2;
  double hedgingCost = mktPrice1 * ptest[0] + mktPrice2 * ptest[1];

  return -(FDMBinaryHedge(K, K1, K2, ptest[0], ptest[1]) - hedgingCost);
}

/*

void FindCenter(Matrix<double>& p, int N, double* center)
{
  for(int j = 0; j < 2; ++j) {
    center[j] = 0;
    for(int i = 0; i < 3; ++i) {
      center[j] = center[j] + p(i,j) / 3;
    } // end for
  } // end for
}

void Ranking(double* y, int N, double& ilo, double& ylo, double& inlo, 
             double& ynlo, double& ihi, double& yhi)
{
  ilo = 1; inlo = 1; ihi = 1;
  ylo = y[int(ilo)];
  ynlo = y[int(inlo)];
  yhi = y[int(ihi)];
  for(int i = 1; i < 3; ++i) {
    if(y[i] < ynlo) {
      if(y[i] < ylo) {
        inlo = ilo;
        ynlo = ylo;
        ilo = i;
        ylo = y[i];
      } else {
        inlo = i;
        ylo = y[i];
      } // end if else
    } // end if
    if(y[i] > yhi) {
      ihi = i;
      yhi = y[i];
    } // end if
  } // next i
}

void Deform(double* y, Matrix<double>& p, int N, 
            double& ilo, double& ylo, int d)
{
  double ptest[2];
  double center[3];
  FindCenter(p,N,center);
  for(int j = 0; j < 2; ++j) {
    ptest[j] = d * p(int(ilo),j) + 
              (1 - d) * (3 / 2 * center[j] - p(int(ilo),j) / N);
  } // next j
  double ytest = func(ptest);
  if(ytest > ylo) {
    y[int(ilo)] = ytest;
    ylo = ytest;
  } // end if
  for(int j = 0; j < 2; ++j) {
    p(int(ilo),j) = ptest[j];
  }
}

void maximise(Matrix<double>& firstGuess, int N, double tol, int maxIt)
{
  // assuming N = 2, => N + 1 = 3
  // 1 to N+1 means N+1 elements is the size
  Matrix<double> output(3,3);
  double y[3];
  Matrix<double> p(3,2);
  double ptemp[2];
  Matrix<double> ranktest(3,2);

  double its = 0; 
  double rtol = 1.0;
  for(int i = 0; i < 3; ++i) {
    for(int j = 0; j < 2; ++j) {
      p(i,j) = firstGuess(i,j);
      ptemp[j] = p(i,j);
    }
    y[i] = func(ptemp);
  }

  double ilo,ylo,inlo,ynlo,ihi,yhi;
  double ysave;
  while((rtol > tol) && (its < maxIt)) {
    Ranking(y,N,ilo,ylo,inlo,ynlo,ihi,yhi);
    Deform(y,p,N,ilo,ylo,-1);
    double ytest = ylo;
    if(ytest >= yhi) {
      Deform(y,p,N,ilo,ylo,2);
      ytest = ylo;
    } else {
      if(ytest <= ynlo) {
        ysave = y[int(ilo)];
        Deform(y,p,N,ilo,ylo,0.5);
        ytest = ylo;
        if(ytest <= ysave) {
          for(int i = 0; i < 3; ++i) {
            if(i != ihi) {
              for(int j = 0; j < 2; ++j) {
                p(i,j) = 0.5 * (p(i,j) + p(int(ihi),j));
                ptemp[j] = p(i,j);
              } // next j
            } // end if
            y[i] = func(ptemp);
          } // next i
        } // end if
      } // end if
    }// end else
    its = its + 1;
    rtol = 2 * std::abs(yhi - ylo) / (std::abs(yhi) + std::abs(ylo));
  } // end while
  for(int i = 0; i < 3; ++i) {
    for(int j = 0; j < 2; ++j) {
      output(i,j) = p(i,j);
    } // next j
    output(i,2) = y[i];
  } // next i

  // display
  std::cout << "Output: " << std::endl;
  output.display();
}
*/

inline void get_psum(Mat_IO_DP& p, Vec_O_DP& psum)
{
  int i,j;
  double sum = 0.0;
  int mpts = p.nrows();
  int ndim = p.ncols();
  for(j = 0; j < ndim; ++j) {
    for(sum = 0, i = 0; i < mpts; ++i)
      sum += p[i][j];
    psum[j] = sum;
    std::cout << "psum(" << j << ")" << psum[j] << std::endl;
  }
}

#define TINY 1.0e-10
#define NMAX 5000
double amotry(Mat_IO_DP& p, Vec_O_DP& y, Vec_IO_DP& psum,int ihi, double fac)
{
  int j;
  double fac1, fac2, ytry;
  int ndim = p.ncols();
  Vec_DP ptry(ndim);
  fac1 = (1.0 - fac) / ndim;
  fac2 = fac1 - fac;
  for (j = 0; j < ndim; j++) 
    ptry[j] = psum[j] * fac1 - p[ihi][j] * fac2;
  ytry = func(ptry);
  if (ytry < y[ihi]) {
    y[ihi] = ytry;
    for (j = 0; j < ndim; j++) {
      psum[j] += ptry[j] - p[ihi][j];
      p[ihi][j] = ptry[j];
    }
  }
  return ytry;
}

void amoeba(Mat_IO_DP& p, Vec_IO_DP& y, double ftol, int& nfunk)
{
  int i, ihi, ilo, inhi, j;
  double rtol, ysave, ytry;
  int mpts = p.nrows();
  int ndim = p.ncols();
  Vec_DP psum(ndim,0);
  nfunk = 0;
  get_psum(p,psum);
  for (;;) {
    ilo = 0;
    ihi = y[0] > y[1] ? (inhi = 1, 0) : (inhi = 0, 1);
    for (i = 0; i < mpts; i++) {
      if (y[i] <= y[ilo]) ilo = i;
      if (y[i] > y[ihi]) {
        inhi = ihi;
        ihi = i;
      } else if (y[i] > y[inhi] && i != ihi) inhi = i;
    }
    rtol = 2.0 * fabs(y[ihi] - y[ilo]) / (fabs(y[ihi]) + fabs(y[ilo]) + TINY);
    if (rtol < ftol) {
      SWAP(y[0], y[ilo]);
      for (i = 0; i < ndim; i++)
        SWAP(p[0][i], p[ilo][i]);
      break;
    }
    if (nfunk >= NMAX) {
      std::cout << ("NMAX exceeded") << std::endl;
      return;
    }
    nfunk += 2;
    ytry = amotry(p, y, psum, ihi, -1.0);
    if (ytry <= y[ilo])
      ytry = amotry(p, y, psum, ihi, 2.0);
    else if (ytry >= y[inhi]) {
      ysave = y[ihi];
      ytry = amotry(p, y, psum, ihi, 0.5);
      if (ytry >= ysave) {
        for (i = 0; i < mpts; i++) {
          if (i != ilo) {
            for (j = 0; j < ndim; j++)
              p[i][j] = psum[j] = 0.5 * (p[i][j] + p[ilo][j]);
            y[i] = func(psum);
          }
        }
        nfunk += ndim;
        get_psum(p,psum);
      }
    } else --nfunk;
  }
}

void FDMTest()
{
  /*
  std::cout << "Call - 10%-30% Vol" << std::endl;
  const auto input1 = std::make_tuple(0.1,1,0.05,100,100,1,true,0.3);
  FDMBinary(input1);

  std::cout << "Put - 10%-30% Vol" << std::endl;
  const auto input2 = std::make_tuple(0.1,1,0.05,100,100,1,false,0.3);
  FDMBinary(input2);

  std::cout << "Call - 25%-30% Vol" << std::endl;
  const auto input3 = std::make_tuple(0.25,1,0.05,100,100,1,true,0.3);
  FDMBinary(input3);

  std::cout << "Put - 25%-30% Vol" << std::endl;
  const auto input4 = std::make_tuple(0.25,1,0.05,100,100,1,false,0.3);
  FDMBinary(input4);
  */
  //const auto input4 = std::make_tuple(100.0,80.0,120.0,0.5,0.5);
  //std::cout << FDMBinaryHedge(100,80,120,0.5,0.5) << std::endl;
  FDM(0.2,1,0.05,100,200,true);
  
  /*
  Mat_IO_DP firstguess(3,2);
  firstguess[0][0] = 1;
  firstguess[0][1] = 1;
  firstguess[1][0] = 0;
  firstguess[1][1] = 0;
  firstguess[2][0] = 0.5;
  firstguess[2][1] = 0.5;
  //maximise(firstguess, 2, 0.00001, 10000);

  Vec_DP y(3);
  Vec_DP ptemp(2);
  for(int i = 0; i < 3; ++i) {
    for(int j = 0; j < 2; ++j) {
      ptemp[j] = firstguess[i][j];
    }
    std::cout << ptemp[0] << "," << ptemp[1] << ": ";
    y[i] = func(ptemp);
    std::cout << y[i];
    std::cout << std::endl;
  }

  int fun;
  //amoeba(firstguess, y, 0.1, fun);
  //firstguess.display();
  */

  double d2 = (1 / 0.2) * (0.05 - 0.5 * 0.2 * 0.2);
  std::cout << "Binary Price: " << std::exp(-0.05) * N(d2) << std::endl;
}
