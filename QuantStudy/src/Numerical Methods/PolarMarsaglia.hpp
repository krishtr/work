#include <iostream>
#include <vector>
#include <math.h>
#include <chrono>
#include <random>

// Polar Marsaglia method
class normal_dist {
public:
  normal_dist(double mean, double stddev) 
    : valid_(false), mean_(mean), stddev_(stddev) {}
  template<class Engine>
  double operator()(Engine& eng) const
  {
    double result = 0.0;
    if (valid_) {
      valid_ = false;
      result = X2_;
    } else {
      double V1 = 0.0, V2 = 0.0, S = 0.0;
      for (;;) {
        V1 = 2 * myrand(eng) - 1;
        V2 = 2 * myrand(eng) - 1;
        S = V1 * V1 + V2 * V2;
        if (S < 1.0)
          break;
      }
      auto Fx = sqrt(-2.0 * log(S) / S);
      if (!valid_) {
        X2_ = Fx * V2;
        valid_ = true;
      }
      result = Fx * V1;
    }
    return (result * stddev_ + mean_);
  }
private:
  // random number between [0,1)
  template<class Engine>
  double myrand(Engine& gen) const
  {
    return (gen() / (double)gen.max());
  }
  mutable bool valid_;
  mutable double X2_; 
  double mean_, stddev_;
};

double square(double d)
{
  return d * d;
}

/**
  Uses handcoded normal distribution rather than the
  built-in function. This implementation seems to have
  faster output than the builtin version(!) :)
*/
void myProblem7()
{
  // iterations
  const std::vector<int> 
  N_Vec = {1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000};

  std::default_random_engine gen;
  const normal_dist dist(0.0, 1.0);
  for (const auto& n : N_Vec) {
    double result = 0.0;
    for (int i = 0; i < n; ++i) {
      result += square(dist(gen));
    }
    result /= n;
    std::cout << n << ": " << result << std::endl;
  }
}

void Test()
{
  auto start = std::chrono::system_clock::now();
  myProblem7();
  auto stop = std::chrono::system_clock::now();
  std::cout << "My version took: " << (stop - start).count() << std::endl;
}