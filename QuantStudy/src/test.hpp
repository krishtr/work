#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <boost/lexical_cast.hpp>
namespace
{
	int Dur2Days(const std::string& durationType)
	{
		int factor = -1;
		switch(durationType[0]) {
			case 'W':
				factor = 7;
				break;
			case 'M':
				factor = 31;
				break;
			case 'Y':
				factor = 365;
				break;
			default :
				factor = 2;
				break;
		}
		return factor;
	}
	struct Rate
	{
		std::string ccy;
		std::string durationType;
		int duration;
		Rate(const std::string& c, const std::string& dt, int d)
			: ccy(c), durationType(dt), duration(d)
		{}
		friend std::ostream& operator<< (std::ostream& os, const Rate& r);		
	};
	bool operator== (const Rate& lhs, int days)
	{
		return lhs.duration * Dur2Days(lhs.durationType) == days;
	}
	std::ostream& operator<< (std::ostream& os, const Rate& r)
	{
		os << r.ccy 
		   << boost::lexical_cast<std::string>(r.duration)
		   << std::string(1, r.durationType[0]);
		return os;
	}
	struct CompareRate
		: public std::binary_function<Rate, Rate, bool>
	{	
		bool operator()(const Rate& lhs, const Rate& rhs) const
		{
			return lhs.duration * Dur2Days(lhs.durationType) <= 
				rhs.duration * Dur2Days(rhs.durationType);
		}
	};
	struct CompareRateDuration 
		  : public std::binary_function<int, Rate, bool>
	{	
		bool operator()(int val, const Rate& r) const
		{
			return val <  r.duration * Dur2Days(r.durationType);
		}
		bool operator()(const Rate& r, int val) const
		{
			return r.duration * Dur2Days(r.durationType) < val;
		}
	};
	template<class T>
	void display(std::vector<T>& vec)
	{
		std::copy(vec.begin(), vec.end(), std::ostream_iterator<T>(std::cout, "\n"));
		std::cout << std::endl;
	}
}
int start()
{
	std::vector<Rate> vec;
	//vec.push_back(Rate("EURUSD", "Month", 1));
	//vec.push_back(Rate("EURUSD", "Week", 1));
	vec.push_back(Rate("EURUSD", "Year", 1));
	vec.push_back(Rate("EURUSD", "Month", 6));
	//vec.push_back(Rate("EURUSD", "Month", 3));
	//vec.push_back(Rate("EURUSD", "Year", 10));
	//vec.push_back(Rate("EURUSD", "Week", 8));
	vec.push_back(Rate("EURUSD", "Week", 2));
	//vec.push_back(Rate("EURUSD", "Spot", 0));
	//vec.push_back(Rate("EURUSD", "Year", 2));
	//vec.push_back(Rate("EURUSD", "Week", 10));
	//vec.push_back(Rate("EURUSD", "Month", 10));
	display(vec);
	std::sort(vec.begin(), vec.end(), CompareRate());
	std::cout << "Enter the durationType and duration: ";
	std::string dt; int d;
	std::cin >> dt >> d;
	int days = Dur2Days(dt) * d;	
	typedef std::vector<Rate>::const_iterator RateIter;
	/*
	std::vector<Rate>::const_iterator iter =
		std::upper_bound(vec.begin(), vec.end(), days, CompareRateDuration());
	if (iter == vec.end())
		std::cout << "upper Not found" << std::endl;
	else
		std::cout << "upper: " << *iter << std::endl;
	RateIter iter = std::lower_bound(vec.begin(), vec.end(), days, CompareRateDuration());
	if (iter == vec.end()) {
		std::cout << "Lower not found. Returning the last value: " << vec[vec.size() - 1];
	} else {
		if (*iter == days)
			std::cout << "Found exact match: " << *iter;
		else
			std::cout << "Did not find exact match. Returning previous element: " << *(--iter) << std::endl;
	}
	*/
	
	std::pair<RateIter, RateIter> pair =
		std::equal_range(vec.begin(), vec.end(), days, CompareRateDuration());
	std::cout << "Distance: " << std::distance(pair.first, pair.second) << std::endl;
	if (pair.first == pair.second) {
		std::cout << "Did not find exact match. ";
        if (pair.first == vec.begin())
            std::cout << "Returning first element: " << *(pair.first);
        else
            std::cout << "Returning previous element: " << *(--pair.first);
    } else {
		std::cout << "Found exact match: " << *(pair.first);
    }

	std::cout << std::endl;
	display(vec);

	return 0;
}
