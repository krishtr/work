#include <iostream>
#include <sstream>
#include <vector>

#include <thread>
#include <memory>
#include <functional>
#include <iterator>
#include <mutex>
#include <condition_variable>
#include "test.hpp"

std::mutex m;
std::condition_variable cond;
int count = 0;

void even()
{
  for (;;) {
    std::unique_lock<std::mutex> lock(m);
    cond.wait(lock, [&]() { return !(count % 2 != 0); } );
    std::cout << count++ << ",";
    if (count > 100) break;
    lock.unlock();
    cond.notify_one();
  }
}

void odd()
{
  for (;;) {
    std::unique_lock<std::mutex> lock(m);
    cond.wait(lock, [&]() { return !(count % 2 == 0); } );
    std::cout << count++ << ",";
    if (count > 100) break;
    lock.unlock();
    cond.notify_one();
  }
}

template<typename iterator, typename T>
T parallel_accumulate(iterator begin, iterator end, T init)
{
  if (begin == end) return init;
  const int dataSize = std::distance(begin, end);
  const int min_chunk_size = 25;
  int numThreads = (dataSize - 1) / min_chunk_size + 1;
  const int hwThreads = std::thread::hardware_concurrency();
  int threads = (hwThreads == 0) ? 2 : hwThreads;
  numThreads = std::min(numThreads, threads);
  const int chunk_size = dataSize / numThreads;

  std::cout << "chunk size: " << chunk_size
            << " dataSize: " << dataSize
            << " numThreads: " << numThreads << std::endl;

  std::vector<std::thread> threadPool(numThreads - 1);
  std::vector<T> results(numThreads,0);
  iterator block_start = begin;
  for (int i = 0; i < numThreads - 1; ++i) {
    iterator block_end = block_start;
    std::advance(block_end, chunk_size);
    auto lambda = [&]() {
      results[i] = std::accumulate(block_start, block_end, results[i]);
    };
    threadPool[i] = std::thread(lambda);
    block_start = block_end;
  }

  results[numThreads - 1] = std::accumulate(block_start, end, results[numThreads - 1]);
  std::for_each(threadPool.begin(), threadPool.end(), std::mem_fn(&std::thread::join));

  std::copy(results.begin(), results.end(), std::ostream_iterator<T>(std::cout, ", "));
  return std::accumulate(results.begin(), results.end(), init);
}

void reverse(char* begin, char* end, int64_t num)
{
  if (begin == end || !num) return;
  for (; begin < end && num; ++begin, --end, --num)
    std::swap(*begin, *end);
}

void parallel_reverse(char* str)
{
  const int64_t min_block_size = 25;
  const int64_t maxThreads = strlen(str) / min_block_size + 1;
  const int64_t hwThreads = std::thread::hardware_concurrency();
  const int64_t numThreads = std::min((hwThreads == 0 ? 2 : hwThreads), maxThreads);
  const int64_t block_size = strlen(str) / (2 * numThreads);
  std::vector<std::thread> pool(numThreads);
  for (int i = 0; i < numThreads; ++i) {
    const int64_t pos = block_size * i;
    char* begin = str + pos;
    char* end = str + strlen(str) - 1 - pos;
    pool[i] = std::thread(reverse, begin, end, block_size);
  }
  std::for_each(pool.begin(), pool.end(), std::mem_fn(&std::thread::join));
}

void rev(char* str)
{
  if (!str) return;
  if (strlen(str) == 1) return;
  char* begin = str;
  char* end = str + strlen(str) - 1;
  for (; begin < end; ++begin, --end)
    std::swap(*begin, *end);
}

void deleteChar(char* str, char* del)
{
  if (!str || !del) return;

  char* begin = del;
  char* end = del + strlen(del);
  for (; begin != end; ++begin) {
    char* ptr = std::remove(str, str + strlen(str), *begin);
    *ptr = '\0';
  }
}

void myDelete(char* str, char* del)
{
  if (!str || !del) return;

  char* begin = str, *end = str + strlen(str);
  char* iter = begin;
  char* delBegin = del, *delEnd = del + strlen(del);
  for (; iter != end; ++iter) {
    char* it = delBegin;
    for (; it != delEnd; ++it) {
      if (*it == *iter) {
        memmove(iter, iter+1, end - iter);
        --iter;
        break;
      }
    }
  }
}

struct QF
{
private:
  std::vector<int> vec;
public:
  explicit QF(int N)
  {
    vec.reserve(N);
    for (int i = 0; i < N; ++i)
      vec.push_back(i);
  }

  bool connected(int p, int q)
  {
    return vec[p] == vec[q];
  }

  void unionize(int p, int q)
  {
    int pid = vec[p];
    int qid = vec[q];
    for (uint i = 0; i < vec.size(); ++i)
      if (vec[i] == pid)
        vec[i] = qid;
  }

  void display()
  {
    std::copy(vec.begin(), vec.end(), std::ostream_iterator<int>(std::cout, ", "));
    std::cout << std::endl;
  }
};

struct QU
{
private:
  std::vector<int> vec;
public:
  explicit QU(int N)
  {
    vec.reserve(N);
    for (int i = 0; i < N; ++i)
      vec.push_back(i);
  }

  int root(int p)
  {
    while (p != vec[p])
      p = vec[p];
    return p;
  }

  bool connected(int p, int q)
  {
    return root(p) == root(q);
  }

  void unionize(int p, int q)
  {
    int pid = root(p);
    int qid = root(q);
    vec[pid] = qid;
  }

  void display()
  {
    std::copy(vec.begin(), vec.end(), std::ostream_iterator<int>(std::cout, ", "));
    std::cout << std::endl;
  }
};

struct WQU
{
public:
  struct data
  {
    explicit data(int id, int size) : id_(id), size_(size) {}
    friend std::ostream& operator<<(std::ostream&, const data&);
    int id_, size_;
  };
  explicit WQU(int N)
  {
    vec.reserve(N);
    for (int i = 0; i < N; ++i)
      vec.push_back(data(i, 1));
  }

  int root(int p)
  {
    while (p != vec[p].id_)
      p = vec[p].id_;
    return p;
  }

  bool connected(int p, int q)
  {
    return root(p) == root(q);
  }

  void unionize(int p, int q)
  {
    if (p == q) return;
    int pid = root(p);
    int qid = root(q);
    if (vec[pid].size_ < vec[qid].size_) {
      vec[pid].id_ = qid;
      vec[qid].size_ += vec[pid].size_;
    } else {
      vec[qid].id_ = pid;
      vec[pid].size_ += vec[qid].size_;
    }
  }

  void display()
  {
    std::copy(vec.begin(), vec.end(), std::ostream_iterator<data>(std::cout, ", "));
    std::cout << std::endl;
  }
private:
  std::vector<data> vec;
};

std::ostream& operator<<(std::ostream& os, const WQU::data& d)
{
  os << d.id_;
  return os;
}

void QFTester()
{
  QF qf(10);
  qf.display();
  qf.unionize(4,3);
  qf.unionize(3,8);
  qf.unionize(6,5);
  qf.unionize(9,4);
  qf.unionize(2,1);
  qf.unionize(5,0);
  qf.unionize(7,2);
  qf.unionize(6,1);
  qf.display();
}

void QUTester()
{
  QU qu(10);
  qu.display();
  qu.unionize(4,3);
  qu.unionize(3,8);
  qu.unionize(6,5);
  qu.unionize(9,4);
  qu.unionize(2,1);
  qu.unionize(5,0);
  qu.unionize(7,2);
  qu.unionize(6,1);
  qu.unionize(7,3);
  qu.display();
}

void WQUTester()
{
  WQU qu(10);
  qu.display();
  qu.unionize(4,3);
  qu.unionize(3,8);
  qu.unionize(6,5);
  qu.unionize(9,4);
  qu.unionize(2,1);
  qu.unionize(5,0);
  qu.unionize(7,2);
  qu.unionize(6,1);
  qu.unionize(7,3);
  qu.display();
}

struct date 
{
  unsigned short year, month, day;
  friend std::ostream& operator<<( std::ostream&, const date& );
};

std::ostream& operator<<( std::ostream& os, const date& d )
{
  os << "year: " << d.year << ", month: " << d.month
     << ", day: " << d.day << std::endl;
  return os;
}

void change( const int **collection, int size )
{
  const int **begin = &collection[0];
  const int **end = &collection[0] + size - 1;
  for ( ; begin < end; ++begin, --end ) {
    std::swap( *begin, *end );
  }
}


class test
{
	public:
	test() { std::cout << "ctor" << std::endl; }
	~test() { std::cout << "dtor" << std::endl; }
	test(const test&) { std::cout << "copy ctor" << std::endl; }
	std::vector<int> ret()
	{
		std::vector<int> ids;
		ids.push_back(1);
		ids.push_back(2);
		ids.push_back(3);
		return ids;
	}
};

test foo()
{
	test t;
	std::cout << "Created a test obj" << std::endl;
	return t;
}

std::vector<int> ret()
{
	std::vector<int> ids;
	ids.push_back(1);
	ids.push_back(2);
	ids.push_back(3);
	return ids;
}


int main()
{
	int ret = start();
	return ret;
}
