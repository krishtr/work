/**
 * Header file for Matrix class
 */

#ifndef MATRIX_H
#define MATRIX_H

#include <string>
#include <memory>
#include <cmath>
#include <vector>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>

/**
 * Matrix class that implements the Matrix datastructure
 */
template<class T>
class Matrix
{
public:
  explicit Matrix(int nrows, int ncols)
    : ptr_(new std::vector<dblVec>(ncols, dblVec(nrows, T()))),
      tolerance_(0.00000001)
  {}
  Matrix(Matrix<T>&&);
  ~Matrix() {}

  bool        isSymmetric() const;
  void        solveCovMatrix(Matrix<T>& mat) const;
  void        display() const;
  Matrix<T>   transpose() const;
  void        normalize();
  Matrix<T>   copy() const;

  std::vector<T>   column(int) const;
  std::vector<T>   row(int) const;

  // multiplication by scalar
  const Matrix<T> operator*(double x);
  T& operator()(int row, int col)
  {
    return (*ptr_)[col][row];
  }
  const T& operator()(int row, int col) const
  {
    return (*ptr_)[col][row];
  }

  int rowSize() const
  {
    return ptr_->begin()->size();
  }
  int colSize() const
  {
    return ptr_->size();
  }
  void resizeColumn(int size)
  {
    ptr_->resize(size);
  }
  void resizeRow(int column, int size)
  {
    (*ptr_)[column].resize(size);
  }

  void changeTolerance(double newVal) const
  {
    if (newVal <= 0)
      return;
    tolerance_ = newVal;
  }
private:
  Matrix(const Matrix&) = delete;
  Matrix& operator=(const Matrix&) = delete;
  typedef std::vector<T> dblVec;
  typedef std::vector<dblVec> vecOfVec;
  typedef std::unique_ptr<vecOfVec> uPtr;
  uPtr ptr_; // pointer to underlying structure of Matrix
  mutable double tolerance_;
};

/**
 * Move constructor
 */
template<class T>
Matrix<T>::Matrix(Matrix<T>&& m)
  : ptr_(std::move(m.ptr_)),
    tolerance_(m.tolerance_)
{}

/**
 * Checks if the current Matrix is symmetric and
 * returns the result
 */
template<class T>
bool Matrix<T>::isSymmetric() const
{
  if (!ptr_.get())
    return false;
  if (ptr_->begin() == ptr_->end())
    return false;
  // check if its square
  const int nrow = ptr_->begin()->size();
  const int ncol = ptr_->size();
  if (nrow != ncol) {
    std::cout << "Given Matrix is not Square!" << std::endl;
    return false;
  }
  for (int i = 0; i < nrow; ++i) {
    for (int j = i + 1; j < ncol; ++j) {
      if ((*ptr_)[j][i] != (*ptr_)[i][j])
        return false;
    }
  }
  return true;
}

/**
 * Normalise the matrix
 */
template<class T>
void Matrix<T>::normalize()
{
  // basic checks
  if (!ptr_.get())
    return;
  if (ptr_->begin() == ptr_->end())
    return;
  const int row_size = ptr_->begin()->size();
  const int col_size = ptr_->size();
  // mean and stddev
  dblVec mean(col_size, 0.0);
  for (int j = 0; j < col_size; ++j) {
    for (int i = 0; i < row_size; ++i)
      mean[j] += (*ptr_)[j][i];
    mean[j] /= (double)row_size;
  }

  // subtract the mean
  for (int j = 0; j < col_size; ++j) {
    for (int i = 0; i < row_size; ++i)
      (*ptr_)[j][i] -= mean[j];
  }
}

/**
 * Solves to figure out the covariance matrix of the current
 * matrix. Expects the placeholder matrix as parameter
 */
template<class T>
void Matrix<T>::solveCovMatrix(Matrix<T>& mat) const
{
  if (!ptr_.get())
    return;
  if (ptr_->begin() == ptr_->end())
    return;
  const int row_size = ptr_->begin()->size();
  const int col_size = ptr_->size();
  dblVec mean(col_size, 0.0);
  for (int j = 0; j < col_size; ++j) {
    for (int i = 0; i < row_size; ++i)
      mean[j] += (*ptr_)[j][i];
    mean[j] /= (double)row_size;
  }
  for (int j = 0; j < col_size; ++j) {
    for (int i = 0; i < row_size; ++i)
      (*ptr_)[j][i] -= mean[j];
  }
  const int size = ptr_->size();
  for (int j1 = 0; j1 < size; ++j1) {
    for (int j2 = j1; j2 < size; ++j2) {
      for (int i = 0; i < row_size; ++i) {
        mat(j1, j2) += (*ptr_)[j2][i] * (*ptr_)[j1][i];
      }
      mat(j1, j2) /= row_size;
      mat(j2, j1) = mat(j1, j2);
    }
  }
}

/**
 * Displays the contents of Matrix in a formatted way
 */
template<class T>
void Matrix<T>::display() const
{
  // basic checks
  if (!ptr_.get())
    return;
  for (int i = 0; i < rowSize(); ++i) {
    for (int j = 0; j < colSize(); ++j)
      std::cout << std::setprecision(9) << std::fixed << "\t"
                << (*this)(i, j);
    std::cout << std::endl;
  }
}

/**
 * Generates the transpose of the current Matrix
 */
template<class T>
Matrix<T> Matrix<T>::transpose() const
{
  Matrix<T> m(this->colSize(), this->rowSize());
  for (int i = 0; i < this->rowSize(); ++i) {
    for (int j = 0; j < this->colSize(); ++j)
      m(j, i) = (*this)(i, j);
  }
  return m;
}

/**
 * Generates a copy of the current Matrix(this)
 */
template<class T>
Matrix<T> Matrix<T>::copy() const
{
  Matrix<T> m(this->rowSize(), this->colSize());
  for (int i = 0; i < this->rowSize(); ++i) {
    for (int j = 0; j < this->colSize(); ++j)
      m(i, j) = (*this)(i, j);
  }
  return m;
}

/**
 * overloaded multiplication operator
 */
template<typename T>
inline const Matrix<T> Matrix<T>::operator*(double x)
{
  Matrix<T> temp(rowSize(), colSize());
  for (int j = 0; j < colSize(); ++j) {
    for (int i = 0; i < rowSize(); ++i)
      temp(i, j) = (*this)(i, j) * x;
  }
  return temp;
}

/**
 * returns column number asked for
 */
template<typename T>
inline std::vector<T> Matrix<T>::column(int j) const
{
  std::vector<T> temp(rowSize(), T());
  for (int i = 0; i < rowSize(); ++i)
    temp[i] = (*this)(i, j);
  return temp;
}

/**
 * returns the row vector asked for
 */
template<typename T>
inline std::vector<T> Matrix<T>::row(int i) const
{
  std::vector<T> temp(colSize(), T());
  for (int j = 0; j < colSize(); ++j)
    temp[j] = (*this)(i, j);
  return temp;
}

#endif