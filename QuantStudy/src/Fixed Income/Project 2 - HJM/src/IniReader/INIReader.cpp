// Read an INI file into easy-to-access name/value pairs.

#include <algorithm>
#include <cctype>
#include <cstdlib>
#include "ini.h"
#include "INIReader.h"

using namespace std;

/**
 * Constructor
 */
INIReader::INIReader(const std::string& filename)
{
  _error = ini_parse(filename.c_str(), ValueHandler, this);
}

/**
 * [routine to Parse errors]
 * @return [parsed error]
 */
int INIReader::ParseError()
{
  return _error;
}

/**
 * [generic get routine]
 * @param  section       [field]
 * @param  name          [value]
 * @param  default_value [default]
 * @return               [string representation of value]
 */
string INIReader::Get(const std::string& section, const std::string& name, const std::string& default_value)
{
  string key = MakeKey(section, name);
  return _values.count(key) ? _values[key] : default_value;
}

/**
 * [Routine to read Int sections]
 * @param  section       [field]
 * @param  name          [value]
 * @param  default_value [default if any]
 * @return               [int value]
 */
long INIReader::GetInteger(const std::string& section, const std::string& name, long default_value)
{
  string valstr = Get(section, name, "");
  const char* value = valstr.c_str();
  char* end;
  // This parses "1234" (decimal) and also "0x4D2" (hex)
  long n = strtol(value, &end, 0);
  return end > value ? n : default_value;
}

/**
 * [Routine to read Real/Double sections]
 * @param  section       [field]
 * @param  name          [value]
 * @param  default_value [default if any]
 * @return               [double value]
 */
double INIReader::GetReal(const std::string& section, const std::string& name, double default_value)
{
  string valstr = Get(section, name, "");
  const char* value = valstr.c_str();
  char* end;
  double n = strtod(value, &end);
  return end > value ? n : default_value;
}

/**
 * [Routine to read boolean sections]
 * @param  section       [field]
 * @param  name          [value]
 * @param  default_value [default value if any]
 * @return               [bool value]
 */
bool INIReader::GetBoolean(const std::string& section, const std::string& name, bool default_value)
{
  string valstr = Get(section, name, "");
  // Convert to lower case to make string comparisons case-insensitive
  std::transform(valstr.begin(), valstr.end(), valstr.begin(), ::tolower);
  if (valstr == "true" || valstr == "yes" || valstr == "on" || valstr == "1")
    return true;
  else if (valstr == "false" || valstr == "no" || valstr == "off" || valstr == "0")
    return false;
  else
    return default_value;
}

/**
 * [Makes key for map given params]
 * @param  section [field]
 * @param  name    [value]
 * @return         [manufactured key]
 */
string INIReader::MakeKey(const std::string& section, const std::string& name)
{
  string key = section + "." + name;
  // Convert to lower case to make section/name lookups case-insensitive
  std::transform(key.begin(), key.end(), key.begin(), ::tolower);
  return key;
}

/**
 * [callback handler that handles the appr callback]
 * @param  user    [optional user field]
 * @param  section [which section?]
 * @param  name    [name of the field]
 * @param  value   [value of this field]
 * @return         [return code]
 */
int INIReader::ValueHandler(void* user, const char* section, const char* name,
                            const char* value)
{
  INIReader* reader = (INIReader*)user;
  string key = MakeKey(section, name);
  if (reader->_values[key].size() > 0)
    reader->_values[key] += "\n";
  reader->_values[key] += value;
  return 1;
}
