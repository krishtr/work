#include <iostream>
#include <exception>
#include <random>
#include <chrono>
#include <algorithm>
#include <map>
#include <iterator>
#include "Math.h"
#include "IniReader/INIReader.h"
#include "Application.h"

/**
 * Collects the data given the filename and delimiter into the
 * Matrix object provided
 * @param   m - matrix object to collect the data
 * @param   filename - file to read
 * @param   delimiter - delimiter that separates data in the file
 *          default is CSV
 */
template<typename T>
bool Application::CollectData(Matrix<T>& m,
                              const std::string& filename,
                              char delimiter)
{
  //
  // read the fwd rates data first and then figure out
  // the differences and put that into m
  //
  std::ifstream ifs(filename);
  if (!ifs) {
    std::cout << "Error opening the file \"" << filename <<
              "\". Check its path." << std::endl;
    return false;
  }
  std::string line;
  int i = 0, index = 0;
  const int row_size = fwdRatesData.rowSize();
  const int col_size = fwdRatesData.colSize();
  tenorVec_.resize(col_size);
  while (std::getline(ifs, line, '\n') && i < row_size) {
    if (line.empty())
      continue;
    std::stringstream ss(line);
    std::string word;
    int j = 0;
    while (std::getline(ss, word, delimiter) && j < col_size) {
      if (word.empty() || (atof(word.c_str()) == 0.0))
        continue;
      if (index == 0)
        tenorVec_[j++] = atof(word.c_str());
      else
        fwdRatesData(i, j++) = atof(word.c_str());
    }
    if (index == 0)
      ++index;
    else
      ++i;
  }
  // populate the differences
  for (int j = 0; j < col_size; ++j) {
    for (int i = 1; i < row_size; ++i)
      m(i - 1, j) = fwdRatesData(i, j) - fwdRatesData(i - 1, j);
  }
  return true;
}

/**
 * init - Initializes the row and column sizes
 * @param rowSize - row size
 * @param colSize - column size
 */
void Application::init(int rowSize, int colSize)
{
  fwdRatesData.resizeColumn(colSize);
  for (int i = 0; i < colSize; ++i)
    fwdRatesData.resizeRow(i, rowSize - 1);
}

/**
 * Function that calculates the fitted vol given the maturity
 * @param  index - index of the coefficient vector to read the estimated
 *                  coefficients from
 * @param  tau  - maturity for which the fitted vol is calculated
 * @return - the calculated fitted vol for the maturity
 */
double Application::vol(int index, double tau) const
{
  // return median value for first vol
  if (index == 0)
    return fittedVol(0, 0); // is constant
  else
    return  coeffVec[index][0] +
            coeffVec[index][1] * tau +
            coeffVec[index][2] * tau * tau +
            coeffVec[index][3] * tau * tau * tau;
}

/**
 * Calculation of drift(mbar). Formula = Sum(vol * integral(0->tau)(vol))
 * @param  tau - maturity
 * @return - calculated drift after applying the formula
 * @assumptions - Here we are assuming that the vol Matrix is composed
 * of top 3 Eigen Vectors from PCA analysis
 */
double Application::drift(double tau) const
{
  // drift = M1 + M2 + M3
  using namespace std::placeholders;
  double result = 0.0;
  for (auto i : {
         0, 1, 2
       })
    result += vol(i, tau) * Math::integral(std::bind(&Application::vol, this, i, _1),
                                           0.0,
                                           tau);
  return result;
}

/**
 * Core fwd price generator engine using HJM formula
 * @param  T         [maturity to which rates are to be generated]
 * @param  seed_now  [seed value for current tenor]
 * @param  driftNow  [drift value for current tenor]
 * @param  seed_next [seed value for next tenor]
 * @param  driftNext [drift value for next tenor]
 * @param  dtau      [time diff b/w current and next tenor]
 * @param  isZCB     [is this for ZCB or just fwd rates?]
 * @param  tenorNow  [current tenor]
 * @param  tenorNext [next tenor]
 * @return           [if isZCB is true this gives the sum of rates
 *                   generated till T else it gives the fwd rate
 *                   generated until T using HJM]
 */
double Application::CoreFwdPriceGen(double T, double seed_now, double driftNow,
                                    double seed_next, double driftNext,
                                    double dtau, bool isZCB, double tenorNow,
                                    double tenorNext) const
{
  if (T == 0) return seed_now;
  // random number generator (Marsienne Twister)
  std::mt19937 gen(std::chrono::system_clock::now().time_since_epoch().count());
  // mean = 0.0, std_dev = 1.0
  std::normal_distribution<double> dist(0.0, 1.0);
  const double& dt = 0.01;  // time step
  double sum_dr = seed_now;
  double prev_seed_now = seed_now;
  double prev_seed_next = seed_next;
  for (double d = 0.0; d <= T; d = d + dt) {
    double result = 0.0;
    dist.reset();
    for (auto i : {0, 1, 2})
      #pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
      result += vol(i, tenorNow) * dist(gen);
    seed_now = prev_seed_now + driftNow * dt +
               (result) * std::sqrt(dt) +
               ((prev_seed_next - prev_seed_now) / dtau) * dt;
    if (isZCB)
      sum_dr += seed_now;
    result = 0.0;
    dist.reset();
    for (auto i : {0, 1, 2})
      result += vol(i, tenorNext) * dist(gen);
    seed_next = prev_seed_next + driftNext * dt +
                (result) * std::sqrt(dt) +
                ((prev_seed_next - prev_seed_now) / dtau) * dt;
    prev_seed_now = seed_now;
    prev_seed_next = seed_next;
  }
  if (isZCB)
    return sum_dr;
  else
    return seed_now;
}

/**
 * [Caplet Value calculator]
 * @return [Caplet price generated]
 */
double Application::Caplet() const
{
  // caplet start and caplet end gives the end points.
  dblVec::const_iterator csIter = tenorVec_.begin();
  dblVec::const_iterator ceIter = std::find(tenorVec_.begin(),
                                            tenorVec_.end(),
                                            capletStart_);
  if(ceIter == tenorVec_.end())
    throw std::runtime_error("Cap End tenor not found");
  double sum = 0.0;
  const int row = fwdRatesData.rowSize() - 1;
  for(; csIter <= ceIter; ++csIter) {
    uint col = csIter - tenorVec_.begin(); // gives the index
    if (col == (tenorVec_.size() - 1))
      col = tenorVec_.size() - 2; // if its the last one, use the penultimate
    auto dtau = tenorVec_[col + 1] - tenorVec_[col];

    // generate a forward price for each tenor
    sum += CoreFwdPriceGen(*csIter, fwdRatesData(row,col) * 0.01, driftVec_[col],
                           fwdRatesData(row,col + 1) * 0.01, driftVec_[col + 1],
                           dtau, false, tenorVec_[col], tenorVec_[col + 1]);
  }
  double avg = sum / (capletStart_ * 2.0 + 1.0);
  double DF = ZCB(capletEnd_, fwdRatesData(row, 0) * 0.01, driftVec_[0],
               fwdRatesData(row, 1) * 0.01, driftVec_[1], 0.5,
               tenorVec_[0], tenorVec_[1]);
  double Libor = std::exp(avg) - 1;
  return std::max(Libor - capletStrike_, 0.0) * DF * (capletEnd_ - capletStart_);
}

/**
 * Calculates the Zero Coupon bond prices given parameters below
 * @param  T - Maturity for which this is calculated
 * @param  seed_now - Initial seeding values
 * @param  seed_next - Initial seeding values
 * @param  dtau - time period difference between maturities
 * @return - ZCB price
 */
double Application::ZCB(double T, double seed_now, double driftNow,
                        double seed_next, double driftNext, double dtau,
                        double tenor, double tenorNext) const
{
  //------ZCB Calculation------
  const double sum_dr = CoreFwdPriceGen(T, seed_now, driftNow,
                                        seed_next, driftNext, dtau, true,
                                        tenor, tenorNext);
  return std::exp(-sum_dr * 0.01);
}

/**
 * This routine calculates the simulated ZCB and will
 * be used to test our implementation against analytical ZCB
 * prices
 * @return - return code. Success - 0.
 */
void Application::HJMZCB() const
{
  // take the last row of data
  const int row = fwdRatesData.rowSize() - 1;
  // For ZCB, we need only the first column of rates
  const int col = 0;
  //
  // fwdRatesData will give us the seeding factors
  // required for that maturity
  std::cout << std::endl;
  std::cout << "maturity: " << zcbMaturity_
            << ", Seed1: " << fwdRatesData(row, col)
            << ", Seed2: " << fwdRatesData(row, col + 1)
            << std::endl;
  double zcb = 0.0;
  const double driftT = driftVec_[0];
  const double driftTplus = driftVec_[1];
  for (int i = 1; i <= zcbSimulations_; ++i) {
    // use the last row of fwdRates data as seed
    zcb += ZCB(zcbMaturity_, fwdRatesData(row, col) * 0.01, driftT,
               fwdRatesData(row, col + 1) * 0.01, driftTplus,
               tenorVec_[col + 1] - tenorVec_[col],
               tenorVec_[col], tenorVec_[col + 1]);
    if (szcb() || all()) {
      if (i == 1)
        std::cout << "ZCB iterations" << std::endl;
      std::cout << i << ", " << zcb / i << std::endl;
    }
    if (i == zcbSimulations_) {
      std::cout << std::endl;
      std::cout << "ZCB price: " << zcb / i << std::endl;
    }
  }
}

/**
 * This routine calculates the simulated Caplet and will
 * be used to test our implementation against analytical Caplet
 * prices
 * @return - void.
 */
void Application::HJMCaplet() const
{
  //------Caplet Calculation------
  std::cout << std::endl;
  double capletPrice = 0.0;
  //
  // i is just a counter
  for (int i = 1; i <= capletSimulations_; ++i) {
    // use the last row of fwdRates data as seed
    capletPrice += Caplet();
    if (cap() || all()) {
      if (i == 1)
        std::cout << "Caplet iterations" << std::endl;
      std::cout << i << ", " << capletPrice / i << std::endl;
    }
    if (i == capletSimulations_) {
      std::cout << std::endl;
      std::cout << "Caplet Price for " << capletStart_ << "(y) to "
                << "maturity " << capletEnd_ << "(y) is = "
                << capletPrice / i << std::endl;
    }
  }
}

/**
 * [Start routine-Reads data and generates fitted Vols
 *  and Drifts in preperation to generate forward rates
 *  using HJM]
 * @param  argc [number of command line args]
 * @param  argv [array of cmd line args]
 * @return      [success flag]
 */
bool Application::start(int argc, char* argv[])
{
  if (argc != 2) {
    std::cout << "Wrong syntax." << std::endl;
    std::cout << "Usage: <exe> <ini>" << std::endl;
    return false;
  }

  if (!ReadIniFile(argv[1]))
    return false;

  const int n = nrows_;
  const int m = ncols_;

  // resize the underlying matrix
  std::cout << "Initializing data structures..";
  init(n, m);
  std::cout << "...Complete" << std::endl;
  std::cout << std::endl;
  std::cout << "filename: " << dataFile_ << std::endl;
  std::cout << "#Rows: " << n << ", #Cols: " << m << std::endl;

  // read file and collect data
  // ***************************************
  // ******[ default CSV format! ]****** //
  // ***************************************
  std::cout << "Collecting Data..";
  Matrix<double> data(n - 2, m);
  if (!CollectData(data, dataFile_))
    return false;
  std::cout << "...Complete" << std::endl;
  std::cout << std::endl;
  //
  // normalize the output
  data.normalize();
  //
  // Covariance Matrix
  std::cout << "Calculating the Covariance Matrix..";
  Matrix<double> covMatrix(m, m);
  data.solveCovMatrix(covMatrix);
  std::cout << "...Complete" << std::endl;
  std::cout << std::endl;
  // annualize the rates data
  const double factor = 252.0 / 10000.0;
  for (int i = 0; i < covMatrix.rowSize(); ++i) {
    for (int j = 0; j < covMatrix.colSize(); ++j)
      covMatrix(i, j) *= factor;
  }
  if (cov() || all()) {
    std::cout << "Annualised Covariance Matrix: " << std::endl;
    covMatrix.display();
    std::cout << std::endl;
  }
  //
  // --------Eigen Values/Vectors Calculation--------
  std::cout << "Finding the Eigen Values and Eigen Vectors..";
  dblVec eigenValues(covMatrix.colSize(), 0.0), e(covMatrix.colSize(), 0.0);
  // Householder Reduction
  if (!Math::houseHolderRedn(covMatrix, eigenValues, e))
    return false;
  //
  // QL Implicit method to calculate Eigen values/Vectors
  if (!Math::tQLImplicit(eigenValues, e, covMatrix))
    return false;
  std::cout << "...Complete" << std::endl;
  std::cout << std::endl;
  if (eigVal() || all()) {
    std::cout << "Eigen Values: " << std::endl;
    for (auto& i : eigenValues)
      std::cout << i << std::endl;
    std::cout << std::endl;
  }
  if (eigVec() || all()) {
    std::cout << "EigenVectors(unsorted): " << std::endl;
    covMatrix.display();
    std::cout << std::endl;
  }
  //
  // PCA Matrix
  // taking only 3 largest PCA values
  // --------PCA and Vol Matrix--------
  std::cout << "Building the PCA matrix..";
  const int tauCol = 3;
  Matrix<double> PCAMatrix(covMatrix.rowSize(), tauCol);
  for (int j = 0; j < covMatrix.colSize(); ++j) {
    for (int i = 0; i < tauCol; ++i)
      PCAMatrix(j, i) = covMatrix(j, covMatrix.colSize() - i - 1);
  }
  std::cout << "...Complete" << std::endl;
  std::cout << std::endl;
  if (pca() || all()) {
    std::cout << "PCA Matrix: " << std::endl;
    PCAMatrix.display();
    std::cout << std::endl;
  }
  //
  //  Generate Vol Matrix = sqrt(EigenValues[i]) * PCAMatrix[i]
  //  We take the 3 largest EigenValues and their corresponding
  //  EigenVectors
  std::cout << "Building the Vol Matrix..";
  Matrix<double> VolMatrix(covMatrix.rowSize(), tauCol);
  for (int j = 0; j < tauCol; ++j) {
    for (int i = 0; i < VolMatrix.rowSize(); ++i)
      VolMatrix(i, j) = std::sqrt(eigenValues[m - 1 - j]) * PCAMatrix(i, j);
  }
  std::cout << "...Complete" << std::endl;
  std::cout << std::endl;
  if (svol() || all()) {
    std::cout << "Vol Matrix: " << std::endl;
    VolMatrix.display();
    std::cout << std::endl;
  }
  //  Generate tau (maturity) matrix
  std::cout << "Building the Maturity Matrix..";
  Matrix<double> tauMatrix(VolMatrix.rowSize(), tauCol);
  for (int j = 0; j < tauCol; ++j) {
    for (int i = 0; i < tauMatrix.rowSize(); ++i) {
        tauMatrix(i, j) = std::pow(tenorVec_[i], j + 1);
    }
  }
  std::cout << "...Complete" << std::endl;
  std::cout << std::endl;
  //
  // --------Calculate Fitting coefficients--------
  std::cout << "Finding the fitted coefficients for each Maturity..";
  for (int i = 0; i < tauCol; ++i) {
    //
    // solving after QR decomposition
    auto pairVol(Math::QRSolve(VolMatrix.column(i), tauMatrix));
    //
    // dont need it anymore, so move it
    coeffVec.push_back(std::move(pairVol.first));
  }
  std::cout << "...Complete" << std::endl;
  std::cout << std::endl;
  std::cout << "Building the Fitted Vol Matrix..";
  fittedVol.resizeColumn(3);
  for (int i : {0, 1, 2})
    fittedVol.resizeRow(i, tauMatrix.rowSize());
  dblVec tempVec(VolMatrix.column(0));
  std::sort(tempVec.begin(), tempVec.end());
  double median = 0.0;
  if ((tempVec.size() % 2) == 0)
    median = (tempVec[(tempVec.size() * 0.5) - 1] +
             tempVec[tempVec.size() * 0.5]) * 0.5;  // even
  else
    median = tempVec[tempVec.size() * 0.5];         // odd

  // --------Fit the vols--------
  for (unsigned int i = 0; i < tempVec.size(); ++i)
    fittedVol(i, 0) = median;
  for (int j = 1; j < 3; ++j) {
    for (int i = 0; i < tauMatrix.rowSize(); ++i)
      fittedVol(i, j) = vol(j, tauMatrix(i, 0));
  }
  std::cout << "...Complete" << std::endl;
  std::cout << std::endl;
  if (fit() || all()) {
    std::cout << "Fitted Vol Matrix: " << std::endl;
    fittedVol.display();
    std::cout << std::endl;
  }

  // --------Pre-calculate drift factors--------
  std::cout << "Building the Drift Vector..";
  PopulateDriftVector();
  std::cout << "...Complete" << std::endl;
  std::cout << std::endl;
  if (mbar() || all()) {
    std::cout << "Drift vector: " << std::endl;
    for (auto& i : driftVec_)
      std::cout << i << std::endl;
    std::cout << std::endl;
  }

  return true;
}

/**
 * Populate precalculated drift values to vector
 */
void Application::PopulateDriftVector()
{
  //
  // same size as tauMatrix as there is one drift for each tau
  if (driftVec_.empty())
    driftVec_.resize(fwdRatesData.colSize());
  for (unsigned int i = 0; i < driftVec_.size(); ++i)
    driftVec_[i] = Application::drift(tenorVec_[i]);
}

/**
 * Read ini file
 * @param  file [filename of the file]
 * @return      [success flag]
 */
bool Application::ReadIniFile(const char* file)
{
  INIReader reader(file);
  if (reader.ParseError() < 0) {
    std::cout << "Can't load 'test.ini'\n";
    return false;
  }
  // ----------Read all config sections----------
  // default
  dataFile_ = reader.Get("Default", "data", "HJM.csv");
  nrows_    = reader.GetInteger("Default", "rows", 1245);
  ncols_    = reader.GetInteger("Default", "cols", 51);
  // trace
  showCovMatrix_        = reader.GetBoolean("Trace", "ShowCovMatrix", false);
  showEigenValues_      = reader.GetBoolean("Trace", "ShowEigenValues", false);
  showEigenVectors_     = reader.GetBoolean("Trace", "ShowEigenVectors", false);
  showPCAMatrix_        = reader.GetBoolean("Trace", "ShowPCAMatrix", false);
  showVolMatrix_        = reader.GetBoolean("Trace", "ShowVolMatrix", false);
  showFittedVolMatrix_  = reader.GetBoolean("Trace", "ShowFittedVolMatrix", false);
  showDriftVector_      = reader.GetBoolean("Trace", "ShowDriftVector", false);
  showAll_              = reader.GetBoolean("Trace", "ShowAll", false);
  showZCBSimulations_   = reader.GetBoolean("Trace", "ShowZcbSimulationValues", false);
  showCapletSimulations_ = reader.GetBoolean("Trace", "ShowCapletSimulationValues", false);
  // zcb
  zcbSimulations_     = reader.GetInteger("ZCB", "Simulations", 5000);
  zcbMaturity_        = reader.GetReal("ZCB", "Maturity", 1.0);
  calcZCB_            = reader.GetBoolean("ZCB", "calculateZCB", false);
  // caplet
  capletSimulations_  = reader.GetInteger("Caplet", "Simulations", 5000);
  capletStart_        = reader.GetReal("Caplet", "Start", 1.0);
  capletEnd_          = reader.GetReal("Caplet", "End", 2.0);
  capletStrike_       = reader.GetReal("Caplet", "Strike", 0.04);
  calcCaplet_         = reader.GetBoolean("Caplet", "calculateCaplet", false);
  return true;
}
