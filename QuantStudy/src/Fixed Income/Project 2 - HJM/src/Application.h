#ifndef APPLICATION_H_
#define APPLICATION_H_

#include <vector>
#include <memory>
#include "Matrix.h"

template<class T>
class Matrix;

/**
 * Main Application class
 * Houses the rates data
 * Does all the calculation of prices
 */
class Application
{
public:
  Application()
    : capletStrike_(0.04), calcCaplet_(true), calcZCB_(false),
      zcbMaturity_(0.0), capletStart_(0.0), capletEnd_(0.0),
      zcbSimulations_(0), capletSimulations_(0), dataFile_(),
      nrows_(0), ncols_(0), showCovMatrix_(false), showEigenValues_(false),
      showEigenVectors_(false), showPCAMatrix_(false), showVolMatrix_(false),
      showFittedVolMatrix_(false), showDriftVector_(false),
      showZCBSimulations_(false), showCapletSimulations_(false),
      showAll_(false), fwdRatesData (0, 0), fittedVol(0, 0)
  {}
  ~Application() {}
  bool start(int argc, char* argv[]);
  double vol(int, double) const;
  void HJMZCB() const;
  void HJMCaplet() const;
  bool calcCaplet() const
  {
    return calcCaplet_;
  }
  bool calcZCB() const
  {
    return calcZCB_;
  }
private:
  double capletStrike_;
  bool calcCaplet_, calcZCB_;
  double zcbMaturity_, capletStart_, capletEnd_;
  int zcbSimulations_, capletSimulations_;
  std::string dataFile_;
  int nrows_, ncols_;

  // trace variables
  bool showCovMatrix_, showEigenValues_, showEigenVectors_;
  bool showPCAMatrix_, showVolMatrix_, showFittedVolMatrix_, showDriftVector_;
  bool showZCBSimulations_, showCapletSimulations_, showAll_;
  bool cov() const
  {
    return showCovMatrix_;
  }
  bool eigVal() const
  {
    return showEigenValues_;
  }
  bool eigVec() const
  {
    return showEigenVectors_;
  }
  bool pca() const
  {
    return showPCAMatrix_;
  }
  bool fit() const
  {
    return showFittedVolMatrix_;
  }
  bool mbar() const
  {
    return showDriftVector_;
  }
  bool all() const
  {
    return showAll_;
  }
  bool szcb() const
  {
    return showZCBSimulations_;
  }
  bool cap() const
  {
    return showCapletSimulations_;
  }
  bool svol() const
  {
    return showVolMatrix_;
  }

  Matrix<double> fwdRatesData;
  std::vector<std::unique_ptr<double[]>> coeffVec;
  std::vector<double> driftVec_;
  std::vector<double> tenorVec_;
  Matrix<double> fittedVol;

  void init(int row, int col);
  template<typename T>
  bool CollectData(Matrix<T>& m,
                   const std::string& filename,
                   char delimiter = ',');
  double drift(double tau) const;
  double ZCB(double T, double seed_now, double driftNow,
             double seed_next, double driftNext, double dtau,
             double, double) const;
  double Caplet() const;
  double CoreFwdPriceGen(double T, double seed_now, double driftNow,
                         double seed_next, double driftNext, double dtau,
                         bool, double, double) const;
  void PopulateDriftVector();
  bool ReadIniFile(const char*);
};

#endif