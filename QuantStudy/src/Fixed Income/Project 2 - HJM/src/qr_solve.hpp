#ifndef QR_SOLVER_HPP_
#define QR_SOLVER_HPP_

/**
 * All helper methods needed for QR Solve implementation
 */
struct QRSolver {
  static void    daxpy(int n, double da, double dx[], int incx, double dy[],
                       int incy);
  static double  ddot(int n, double dx[], int incx, double dy[], int incy);
  static double  dnrm2(int n, double x[], int incx);
  static void    dqrank(double a[], int lda, int m, int n, double tol, int& kr,
                        int jpvt[], double qraux[]);
  static void    dqrdc(double a[], int lda, int n, int p, double qraux[],
                       int jpvt[], double work[], int job);
  static int     dqrls(double a[], int lda, int m, int n, double tol, int& kr,
                       double b[], double x[], double rsd[], int jpvt[],
                       double qraux[], int itask);
  static void    dqrlss(double a[], int lda, int m, int n, int kr, double b[],
                        double x[], double rsd[], int jpvt[], double qraux[]);
  static int     dqrsl(double a[], int lda, int n, int k, double qraux[],
                       double y[], double qy[], double qty[], double b[],
                       double rsd[], double ab[], int job);
  static void    drot(int n, double x[], int incx, double y[], int incy,
                      double c, double s);
  static void    drotg(double* sa, double* sb, double* c, double* s);
  static void    dscal(int n, double sa, double x[], int incx);
  static void    dswap(int n, double x[], int incx, double y[], int incy);
  static std::unique_ptr<double[]>
  qr_solve(int m, int n, double a[], double b[]);
  static std::unique_ptr<double[]>
  qr_solve(int m, int n, const double* a, const double* b);

};

#endif