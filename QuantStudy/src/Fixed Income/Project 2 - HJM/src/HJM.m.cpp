#include "Application.h"

/**
 * [Start of the program]
 * @param  argc [number of cmd line args]
 * @param  argv [array of cmd line args]
 * @return      [return code]
 */
int main(int argc, char *argv[])
{
  Application appl;
  if (!appl.start(argc, argv))
    return 1;

  // Start the show
  if (appl.calcZCB()) {
    std::cout << "Running the simulations for ZCB (Please wait)..";
    appl.HJMZCB();
    std::cout << "...Complete" << std::endl;
  }

  if (appl.calcCaplet()) {
    std::cout << "Running the simulations for Cap (Please wait)..";
    appl.HJMCaplet();
    std::cout << "...Complete" << std::endl;
  }

  std::cout << std::endl;
  return 0;
}