#include <iostream>
#include <utility>
#include <numeric>
#include <cmath>
#include <functional>
#include "Matrix.h"
#include "Math.h"
#include "qr_solve.hpp"

#define SIGN(a, b) ( (b) < 0 ? -fabs(a) : fabs(a) )

/**
 * [Householder reduction algorithm]
 * @param  mat [matrix to reduce to orthogonal form]
 * @param  d   [diagonal elements]
 * @param  e   [off-diagonal elements]
 * @return     [success flag]
 */
bool Math::houseHolderRedn(Matrix<double>& mat, 
                            dblVec& d, 
                            dblVec& e)
{
  if (d.size() == 0 || e.size() == 0)
    return false;

  // check for symmetry
  if (!mat.isSymmetric()) {
    std::cout << "Given Matrix is not symmetric!" << std::endl;
    return false;
  }

  int n = d.size();
  for(int i = n-1; i > 0; --i) {
    int l = i-1;
    double h = 0.0, scale = 0.0;
    if (l > 0) {
      for(int k = 0; k < l+1; ++k)
        scale += fabs(mat(i,k));
      if (scale == 0.0)
        e[i] = mat(i,l);
      else {
        for(int k = 0; k < l+1; ++k) {
          mat(i,k) /= scale;
          h += mat(i,k) * mat(i,k);
        }
        double f = mat(i,l);
        double g = ((f >= 0.0) ? -std::sqrt(h) : std::sqrt(h));
        e[i] = scale * g;
        h -= f * g;
        mat(i,l) = f - g;
        f = 0.0;
        for(int j = 0; j < l+1; ++j) {
          mat(j,i) = mat(i,j) / h;
          g = 0.0;
          for(int k = 0.0; k < j+1; ++k)
            g += mat(j,k) * mat(i,k);
          for(int k = j+1; k < l+1; ++k)
            g += mat(k,j) * mat(i,k);

          e[j] = g / h;
          f += e[j] * mat(i,j);
        }

        double hh = f / (h+h);
        for(int j = 0; j < l+1; ++j) {
          f = mat(i,j);
          e[j] = g = e[j] - hh * f;
          for(int k = 0; k < j+1; ++k)
            mat(j,k) -= (f * e[k] + g * mat(i,k));
        }
      }
    } else
      e[i] = mat(i,l);
    d[i] = h;
  }

  // For Eigen Vectors
  d[0] = 0.0;
  e[0] = 0.0;
  for(int i = 0; i < n; ++i) {
    int l = i;
    if (d[i] != 0.0) {
      for(int j = 0; j < l; ++j) {
        double g = 0.0;
        for(int k = 0; k < l; ++k)
          g += mat(i,k) * mat(k,j);
        for(int k = 0; k < l; ++k)
          mat(k,j) -= g * mat(k,i);
      }
    }
    d[i] = mat(i,i);
    mat(i,i) = 1.0;
    for(int j = 0; j < l; ++j)
      mat(j,i) = mat(i,j) = 0.0;
  }

  return true;
}

/**
 * [calculates pythagoras hypotenuse. sqrt(a^2 + b^2)]
 * @param  a [side 1]
 * @param  b [side 2]
 * @return   [hypotenuse]
 */
double Math::pythag(double a, double b)
{
  double absa,absb;
  absa=fabs(a);
  absb=fabs(b);
  if (absa > absb) 
    return absa*sqrt(1.0+(absb/absa)*(absb/absa));
  else 
    return (absb == 0.0 ? 0.0 : absb*sqrt(1.0+(absa/absb)*(absa/absb)));
}

/**
 * [Implicit QL Reduction]
 * @param  d   [diagonal elements]
 * @param  e   [off diagonal elements]
 * @param  mat [matrix to solve for eigens]
 * @return     [returns success flag]
 */
bool Math::tQLImplicit(dblVec& d, 
                       dblVec& e, 
                       Matrix<double>& mat)
{
  if (d.size() == 0 || e.size() == 0)
    return false;

  // remember the elements of e
  int n = d.size();
  for(int i = 1; i < n; ++i)
    e[i-1] = e[i];
  e[n-1] = 0.0;
  for(int l = 0; l < n; ++l) {
    int iter = 0, m = 0;
    do {
      for(m = l; m < n-1; ++m) {
        double dd = fabs(d[m]) + fabs(d[m+1]);
        if (fabs(e[m]) + dd == dd)
          break;
      }
      if (m != l) {
        if (iter++ == 30)
          std::cout << "Too many iterations" << std::endl;
        double g = (d[l+1] - d[l]) / (2.0 * e[l]);
        double r = pythag(g,1.0);
        g = d[m] - d[l] + e[l] / (g + SIGN(r,g));
        double s = 1.0, c = 1.0, p = 0.0;
        int i = 0;
        for (i = m-1; i >= l; --i) {
          double f = s * e[i];
          double b = c * e[i];
          r = pythag(f,g);
          e[i+1] = r;
          if (r == 0.0) {
            d[i+1] -= p;
            e[m] = 0.0;
            break;
          }
          s = f / r;
          c = g / r;
          g = d[i+1] - p;
          r = (d[i] - g) * s + 2.0 * c * b;
          p = s * r;
          d[i + 1] = g + p;
          g = c * r - b;
          for(int k = 0; k < n; ++k) {
            f = mat(k,i+1);
            mat(k,i+1) = s * mat(k,i) + c * f;
            mat(k,i) = c * mat(k,i) - s * f;
          }
        }
        if (r == 0.0 && i >= l)
          continue;
        d[l] -= p;
        e[l] = g;
        e[m] = 0.0;
      }
    } while (m != l);
  }

  return true;
}

/**
 * QRSolver routine that takes 2 matrices. Returns pair of smart pointer of
 * array of coeffients and the number of elements 
 */
std::pair<std::unique_ptr<double[]>,int> Math::QRSolve(Matrix<double>& Y, 
                                                       Matrix<double>& X)
{
  //
  // make copies since the underlying routine will
  // destroy what is given to it
  //
  const int mx = X.rowSize();
  const int nx = X.colSize();
  std::vector<double> xVec(mx*nx, 0.0);
  for(int i = 0; i < mx; ++i) {
    for(int j = 0; j < nx; ++j)
      xVec[i * nx + j] = X(i,j);
  }
  const int my = Y.rowSize();
  const int ny = Y.colSize();
  std::vector<double> yVec(my*ny, 0.0);
  for(int i = 0; i < my; ++i) {
    for(int j = 0; j < ny; ++j)
      yVec[i * ny + j] = Y(i,j);
  }
  return std::make_pair(QRSolver::qr_solve(mx, nx, xVec.data(), yVec.data()),
                        nx);
}

/**
 * Overload of QRSolve that takes 2 vectors
 */
std::pair<std::unique_ptr<double[]>,int> Math::QRSolve(const std::vector<double>& Y, 
                                                       const std::vector<double>& X)
{
  return std::make_pair(QRSolver::qr_solve(X.size(), 1, X.data(), Y.data()),1);
}

/**
 * Overload of QRSolve method that takes a vector and Matrix
 */
std::pair<std::unique_ptr<double[]>,int> Math::QRSolve(const std::vector<double>& Y,
                                                       Matrix<double>& X)
{
  const int mx = X.rowSize();
  const int nx = X.colSize();
  std::vector<double> xVec(mx*(nx+1), 1.0);
  for(int i = 0; i < mx; ++i) {
    for(int j = 0; j < nx; ++j) {
      xVec[(j+1) * mx + i] = X(i,j);
    }
  }
  return std::make_pair(QRSolver::qr_solve(mx, nx+1, xVec.data(), Y.data()),nx+1);
}

/**
 * Numerical Integration implementation by Trapezoidal rule
 * @param  f - function
 * @param  a - lower limit
 * @param  b - upper limit
 * @return - value
 */
double Math::integral(const std::function<double(double)>& f, double a, double b)
{
  if (a == 0 && b == 0) return 0.0;
  const double& dt = 0.01;
  const int& N = (b - a) / dt;

  // numerical integration formula
  // extended trapezoidal rule
  // Integral(a,b) of f(x) = h * [f0/2 + Sum(f1..fN-1) + fN/2]
  // where h = (b - a) / N;
  // here, b = tau; a = 0 => h = dt
  double sum = 0.5 * (f(a) + f(b));
  for (int i = 1; i < N; ++i)
    sum += f(i * dt);

  return sum * dt;
}