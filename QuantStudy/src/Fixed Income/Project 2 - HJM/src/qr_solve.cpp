#include <cstdlib>
#include <memory>
#include <iostream>
#include <limits>
#include <iomanip>
#include <cmath>
#include <ctime>

using namespace std;

#include "qr_solve.hpp"

#define SIGN(x) ((x) < 0 ? -1 : 1)

//****************************************************************************80

double mat_max(int m, int n, double a[])

//****************************************************************************80
//
//  Purpose:
//
//    R8MAT_AMAX returns the maximum absolute value entry of an R8MAT.
//
//  Discussion:
//
//    An R8MAT is a doubly dimensioned array of R8 values, stored as a vector
//    in column-major order.
//
//  Parameters:
//
//    Input, int M, the number of rows in A.
//
//    Input, int N, the number of columns in A.
//
//    Input, double A[M*N], the M by N matrix.
//
//    Output, double R8MAT_AMAX, the maximum absolute value entry of A.
//
{
  int i;
  int j;
  double value;
  value = fabs(a[0 + 0 * m]);
  for (j = 0; j < n; j++) {
    for (i = 0; i < m; i++) {
      value = std::max(value, fabs(a[i + j * m]));
    }
  }
  return value;
}

//****************************************************************************80

void QRSolver::daxpy(int n, double da, double dx[], int incx, double dy[],
                     int incy)

//****************************************************************************80
//
//  Purpose:
//
//    DAXPY computes constant times a vector plus a vector.
//
//  Discussion:
//
//    This routine uses unrolled loops for increments equal to one.
//
//  Parameters:
//
//    Input, int N, the number of elements in DX and DY.
//
//    Input, double DA, the multiplier of DX.
//
//    Input, double DX[*], the first vector.
//
//    Input, int INCX, the increment between successive entries of DX.
//
//    Input/output, double DY[*], the second vector.
//    On output, DY[*] has been replaced by DY[*] + DA * DX[*].
//
//    Input, int INCY, the increment between successive entries of DY.
//
{
  int i = 0, ix = 0, iy = 0, m = 0;
  if (n <= 0) {
    return;
  }
  if (da == 0.0) {
    return;
  }
  //
  //  Code for unequal increments or equal increments
  //  not equal to 1.
  //
  if (incx != 1 || incy != 1) {
    if (0 <= incx) {
      ix = 0;
    } else {
      ix = (- n + 1) * incx;
    }
    if (0 <= incy) {
      iy = 0;
    } else {
      iy = (- n + 1) * incy;
    }
    for (i = 0; i < n; i++) {
      dy[iy] = dy[iy] + da * dx[ix];
      ix = ix + incx;
      iy = iy + incy;
    }
  }
  //
  //  Code for both increments equal to 1.
  //
  else {
    m = n % 4;
    for (i = 0; i < m; i++) {
      dy[i] = dy[i] + da * dx[i];
    }
    for (i = m; i < n; i = i + 4) {
      dy[i  ] = dy[i  ] + da * dx[i  ];
      dy[i + 1] = dy[i + 1] + da * dx[i + 1];
      dy[i + 2] = dy[i + 2] + da * dx[i + 2];
      dy[i + 3] = dy[i + 3] + da * dx[i + 3];
    }
  }
  return;
}
//****************************************************************************80

double QRSolver::ddot(int n, double dx[], int incx, double dy[], int incy)

//****************************************************************************80
//
//  Purpose:
//
//    DDOT forms the dot product of two vectors.
//
//  Discussion:
//
//    This routine uses unrolled loops for increments equal to one.
//
//  Parameters:
//
//    Input, int N, the number of entries in the vectors.
//
//    Input, double DX[*], the first vector.
//
//    Input, int INCX, the increment between successive entries in DX.
//
//    Input, double DY[*], the second vector.
//
//    Input, int INCY, the increment between successive entries in DY.
//
//    Output, double DDOT, the sum of the product of the corresponding
//    entries of DX and DY.
//
{
  double dtemp = 0.0;
  int i = 0, ix = 0, iy = 0, m = 0;
  if (n <= 0) {
    return dtemp;
  }
  //
  //  Code for unequal increments or equal increments
  //  not equal to 1.
  //
  if (incx != 1 || incy != 1) {
    if (0 <= incx) {
      ix = 0;
    } else {
      ix = (- n + 1) * incx;
    }
    if (0 <= incy) {
      iy = 0;
    } else {
      iy = (- n + 1) * incy;
    }
    for (i = 0; i < n; i++) {
      dtemp = dtemp + dx[ix] * dy[iy];
      ix = ix + incx;
      iy = iy + incy;
    }
  }
  //
  //  Code for both increments equal to 1.
  //
  else {
    m = n % 5;
    for (i = 0; i < m; i++) {
      dtemp = dtemp + dx[i] * dy[i];
    }
    for (i = m; i < n; i = i + 5) {
      dtemp = dtemp + dx[i  ] * dy[i  ]
              + dx[i + 1] * dy[i + 1]
              + dx[i + 2] * dy[i + 2]
              + dx[i + 3] * dy[i + 3]
              + dx[i + 4] * dy[i + 4];
    }
  }
  return dtemp;
}
//****************************************************************************80

double QRSolver::dnrm2(int n, double x[], int incx)

//****************************************************************************80
//
//  Purpose:
//
//    DNRM2 returns the euclidean norm of a vector.
//
//  Discussion:
//
//     DNRM2(X) = sqrt(X' * X)
//
//  Parameters:
//
//    Input, int N, the number of entries in the vector.
//
//    Input, double X[*], the vector whose norm is to be computed.
//
//    Input, int INCX, the increment between successive entries of X.
//
//    Output, double DNRM2, the Euclidean norm of X.
//
{
  int i = 0, ix = 0;
  double absxi = 0.0, norm = 0.0, scale = 0.0, ssq = 0.0;
  if (n < 1 || incx < 1) {
    norm = 0.0;
  } else if (n == 1) {
    norm = fabs(x[0]);
  } else {
    scale = 0.0;
    ssq = 1.0;
    ix = 0;
    for (i = 0; i < n; i++) {
      if (x[ix] != 0.0) {
        absxi = fabs(x[ix]);
        if (scale < absxi) {
          ssq = 1.0 + ssq * (scale / absxi) * (scale / absxi);
          scale = absxi;
        } else {
          ssq = ssq + (absxi / scale) * (absxi / scale);
        }
      }
      ix = ix + incx;
    }
    norm  = scale * sqrt(ssq);
  }
  return norm;
}
//****************************************************************************80

void QRSolver::dqrank(double a[], int lda, int m, int n, double tol, int& kr,
                      int jpvt[], double qraux[])

//****************************************************************************80
//
//  Purpose:
//
//    DQRANK computes the QR factorization of a rectangular matrix.
//
//  Discussion:
//
//    This routine is used in conjunction with sqrlss to solve
//    overdetermined, underdetermined and singular linear systems
//    in a least squares sense.
//
//    DQRANK uses the LINPACK subroutine DQRDC to compute the QR
//    factorization, with column pivoting, of an M by N matrix A.
//    The numerical rank is determined using the tolerance TOL.
//
//    Note that on output, ABS(A(1,1)) / ABS(A(KR,KR)) is an estimate
//    of the condition number of the matrix of independent columns,
//    and of R.  This estimate will be <= 1/TOL.
//
//  Parameters:
//
//    Input/output, double A[LDA*N].  On input, the matrix whose
//    decomposition is to be computed.  On output, the information from DQRDC.
//    The triangular matrix R of the QR factorization is contained in the
//    upper triangle and information needed to recover the orthogonal
//    matrix Q is stored below the diagonal in A and in the vector QRAUX.
//
//    Input, int LDA, the leading dimension of A, which must
//    be at least M.
//
//    Input, int M, the number of rows of A.
//
//    Input, int N, the number of columns of A.
//
//    Input, double TOL, a relative tolerance used to determine the
//    numerical rank.  The problem should be scaled so that all the elements
//    of A have roughly the same absolute accuracy, EPS.  Then a reasonable
//    value for TOL is roughly EPS divided by the magnitude of the largest
//    element.
//
//    Output, int &KR, the numerical rank.
//
//    Output, int JPVT[N], the pivot information from DQRDC.
//    Columns JPVT(1), ..., JPVT(KR) of the original matrix are linearly
//    independent to within the tolerance TOL and the remaining columns
//    are linearly dependent.
//
//    Output, double QRAUX[N], will contain extra information defining
//    the QR factorization.
//
{
  int i = 0, j = 0, job = 0, k = 0;
  double* work = 0;
  for (i = 0; i < n; i++) {
    jpvt[i] = 0;
  }
  work = new double[n];
  job = 1;
  dqrdc(a, lda, m, n, qraux, jpvt, work, job);
  kr = 0;
  k = std::min(m, n);
  for (j = 0; j < k; j++) {
    if (fabs(a[j + j * lda]) <= tol * fabs(a[0 + 0 * lda])) {
      return;
    }
    kr = j + 1;
  }
  delete [] work;
  return;
}
//****************************************************************************80

void QRSolver::dqrdc(double a[], int lda, int n, int p, double qraux[],
                     int jpvt[], double work[], int job)

//****************************************************************************80
//
//  Purpose:
//
//    DQRDC computes the QR factorization of a real rectangular matrix.
//
//  Discussion:
//
//    DQRDC uses Householder transformations.
//
//    Column pivoting based on the 2-norms of the reduced columns may be
//    performed at the user's option.
//
//  Parameters:
//
//    Input/output, double A(LDA,P).  On input, the N by P matrix
//    whose decomposition is to be computed.  On output, A contains in
//    its upper triangle the upper triangular matrix R of the QR
//    factorization.  Below its diagonal A contains information from
//    which the orthogonal part of the decomposition can be recovered.
//    Note that if pivoting has been requested, the decomposition is not that
//    of the original matrix A but that of A with its columns permuted
//    as described by JPVT.
//
//    Input, int LDA, the leading dimension of the array A.  LDA must
//    be at least N.
//
//    Input, int N, the number of rows of the matrix A.
//
//    Input, int P, the number of columns of the matrix A.
//
//    Output, double QRAUX[P], contains further information required
//    to recover the orthogonal part of the decomposition.
//
//    Input/output, integer JPVT[P].  On input, JPVT contains integers that
//    control the selection of the pivot columns.  The K-th column A(*,K) of A
//    is placed in one of three classes according to the value of JPVT(K).
//      > 0, then A(K) is an initial column.
//      = 0, then A(K) is a free column.
//      < 0, then A(K) is a final column.
//    Before the decomposition is computed, initial columns are moved to
//    the beginning of the array A and final columns to the end.  Both
//    initial and final columns are frozen in place during the computation
//    and only free columns are moved.  At the K-th stage of the
//    reduction, if A(*,K) is occupied by a free column it is interchanged
//    with the free column of largest reduced norm.  JPVT is not referenced
//    if JOB == 0.  On output, JPVT(K) contains the index of the column of the
//    original matrix that has been interchanged into the K-th column, if
//    pivoting was requested.
//
//    Workspace, double WORK[P].  WORK is not referenced if JOB == 0.
//
//    Input, int JOB, initiates column pivoting.
//    0, no pivoting is done.
//    nonzero, pivoting is done.
//
{
  int j = 0, jp = 0, l = 0, lup = 0, maxj = 0;
  int pl = 0, pu = 0;
  double maxnrm = 0.0, nrmxl = 0.0, t = 0.0, tt = 0.0;
  bool swapj = false;
  pl = 1;
  pu = 0;
  //
  //  If pivoting is requested, rearrange the columns.
  //
  if (job != 0) {
    for (j = 1; j <= p; j++) {
      swapj = (0 < jpvt[j - 1]);
      if (jpvt[j - 1] < 0) {
        jpvt[j - 1] = -j;
      } else {
        jpvt[j - 1] = j;
      }
      if (swapj) {
        if (j != pl) {
          dswap(n, a + 0 + (pl - 1)*lda, 1, a + 0 + (j - 1), 1);
        }
        jpvt[j - 1] = jpvt[pl - 1];
        jpvt[pl - 1] = j;
        pl = pl + 1;
      }
    }
    pu = p;
    for (j = p; 1 <= j; j--) {
      if (jpvt[j - 1] < 0) {
        jpvt[j - 1] = -jpvt[j - 1];
        if (j != pu) {
          dswap(n, a + 0 + (pu - 1)*lda, 1, a + 0 + (j - 1)*lda, 1);
          jp = jpvt[pu - 1];
          jpvt[pu - 1] = jpvt[j - 1];
          jpvt[j - 1] = jp;
        }
        pu = pu - 1;
      }
    }
  }
  //
  //  Compute the norms of the free columns.
  //
  for (j = pl; j <= pu; j++) {
    qraux[j - 1] = dnrm2(n, a + 0 + (j - 1) * lda, 1);
  }
  for (j = pl; j <= pu; j++) {
    work[j - 1] = qraux[j - 1];
  }
  //
  //  Perform the Householder reduction of A.
  //
  lup = std::min(n, p);
  for (l = 1; l <= lup; l++) {
    //
    //  Bring the column of largest norm into the pivot position.
    //
    if (pl <= l && l < pu) {
      maxnrm = 0.0;
      maxj = l;
      for (j = l; j <= pu; j++) {
        if (maxnrm < qraux[j - 1]) {
          maxnrm = qraux[j - 1];
          maxj = j;
        }
      }
      if (maxj != l) {
        dswap(n, a + 0 + (l - 1)*lda, 1, a + 0 + (maxj - 1)*lda, 1);
        qraux[maxj - 1] = qraux[l - 1];
        work[maxj - 1] = work[l - 1];
        jp = jpvt[maxj - 1];
        jpvt[maxj - 1] = jpvt[l - 1];
        jpvt[l - 1] = jp;
      }
    }
    //
    //  Compute the Householder transformation for column L.
    //
    qraux[l - 1] = 0.0;
    if (l != n) {
      nrmxl = dnrm2(n - l + 1, a + l - 1 + (l - 1) * lda, 1);
      if (nrmxl != 0.0) {
        if (a[l - 1 + (l - 1)*lda] != 0.0) {
          nrmxl = nrmxl * SIGN(a[l - 1 + (l - 1) * lda]);
        }
        dscal(n - l + 1, 1.0 / nrmxl, a + l - 1 + (l - 1)*lda, 1);
        a[l - 1 + (l - 1)*lda] = 1.0 + a[l - 1 + (l - 1) * lda];
        //
        //  Apply the transformation to the remaining columns, updating the norms.
        //
        for (j = l + 1; j <= p; j++) {
          t = -ddot(n - l + 1, a + l - 1 + (l - 1) * lda, 1, a + l - 1 + (j - 1) * lda, 1)
              / a[l - 1 + (l - 1) * lda];
          daxpy(n - l + 1, t, a + l - 1 + (l - 1)*lda, 1, a + l - 1 + (j - 1)*lda, 1);
          if (pl <= j && j <= pu) {
            if (qraux[j - 1] != 0.0) {
              tt = 1.0 - pow(fabs(a[l - 1 + (j - 1) * lda]) / qraux[j - 1], 2);
              tt = std::max(tt, 0.0);
              t = tt;
              tt = 1.0 + 0.05 * tt * pow(qraux[j - 1] / work[j - 1], 2);
              if (tt != 1.0) {
                qraux[j - 1] = qraux[j - 1] * sqrt(t);
              } else {
                qraux[j - 1] = dnrm2(n - l, a + l + (j - 1) * lda, 1);
                work[j - 1] = qraux[j - 1];
              }
            }
          }
        }
        //
        //  Save the transformation.
        //
        qraux[l - 1] = a[l - 1 + (l - 1) * lda];
        a[l - 1 + (l - 1)*lda] = -nrmxl;
      }
    }
  }
  return;
}
//****************************************************************************80

int QRSolver::dqrls(double a[], int lda, int m, int n, double tol, int& kr,
                    double b[], double x[], double rsd[], int jpvt[],
                    double qraux[], int itask)

//****************************************************************************80
//
//  Purpose:
//
//    DQRLS factors and solves a linear system in the least squares sense.
//
//  Discussion:
//
//    The linear system may be overdetermined, underdetermined or singular.
//    The solution is obtained using a QR factorization of the
//    coefficient matrix.
//
//    DQRLS can be efficiently used to solve several least squares
//    problems with the same matrix A.  The first system is solved
//    with ITASK = 1.  The subsequent systems are solved with
//    ITASK = 2, to avoid the recomputation of the matrix factors.
//    The parameters KR, JPVT, and QRAUX must not be modified
//    between calls to DQRLS.
//
//    DQRLS is used to solve in a least squares sense
//    overdetermined, underdetermined and singular linear systems.
//    The system is A*X approximates B where A is M by N.
//    B is a given M-vector, and X is the N-vector to be computed.
//    A solution X is found which minimimzes the sum of squares(2-norm)
//    of the residual,  A*X - B.
//
//    The numerical rank of A is determined using the tolerance TOL.
//
//    DQRLS uses the LINPACK subroutine DQRDC to compute the QR
//    factorization, with column pivoting, of an M by N matrix A.
//
//  Parameters:
//
//    Input/output, double A[LDA*N], an M by N matrix.
//    On input, the matrix whose decomposition is to be computed.
//    In a least squares data fitting problem, A(I,J) is the
//    value of the J-th basis(model) function at the I-th data point.
//    On output, A contains the output from DQRDC.  The triangular matrix R
//    of the QR factorization is contained in the upper triangle and
//    information needed to recover the orthogonal matrix Q is stored
//    below the diagonal in A and in the vector QRAUX.
//
//    Input, int LDA, the leading dimension of A.
//
//    Input, int M, the number of rows of A.
//
//    Input, int N, the number of columns of A.
//
//    Input, double TOL, a relative tolerance used to determine the
//    numerical rank.  The problem should be scaled so that all the elements
//    of A have roughly the same absolute accuracy EPS.  Then a reasonable
//    value for TOL is roughly EPS divided by the magnitude of the largest
//    element.
//
//    Output, int &KR, the numerical rank.
//
//    Input, double B[M], the right hand side of the linear system.
//
//    Output, double X[N], a least squares solution to the linear
//    system.
//
//    Output, double RSD[M], the residual, B - A*X.  RSD may
//    overwrite B.
//
//    Workspace, int JPVT[N], required if ITASK = 1.
//    Columns JPVT(1), ..., JPVT(KR) of the original matrix are linearly
//    independent to within the tolerance TOL and the remaining columns
//    are linearly dependent.  ABS(A(1,1)) / ABS(A(KR,KR)) is an estimate
//    of the condition number of the matrix of independent columns,
//    and of R.  This estimate will be <= 1/TOL.
//
//    Workspace, double QRAUX[N], required if ITASK = 1.
//
//    Input, int ITASK.
//    1, DQRLS factors the matrix A and solves the least squares problem.
//    2, DQRLS assumes that the matrix A was factored with an earlier
//       call to DQRLS, and only solves the least squares problem.
//
//    Output, int DQRLS, error code.
//    0:  no error
//    -1: LDA < M  (fatal error)
//    -2: N < 1    (fatal error)
//    -3: ITASK < 1(fatal error)
//
{
  int ind = 0;
  if (lda < m) {
    cerr << "\n";
    cerr << "DQRLS - Fatal error!\n";
    cerr << "  LDA < M.\n";
    ind = -1;
    return ind;
  }
  if (n <= 0) {
    cerr << "\n";
    cerr << "DQRLS - Fatal error!\n";
    cerr << "  N <= 0.\n";
    ind = -2;
    return ind;
  }
  if (itask < 1) {
    cerr << "\n";
    cerr << "DQRLS - Fatal error!\n";
    cerr << "  ITASK < 1.\n";
    ind = -3;
    return ind;
  }
  ind = 0;
  //
  //  Factor the matrix.
  //
  if (itask == 1) {
    dqrank(a, lda, m, n, tol, kr, jpvt, qraux);
  }
  //
  //  Solve the least-squares problem.
  //
  dqrlss(a, lda, m, n, kr, b, x, rsd, jpvt, qraux);
  return ind;
}
//****************************************************************************80

void QRSolver::dqrlss(double a[], int lda, int m, int n, int kr, double b[],
                      double x[], double rsd[], int jpvt[], double qraux[])

//****************************************************************************80
//
//  Purpose:
//
//    DQRLSS solves a linear system in a least squares sense.
//
//  Discussion:
//
//    DQRLSS must be preceeded by a call to DQRANK.
//
//    The system is to be solved is
//      A * X = B
//    where
//      A is an M by N matrix with rank KR, as determined by DQRANK,
//      B is a given M-vector,
//      X is the N-vector to be computed.
//
//    A solution X, with at most KR nonzero components, is found which
//    minimizes the 2-norm of the residual(A*X-B).
//
//    Once the matrix A has been formed, DQRANK should be
//    called once to decompose it.  Then, for each right hand
//    side B, DQRLSS should be called once to obtain the
//    solution and residual.
//
//  Parameters:
//
//    Input, double A[LDA*N], the QR factorization information
//    from DQRANK.  The triangular matrix R of the QR factorization is
//    contained in the upper triangle and information needed to recover
//    the orthogonal matrix Q is stored below the diagonal in A and in
//    the vector QRAUX.
//
//    Input, int LDA, the leading dimension of A, which must
//    be at least M.
//
//    Input, int M, the number of rows of A.
//
//    Input, int N, the number of columns of A.
//
//    Input, int KR, the rank of the matrix, as estimated by DQRANK.
//
//    Input, double B[M], the right hand side of the linear system.
//
//    Output, double X[N], a least squares solution to the
//    linear system.
//
//    Output, double RSD[M], the residual, B - A*X.  RSD may
//    overwite B.
//
//    Input, int JPVT[N], the pivot information from DQRANK.
//    Columns JPVT[0], ..., JPVT[KR-1] of the original matrix are linearly
//    independent to within the tolerance TOL and the remaining columns
//    are linearly dependent.
//
//    Input, double QRAUX[N], auxiliary information from DQRANK
//    defining the QR factorization.
//
{
  int i = 0, j = 0, job = 0, k = 0;
  double t = 0.0;
  if (kr != 0) {
    job = 110;
    dqrsl(a, lda, m, kr, qraux, b, rsd, rsd, x, rsd, rsd, job);
  }
  for (i = 0; i < n; i++) {
    jpvt[i] = - jpvt[i];
  }
  for (i = kr; i < n; i++) {
    x[i] = 0.0;
  }
  for (j = 1; j <= n; j++) {
    if (jpvt[j - 1] <= 0) {
      k = - jpvt[j - 1];
      jpvt[j - 1] = k;
      while (k != j) {
        t = x[j - 1];
        x[j - 1] = x[k - 1];
        x[k - 1] = t;
        jpvt[k - 1] = -jpvt[k - 1];
        k = jpvt[k - 1];
      }
    }
  }
  return;
}
//****************************************************************************80

int QRSolver::dqrsl(double a[], int lda, int n, int k, double qraux[],
                    double y[], double qy[], double qty[], double b[],
                    double rsd[], double ab[], int job)

//****************************************************************************80
//
//  Purpose:
//
//    DQRSL computes transformations, projections, and least squares solutions.
//
//  Discussion:
//
//    DQRSL requires the output of DQRDC.
//
//    For K <= min(N,P), let AK be the matrix
//
//      AK =(A(JPVT[0]), A(JPVT(2)), ..., A(JPVT(K)))
//
//    formed from columns JPVT[0], ..., JPVT(K) of the original
//    N by P matrix A that was input to DQRDC.  If no pivoting was
//    done, AK consists of the first K columns of A in their
//    original order.  DQRDC produces a factored orthogonal matrix Q
//    and an upper triangular matrix R such that
//
//      AK = Q *(R)
//              (0)
//
//    This information is contained in coded form in the arrays
//    A and QRAUX.
//
//    The parameters QY, QTY, B, RSD, and AB are not referenced
//    if their computation is not requested and in this case
//    can be replaced by dummy variables in the calling program.
//    To save storage, the user may in some cases use the same
//    array for different parameters in the calling sequence.  A
//    frequently occuring example is when one wishes to compute
//    any of B, RSD, or AB and does not need Y or QTY.  In this
//    case one may identify Y, QTY, and one of B, RSD, or AB, while
//    providing separate arrays for anything else that is to be
//    computed.
//
//    Thus the calling sequence
//
//      dqrsl(a, lda, n, k, qraux, y, dum, y, b, y, dum, 110, info)
//
//    will result in the computation of B and RSD, with RSD
//    overwriting Y.  More generally, each item in the following
//    list contains groups of permissible identifications for
//    a single calling sequence.
//
//      1.(Y,QTY,B)(RSD)(AB)(QY)
//
//      2.(Y,QTY,RSD)(B)(AB)(QY)
//
//      3.(Y,QTY,AB)(B)(RSD)(QY)
//
//      4.(Y,QY)(QTY,B)(RSD)(AB)
//
//      5.(Y,QY)(QTY,RSD)(B)(AB)
//
//      6.(Y,QY)(QTY,AB)(B)(RSD)
//
//    In any group the value returned in the array allocated to
//    the group corresponds to the last member of the group.
//
//  Parameters:
//
//    Input, double A[LDA*P], contains the output of DQRDC.
//
//    Input, int LDA, the leading dimension of the array A.
//
//    Input, int N, the number of rows of the matrix AK.  It must
//    have the same value as N in DQRDC.
//
//    Input, int K, the number of columns of the matrix AK.  K
//    must not be greater than min(N,P), where P is the same as in the
//    calling sequence to DQRDC.
//
//    Input, double QRAUX[P], the auxiliary output from DQRDC.
//
//    Input, double Y[N], a vector to be manipulated by DQRSL.
//
//    Output, double QY[N], contains Q * Y, if requested.
//
//    Output, double QTY[N], contains Q' * Y, if requested.
//
//    Output, double B[K], the solution of the least squares problem
//      minimize norm2(Y - AK * B),
//    if its computation has been requested.  Note that if pivoting was
//    requested in DQRDC, the J-th component of B will be associated with
//    column JPVT(J) of the original matrix A that was input into DQRDC.
//
//    Output, double RSD[N], the least squares residual Y - AK * B,
//    if its computation has been requested.  RSD is also the orthogonal
//    projection of Y onto the orthogonal complement of the column space
//    of AK.
//
//    Output, double AB[N], the least squares approximation Ak * B,
//    if its computation has been requested.  AB is also the orthogonal
//    projection of Y onto the column space of A.
//
//    Input, integer JOB, specifies what is to be computed.  JOB has
//    the decimal expansion ABCDE, with the following meaning:
//
//      if A != 0, compute QY.
//      if B != 0, compute QTY.
//      if C != 0, compute QTY and B.
//      if D != 0, compute QTY and RSD.
//      if E != 0, compute QTY and AB.
//
//    Note that a request to compute B, RSD, or AB automatically triggers
//    the computation of QTY, for which an array must be provided in the
//    calling sequence.
//
//    Output, int DQRSL, is zero unless the computation of B has
//    been requested and R is exactly singular.  In this case, INFO is the
//    index of the first zero diagonal element of R, and B is left unaltered.
//
{
  bool cab = false, cb = false, cqty = false;
  bool cqy = false, cr = false;
  int i = 0, info = 0, j = 0, jj = 0, ju = 0;
  double t = 0.0, temp = 0.0;
  //
  //  set info flag.
  //
  info = 0;
  //
  //  Determine what is to be computed.
  //
  cqy = (  job / 10000          != 0);
  cqty = ((job %  10000)       != 0);
  cb =  ((job %   1000) / 100 != 0);
  cr =  ((job %    100) /  10 != 0);
  cab = ((job %     10)       != 0);
  ju = std::min(k, n - 1);
  //
  //  Special action when N = 1.
  //
  if (ju == 0) {
    if (cqy) {
      qy[0] = y[0];
    }
    if (cqty) {
      qty[0] = y[0];
    }
    if (cab) {
      ab[0] = y[0];
    }
    if (cb) {
      if (a[0 + 0 * lda] == 0.0) {
        info = 1;
      } else {
        b[0] = y[0] / a[0 + 0 * lda];
      }
    }
    if (cr) {
      rsd[0] = 0.0;
    }
    return info;
  }
  //
  //  Set up to compute QY or QTY.
  //
  if (cqy) {
    for (i = 1; i <= n; i++) {
      qy[i - 1] = y[i - 1];
    }
  }
  if (cqty) {
    for (i = 1; i <= n; i++) {
      qty[i - 1] = y[i - 1];
    }
  }
  //
  //  Compute QY.
  //
  if (cqy) {
    for (jj = 1; jj <= ju; jj++) {
      j = ju - jj + 1;
      if (qraux[j - 1] != 0.0) {
        temp = a[j - 1 + (j - 1) * lda];
        a[j - 1 + (j - 1)*lda] = qraux[j - 1];
        t = -ddot(n - j + 1, a + j - 1 + (j - 1) * lda, 1, qy + j - 1, 1) / a[j - 1 + (j - 1) * lda];
        daxpy(n - j + 1, t, a + j - 1 + (j - 1)*lda, 1, qy + j - 1, 1);
        a[j - 1 + (j - 1)*lda] = temp;
      }
    }
  }
  //
  //  Compute Q'*Y.
  //
  if (cqty) {
    for (j = 1; j <= ju; j++) {
      if (qraux[j - 1] != 0.0) {
        temp = a[j - 1 + (j - 1) * lda];
        a[j - 1 + (j - 1)*lda] = qraux[j - 1];
        t = -ddot(n - j + 1, a + j - 1 + (j - 1) * lda, 1, qty + j - 1, 1) / a[j - 1 + (j - 1) * lda];
        daxpy(n - j + 1, t, a + j - 1 + (j - 1)*lda, 1, qty + j - 1, 1);
        a[j - 1 + (j - 1)*lda] = temp;
      }
    }
  }
  //
  //  Set up to compute B, RSD, or AB.
  //
  if (cb) {
    for (i = 1; i <= k; i++) {
      b[i - 1] = qty[i - 1];
    }
  }
  if (cab) {
    for (i = 1; i <= k; i++) {
      ab[i - 1] = qty[i - 1];
    }
  }
  if (cr && k < n) {
    for (i = k + 1; i <= n; i++) {
      rsd[i - 1] = qty[i - 1];
    }
  }
  if (cab && k + 1 <= n) {
    for (i = k + 1; i <= n; i++) {
      ab[i - 1] = 0.0;
    }
  }
  if (cr) {
    for (i = 1; i <= k; i++) {
      rsd[i - 1] = 0.0;
    }
  }
  //
  //  Compute B.
  //
  if (cb) {
    for (jj = 1; jj <= k; jj++) {
      j = k - jj + 1;
      if (a[j - 1 + (j - 1)*lda] == 0.0) {
        info = j;
        break;
      }
      b[j - 1] = b[j - 1] / a[j - 1 + (j - 1) * lda];
      if (j != 1) {
        t = -b[j - 1];
        daxpy(j - 1, t, a + 0 + (j - 1)*lda, 1, b, 1);
      }
    }
  }
  //
  //  Compute RSD or AB as required.
  //
  if (cr || cab) {
    for (jj = 1; jj <= ju; jj++) {
      j = ju - jj + 1;
      if (qraux[j - 1] != 0.0) {
        temp = a[j - 1 + (j - 1) * lda];
        a[j - 1 + (j - 1)*lda] = qraux[j - 1];
        if (cr) {
          t = -ddot(n - j + 1, a + j - 1 + (j - 1) * lda, 1, rsd + j - 1, 1)
              / a[j - 1 + (j - 1) * lda];
          daxpy(n - j + 1, t, a + j - 1 + (j - 1)*lda, 1, rsd + j - 1, 1);
        }
        if (cab) {
          t = -ddot(n - j + 1, a + j - 1 + (j - 1) * lda, 1, ab + j - 1, 1)
              / a[j - 1 + (j - 1) * lda];
          daxpy(n - j + 1, t, a + j - 1 + (j - 1)*lda, 1, ab + j - 1, 1);
        }
        a[j - 1 + (j - 1)*lda] = temp;
      }
    }
  }
  return info;
}
//****************************************************************************80

void QRSolver::drot(int n, double x[], int incx, double y[], int incy, double c,
                    double s)

//****************************************************************************80
//
//  Purpose:
//
//    DROT applies a plane rotation.
//
//  Licensing:
//
//    This code is distributed under the GNU LGPL license.
//
//  Parameters:
//
//    Input, int N, the number of entries in the vectors.
//
//    Input/output, double X[*], one of the vectors to be rotated.
//
//    Input, int INCX, the increment between successive entries of X.
//
//    Input/output, double Y[*], one of the vectors to be rotated.
//
//    Input, int INCY, the increment between successive elements of Y.
//
//    Input, double C, S, parameters(presumably the cosine and
//    sine of some angle) that define a plane rotation.
//
{
  int i = 0, ix = 0, iy = 0;
  double stemp = 0.0;
  if (n <= 0) {
  } else if (incx == 1 && incy == 1) {
    for (i = 0; i < n; i++) {
      stemp = c * x[i] + s * y[i];
      y[i]  = c * y[i] - s * x[i];
      x[i]  = stemp;
    }
  } else {
    if (0 <= incx) {
      ix = 0;
    } else {
      ix = (- n + 1) * incx;
    }
    if (0 <= incy) {
      iy = 0;
    } else {
      iy = (- n + 1) * incy;
    }
    for (i = 0; i < n; i++) {
      stemp = c * x[ix] + s * y[iy];
      y[iy] = c * y[iy] - s * x[ix];
      x[ix] = stemp;
      ix = ix + incx;
      iy = iy + incy;
    }
  }
  return;
}
//****************************************************************************80

void QRSolver::drotg(double* sa, double* sb, double* c, double* s)

//****************************************************************************80
//
//  Purpose:
//
//    DROTG constructs a Givens plane rotation.
//
//  Discussion:
//
//    Given values A and B, this routine computes
//
//    SIGMA = sign(A) if abs(A) >  abs(B)
//          = sign(B) if abs(A) <= abs(B);
//
//    R     = SIGMA *(A * A + B * B);
//
//    C = A / R if R is not 0
//      = 1     if R is 0;
//
//    S = B / R if R is not 0,
//        0     if R is 0.
//
//    The computed numbers then satisfy the equation
//
//   ( C  S)(A) =(R)
//   (-S  C)(B) =(0)
//
//    The routine also computes
//
//    Z = S     if abs(A) > abs(B),
//      = 1 / C if abs(A) <= abs(B) and C is not 0,
//      = 1     if C is 0.
//
//    The single value Z encodes C and S, and hence the rotation:
//
//    If Z = 1, set C = 0 and S = 1;
//    If abs(Z) < 1, set C = sqrt(1 - Z * Z) and S = Z;
//    if abs(Z) > 1, set C = 1/ Z and S = sqrt(1 - C * C);
//
//  Parameters:
//
//    Input/output, double *SA, *SB,  On input, SA and SB are the values
//    A and B.  On output, SA is overwritten with R, and SB is
//    overwritten with Z.
//
//    Output, double *C, *S, the cosine and sine of the Givens rotation.
//
{
  double r = 0.0, roe = 0.0, scale = 0.0, z = 0.0;
  if (fabs(*sb) < fabs(*sa)) {
    roe = *sa;
  } else {
    roe = *sb;
  }
  scale = fabs(*sa) + fabs(*sb);
  if (scale == 0.0) {
    *c = 1.0;
    *s = 0.0;
    r = 0.0;
  } else {
    r = scale * sqrt((*sa / scale) * (*sa / scale)
                     + (*sb / scale) * (*sb / scale));
    r = SIGN(roe) * r;
    *c = *sa / r;
    *s = *sb / r;
  }
  if (0.0 < fabs(*c) && fabs(*c) <= *s) {
    z = 1.0 / *c;
  } else {
    z = *s;
  }
  *sa = r;
  *sb = z;
  return;
}
//****************************************************************************80

void QRSolver::dscal(int n, double sa, double x[], int incx)

//****************************************************************************80
//
//  Purpose:
//
//    DSCAL scales a vector by a constant.
//
//  Parameters:
//
//    Input, int N, the number of entries in the vector.
//
//    Input, double SA, the multiplier.
//
//    Input/output, double X[*], the vector to be scaled.
//
//    Input, int INCX, the increment between successive entries of X.
//
{
  int i = 0, ix = 0, m = 0;
  if (n <= 0) {
  } else if (incx == 1) {
    m = n % 5;
    for (i = 0; i < m; i++) {
      x[i] = sa * x[i];
    }
    for (i = m; i < n; i = i + 5) {
      x[i]   = sa * x[i];
      x[i + 1] = sa * x[i + 1];
      x[i + 2] = sa * x[i + 2];
      x[i + 3] = sa * x[i + 3];
      x[i + 4] = sa * x[i + 4];
    }
  } else {
    if (0 <= incx) {
      ix = 0;
    } else {
      ix = (- n + 1) * incx;
    }
    for (i = 0; i < n; i++) {
      x[ix] = sa * x[ix];
      ix = ix + incx;
    }
  }
  return;
}
//****************************************************************************80

void QRSolver::dswap(int n, double x[], int incx, double y[], int incy)

//****************************************************************************80
//
//  Purpose:
//
//    DSWAP interchanges two vectors.
//
//  Parameters:
//
//    Input, int N, the number of entries in the vectors.
//
//    Input/output, double X[*], one of the vectors to swap.
//
//    Input, int INCX, the increment between successive entries of X.
//
//    Input/output, double Y[*], one of the vectors to swap.
//
//    Input, int INCY, the increment between successive elements of Y.
//
{
  int i = 0, ix = 0, iy = 0, m = 0;
  double temp = 0.0;
  if (n <= 0) {
    //
    // do nothing
    //
  } else if (incx == 1 && incy == 1) {
    m = n % 3;
    for (i = 0; i < m; i++) {
      temp = x[i];
      x[i] = y[i];
      y[i] = temp;
    }
    for (i = m; i < n; i = i + 3) {
      temp = x[i];
      x[i] = y[i];
      y[i] = temp;
      temp = x[i + 1];
      x[i + 1] = y[i + 1];
      y[i + 1] = temp;
      temp = x[i + 2];
      x[i + 2] = y[i + 2];
      y[i + 2] = temp;
    }
  } else {
    if (0 <= incx) {
      ix = 0;
    } else {
      ix = (- n + 1) * incx;
    }
    if (0 <= incy) {
      iy = 0;
    } else {
      iy = (- n + 1) * incy;
    }
    for (i = 0; i < n; i++) {
      temp = x[ix];
      x[ix] = y[iy];
      y[iy] = temp;
      ix = ix + incx;
      iy = iy + incy;
    }
  }
  return;
}
//****************************************************************************80

std::unique_ptr<double[]> QRSolver::qr_solve(int m, int n, double a[],
    double b[])

//****************************************************************************80
//
//  Purpose:
//
//    QR_SOLVE solves a linear system in the least squares sense.
//
//  Discussion:
//
//    If the matrix A has full column rank, then the solution X should be the
//    unique vector that minimizes the Euclidean norm of the residual.
//
//    If the matrix A does not have full column rank, then the solution is
//    not unique; the vector X will minimize the residual norm, but so will
//    various other vectors.
//
//  Parameters:
//
//    Input, int M, the number of rows of A.
//
//    Input, int N, the number of columns of A.
//
//    Input, double A[M*N], the matrix.
//
//    Input, double B[M], the right hand side.
//
//    Output, double QR_SOLVE[N], the least squares solution.
//
{
  int kr = 0;
  std::unique_ptr<double[]>   ptr( new double[n] );
  std::unique_ptr<int[]>      jpvt( new int[n] );
  std::unique_ptr<double[]>   qraux( new double[n] );
  double tol = 1.0E-06;
  int rc = dqrls(a, m, m, n, tol, kr, b,
                 ptr.get(), b, jpvt.get(), qraux.get(), 1);
  if (rc != 0) {
    std::cout << "Return code from dqrls: " << rc << std::endl;
    return nullptr;
  }
  return ptr;
}

std::unique_ptr<double[]> QRSolver::qr_solve(int m, int n, const double* a,
    const double* b)
{
  return QRSolver::qr_solve(m, n, const_cast<double*>(a), const_cast<double*>(b));
}
