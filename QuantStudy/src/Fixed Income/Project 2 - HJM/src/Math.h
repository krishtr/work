#ifndef MATH_H_
#define MATH_H_

#include <vector>
#include <memory>
#include <functional>

template<class T>
class Matrix;
typedef std::vector<double> dblVec;

/**
 * Helper class to help with Matrix Algos
 */
class Math
{
public:
  static bool houseHolderRedn(Matrix<double>&, dblVec&, dblVec&);
  static bool tQLImplicit(dblVec&, dblVec&, Matrix<double>&);
  static std::pair<std::unique_ptr<double[]>, int> QRSolve(Matrix<double>& Y,
      Matrix<double>& X);
  static std::pair<std::unique_ptr<double[]>, int> QRSolve(const std::vector<double>& Y,
      const std::vector<double>& X);
  static std::pair<std::unique_ptr<double[]>, int> QRSolve(const std::vector<double>& Y,
      Matrix<double>& X);
  static double integral(const std::function<double(double)>& f, double a, double b);
private:
  static double pythag(double a, double b);
};

#endif
