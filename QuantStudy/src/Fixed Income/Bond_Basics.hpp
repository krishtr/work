#include <iostream>
#include <vector>
#include <memory>
#include <numeric>
#include <math.h>

double PV(double principal, double rate, double t)
{
  return principal * std::exp(-rate * t);
}

double FV(double principal, double rate, double t)
{
  return principal * std::exp(rate * t);
}

//  rate is continuous compounding rate
double ContinuousToMCompunding(double rate, double m)
{
  return (std::exp(rate/m) - 1.0) * m;
}

// rate is 'm' rate
double MCompoundingToContinuous(double rate, double m)
{
  return m * std::log(1.0 + (rate / m));
}

double BondPrice_Yield(double couponRate, double m, double yield, int term)
{
  // assuming face value of 100
  double price = 0.0;
  double dt = 1.0/m;
  for(double i = dt; i <= (m * term - 1) * dt; i = i + dt) {
    price += PV(couponRate/m, yield, i);
  }

  return (price + PV(100 + couponRate/m, yield, term));
}

double YieldRate(double price, double m, double coupon, int term)
{
  // newton raphsson
  double error = 0.000000001, tolerance = 0.0, yield = 0.001;
  auto f = BondPrice_Yield;
  auto f_dash = [&](double c, double m, double y, int t)->double {
    double price = 0.0, dt = 1.0/m;
    for(double i = dt; i <= (m * t - 1) * dt; i = i + dt) {
      price += -i * PV(c/m, y, i);
    }
    return (price + -t * PV(100 + c/m, y, t));   
  };

  for(;;) {
    auto fx = f(coupon, m, yield, term);
    fx -= price;
    auto fdashx = f_dash(coupon, m, yield, term);
    tolerance = fx / fdashx;
    if (std::abs(tolerance) < error)
      break;
    yield -= tolerance;
  }

  return yield;
}

// calculates the par yield given the zero rates
// yield returned will have 'm' compounding
double ParYield(const std::vector<double>& zeros, double m, int term)
{
  double dt = 1 / m, value = 0.0;
  unsigned int i = 0;
  for(double t = dt; (t <= m * dt * term) && i < zeros.size(); 
      t = t + dt, ++i) {
    value += PV(1.0, zeros[i], t);
  }
  return (m * (100 - PV(100,zeros[zeros.size() - 1], trem)) / value);
}

// Forward Rates calculator
// Given zero rate vector, continuous compounding assumed
std::unique_ptr<std::vector<double>> 
  FwdRateCalc(const std::vector<double>& zeros)
{
  typedef std::unique_ptr<std::vector<double>> autoVec;
  autoVec ptr(new std::vector<double>(zeros.size(), 0.0));
  for(unsigned int i = 1; i <= zeros.size(); ++i) {
    (*ptr)[i-1] = (i * zeros[i-1] - std::accumulate((*ptr).begin(),
                                                    (*ptr).end(), 
                                                    0.0));
  }
  return ptr;
}


double BondPrice(const std::vector<double>& zeros, 
                double coupon, double m, int term)
{
  return 0.0;
}
int Test()
{
  std::vector<double> zeros = {3,4,4.6,5,5.3};
  std::unique_ptr<std::vector<double>> ptr (FwdRateCalc(zeros));
  for(auto i : *ptr)
    std::cout << i << ",";
  std::cout << std::endl;
  return 0;
}