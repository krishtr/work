#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <math.h>

/**
* 5 year bond, principal = $1, 2 payments of 3c per year
* Mkt value (ie bond price) is 96c. What is the YTM?
*/
double PV(double value, double t, double rate)
{
  return value * std::exp(-rate * t);
}

void YTM(double principal, int time, 
        double coupon, double bond_price, int periodicity)
{
  // bond price = PV(principal) + Sum(PV(coupons))
  coupon *= periodicity;        // convert it to yearly terms
  auto yield = 0.1;             // starting value
  std::cout << "starting yield: " << yield << std::endl;
  auto error = 0.00000001/100;  // accepted error
  auto tolerance = 0.0;
  for(;;) {
    auto fx = PV(principal,time,yield);
    for(int i = 1; i <= time; ++i) {
      fx += PV(coupon,i,yield);
    }
    fx -= bond_price;           // f(x) = 0 form

    auto f_dash_x = -time * PV(principal,time,yield);
    for(int i = 1; i <= time; ++i) {
      f_dash_x += -i * PV(coupon,i,yield);
    }

    tolerance = fx / f_dash_x;
    if (std::abs(tolerance) < error)
      break;

    yield -= tolerance;
  }

  std::cout << "calculated yield: " << yield << std::endl;
}

void Test(void) 
{
  YTM(1.0, 5, 0.03, 0.96, 2);
}
