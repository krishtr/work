#include <iostream>
#include <vector>
#include <map>
#include <math.h>
#include <iterator>
#include <iomanip>
#include <exception>

struct Params
{
    double T,r,vol,div;
};

typedef std::vector<std::vector<double>> LatticeType;

namespace
{
    void displayLatticeType(const LatticeType& vec)
    {
        int index = 0;
        std::cout << std::endl;
        for(auto v : vec) {
            std::cout << index++ << ": ";
            std::cout << std::setprecision(5);
            std::copy(v.begin(), v.end(), std::ostream_iterator<double>(std::cout, " "));
            std::cout << std::endl;
        }
    }
}

// spot interest rates
class SpotRate
{
public:
    SpotRate() {}
    SpotRate(double r, double u, double d, double q, int p)
        :_r(r), _u(u), _d(d), _q(q), _period(p)
    {
        buildSpotRateLattice();
    }
    double getRiskFreeRate() const { return _r; }
    double getUpmoveFactor() const { return _u; }
    double getDownMoveFactor() const { return _d; }
    double getRiskNeutralProbability() const { return _q; }
    int getBinomialPeriod() const { return _period; }
    
    void setRiskFreeRate(double r) { _r = r; }
    void setUpmoveFactor(double u) { _u = u; }
    void setDownMoveFactor(double d) { _d = d; }
    void setRiskNeutralProbability(double q) { _q = q; }
    void setBinomialPeriod(int p) { _period = p; }

    void buildSpotRateLattice();
    double getSpotRate(uint i, uint j) const;
    void display() const;
    std::vector<double>& operator[](int i) { return _spotRateLattice[i]; }
private:
    double _r, _u, _d, _q;
    int _period;
    LatticeType _spotRateLattice;    
};

void SpotRate::display() const
{
   displayLatticeType(_spotRateLattice); 
}

double SpotRate::getSpotRate(uint i, uint j) const
{
    if(i < 0 || i >= _spotRateLattice.size())
        throw std::runtime_error("Array out of bounds");
    if(j < 0 || j >= _spotRateLattice[i].size())
        throw std::runtime_error("Array out of bounds");

    return _spotRateLattice[i][j];
}

void SpotRate::buildSpotRateLattice()
{
    if(_period < 0)
        throw std::runtime_error("buildSpotRateLattice: Invalid Binomial Period");

    _spotRateLattice.resize(_period + 1);
    for(int i = 0; i <= _period; ++i) {
        if (i == 0) {
            _spotRateLattice[0] = std::vector<double>(1, {_r});
            continue;
        }
        _spotRateLattice[i].resize(i + 1);
        for(int j = 0; j <= i; ++j)
            _spotRateLattice[i][j] = 
                (i == j) ? _spotRateLattice[i - 1][j - 1] * _u
                         : _spotRateLattice[i - 1][j] * _d;
    }
}

class ZCB
{
public:
    ZCB(SpotRate& rate) : _rates(rate) {}
    void buildZCBLattice();
    double getZcb(uint i, uint j) const;
    void display() const;

    double getRiskNeutralProbability() const { return _q; }
    double getFaceValue() const { return _faceValue; }
    int getBinomialPeriod() const { return _period; }
    void setRiskNeutralProbability(double q) { _q = q; }
    void setBinomialPeriod(int p) { _period = p; }
    void setFaceValue(double fv) { _faceValue = fv; }
private:
    double _faceValue, _q;
    int _period;
    SpotRate& _rates; // represents the rates lattice
    LatticeType _zcb; // holder for the zcb values
};

void ZCB::display() const
{
    displayLatticeType(_zcb);
}

double ZCB::getZcb(uint i, uint j) const
{
    if(i < 0 || i >= _zcb.size())
        throw std::runtime_error("Array out of bounds");
    if(j < 0 || j >= _zcb[i].size())
        throw std::runtime_error("Array out of bounds");

    return _zcb[i][j];
}

void ZCB::buildZCBLattice()
{
    _zcb.resize(_period + 1);
    
    // build the final period first
    _zcb[_period].resize(_period + 1);
    for(int j = 0; j <= _period; ++j)
        _zcb[_period][j] = _faceValue;
    
    const double qdash = 1 - _q;
    for(int i = _period - 1; i >= 0; --i) {
        _zcb[i].resize(i + 1);
        for(int j = 0; j <= i; ++j)
            _zcb[i][j] = (1/(1+_rates[i][j])) * (_q * _zcb[i+1][j+1] + qdash * _zcb[i+1][j]);
    }
}

/*
LatticeType optionPriceLattice;
void buildSpotPriceLattice(double S, int period, Params& p)
{
    const double u = exp( p.vol * sqrt(p.T / period) );
    const double d = 1/u;
    spotPriceLattice.resize(period + 1);
    for( int i = 0; i <= period; ++i ) {
        if( i == 0 ) {
            spotPriceLattice[0] = std::vector<double>( 1, {S} );
            continue;
        }
        spotPriceLattice[i].resize(i + 1);
        for( int j = 0; j <= i; ++j )
            spotPriceLattice[i][j] = 
                (i == j) ? spotPriceLattice[i-1][j-1] * u
                         : spotPriceLattice[i-1][j] * d;
    }
    display(spotPriceLattice);
}


//
 * Build a ZCB lattice for given period and face Value
 * q is the risk neutral probability and spot rate lattice
 /
void buildZCBLattice(double FV, double q, int period, 
        const LatticeType& spotRateLattice, LatticeType& zcb)
{
    }

void buildOptionPriceLattice(int period, double K, bool call, bool isAmerican, Params& p)
{
    const double u = exp(p.vol * sqrt(p.T / period));
    const double d = 1 / u;
    const double R = exp((p.r - p.div) * p.T / period);
    const double invR = 1 / R;
    const double q = (R - d) / (u - d);
    const double qdash = 1 - q;
    const double factor = call ? 1 : -1;

    // build the payoff
    optionPriceLattice.resize(period + 1);
    optionPriceLattice[period].resize(period + 1);
    for(int j = 0; j <= period; ++j)
        optionPriceLattice[period][j]
            = std::max(0.0, factor * (spotPriceLattice[period][j] - K));

    // build the rest
    double optionPrice = 0.0;
    for( int i = period - 1; i >= 0; --i ) {
        optionPriceLattice[i].resize(i + 1);
        for( int j = 0; j <= i; ++j ) {
            optionPrice = invR * (q * optionPriceLattice[i+1][j+1] + qdash * optionPriceLattice[i+1][j]);
            optionPriceLattice[i][j]
                = isAmerican ? std::max(factor * (spotPriceLattice[i][j] - K), optionPrice)
                             : optionPrice;
        }
    }
    display(optionPriceLattice);
}

void buildForwardBondLattice(int period, double q, double FV, double u, double d, double r)
{
    //
    // build a spot rate lattice
    // build a zcb lattice
    // build a forward rate lattice
    //
    LatticeType spotRateLattice;
    buildSpotRateLattice(r, period, u, d, q, spotRateLattice);
    display(spotRateLattice);
}
*/



