#include <iostream>
#include "Week4.hpp"

int main()
{
    std::cout << "Periods: ";
    int period;
    std::cin >> period;
    SpotRate rate;
    
    rate.setRiskFreeRate(0.05);
    rate.setUpmoveFactor(1.1);
    rate.setDownMoveFactor(0.9);
    rate.setRiskNeutralProbability(0.5);
    rate.setBinomialPeriod(period);
    
    try {
        rate.buildSpotRateLattice();
        std::cout << "Rates Lattice: " << std::endl;
        rate.display();

        ZCB _zcb(rate);
        _zcb.setFaceValue(100.0);
        _zcb.setBinomialPeriod(period);
        _zcb.setRiskNeutralProbability(0.5);
        _zcb.buildZCBLattice();

        std::cout << "ZCB Lattice" << std::endl;
        _zcb.display();
        
        int i = 0, j = 0;
        std::cin >> i >> j;

        std::cout << "some rate: " << rate.getSpotRate(i,j) << std::endl;
        std::cout << "Corresponding ZCB value: " << _zcb.getZcb(i,j) << std::endl;
    } catch(std::runtime_error& e) {
        std::cout << "Runtime Exception caught: " << e.what() << std::endl;
        std::cout << "Exiting..." << std::endl;
        exit(1);
    } catch(std::exception& ex) {
        std::cout << "Unknown exception caught: " << ex.what() << std::endl;
        std::cout << "Exiting..." << std::endl;
        exit(1);
    }
    return 0;
}
