//
// Google Code Jam
// Minimum Scalar Product
// https://code.google.com/codejam/contest/32016/dashboard#s=p0
//
#include <iostream>
#include <vector>
#include <fstream>
#include <algorithm>
#include <iterator>
#include <boost/optional.hpp>
#include <boost/lexical_cast.hpp>

boost::optional<long long> FindMinScalarProd(std::vector<long>& vec1,
        std::vector<long>& vec2)
{
    if (vec1.size() != vec2.size())
        return boost::optional<long long>();

    if (vec1.empty() || vec2.empty())
        return boost::optional<long long>(0);

    // vec1 in descending order
    std::sort(vec1.begin(), vec1.end(), std::greater<long>());
    // vec2 in ascending order
    std::sort(vec2.begin(), vec2.end());

    // find scalar prod
    long long prod = 0;
    for (unsigned int  i = 0; i < vec1.size(); ++i) {
        prod += long(vec1[i]) * long(vec2[i]);
    }

    return prod;
}

void parse(const std::string& input)
{
    std::fstream file(input);
    std::string line;
    std::getline(file, line, '\n'); // ignore 1st line
    std::vector<long> vec1, vec2;
    int size = -1, index = 1;
    while (std::getline(file, line, '\n')) {
        if (line.empty()) continue;
        if (size == -1) {
            size = boost::lexical_cast<int>(line);
            continue;
        }
        else if (vec1.size() == 0) {
            std::stringstream os(line);
            std::string word;
            while(std::getline(os, word, ' '))
                vec1.push_back(boost::lexical_cast<long>(word));
            continue;
        } else {
            std::stringstream os(line);
            std::string word;
            while(std::getline(os, word, ' '))
                vec2.push_back(boost::lexical_cast<long>(word));
        }
        auto result = FindMinScalarProd(vec1, vec2);
        if (result.is_initialized())
            std::cout << "Case #" << index++ << ": " << std::fixed << *result << std::endl;

        vec1.clear();
        vec2.clear();
        size = -1;
    }
}

int main(int argc, char* argv[])
{
    if (argc == 2)
        parse(argv[1]);
    return 0;
}
