//
// Google Code jam
// Reverse words
// https://code.google.com/codejam/contest/351101/dashboard#s=p1
// 
#include <iostream>
#include <string>
#include <vector>
#include <fstream>
#include <boost/lexical_cast.hpp>

void Approach1()
{    
    std::fstream file("input");
    int count = -1, index = 0;
    std::string line;
    std::vector<std::vector<std::string>> tests;
    while (std::getline(file, line, '\n')) {
        if (line.empty())
            continue;
        if (count == -1) {
            count = boost::lexical_cast<int>(line);
            tests.resize(count);
        } else {
            std::stringstream os(line);
            std::string word;
            std::vector<std::string>& vec = tests[index];
            while (std::getline(os, word, ' ')) {
                vec.push_back(word);
            }
            ++index;
        }            
    }
    
    // output
    count = 1;
    for (const auto& vec : tests) {
        std::cout << "Case #" << count << ": ";
        std::vector<std::string>::const_reverse_iterator riter = vec.rbegin(), rend = vec.rend();
        for ( ; riter != rend; ++riter) {
            std::cout << *riter;
            if (riter != rend - 1)
                std::cout << " ";
        }
        std::cout << std::endl;
        ++count;
    }
}

void Approach2(const std::string& input)
{
    std::fstream file(input);
    std::string line;
    int index = 0;
    while (std::getline(file, line, '\n')) {
        if (line.empty()) continue;
        if (index == 0) {
            ++index;
        } else {
            std::cout << "Case #" << index++ << ": ";
            std::reverse(line.begin(), line.end());
            std::string word;
            std::stringstream os(line);
            while (std::getline(os, word, ' ')) {
                std::reverse(word.begin(), word.end());
                std::cout << word << " ";
            }
            std::cout << std::endl;
        }
    }
}

int main(int argc, char* argv[])
{
    if (argc == 2)
        Approach2(argv[1]);
    return 0;
}

