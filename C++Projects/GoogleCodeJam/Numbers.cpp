//
// Google Code Jam
// Numbers
// https://code.google.com/codejam/contest/32016/dashboard#s=p2
//
#include <iostream>
#include <iomanip>
#include <fstream>
#include <boost/lexical_cast.hpp>
#include <boost/multiprecision/cpp_int.hpp>
#include <boost/multiprecision/cpp_dec_float.hpp>
#include <boost/multiprecision/number.hpp>

using Num = boost::multiprecision::number<boost::multiprecision::cpp_dec_float<1799>>;

void Solve(const std::string& input)
{
    using boost::multiprecision::cpp_int;
    using boost::multiprecision::cpp_dec_float;

    if (input.empty()) return;
    std::fstream file(input);
    std::string line;
    std::getline(file, line, '\n');
    int index = 1;
    const Num  base = 3 + std::sqrt(5);
    const Num th("1000");
    while (std::getline(file, line, '\n')) {
        if (line.empty()) continue;
        int n = boost::lexical_cast<int>(line);

        Num result = boost::multiprecision::pow(base, n);
        std::cout << "Case #" << index++ << ": " 
            << std::setfill('0') << std::setw(3)
            << boost::multiprecision::floor(boost::multiprecision::fmod(result, th))
            << std::endl;
    }
}

int main(int argc, char* argv[])
{
    if (argc == 2)
        Solve(argv[1]);
    return 0;
}
