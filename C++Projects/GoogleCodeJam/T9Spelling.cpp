#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

const char* lookup[26] =
{
    "2","22","222","3","33",
    "333","4","44","444","5",
    "55","555","6","66","666",
    "7","77","777","7777","8",
    "88","888","9","99","999",
    "9999"
};

void Approach1(const std::string& input)
{
    std::fstream file(input);
    std::string line;
    std::getline(file, line, '\n');
    int index = 1;
    while (std::getline(file, line, '\n')) {
        if (line.empty()) continue;
        std::cout << "Case #" << index++ << ": ";
        std::stringstream os;
        for(auto c : line) {
            auto size = os.str().size();
            std::string ans;
            if (c == ' ')
                ans = "0";
            else
                ans = lookup[std::toupper(c) - 'A'];
            if (size != 0 && (os.str()[size - 1] == ans[0]))
                os << " ";
            os << ans;
        }
        std::cout << os.str() << std::endl;
    }
}

int main(int argc, char* argv[])
{
    if (argc == 2)
        Approach1(argv[1]);
    
    return 0;
}
