//
// Google Code Jam
// Store Credit
// https://code.google.com/codejam/contest/351101/dashboard#s=p0
//
#include <iostream>
#include <fstream>
#include <vector>
#include <boost/lexical_cast.hpp>

void Approach1(const std::string& input)
{
    std::fstream file(input);
    std::string line;
    int sum = -1, totalItems = -1, index = 1;
    std::getline(file, line, '\n'); // skip the first line
    while (std::getline(file, line, '\n')) {
        if (line.empty()) continue;
        if (sum == -1) {
            sum = boost::lexical_cast<int>(line);
        } else if (totalItems == -1) {
            totalItems = boost::lexical_cast<int>(line);
        } else {
            std::cout << "Case #" << index++ << ": ";
            std::vector<int> vec;
            std::string word;
            std::stringstream os(line);
            while (std::getline(os, word, ' ')) {
                vec.push_back(boost::lexical_cast<int>(word));
            }
            
            // parse the vector
            std::vector<int>::const_iterator iter = vec.begin(),
                end = vec.end();

            for ( ; iter != end; ++iter) {
                std::vector<int>::const_iterator next = iter + 1;
                for (; next != end; ++next) {
                    if (*iter == sum - *next) {
                        std::cout << (iter - vec.begin() + 1) 
                            << " " << (next - vec.begin() + 1);
                        goto out;
                    }
                }
            }
out:
            std::cout << std::endl;
            sum = totalItems = -1;
        }
    }
}

int main(int argc, char* argv[])
{
    if (argc == 2)
        Approach1(argv[1]);
    return 0;
}
