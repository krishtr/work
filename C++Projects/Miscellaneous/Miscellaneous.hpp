#include <iostream>
#include <iomanip>
#include <algorithm>
#include <set>
#include <cctype>
#include <vector>
#include <string>
#include <fstream>
#include <bitset>

//  1 1 2 3 5 8 13 21
int Fibo ( int n ){

  if( n <= 0 ) {
    return 0;
  }
  if( n == 1 ) {
    return 1;
  }

  return Fibo ( n - 1 ) + Fibo ( n - 2 );
}

bool IsPrime ( int n )
{

  if( n <= 0 || n == 1 ) {
    return false;
  }

  int squareRoot = ( int )sqrt( n );
  for( int i = 2; i <= squareRoot; i++ ) {
    if( n % i == 0 ) {
      return false;
    }
  }

  return true;
}

bool IsDivisible( int number, int divisor )
{
  return number % divisor == 0;
}

void FizzBuzzTest( int n )
{
  for( auto i = 1; i <= n; ++i ) {
    if( IsDivisible( i, 3 ) && IsDivisible( i, 5 ) ) {
      std::cout << " FizzBuzz ";
    } else if( IsDivisible( i, 3 ) ) {
      std::cout << " Fizz ";
    } else if( IsDivisible( i, 5 ) ) {
      std::cout << " Buzz ";
    } else {
      std::cout << i << " ";
    }
  }
}

void FizzBuzzTest2( int n )
{
  for( auto i = 1; i <= n; ++i ) {
    if( !IsDivisible( i, 3 ) && !IsDivisible( i, 5 ) ) {
      std::cout << i << " ";
    } else {
      if( IsDivisible( i, 3 ) ) {
        std::cout << "Fizz";
      }
      if( IsDivisible( i, 5 ) ) {
        std::cout << "Buzz";
      }
      std::cout << " ";
    }
  }
}

void PrintNewNumber ( int num )
{
  //
  // 12391 -> 23402
  //

  int result = 0, index = 0;
  while( num != 0 ) {
    int mod = num % 10;
    num = ( num - mod ) / 10;
    mod++;
    if( mod == 10 ) {
      mod = 0;
    }
    result += mod * static_cast<int>( std::pow( 10, index++ ) );
  }

  std::cout << std::endl <<  result << std::endl;
}

int GCD ( int u, int v )
{
  if( u <= 0 || v <= 0 ) {
    return 0;
  }

  while( u > 0 ) {
    if( v > u ) {
      std::swap ( u, v );
    }
    u = u % v;
  }

  return v;
}

void PrintBinaryEquivalent ( int num )
{
  if( num == 0 ) {
    return;
  }

  int result = num % 2;
  PrintBinaryEquivalent( num / 2 );
  std::cout << std::endl <<  result;
}

/*
Given a letter print a diamond starting with 'A'
with the supplied letter at the widest point.

For example: print-diamond 'E' prints

  A
 B B
C   C
D     D
E       E
D     D
C   C
 B B
  A

For example: print-diamond 'C' prints

A
B B
C   C
B B
A
*/

void PrintDiamond( char ch )
{
  int numSpace = toupper( ch ) - 'A' + 1;
  for( int i = numSpace, index = 1; i > 0; --i ) {
    std::cout << std::endl <<  std::setw( i ) <<  char( numSpace - i + 'A' );
    if( ( numSpace - i ) != 0 ) {
      std::cout << std::endl <<  std::setw( index + 1 ) << char( numSpace - i + 'A' );
      index += 2;
    }
    std::cout << std::endl <<  std::endl;
  }

  numSpace -= 1;
  for( int i = 1, index = ( numSpace << 1 ) - 1; i <= numSpace; ++i ) {
    std::cout << std::endl <<  std::setw( i + 1 ) <<  char( numSpace - i + 'A' );
    if( ( numSpace - i ) != 0 ) {
      std::cout << std::endl <<  std::setw( index - 1 ) << char( numSpace - i + 'A' );
      index -= 2;
    }
    std::cout << std::endl <<  std::endl;
  }
}

//
// An integer number was given and you had to write a function that would
// return the unique numbers of the number e.g. 156->3, 155->2
//
void PrintNumberOfUniqueNumbers( int num )
{
  if( num == 0 ) {
    std::cout << std::endl <<  "Number of Unique numbers: 0" << std::endl;
    return;
  }
  std::set<int> set_;
  while( abs( num ) > 0 ) {
    int mod = num % 10;
    if( set_.find( mod ) == std::end( set_ ) ) {
      set_.insert( mod );
    }
    num = static_cast<int> ( ( num / 10 ) );
  }

  std::cout << std::endl <<  "Number of Unique numbers: " << set_.size() << std::endl;
}
/*
* Glob style pattern matching
* [a-z] - sequence/range
* [bc] - one or the other
* ? - single character
* * - matches all characters
*/
bool PatternMatch ( const char*pattern, int patternLen, const char*string, int stringLen, bool nocase )
{

  //
  // wildcards - *, ?, []
  // example: *at - Cat, Barat
  std::string testPattern( pattern );

  while( patternLen > 0 ) {
    switch( pattern[0] ) {
    case '*': {
      const char *initialLocation = 0;
      pattern++; patternLen--;
      initialLocation = pattern; // to come back to this location if necessary
      for( ;; ) {
        if( nocase ) {
          if( pattern[0] == '*' ) {
            pattern++; patternLen--;
            initialLocation = pattern;
          }

          if( pattern[0] == '[' ) {
            // does the closing brace exist?
            auto closeBracePos = strchr( pattern, ']' );
            if( closeBracePos == nullptr ) {
              return false;
            }

            // do we have a '-'?
            auto dashPos = strchr( pattern, '-' );
            if( dashPos == nullptr ) { // does not exist
              pattern++; patternLen--;
              if( pattern[0] != string[0] ) {
                if( ( pattern + 1 ) == closeBracePos ) {
                  return false;
                } else {
                  pattern++; patternLen--;
                }
              } else {
                pattern++; patternLen--;
                string++; stringLen--;
                if( closeBracePos - pattern >= 0 ) {
                  pattern = closeBracePos + 1;
                  patternLen = strlen( pattern );
                  break;
                }
              }
            } else {
              if( string[0] >= *( dashPos - 1 ) && string[0] <= *( dashPos + 1 ) ) {
                // if they match, increment both so that they can be checked again
                string++; stringLen--;
                if( patternLen != 0 ) {
                  // if we have reached the end of pattern, we still need to wait for end of string
                  pattern = closeBracePos + 1; patternLen  = strlen( pattern );
                  if( patternLen == 0 && stringLen != 0 ) {
                    patternLen += ( pattern - initialLocation );
                    pattern = initialLocation;
                  }
                }
              } else {
                // reset the initial pointer so that search can again begin, increment the string ptr
                if( pattern != initialLocation ) {
                  patternLen += ( pattern - initialLocation );
                  pattern = initialLocation;
                }
                string++; stringLen--;
              }
            }
          } else {
            if( std::tolower( pattern[0] ) != std::tolower( string[0] ) ) {
              //
              // if the items dont match -
              // 1. reset the pattern ptr if the pointer has moved
              // 2. increment str pointer
              //
              if( pattern != initialLocation ) {
                patternLen += ( pattern - initialLocation );
                pattern = initialLocation;
              }
              string++; stringLen--;
            } else {
              //
              // if the items match, increment both so that they can be checked again
              //
              string++; stringLen--;
              if( patternLen != 0 ) {
                // if we have reached the end of pattern, we still need to wait for end of string
                pattern++; patternLen--;
                if( patternLen == 0 && stringLen != 0 ) {
                  patternLen += ( pattern - initialLocation );
                  pattern = initialLocation;
                }
              }
            }
          }
        } else {
        }
        if( patternLen == 0 && stringLen <= 0 ) { // exhausted the pattern and string
          return true;
        }
        if( stringLen < 0 ) { // exhausted string but pattern is still alive
          return false;
        }
      }
    }
    break;

    case '[': {
      // does the closing brace exist?
      auto closeBracePos = strchr( pattern, ']' );
      if( closeBracePos == nullptr ) {
        return false;
      }

      const char* openBracePos = 0, *patternPtr = pattern, *strPtr = string;
      if( *patternPtr == '-' ) {
        if( *patternPtr != *strPtr ) {
          return false;
        } else {
          patternPtr++; strPtr++;
        }
      }

      for( ;; ) {
        if( *patternPtr == '[' || *patternPtr == ']' ) {
          if( *patternPtr == '[' ) {
            openBracePos = patternPtr;
            closeBracePos = strchr( patternPtr, ']' ); // recalculate the ] position
            if( closeBracePos == nullptr ) {
              return false;
            }
          }
          patternPtr++;
        } else {
          if( *( patternPtr + 1 ) == '-' ) {
            if( *strPtr >= *patternPtr && *strPtr <= *( patternPtr + 2 ) ) {
              patternPtr = closeBracePos + 1; strPtr++; // present in the sequence
              pattern = patternPtr; patternLen = strlen( pattern );
              string = strPtr; stringLen = strlen( string );
              break;
            } else {
              return false; // the character in string does not fall in the sequence
            }
          } else {
            if( *patternPtr != *strPtr ) {
              if( ( ( patternPtr + 1 ) == closeBracePos ) || ( patternPtr - 1 ) >= closeBracePos ) {
                //
                // if we have reached a character just before ']' or passed the ]
                // then any mismatch would mean a string mismatch
                //
                return false;
              } else {
                //
                // if we havent passed the ']' position, we need to continue
                // checking in the event of a mismatch.
                // Eg: [bc]at should work for bat and cat,
                // so if b doesnt work, try with c
                //
                patternPtr++;
              }
            } else {
              patternPtr++; strPtr++;
              if( closeBracePos - patternPtr >= 0 ) {
                patternPtr = closeBracePos + 1;
                pattern = patternPtr; patternLen = strlen( pattern );
                string = strPtr; stringLen = strlen( string );
                break;
              }
            }
          }
        }
        // exhausted the pattern and string
        if( ( patternPtr - pattern ) == patternLen && ( strPtr - string ) >= stringLen ) {
          return true;
        }
        // exhausted string but pattern is still alive
        if( ( ( strPtr - string ) ) < 0 ) {
          return false;
        }
      }
    }
    break;
    case '?': {
      pattern++; patternLen--;
      string++; stringLen--;
    }
    break;
    default:
      if( nocase ) {
        if( std::tolower( pattern[0] ) == std::tolower( string[0] ) ) {
          // just increment the pointers and go to the next character
          pattern++; patternLen--;
          string++; stringLen--;
        } else {
          return false;
        }
      } else {
        if( pattern[0] == string[0] ) {
          // just increment the pointers and go to the next character
          pattern++; patternLen--;
          string++; stringLen--;
        } else {
          return false;
        }
      }
      break;
    }
  }

  if( patternLen == 0 && stringLen <= 0 ) {
    return true;
  }

  return false;
}

/*
* Reference for Glob style pattern matching
* Taken from Redis
*/
bool stringmatchlen( const char *pattern, int patternLen,
                     const char *string, int stringLen, int nocase )
{
  while( patternLen ) {
    switch( pattern[0] ) {
    case '*':
      while( pattern[1] == '*' ) {
        pattern++;
        patternLen--;
      }
      if( patternLen == 1 )
        return true; /* match */
      while( stringLen ) {
        if( stringmatchlen( pattern + 1, patternLen - 1,
                            string, stringLen, nocase ) )
          return true; /* match */
        string++;
        stringLen--;
      }
      return false; /* no match */
      break;
    case '?':
      if( stringLen == 0 )
        return false; /* no match */
      string++;
      stringLen--;
      break;
    case '[': {
      int not, match;

      pattern++;
      patternLen--;
      not = pattern[0] == '^';
      if( not ) {
        pattern++;
        patternLen--;
      }
      match = 0;
      for( ;; ) {
        if( pattern[0] == '\\' ) {
          pattern++;
          patternLen--;
          if( pattern[0] == string[0] )
            match = 1;
        } else if( pattern[0] == ']' ) {
          break;
        } else if( patternLen == 0 ) {
          pattern--;
          patternLen++;
          break;
        } else if( pattern[1] == '-' && patternLen >= 3 ) {
          int start = pattern[0];
          int end = pattern[2];
          int c = string[0];
          if( start > end ) {
            int t = start;
            start = end;
            end = t;
          }
          if( nocase ) {
            start = tolower( start );
            end = tolower( end );
            c = tolower( c );
          }
          pattern += 2;
          patternLen -= 2;
          if( c >= start && c <= end )
            match = 1;
        } else {
          if( !nocase ) {
            if( pattern[0] == string[0] )
              match = 1;
          } else {
            if( tolower( ( int )pattern[0] ) == tolower( ( int )string[0] ) )
              match = 1;
          }
        }
        pattern++;
        patternLen--;
      }
      if( not )
        match = !match;
      if( !match )
        return false; /* no match */
      string++;
      stringLen--;
      break;
    }
    case '\\':
      if( patternLen >= 2 ) {
        pattern++;
        patternLen--;
      }
    /* fall through */
    default:
      if( !nocase ) {
        if( pattern[0] != string[0] )
          return false; /* no match */
      } else {
        if( tolower( ( int )pattern[0] ) != tolower( ( int )string[0] ) )
          return false; /* no match */
      }
      string++;
      stringLen--;
      break;
    }
    pattern++;
    patternLen--;
    if( stringLen == 0 ) {
      while( *pattern == '*' ) {
        pattern++;
        patternLen--;
      }
      break;
    }
  }
  if( patternLen == 0 && stringLen == 0 )
    return true;
  return false;
}

void PatternMatchTest()
{

  /*std::string testString("cgcare8tion");
  std::string pattern("*care[0-9]t*");*/

  /*std::string testString("hello.txtahello.doc");
  std::string pattern("*.txt[!abc]*.doc");

  std::cout << "Expected Result: " << ((stringmatchlen(pattern.c_str(), pattern.length(), testString.c_str(), testString.length(), true) == 1)
  ? "true" : "false") << std::endl;

  std::cout << "Actual Result: " << ((PatternMatch(pattern.c_str(), pattern.length(), testString.c_str(), testString.length(), true) == 1)
  ? "true" : "false") << std::endl;*/

  std::fstream ifs( "test.txt" );
  std::fstream pattern_fs( "Patterns.txt" );
  std::string line, patt;
  std::vector<std::string> vec, pattern_vec;
  while( std::getline( ifs, line ) ) {
    vec.push_back( line );
  }

  while( std::getline( pattern_fs, patt ) ) {
    pattern_vec.push_back( patt );
  }

  bool expected = false, actual = false;
  int totalPass = 0, totalFail = 0;
  for( auto pattern : pattern_vec ) {
    if( pattern[0] == '#' ) {
      continue;
    }
    for( auto str : vec ) {
      if( ( expected = stringmatchlen( pattern.c_str(), pattern.length(), str.c_str(), str.length(), true ) )
          != ( actual = PatternMatch( pattern.c_str(), pattern.length(), str.c_str(), str.length(), true ) ) ) {
        std::cout << "*****************************************************************************" << std::endl;
        std::cout << "String: " << str << ", Pattern: " << pattern << " dont match!" << std::endl;
        std::cout << "Expected Result: " << ( ( expected == true ) ? "Pass" : "Fail" );
        std::cout << "\tActual Result: " << ( ( actual == true ) ? "Pass" : "Fail" ) << std::endl;
        std::cout << std::endl;
        std::cout << "*****************************************************************************" << std::endl;
        totalFail++;
      } else {
        /*std::cout << "String: " << str << ", Pattern: " << pattern << " match!" << std::endl;
        std::cout << "Expected Result: " << ((expected == true) ? "Pass" : "Fail");
        std::cout << "\tActual Result: " << ((actual == true) ? "Pass" : "Fail") << std::endl;
        std::cout << std::endl;*/
        totalPass++;
      }
    }
  }
  std::cout << std::endl;
  std::cout << "Total Pass: " << totalPass << ", Total Fail: " << totalFail << std::endl;
}

//
// Assumptions:
// 1. input assumed to be comma separated
// 2. Min number is 0 and max number is 255
//
int PrintNumberThatOccursMaxTimes( const std::string& input )
{
  if( input.empty() )
    return -1;

  std::string str ( input );
  int arr[256];
  std::fill( std::begin( arr ), std::end( arr ), 0 );
  std::size_t pos = 0, pos1 = 0;
  unsigned int i = 0;
  for( ;; ) {
    pos1 = str.find_first_of( ',', pos );
    if( pos1 == std::string::npos ) {
      i = atoi( str.substr( pos, pos1 ).c_str() );
      arr[i]++;
      break;
    }

    i = atoi( str.substr( pos, pos1 - pos ).c_str() );
    arr[i]++;
    pos = pos1 + 1;
  }

  return ( std::max_element( std::begin( arr ), std::end( arr ) ) - std::begin( arr ) );
}

/*
      Given an array of random integers, write an algorithm in C that
      removes duplicated numbers and return the unique numbers
      in the original array.
*/
int UniqueArray( int arr[], int Length )
{
  int i = 0, j = 0, revisedLength = 1;
  for( i = 1; i < Length; i++ ) {
    for( j = 0; j < revisedLength; j++ ) {
      if( arr[i] == arr[j] )
        break;
    }
    if( j == revisedLength )
      arr[revisedLength++] = arr[i];
  }

  return revisedLength;
}

struct Data {
  int numOccurance;
  int position;
};

int UniqueElementPosition( int arr[], int length )
{
  std::vector<Data> vec_, vec1_;
  vec_.reserve( 256 ); vec1_.reserve( 256 );
  vec_.resize( 256 ); vec1_.resize( 256 );

  // initialise
  std::for_each( std::begin( vec_ ), std::end( vec_ ),
  [&]( Data & d ) {
    d.numOccurance = 0; d.position = -1;
  } );

  std::for_each( std::begin( vec1_ ), std::end( vec1_ ),
  [&]( Data & d ) {
    d.numOccurance = 0; d.position = 256;
  } );

  // Parse
  for( int i = 0; i < length; ++i ) {
    vec_[arr[i]].numOccurance++;
    vec_[arr[i]].position = i + 1;
  }

  std::copy_if( std::begin( vec_ ), std::end( vec_ ), std::begin( vec1_ ), [&]( const Data & d ) { return ( d.numOccurance == 1 ); } );
  auto iter = std::min_element( std::begin( vec1_ ), std::end( vec1_ ),
  [&]( const Data & lhs, const Data & rhs ) { return lhs.position < rhs.position; } );
  if( iter == std::end( vec1_ ) )
    return -1;

  return iter->position;
}

int UniqueElementPositionArray( int arr[], int length )
{
  int occuranceArray[256] = {0};
  int positionArray[256] = {0};

  // parse
  for( int i = 0; i < length; ++i ) {
    occuranceArray[arr[i]]++;
    positionArray[arr[i]] = i + 1;
  }

  // find
  int uniqueElement = 0, firstPos = 256;
  for( int i = 0; i < 256; ++i ) {
    if( occuranceArray[i] == 1 ) {
      if( positionArray[i] < firstPos ) {
        uniqueElement = i;
        firstPos = positionArray[i];
      }
    }
  }

  if( firstPos == 256 )
    return -1;

  return firstPos;
}

void FirstNonRepeatingChar( char *str )
{
  if( str == NULL || strlen( str ) == 0 ) return;
  char result[256] = {0};
  int position[256] = {0};
  for( unsigned int i = 0; i < strlen( str ); ++i ) {
    result[str[i]]++;
    position[str[i]] = i + 1;
  }

  char ans = 0;
  int u_pos = 256;
  for( int i = 0; i < 256; ++i ) {
    if( result[i] == 1 ) {
      if( position[i] < u_pos ) {
        u_pos = position[i];
        ans = ( char )i;
      }
    }
  }

  printf( "%c = %d\n", ans, u_pos );
}

/*
      find the character that has most successive occurances in a string.
      Print the char and the number of occurrances.
*/
void FindMostSuccessiveOccurrance( char arr[], int length )
{
  if( length == 0 )
    return;

  char currentElement = arr[0], result = 0;
  int maxCount = 0, count = 0;
  for( int i = 0; i < length; ++i ) {
    if( arr[i] == currentElement ) {
      ++count;
    } else {
      if( count > maxCount ) {
        maxCount = count;
        result = currentElement;
      }

      currentElement = arr[i];
      count = 1;
    }
  }

  // elements are done, check again
  if( count > maxCount ) {
    maxCount = count;
    result = currentElement;
  }

  printf( "Result: %c, Occurrance: %d\n", result, maxCount );
}

void maxSequence( const char *str )
{
  auto begin = str, end = str + strlen( str );
  if( begin == end ) return;

  int arr[256] = {0};
  char curr_char = *begin;
  int curr_count = 0;
  for( ; begin != end; ++begin ) {
    if( *begin != curr_char ) {
      if( curr_count > arr[curr_char] ) {
        arr[curr_char] = curr_count;
      }
      curr_char = *begin;
      curr_count = 1;
    } else {
      ++curr_count;
    }
  }

  if( curr_count > arr[curr_char] ) {
    arr[curr_char] = curr_count;
  }

  auto iter = std::max_element( arr, arr + 256 );
  printf( "%c -> %d\n", iter - arr, *iter );
}

int GetLeastSignificantBitPos( int num )
{
  int position = 0;
  while( ( num & 1 ) != 1 ) {
    position++;
    num >>= 1;
  }
  return position;
}

int GetBitPos( int num, int pos )
{
  return ( ( num >> pos ) & 1 );
}

/*
*       You are given an integer array, where all numbers except for
*       TWO numbers appear even number of times.
*       Q: Find out the two numbers which appear odd number of times.
*/
void PrintOddValues( int *input, int length )
{
  if( input == NULL ) return;
  if( length == 0 ) return;
  int result = input[0];
  for( int i = 1; i < length; ++i ) {
    result ^= input[i];
  }
  int position = GetLeastSignificantBitPos( result );
  int num1 = 0, num2 = 0;
  for( int i = 0; i < length; ++i ) {
    if( GetBitPos( input[i], position ) == 0 ) {
      num1 ^= input[i];
    } else {
      num2 ^= input[i];
    }
  }

  printf ( "%d, %d\n", num1, num2 );
}

int MaxDistance( int arr[], int length )
{
  if( arr == NULL || length == 0 ) return -1;
  int *result = ( int * )calloc( length, sizeof( int ) );
  if( !result ) return -1;
  std::fill( result, result + length, 0 );
  for( int i = 0; i < length; ++i ) {
    result[arr[i]] = abs( result[arr[i]] - ( i + 1 ) );
  }

  int max = 0, pos = 0;
  for( int i = 0; i < length; ++i ) {
    if( result[i] > max ) {
      max = result[i];
      pos = i;
    }
  }

  return pos;
}


double Sqrt( int num )
{
  return std::exp( 0.5 * std::log( num ) );
}

double Sqrt2( int num )
{
  double i = 1;
  while( i * i < num ) {
    i += 0.1;
  }

  double res = 0.5 * ( i + ( num / i ) );
  int precision = 100;
  while( precision-- ) {
    res = 0.5 * ( res + ( num / res ) );
  }

  return res;
}


int FindUniqueOccurrance( int arr [], int length )
{

  if( length == 0 ) return 0;
  if( arr == NULL ) return 0;

  std::vector<int> vec( length );
  for( int i = 0; i < length; ++i ) {
    vec[arr[i]]++;
  }
  auto iter = std::find( std::begin( vec ), std::end( vec ), 1 );
  if( iter == std::end( vec ) ) return 0;
  return iter - std::begin( vec );
}

int SetBitPosition( int num )
{
  if( ( num & ( num - 1 ) ) != 0 )
    return -1;

  int count = 0;
  while( num ) {
    num >>= 1;
    ++count;
  }

  return count;
}

bool IsUnique ( char *str )
{
  if( str == NULL ) return false;
  if( strlen( str ) == 0 || strlen( str ) > 256 ) return false;

  std::bitset<256> bit_array( 0 );
  char *iter = &str[0];
  while( *iter != '\0' ) {
    if( bit_array[*iter] == 1 )
      return false;

    bit_array.set( *iter++ );
  }

  return true;
}


struct position {
  int i, j;
};

void displayPathtoPrincess( int n, const std::vector<std::string>& grid )
{

  std::cout << std::endl;
  std::size_t m_dist = -1, p_dist = -1;
  auto count = 0;
  for( auto& i : grid ) {
    auto pos = i.find( "m" );
    if( pos == std::string::npos ) {
      ++count;
      continue;
    }
    m_dist = count * n + pos + 1;
  }
  count = 0;
  for( auto& i : grid ) {
    auto pos = i.find( "p" );
    if( pos == std::string::npos ) {
      ++count;
      continue;
    }
    p_dist = count * n + pos + 1;
  }

  position m_pos, p_pos;
  m_pos.i = std::ceil( double( m_dist ) / double( n ) );
  m_pos.j = ( m_dist % n == 0 ) ? n : ( m_dist % n );
  p_pos.i = std::ceil( double( p_dist ) / double( n ) );
  p_pos.j = ( p_dist % n == 0 ) ? n : ( p_dist % n );

  std::cout << "m: " << m_pos.i << "," << m_pos.j << std::endl;
  std::cout << "p: " << p_pos.i << "," << p_pos.j << std::endl;

  char buff[32] = {0};
  if( p_pos.i > m_pos.i )
    sprintf( buff, "%s\n", "DOWN" );
  else if( p_pos.i < m_pos.i )
    sprintf( buff, "%s\n", "UP" );

  for( int i = std::abs( m_pos.i - p_pos.i ); i > 0; --i )
    std::cout << buff;

  if( p_pos.j > m_pos.j )
    sprintf( buff, "%s\n", "RIGHT" );
  else if( p_pos.j < m_pos.j )
    sprintf( buff, "%s\n", "LEFT" );

  for( int i = std::abs( m_pos.j - p_pos.j ); i > 0; --i )
    std::cout << buff;
}

int CountDifference ( int num )
{
  return std::bitset<32>( num ).count();
}

#include <vector>
#include <algorithm>

struct Node {
  int data;
  Node* left, *right;
  Node( int i ) : data( i ), left( nullptr ), right( nullptr )
  {}
};

Node* BuildTree()
{
  Node *head = new Node( 1 );
  head->left = new Node( 2 );
  head->right = new Node( 3 );
  Node* l = head->left;
  Node* r = head->right;
  l->left = new Node( 4 );
  l->right = new Node( 5 );
  l->left->right = new Node( 6 );
  l->left->right->left = new Node( 14 );
  l->right->left = new Node( 7 );
  l->right->right = new Node( 8 );
  r->left = new Node( 9 );
  r->right = new Node( 10 );
  r->left->right = new Node( 12 );
  r->right->right = new Node( 11 );
  r->right->right->left = new Node( 13 );
  return head;
}

void BuildEulerWalk( Node *head, std::vector<int>& euler )
{
  if( head == nullptr ) {
    return;
  }
  if( head->left == nullptr && head->right == nullptr ) {
    euler.push_back( head->data );
    return;
  }
  euler.push_back( head->data );
  if( head->left != nullptr ) {
    BuildEulerWalk( head->left, euler );
    euler.push_back( head->data );
  }
  if( head->right != nullptr ) {
    BuildEulerWalk( head->right, euler );
    euler.push_back( head->data );
  }
}

/**
 * Finds Least Common Ancestor given 2 nodes
 * @param  euler euler walk of the binary tree
 * @param  a     node 1
 * @param  b     node 2
 * @return       LCA value or -1 if not found
 */
int FindLCA( const std::vector<int>& euler, int a, int b )
{
  if( euler.empty() ) return -1;
  auto a_iter = std::find( euler.begin(), euler.end(), a );
  if( a_iter == euler.end() ) return -1;
  auto b_iter = std::find( euler.begin(), euler.end(), b );
  if( b_iter == euler.end() ) return -1;
  auto lhs_iter = a_iter;
  auto rhs_iter = b_iter;
  if( a_iter < b_iter ) {
    auto tempIter = std::find( a_iter, b_iter, a );
    if( tempIter != b_iter ) {
      a_iter = tempIter;
    }
    lhs_iter = a_iter;
    rhs_iter = b_iter;
  } else {
    auto tempIter = std::find( b_iter, a_iter, b );
    if( tempIter != a_iter ) {
      b_iter = tempIter;
    }
    lhs_iter = b_iter;
    rhs_iter = a_iter;
  }
  auto minIter = std::min_element( lhs_iter, rhs_iter );
  return *minIter;
}

void LCATester()
{
  Node *head = BuildTree();
  std::vector<int> euler;
  BuildEulerWalk( head, euler );
  for( auto n : euler ) {
    std::cout << n << ", ";
  }

  std::cout << "Enter 2 elements: ";
  int a, b;
  std::cin >> a >> b;
  std::cout << std::endl;
  auto min = FindLCA( euler, a, b );
  std::cout << "LCA of " << a << " and " << b << " is " << min << std::endl;
}