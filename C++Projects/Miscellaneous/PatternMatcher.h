#ifndef _PATTERN_MATCHER_
#define _PATTERN_MATCHER_

#include <string>

class PatternMatcher
{
private:
    bool static WildcardCompare(std::string str1, const std::string& pattern, bool allowEverything);
    bool static CheckBridgeParameters(const std::string& configString, const std::string& pattern);

public:
    void static PatternTest();
};

#endif