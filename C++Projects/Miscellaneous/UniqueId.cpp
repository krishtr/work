#include "StdAfx.h"
#include "UniqueId.h"
#include <ctime>
#include <windows.h>
#include <winbase.h>				//	InterlockedIncrement

namespace {
    unsigned long long num0 = 1900;
    volatile LONG num;
}

int Plugin::UniqueIdInit::count;

Plugin::UniqueIdInit::UniqueIdInit()
{
    if	(count++ == 0) {
        const std::time_t t = std::time(0);
        const std::tm* gt = std::gmtime(&t);

        num0 += gt->tm_year;
        num0 *= 100;
        num0 += gt->tm_mon + 1;
        num0 *= 100;
        num0 += gt->tm_mday;
        num0 *= 100;
        num0 += gt->tm_hour;
        num0 *= 100;
        num0 += gt->tm_min;
        num0 *= 100;
        num0 += gt->tm_sec;
        num0 *= 100000;
    }
}

long long Plugin::UniqueId()
{
    return num0 + InterlockedIncrement(&num);
}
