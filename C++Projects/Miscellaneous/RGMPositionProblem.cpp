#include <iostream>
#include <fstream>
#include <vector>
#include <map>

struct packet {
    std::string latest_timestamp;
    std::string symbol;
    int effective_position;
    int total_position;
    packet(): latest_timestamp()
              , symbol()
              , effective_position(0)
              , total_position(0)
    {}
};

void Split (std::vector<std::string>& coll, const std::string& str, char token) {
    auto old_pos = 0;
    auto new_pos = str.find_first_of(token);
    while (new_pos != std::string::npos) {
        coll.push_back(str.substr(old_pos, new_pos - old_pos));

        old_pos = new_pos + 1;
        new_pos = str.find_first_of(token, old_pos);
    }
    coll.push_back(str.substr(old_pos, str.length() - old_pos));
}

void RGMPositionStart() {

    std::ifstream ifs("RGM - PositionProb-Input.txt");
    std::string line;
    std::map<std::string, packet> map_;

    std::vector<std::string> vec_;
    vec_.reserve(6);
    while(std::getline(ifs, line)) {
        if (!vec_.empty()) {
            vec_.clear();
            vec_.reserve(6);
        }
        Split(vec_, line, ' ');
        if (vec_.size() != 5) {
            std::cout << "WARN: Input does not have the "
                "right number of elements" << std::endl;
            continue;
        }

        char direction = (char)toupper(vec_[1][0]);
        if (direction != 'B' && direction != 'S') {
            std::cout << "WARN: This is not Buy or Sell" << std::endl;
            continue;
        }

        std::string symbol(vec_[2]);
        map_[symbol].latest_timestamp = vec_[0];
        int num = atoi(vec_[3].c_str());
        map_[symbol].total_position += num;
        map_[symbol].effective_position += (direction == 'B') ? num : -num;
        map_[symbol].symbol = symbol;
    }

    // Aggregate position
    std::cout << "Aggregate position: " << std::endl;
    for (auto pack : map_) {
        if (pack.second.effective_position != 0) {
            std::cout << pack.second.symbol << " "
                << pack.second.effective_position << std::endl;
        }
    }

    // total position
    std::cout << std::endl;
    std::cout << "total position: " << std::endl;
    for (auto pack : map_) {
        std::cout << pack.second.symbol << " "
            << pack.second.total_position << " "
            << pack.second.latest_timestamp
            << std::endl;
    }
}

int main()
{
    RGMPositionStart();
    return 0;
}
