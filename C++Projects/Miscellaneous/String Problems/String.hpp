#include <tchar.h>
#include <iostream>
#include <algorithm>
#include <vector>
#include <string>
#include <stack>

//
// Input - "Aarthi is a beautiful babe!"
// output - "ihtraA si a lufituaeb !ebab"
void flipWordsInString(char *str) {
		auto begin = str, end = str + strlen(str);
		auto iter = begin;
		for (; ++iter < end; ) {
				if (*iter == ' ') {
						std::reverse(begin, iter);
						begin = iter + 1; // skip the whitespace
				}
		}

		if (iter == end) {
				std::reverse(begin, end);
		}
}

void flip (char *str)
{
		auto first = str, last = str + strlen(str);
		for (; first != last && first < --last ; ++first)
				std::iter_swap(first, last);
}

void stripChar(char *str, char c) {
		auto iter = std::remove(str, str + strlen(str), c);
		*iter = 0;
}


void ReplaceChar(char *str, char oldVal, char newVal) {
		std::replace(str, str + strlen(str), oldVal, newVal);
}

// find and replace
void ReplaceString(char *str, char *oldStr, char *newStr) {
		auto end = str + strlen(str);
		auto iter = std::search(str, end, oldStr, oldStr + strlen(oldStr));
		if (iter == end)
				return;

		memmove(iter + strlen(newStr), 
				iter + strlen(oldStr), 
				std::distance(iter + strlen(oldStr), end) + 1);
		std::copy(newStr, newStr + strlen(newStr), iter);
}

int BasicPatternMatching (char *mainString, char *pattern) {

        //
        //	Aarthi is a beautiful babe
        //	"babe"
        //
		auto begin = mainString, end = mainString + strlen(mainString);
		if (begin == end) return -1;

		auto iter = std::search(begin, end, pattern, pattern + strlen(pattern));
		if (iter == end) return -1;

		return std::distance(begin, iter);
}

//
//  two strings are given, remove all occurrences of the characters in the second string from the first string
//
void RemoveMultipleChar(char *mainStr, char *bucket) {
		auto bucket_begin = bucket, bucket_end = bucket + strlen(bucket);
		if (bucket_begin == bucket_end)
				return;
		
		auto first = mainStr; // location of first will not change
		for (; bucket_begin < bucket_end; ++bucket_begin) {
				auto last = mainStr + strlen(mainStr); // recalculate the last since strlen will change
				if (first == last) return;
				auto iter = std::remove(first, last, *bucket_begin);
				if (iter == last) continue; // if char is not present, dont do anything
				*iter = 0;
		}
}

void DeleteCharacters(char *str, char *toDel) {
        auto begin = str, end = str + strlen(str);
        if (begin == end) return;

        auto toDel_begin = toDel, toDel_end = toDel + strlen(toDel);
        if (toDel_begin == toDel_end) return;

        bool changed = false;
        while (begin != end) {
                auto iter = toDel_begin;
                for (; iter != toDel_end; ++iter) {
                        if (*iter == *begin) {
                                memmove(begin, begin + 1, end - begin);
                                changed = true;
                        }
                }
                if (!changed) {
                        ++begin;
                } else {
                        changed = false;
                }
        }
}

/*
*		Replace single character from a string
*/
void ReplaceCharacter(char *str, char c)
{
		char *iter = &str[0];
		while (*iter != '\0') {
				if (*iter == c) {
						memmove(iter, iter + 1, strlen(iter + 1) + 1);
				} else {
						++iter;
				}
		}
}

/*
*		Replace multiple characters from a string
*/
void ReplaceMultiCharacter(char *str, char c[])
{
		if (str == NULL)
				return;
		if (strlen(str) == 0 || strlen(c) == 0)
				return;

		char *iter = &str[0];
		bool replaced = false;
		while (*iter != '\0') {
				for (unsigned int i = 0; i < strlen(c); ++i) {
						if (*iter == c[i]) {
								memmove (iter, iter + 1, strlen(iter + 1) + 1);
								replaced = true;
								break;
						}
				}

				if (!replaced) {
						++iter;
				} else {
						replaced = false;
				}
		}
}

//
//  Print characters in the range between begin and end
//  starts from next character from begin and does not
//  include end
//
void PrintRange (const char *begin, const char *end) {

        if (begin == end) {
                return;
        }

        const char *iter = begin;
        while (iter != end) {
                std::cout << *iter++;
        }
}

//
//  reverse the order of the words in a string
//
void ReverseWordOrder (const char *str) {

        //
        //  str - Aarthi is a beautiful babe
        //  result - babe beautiful a is Aarthi
        //

        auto len = strlen (str);
        if (len == 0) {
                return;
        }

        const char * head = &str[0];
        const char * tail = &str[len];
        const char * iter = &str[len];

        while (iter != head) {
                if (*iter == ' ') {
                        PrintRange (iter, tail);
                        tail = iter;
                }

                iter--;
        }

        PrintRange (iter, tail);
}

/*
*		Print Reverse Order of words.
*		free() should be called on the resulting string
*		this method does not require to "print" range unlike the previous method
*/
char * ReverseWordOrderNew(char *str)
{
		if (str == NULL)
				return str;
		if (strlen(str) == 0)
				return NULL;

		char *result = (char*)calloc(strlen(str) + 1, 1);
		if (!result)
				return NULL;

		unsigned int currLength = 0;
		char *riter1 = &str[strlen(str) - 1], *riter2 = &str[strlen(str)];
		while (riter2 != &str[0]) {
				while (*riter1 != ' ' && riter1 != &str[0]) {
						riter1--;
				}

				memcpy(result + currLength, riter1, riter2 - riter1);
				currLength += (riter2 - riter1);
				riter2 = riter1--;
		}

		return result;
}

//
//  reverse the order of the words in a string
//
void ReverseWordOrder2 (char *temp) {

        //
        //  str - Aarthi is a beautiful babe
        //  result - babe beautiful a is Aarthi
        //
        char str[512];
        strcpy (str, temp);
        auto len = strlen (str) - 1;
        if (len == 0) {
                return;
        }

        const char * head = &str[0];
        char * iter = &str[len];

        while (iter != head) {
                if (*iter == ' ') {
                        std::cout << iter;
                        *iter = '\0';
                }

                iter--;
        }

        std::cout << iter << std::endl;
}

int GetNumberOfWords (char *str) {

        //
        //  count the\ number of whitespaces
        //  Number of words will be one more than the count
        //  (assuming that words are separated by just one space)
        //

        char temp[512];
        strcpy (temp, str);
        char *pch = strtok (temp, " ");
        if (!pch) {
                return 0;
        }

        int index = 0;
        while (pch) {
                index++;
                pch = strtok (0, " ");
        }

        return index;
}

//
//  write a function that takes an integer and a string of multiple words and 
//  rotates the words of the string by the specified number.
//
char * RotateWordsByValue (int value, char *str) {

        if (value == 0) {
                return str;
        }

        if (strlen(str) == 0) {
                return str;
        }

        int count = GetNumberOfWords (str), actualTimes = 0;
        if (value > count) {
                actualTimes = value % count;
        } else {
                actualTimes = value;
        }

        if (actualTimes == 0) {
                return str;
        }

        char *head = &str[0];
        char *iter = &str[strlen(str)-1];
        while (iter != head) {
                if (*iter == ' ') {
                        actualTimes--;
                        if (actualTimes == 0) {
                                break;
                        }
                }

                iter--;
        }

        *iter++ = '\0';
        strcat (iter, head);

        return iter;
}

int WordCount (char *str)
{
        if (strlen(str) == 0)
                return 0;

        int count = 0;
        char *iter = &str[0];
        while (iter++ != &str[strlen(str)]) {
                if (*iter == ' ')
                        ++count;
                if (iter == &str[strlen(str)]) {
                        ++count;
                        break;
                }
        }

        return count;
}

/*
*       Rotates the words in the sentence by the specified value.
*       This method uses dynamic allocation and a separate placeholder
*       for the result unlike the RotateWordsByValue() [good method BTW!]
*       which is evasive (modifies the original str)
*/
char * RotateSentence (char *str, int value)
{
        if (str == NULL)
                return NULL;

        char *result = (char *)calloc(strlen(str) + 1, 1);
        if (!result)
                return NULL;

        if (value == 0 || strlen(str) == 0) {
                memcpy(result, str, strlen(str) + 1);
                return result;
        }

        int count = value % WordCount(str);
        if (count == 0) {
                memcpy(result, str, strlen(str) + 1);
                return result;
        }

        char *riter1 = &str[strlen(str)], *riter2 = &str[strlen(str)];
        char *head = &str[0];
        while (count != 0) {
                riter1--;
                if (*riter1 == ' ')
                        --count;
        }

        memcpy (result, riter1, riter2 - riter1);
        memcpy (result + (riter2 - riter1), head, riter1 - head);
        memcpy (result + strlen(str), "\0", 1);
        return result;
}

//
//	convert ASCII to integer
//
int ConvertAsciiToInt (char c) 
{
        return (int)c;
}

//
//	convert integer to Ascii
//
char ConvertIntToAscii (int i) 
{
        return (char)i;
}

int StringToInt (const char *str)
{
        //
        // 1. Check for whitespace and tab
        // 2. Check for -/+
        // 3. Now check for each digit
        //
        int n = 0; bool isNegative = false;

        if (*str == ' ' || *str == '\t') {
                str++; // skip white space
        }

        if (*str == '-') {
                if (!isNegative) {
                        isNegative = true;
                        str++;
                }
        } else if (*str == '+') {
                str++;
        }

        while (*str) {
                unsigned char c = *str;
                if (!isdigit (c)) {
                        break;
                }
                n = 10 * n + (*str - '0');
                str++;
        }

        return isNegative ? -n : n;
}

void FindReplace (char *str, char *findStr, char *replStr)
{
        if (strlen (str) == 0 
        || strlen (findStr) == 0
        || strlen (replStr) == 0) {
                        return;
        }

        for (unsigned int i = 0; i < strlen (str); ++i) {
                if (str[i] == findStr[0]) {
                        unsigned int count = 1;
                        for (unsigned int ptr = i + 1, j = 1; j < strlen (findStr); ++ptr, ++j) {
                                if (str[ptr] != findStr[j]) {
                                        break;
                                }
                                ++count;
                        }

                        if (count == strlen (findStr)) {
                                // start replacing
                                int len = strlen(str);
                                memmove (str + i + strlen(replStr), str + i + count, strlen(str) - i - count + 1);
                                str[len + (strlen(replStr) - strlen(findStr))] = '\0';
                                for (unsigned int j = 0; j < strlen(replStr); ++j, ++i) {
                                        str[i] = replStr[j];
                                }
                        }
                }
        }
}

void FindReplaceLibrary (char *str, char *findStr, char *replStr)
{
        char *pch = strstr (str, findStr);
        if (pch == NULL) {
                return;
        }

        while (pch != NULL) {
                // copy replstr to pch,
                strncpy (pch, replStr, strlen(replStr));
                // start searching again from the next character
                pch = strstr (pch+1, findStr);
        }
}

/*
*       Find 'findstr' in 'str' and replace it with 'replacestr'
*/

char * FindReplaceCFunctions(char *str, char *findStr, char *replaceStr)
{
        char * pos = strstr(str, findStr);
        if (!pos)
                return str;

        char *result = (char*)calloc(strlen(str) - strlen(findStr) + strlen(replaceStr) + 1, 1);
        if (result == NULL)
                return str;
        memcpy(result, str, pos - str);
        memcpy(result + (pos - str), replaceStr, strlen(replaceStr));
        memcpy(result + (pos - str) + strlen(replaceStr),
                pos + strlen(findStr),
                1 + strlen(str) - ((pos - str) + strlen(findStr)));
        return result;
}

//
// 44250 -> Fourty four Thousand two hundered and fifty
// assumptions - number will be below 100,000. Max is 99,999
//
namespace StringProblem {

        std::pair<int, int> CalcZero (int num)
        {
                int count = 0, copy = num;
                while (copy != 0) {
                        if (copy % 10 == 0) {
                                count++;
                        }
                        copy /= 10;
                }
                return std::make_pair (count, num / static_cast<int>((pow(10,count))));
        }

        void PrintString (int num)
        {
                auto ret = CalcZero(num);
                switch (ret.second) {
                case 1:
                        if (ret.first == 1 || ret.first == 4) {
                                std::cout << "Ten ";
                        } else {
                                std::cout << "One ";
                        }
                        break;
                case 2:
                        if (ret.first == 1 || ret.first == 4) {
                                std::cout << "Twenty ";
                        } else {
                                std::cout << "Two ";
                        }
                        break;
                case 3:
                        if (ret.first == 1 || ret.first == 4) {
                                std::cout << "Thirty ";
                        } else {
                                std::cout << "Three ";
                        }
                        break;
                case 4:
                        if (ret.first == 1 || ret.first == 4) {
                                std::cout << "Fourty ";
                        } else {
                                std::cout << "Four ";
                        }
                        break;
                case 5:
                        if (ret.first == 1 || ret.first == 4) {
                                std::cout << "Fifty ";
                        } else {
                                std::cout << "Five ";
                        }
                        break;
                case 6:
                        if (ret.first == 1 || ret.first == 4) {
                                std::cout << "Sixty ";
                        } else {
                                std::cout << "Six ";
                        }
                        break;
                case 7:
                        if (ret.first == 1 || ret.first == 4) {
                                std::cout << "Seventy ";
                        } else {
                                std::cout << "Seven ";
                        }
                        break;
                case 8:
                        if (ret.first == 1 || ret.first == 4) {
                                std::cout << "Eighty ";
                        } else {
                                std::cout << "Eight ";
                        }
                        break;
                case 9:
                        if (ret.first == 1 || ret.first == 4) {
                                std::cout << "Ninety ";
                        } else {
                                std::cout << "Nine ";
                        }
                        break;
                case 11:
                        std::cout << "Eleven ";
                        break;
                case 12:
                        std::cout << "Twelve ";
                        break;
                case 13:
                        std::cout << "Thirteen ";
                        break;
                case 14:
                        std::cout << "Fourteen ";
                        break;
                case 15:
                        std::cout << "Fifteen ";
                        break;
                case 16:
                        std::cout << "Sixteen ";
                        break;
                case 17:
                        std::cout << "Seventeen ";
                        break;
                case 18:
                        std::cout << "Eighteen ";
                        break;
                case 19:
                        std::cout << "Nineteen ";
                        break;
                }

                if (ret.first == 2) {
                        std::cout << "Hundered ";
                }

                if (ret.first == 3) {
                        std::cout << "Thousand ";
                }
        }

        void PrintFullString (int num)
        {
                if (num == 0) {
                        return;
                }

                static int digit = 1;
                int temp = static_cast<int>(std::pow (10, digit++));
                int mod = num % temp;
                num -= mod;
                if (num / temp < 20 ) {
                        PrintString (num);
                } else {
                        PrintFullString (num);
                }
                PrintString (mod);
        }
}

void Split (std::vector<std::string>& coll, const std::string& str, char delimiter) {
        auto old_pos = 0;
        auto new_pos = str.find_first_of(delimiter);
        while (new_pos != std::string::npos) {
                coll.push_back(str.substr(old_pos, new_pos - old_pos));

                old_pos = new_pos + 1;
                new_pos = str.find_first_of(delimiter, old_pos);
        }
        coll.push_back(str.substr(old_pos, str.length() - old_pos));
}

int factorial(int n)
{
        if (n == 0)
                return 1;
        if (n == 1)
                return 1;

        return n * factorial(n-1);
}

void PrintStringCombinations(const std::string& s)
{
        if (s.empty()) {
                std::cout << "Input string is empty. No combinations possible" << std::endl;
                return;
        }

        if (s.length() == 1) {
                std::cout << s << std::endl;
                return;
        }

        std::string str(s);
        int combinations = factorial(str.length());
        for (int i = 0; i < combinations; ++i) {
                std::cout << str << ", ";
                std::next_permutation(std::begin(str), std::end(str));
        }

        std::cout << std::endl;
}

/*
        Given a string like "/foo/bar/../baz/../" representing a directory path, where ".." 
        denotes "move up a directory", write a function that returns the 
        "reduced" path � in this case the reduced path would be just "/foo/".
*/
std::string ParsePathNames(const std::string& path)
{
        if (path.empty()) {
                return path;
        }

        std::stack<std::string> stack_;
        std::string result;
        for (auto c : path) {
                if (c == '/') {
                        if (!result.empty()) {
                                if (result == "..") {
                                        if (!stack_.empty())
                                                stack_.pop();
                                } else {
                                        stack_.push(result);
                                }
                                result.clear();
                        }
                } else {
                        result.push_back(c);
                }
        }

        std::stack<std::string> resultStack_;
        while(!stack_.empty()) {
                resultStack_.push(stack_.top());
                stack_.pop();
        }
        result.clear();
        result.append("/");
        while (!resultStack_.empty()) {
                result.append(resultStack_.top()).append("/");
                resultStack_.pop();
        }

        return result;
}

/*
*       Given 2 strings, are they anagrams of eachother?
*       testcases:
*       IsAnagram("Gold", "Gdlo");
*       IsAnagram("Gold", "dloG");
*       IsAnagram("Gold", "Gmlf");
*       IsAnagram("Gold", "MILF");
*       IsAnagram("Gold", "Go");
*       IsAnagram("Gold", "gold");
*       IsAnagram("Gold", "Gold");
*       IsAnagram("Gold", "GOLD");
*/
bool IsAnagram (char *str1, char *str2)
{
        if (str1 == NULL || str2 == NULL) {
                printf ("Are %s and %s anagrams? : %s\n", str1, str2, "No");
                return false;
        }
        if (strlen(str1) != strlen(str2)) {
                printf ("Are %s and %s anagrams? : %s\n", str1, str2, "No");
                return false;
        }

        bool result = true;
        for (unsigned int i = 0; (i < strlen(str1) && result); ++i) {
                for (unsigned int j = 0; j < strlen(str2); ++j) {
                        if (str1[i] == str2[j]) {
                                result = true;
                                break;
                        } else {
                                result = false;
                        }
                }
        }

        printf ("Are %s and %s anagrams? : %s\n", str1, str2, result ? "Yes" : "No");
        return result;
}

// using STL
bool IsAnagram1(char *str1, char *str2) {
		return std::is_permutation(str1, str1 + strlen(str1), str2);
}

/*
*		Gayle 1.1: Implement an algorithm to determine if a string has all unique characters. What
*		if you cannot use additional data structures?
*		This is using an additional datastructure (array)
*/
bool IsUniqueString (char *str)
{
		auto begin = str, end = str + strlen(str);
		if (begin == end) return false;
		if (std::distance(begin, end) > 256) return false;

		int result[256] = {0};
		while (begin < end) {
                if (result[*begin] == 1) return false;
				result[*begin++]++;
		}

        return true;
}

/*
*		Gayle 1.1: Implement an algorithm to determine if a string has all unique characters. What
*		if you cannot use additional data structures?
*		Without data structure
*/
bool IsUniqueStringNoArray(char *str)
{
		auto begin = str, end = str + strlen(str);
		if (begin == end) return false;
		for (; begin < end; ++begin) {
				auto iter = begin + 1;
				while (iter != end) {
						if (*iter++ == *begin) return false;
				}
		}

		return true;
}

/*
        Write a method to replace all spaces in a string with '%20'.You may assume that
        the string has sufficient space at the end of the string to hold the additional
        characters, and that you are given the "true" length of the string.
        EXAMPLE
                Input:"Mr John Smith    "
                Output:"Mr%20John%20Smith"
*/
void ReplaceSpace (char *str, int true_length)
{
        if (str == NULL) return;
        if (true_length == 0) return;

        char *s = "%20";
        for (unsigned int i = 0; i < strlen(str); ++i) {
                if (str[i] == ' ') {
                        memmove(str + i + strlen(s),
                                str + i + 1,
                                strlen(str) - i + 1);
                        for (unsigned int j = 0; j < strlen(s); ++j,++i) {
                                str[i] = s[j];
                        }
                }
        }
}

// iterator and STL version
void ReplaceSpace1 (char *str, char *newVal)
{
		auto begin = str, end = str + strlen(str);
		if (begin == end) return;

        for (; begin < end; ++begin) {
                if (*begin == ' ') {
                        memmove(begin + strlen(newVal),
                                begin + 1,
                                std::distance(begin,end));
						std::copy(newVal, newVal + strlen(newVal), begin);
						begin += strlen(newVal);
						end = str + strlen(str); // end is changing, so recalculate
                }
        }
}

// simpler but with additional data structure
std::string ReplaceSpaceWithGivenString(char *str, char *newVal) {
		auto begin = str, end = str + strlen(str);
		if (begin == end) return std::string();

		std::string result;
		result.reserve(strlen(str));
		for (; begin != end; ++begin) {
				if (*begin == ' ') result.append(newVal);
				else result.push_back(*begin);
		}
		return result;
}

/*
        Implement a method to perform basic string compression using the counts
        of repeated characters. Forexample, the string aabcccccaaa would become
        a2blc5a3. If the "compressed" string would not become smaller than the 
        original string, your method should return the original string.
*/
std::string toString(int i) {
		char temp[16];
		sprintf(temp, "%d", i);
		return temp;
}

std::string BasicStringCompression(char *str) {
		auto begin = str, end = str + strlen(str);
		if (begin == end) return std::string();

		auto count = 0; std::string result;
		auto iter = begin;
		for (; begin < end; ++begin, ++count) {
				if (*begin != *iter) {
						result.push_back(*iter);
						result.append(toString(count));
						iter = begin;
						count = 0;
				}
		}

		result.push_back(*iter);
		result.append(toString(count));
		return result;
}

bool IsSubString(char *str1, char *str2)
{
		return (strstr(str1, str2) != NULL);
}

/*
		Assume you have a method isSubstring which checks if one word is a
		substring of another. Given two strings, s1 and s2, write code to check if s2 is
		a rotation of s1 using only one call to isSubstring (e.g.,"waterbottle"is a rota-
		tion of "erbottlewat")
*/
bool IsRotated(char *str1, char *str2)
{
		if (str1 == NULL || str2 == NULL) return false;
		if (strlen(str1) != strlen(str2)) return false;
		unsigned int i = 0; bool found = false;
		for (; i < strlen(str2); ++i) {
				if (str2[i] == str1[0]) {
						found = true;
						break;
				}
		}

		if (!found) return false;
		unsigned int mid = (int) (strlen(str2)/2);
		if (i < mid) {
				if (IsSubString (str1, str2 + i)) {
						for (unsigned int j = 0; j < i; ++j) {
								if (str2[j] != str1[strlen(str1) - i + j])
										return false;
						}
						return true;
				} else {
						return false;
				}
		} else {
				for (unsigned int j = 0; j < strlen(str2) - i; ++j) {
						if (str1[j] != str2[i + j])
								return false;
				}

				char *temp = (char *)calloc(1 + i, 1);
				if (!temp) return false;
				memcpy(temp, str2, i);
				bool result = IsSubString(str1, temp);
				free(temp);
				return result;
		}
}

// find all combinations of strings a phone number can make (2 � ABC, 2 � DEF, etc)
const char *arr[] = {{""}, {""}, {"abc"}, {"def"}, {"ghi"}, {"jkl"}, {"mno"}, {"pqrs"}, {"tuv"}, {"wxyz"}};
static int count;
void PrintStringPermutations(const std::string& str, std::string& result)
{
		if (str.empty()) {
				//std::cout << result << std::endl;
                count++;
				return;
		}

        auto iter = str.begin();
        /*while ((*iter == '0') || (*iter == '1'))
                ++iter;*/

		const char *s = arr[*iter - '0'];
		for (unsigned int i = 0; i < strlen(s); ++i) {
				result.push_back(s[i]);
				PrintStringPermutations(str.substr(iter + 1 - str.begin()), result);
				result.pop_back();
		}
}

//
// input -> "_hello___World____" c = "_l"
// output -> "_helo_World_"
//
std::string& RemoveSpecifiedRepeatedChar(std::string& str, const char * c) {
        auto it = std::unique(str.begin(), str.end(), 
                                [=](char a, char b)->bool { 
                                        auto iter = std::find(c, c+strlen(c), a);
                                        if (iter == c+strlen(c))
                                                return false;

                                        return (a==b) && (a==*iter);
                                });
        str.erase(it, str.end());
        return str;
}

char *StrStr(char *haystack, char *needle) {

        for (; haystack != nullptr ; ++haystack) {
                char *h = haystack;
                for (char *n = needle; ; ++n, ++h) {
                        if (*h != *n) break;
                        if (!*n) return haystack;
                }
        }

        return nullptr;
}

void PatternMatching(char *haystack, char *needle) {

        auto haystackBegin = haystack;
        auto hayStackEnd = haystack + strlen(haystack);
        if (haystackBegin == hayStackEnd) return;

        auto iter = std::search(haystackBegin, hayStackEnd, 
                                needle, needle + strlen(needle));
        if (iter == hayStackEnd) return;

        printf("%s occurs at position %d\n", needle, iter - haystackBegin);
}

void Reverse(char *str) {
        auto begin = str;
        auto end = str + strlen(str) - 1;

        if (begin == end) return;
        for (; begin < end; ++begin, --end)
                std::swap(*begin, *end);
}

void StringStart() {
        
        char arr[] = "Aarth";
        std::cout << arr << std::endl;
        Reverse(arr);
        std::cout << arr << std::endl;
}