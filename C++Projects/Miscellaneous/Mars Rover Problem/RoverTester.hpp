#ifndef ROVERTESTER_H_
#define ROVERTESTER_H_

#include <iostream>
#include <string>
#include "Rover.h"

class RoverTester
{
public:
        static void Run();
};

void RoverTester::Run()
{
        std::cout << "Welcome to the Mars Rover program!" << std::endl;
        std::cout << std::endl;

        std::cout << "Please type your input to start" << std::endl;
        Rover rover;
        rover.start();

        std::string input;
        std::getline (std::cin, input);
        rover.decodeMaxCoordinates (input);

        while (! input.empty()) {
                std::cout << std::endl;
                std::getline (std::cin, input);
                if	(input == "Q")
                        break;

                rover.decodeCoordinates (input);
                std::getline (std::cin, input);
                rover.decodeMovements (input);
                std::cout << std::endl;
                std::cout << "*****" << std::endl;
                rover.display();
                std::cout << "*****" << std::endl;
        }

        std::system ("cls");
        std::cout << "Thanks for using the Mars Rover program!" << std::endl;
        std::cout << std::endl;
}

#endif