#ifndef ROVER_H_
#define ROVER_H_

#include "Direction.h"

class Rover
{
public:
  Rover();
  ~Rover();
  void start();
  void display() const;

  //
  //	Setters and Getters for X & Y
  //
  void setX (int value);
  void setY (int value);
  int getX() const { return x_; }
  int getY() const { return y_; }

  //
  //	Increment or Decrement X & Y
  //
  void incrementX();
  void decrementX();
  void incrementY();
  void decrementY();

  //
  //	Setters and Getters for MaxX and MaxY
  //
  void setMaxX(int value) { maxX_ = value; }
  void setMaxY(int value) { maxY_ = value; }
  int getMaxX() const { return maxX_; }
  int getMaxY() const { return maxY_; }

  //
  //	set Direction
  //
  void setDirection(Direction* direction) { currentDirectionPtr_ = direction; }

  //
  //	get Directions
  //
  Direction* getNorthDirection() const	{ return northDirection_; }
  Direction* getEastDirection() const		{ return eastDirection_; }
  Direction* getWestDirection() const		{ return westDirection_; }
  Direction* getSouthDirection() const	{ return southDirection_; }

  //
  //	Decode
  //
  void decodeCoordinates(const std::string&);
  void decodeMovements(const std::string&);
  void decodeMaxCoordinates(const std::string&);
private:
  void moveLeft()		{ currentDirectionPtr_->moveLeft (); }
  void moveRight()	{ currentDirectionPtr_->moveRight (); }
  void move()			{ currentDirectionPtr_->move (); }

  Direction* northDirection_;
  Direction* eastDirection_;
  Direction* westDirection_;
  Direction* southDirection_;

  Direction* currentDirectionPtr_;	//	current direction
  int x_,y_, maxX_, maxY_;
};

#endif