#include <iostream>

#include "NorthDirection.h"
#include "SouthDirection.h"
#include "EastDirection.h"
#include "WestDirection.h"
#include "Rover.h"

Rover::Rover()
  : northDirection_(0)
  , eastDirection_(0)
  , westDirection_(0)
  , southDirection_(0)
  , currentDirectionPtr_(0)
  , x_(0), y_(0)
  , maxX_(0), maxY_(0)
{
}

void Rover::start ()
{
  if	(!northDirection_) {
    northDirection_ = new NorthDirection (this);
  }

  if	(!eastDirection_) {
    eastDirection_ = new EastDirection (this);
  }

  if	(!westDirection_) {
    westDirection_ = new WestDirection (this);
  }

  if	(!southDirection_) {
    southDirection_ = new SouthDirection (this);
  }

}

Rover::~Rover()
{
  if	(northDirection_) {
    delete northDirection_;
    northDirection_ = 0;
  }

  if	(eastDirection_) {
    delete eastDirection_;
    eastDirection_ = 0;
  }

  if	(westDirection_) {
    delete westDirection_;
    westDirection_ = 0;
  }

  if	(southDirection_) {
    delete southDirection_;
    southDirection_ = 0;
  }

  //	its not newed, so should not use delete here
  if (currentDirectionPtr_) {
    currentDirectionPtr_ = 0;
  }
}

void Rover::setX (int value) 
{
  if (value < 0) {
    value = 0;
  } else if (value > maxX_) {
    value = maxX_;
  }

  x_ = value; 
}

void Rover::incrementX()
{
  x_++;
  if (x_ > maxX_) {
    x_ = maxX_;
  }
}

void Rover::decrementX()
{
  x_--;
  if (x_ < 0) {
    x_ = 0;
  }
}

void Rover::setY (int value) 
{
  if (value < 0) {
    value = 0;
  } else if (value > maxY_) {
    value = maxY_;
  }

  y_ = value; 
}

void Rover::incrementY()
{
  y_++;
  if (y_ > maxY_) {
    y_ = maxY_;
  }
}

void Rover::decrementY()
{
  y_--;
  if (y_ < 0) {
    y_ = 0;
  }
}

void Rover::decodeCoordinates (const std::string& line)
{
  if (line.empty()) {
    std::cout << "Invalid coordinates!" << std::endl;
    return;
  }
  if (line.length() != 5) {
    std::cout << "Invalid coordinates! Unexpected length of string!" << std::endl;
    return;
  }

  setX (line[0] - '0');
  setY (line[2] - '0');
  switch (line[4]) {
  case 'N':
      currentDirectionPtr_ = northDirection_;
      break;
  case 'E':
      currentDirectionPtr_ = eastDirection_;
      break;
  case 'W':
      currentDirectionPtr_ = westDirection_;
      break;
  case 'S':
      currentDirectionPtr_ = southDirection_;
      break;
  }
}

void Rover::decodeMovements (const std::string& line)
{
  if (line.empty()) {
    std::cout << "Invalid input string!" << std::endl;
    return;
  }

  if (!currentDirectionPtr_) {
    std::cout << "Direction of Rover is not initialised!" << std::endl;
    return;
  }

  for (auto c : line) {
    switch (c) {
    case 'L':
        currentDirectionPtr_->moveLeft();
        break;
    case 'R':
        currentDirectionPtr_->moveRight();
        break;
    case 'M':
        currentDirectionPtr_->move();
        break;
    default:
        break;
    }
  }
}

void Rover::decodeMaxCoordinates (const std::string& line)
{
  if	(line.empty()) {
    std::cout << "Invalid input string!" << std::endl;
    return;
  }

  if (line.length() != 3) {
    std::cout << "Invalid coordinates! Unexpected length of string!" << std::endl;
    return;
  }

  setMaxX (line[0] - '0');
  setMaxY (line[2] - '0');
}

void Rover::display () const
{
  std::cout << getX() << " " << getY() << " " << currentDirectionPtr_->description() << std::endl;
}