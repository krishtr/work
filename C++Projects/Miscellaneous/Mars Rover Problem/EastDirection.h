#ifndef EAST_H_
#define EAST_H_

#include "Direction.h"
#include "Rover.h"

class EastDirection : public Direction
{
public:
        EastDirection(Rover* ptr) : vehicle_(ptr) {}
private:
        EastDirection();
        EastDirection(const EastDirection&);
        EastDirection& operator=(const EastDirection&);

        void moveLeft() { vehicle_->setDirection (vehicle_->getNorthDirection()); }
        void moveRight() { vehicle_->setDirection (vehicle_->getSouthDirection()); }
        void move() { vehicle_->incrementX(); }
        const char* description() const { return "E"; }

        Rover* vehicle_;
};

#endif