#ifndef WEST_H_
#define WEST_H_

#include "Direction.h"
#include "Rover.h"

class WestDirection : public Direction
{
public:
        WestDirection(Rover* ptr) : vehicle_(ptr) {}
private:
        WestDirection();
        WestDirection(const WestDirection&);
        WestDirection& operator=(const WestDirection&);

        void moveLeft() { vehicle_->setDirection (vehicle_->getSouthDirection()); }
        void moveRight() { vehicle_->setDirection (vehicle_->getNorthDirection()); }
        void move() { vehicle_->decrementX(); }
        const char* description() const { return "W"; }

        Rover* vehicle_;
};

#endif