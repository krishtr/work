#ifndef NORTH_H_
#define NORTH_H_

#include "Direction.h"
#include "Rover.h"

class NorthDirection : public Direction
{
public:
        NorthDirection(Rover* ptr) : vehicle_(ptr) {}
private:
        NorthDirection();
        NorthDirection(const NorthDirection&);
        NorthDirection& operator=(const NorthDirection&);

        void moveLeft() { vehicle_->setDirection (vehicle_->getWestDirection()); }
        void moveRight() { vehicle_->setDirection (vehicle_->getEastDirection()); }
        void move() { vehicle_->incrementY(); }
        const char* description() const { return "N"; }

        Rover* vehicle_;
};

#endif