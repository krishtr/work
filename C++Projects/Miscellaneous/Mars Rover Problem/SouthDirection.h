#ifndef SOUTH_H_
#define SOUTH_H_

#include "Direction.h"
#include "Rover.h"

class SouthDirection : public Direction
{
public:
        SouthDirection(Rover* ptr) : vehicle_(ptr) {}
private:
        SouthDirection();
        SouthDirection(const SouthDirection&);
        SouthDirection& operator=(const SouthDirection&);

        void moveLeft() { vehicle_->setDirection (vehicle_->getEastDirection()); }
        void moveRight() { vehicle_->setDirection (vehicle_->getWestDirection()); }
        void move() { vehicle_->decrementY(); }
        const char* description() const { return "S"; }

        Rover* vehicle_;
};

#endif