#ifndef DIRECTION_H_
#define DIRECTION_H_

class Direction
{
public:
        virtual void moveLeft() = 0;
        virtual void moveRight() = 0;
        virtual void move() = 0;
        virtual const char* description() const = 0;
};

#endif