#pragma once

#ifndef UniqueId_h
#define UniqueId_h

namespace Plugin {
    //	will produce unique number in the form current timestamp + sequence:
    //	2005-12-31 15:15:30 + 1 -> 2005123115153000001;
    //	time is set to time when process starts; sequence goes up on each call
    long long UniqueId();

    class UniqueIdInit {
    public:
        UniqueIdInit();
    private:
        static int count;
    };

    namespace {
        UniqueIdInit unique_id_init;
    }
}

#endif
