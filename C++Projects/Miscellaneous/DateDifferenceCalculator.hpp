#include "tchar.h"
#include <time.h>
#include <iostream>
#include <sstream>


bool IsLeapYear (int year)
{
	if	(year%400 == 0)
		return true;
	else if	(year%100 == 0)
		return false;
	else if	(year%4 == 0)
		return true;
	else
		return false;

}

int DateDifferenceCalculator ()
{
	int non_leap_months[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

	time_t rawtime;
	tm * ptm;
	time (&rawtime);
	ptm = gmtime(&rawtime);

	int curr_year = ptm->tm_year + 1900;
	int curr_month = ptm->tm_mon + 1;
	int curr_day = ptm->tm_mday;

	std::ostringstream os;

	os << "Current date: " << curr_year << "/" << curr_month << "/" << curr_day;

	std::cout << "Enter a date (yyyy/mm/dd) : ";
	std::string date;
	std::cin >> date;
	std::cout << std::endl;

	int year = 0, month = 0, days = 0;
	size_t pos = date.find_first_of('/');
	if	(pos == std::string::npos)
	{
		std::cout << "Invalid format" << std::endl;
		return 1;
	}

	year = atoi (date.substr (0, pos).c_str ());
	if	(year < 1000 || year > curr_year)
	{
		std::cout << "Incorrect year" << std::endl;
		return 1;
	}

	size_t p = date.find ('/', pos+1);
	if	(p == std::string::npos)
	{
		std::cout << "Invalid format" << std::endl;
		return 1;
	}

	month = atoi (date.substr (pos+1, p).c_str ());
	if	(month < 1 || month > 12)
	{
		std::cout << "Incorrect month" << std::endl;
		return 1;
	}

	days = atoi (date.substr (p+1, date.length ()).c_str());
	if	(days < 1 || days > 31)
	{
		std::cout << "Invalid date" << std::endl;
		return 1;
	}

	/*
	 *	if its the same year, the month cannot be greater than current month
	 */

	int year_diff = 0, month_diff = 0, day_diff = 0;
	year_diff = curr_year - year;
	month_diff = curr_month - month;
	if	(year_diff == 0)	//	same year
	{
		if	(month_diff < 0)
		{
			std::cout << "Invalid date. Entered month is greater than the current month in the same year" << std::endl;
			return 1;
		}
	}

	if	(month_diff < 0)
	{
		month_diff = 12 - month + curr_month;
		year_diff--;
	}

	day_diff = curr_day - days;
	if	(day_diff < 0)
	{
		month_diff--;
		if	(IsLeapYear (curr_year))	//	passed Feb
		{
			if	(curr_month > 2)
				day_diff = non_leap_months[curr_month] - days + curr_day + 1;	//	1 for leap year
			else
				day_diff = non_leap_months[curr_month] - days + curr_day;
		}
		else
			day_diff = non_leap_months[curr_month] - days + curr_day;
	}

	std::cout << "Diff b/w " << os.str () << " and " << date << " is " << year_diff << " year(s) " << month_diff << " month(s) " << day_diff << " day(s)";
	std::cout << std::endl;
}