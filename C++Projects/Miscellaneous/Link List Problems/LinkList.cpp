#include <iostream>
#include <cassert>

struct Node
{
    int data;
    Node* next;
};

void display(Node* head)
{
    while(head != nullptr) {
        std::cout << head->data << ", ";
        head = head->next;
    }
    std::cout << std::endl;
}

int getSize(Node *head)
{
    int count = 0;
    while (head != nullptr) {
        ++count;
        head = head->next;
    }
    return count;
}


int Count(Node* head, int searchFor)
{
    int count = 0;
    while(head != nullptr) {
        if(head->data == searchFor)
            ++count;
        head = head->next;
    }
    return count;
}

void DeleteList(Node **head)
{
    Node* prev = (*head)->next;
    Node* next = prev->next;
    while(next != nullptr) {
        delete prev;
        prev = next;
        next = prev->next;
    }
    delete prev;
    delete *head;
    *head = nullptr;
}

int Pop(Node** head)
{
    int data = -1;
    Node* iter = *head;
    *head = iter->next;
    data = iter->data;
    delete iter;
    iter = nullptr;
    return data;
}

Node* Push(Node** head, int data)
{
    Node* newHead = new Node();
    newHead->data = data;
    newHead->next = *head;
    *head = newHead;
    return *head;
}

void InsertNth(Node** head, int index, int data)
{
    int size = getSize(*head);
    assert(index >= 0 && index <= size);
    if (index == 0) {
        Node* newHead = new Node();
        newHead->data = data;
        newHead->next = *head;
        *head = newHead;
        return;        
    }
    assert(*head != nullptr);
    Node *iter = *head;
    while(--index) iter = iter->next;
    Node* newNode = new Node();
    newNode->data = data;
    newNode->next = iter->next;
    iter->next = newNode;
}

void SortedInsert(Node** head, Node* newNode)
{
    assert(*head != nullptr);
    assert(newNode != nullptr);
    Node *iter = *head;
    // should we insert at head?
    if(iter->data > newNode->data) {
        newNode->next = iter;
        *head = newNode;
        return;
    }
    while(iter->next != nullptr && iter->next->data < newNode->data)
        iter = iter->next;
    newNode->next = iter->next;
    iter->next = newNode;
}

void InsertSort(Node** head)
{
    assert(*head != nullptr);
    Node* next = (*head)->next;
    Node* newHead = new Node();
    newHead->data = (*head)->data;
    newHead->next = nullptr;
    while(next != nullptr) {
        Node* newnode = new Node();
        newnode->next = nullptr;
        newnode->data = next->data;
        SortedInsert(&newHead, newnode);
        delete *head;
        *head = next;
        next = (*head)->next;
    }
    *head = newHead;
}

int GetNth(Node* head, int index)
{
    int size = getSize(head);
    if (size == 0) return -1;
    assert(index >= 0 && index < size);
    Node* iter = head;
    while (index--) iter = iter->next;
    return iter->data;
}

Node* build(int N)
{
    Node* head = new Node();
    head->data = 1;
    head->next = nullptr;
    Node* prev = head;
    for (int i = 2; i <= N; ++i) {
        Node* node = new Node();
        node->data = i;
        node->next = nullptr;
        prev->next = node;
        prev = node;
    }
    return head;
}

int main()
{
    Node *head = nullptr;
    //int N;
    //std::cin >> N;
    //head = build(N);
    InsertNth(&head, 0, 1000);
    InsertNth(&head, 1, 1100);
    InsertNth(&head, 2, 1);
    InsertNth(&head, 3, 20);
    InsertNth(&head, 4, 15);
    InsertNth(&head, 5, 35);
    InsertNth(&head, 6, 125);
    InsertNth(&head, 7, 235);
    InsertNth(&head, 8, -100);
    InsertNth(&head, 9, -10000);
    InsertNth(&head, getSize(head), -1);
    display(head);
    
    //int n;
    //std::cin >> n;
    //Node *newNode = new Node();
    //newNode->data = n;
    //newNode->next = nullptr;
    //SortedInsert(&head, newNode);
    InsertSort(&head);    
    display(head);
    DeleteList(&head);

    //std::cout << "Head: " << Pop(&head) << std::endl;
    //std::cout << "Head: " << Pop(&head) << std::endl;
    //int n;
    //std::cin >> n;
    //head = Push(&head, n);
    //display(head);
    //std::cout << "Head: " << Pop(&head) << std::endl;
    //std::cout << "len: " << getSize(head) << std::endl;
    
    //display(head);
    
    //std::cout << Count(head, n) << std::endl;
    //std::cout << GetNth(head, n) << std::endl;
    //DeleteList(&head);
    //std::cout << getSize(head) << std::endl;
    //assert(getSize(head) == 0);
    return 0;
}
