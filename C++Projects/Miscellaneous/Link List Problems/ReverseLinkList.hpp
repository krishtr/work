// C++Test.cpp : Defines the entry point for the console application.
//

#include <tchar.h>
#include <iostream>
#include <map>

struct node {
        int data;
        node * next;
};

//
//	Display the contents of the list
//	i/p - head of the list
//	returns - void
//
void display(node * head) {

        if (!head) {
                return;
        }

        node * iter = head;
        while (iter) {
                std::cout << iter->data << std::endl;
                iter = iter->next;
        }
}

//
//	Build a link list with number of elements = size
//	Return the head of the list
//
node * Build(int size) {

        if (0 == size) {
                return 0;
        }

        static int index = 0;
        node * temp = new node();
        temp->data = ++index;
        temp->next = Build(size - 1);

        return temp;
}

//
//	Reverse the list
//	i/p - head of the list to be reversed
//	returns - new head of the reversed list 
//
node * reverse(node * ptr) {

        node * temp;
        node * previous = 0;
        while (ptr) {
                temp = ptr->next;
                ptr->next = previous;
                previous = ptr;
                ptr = temp;
        }
        return previous;
}

node * RecursiveReverse (node *root, node *prev = 0, node *next = 0)
{
        if (root == NULL) {
                return root;
        }

        next = root->next;
        root->next = prev;
        prev = root;
        root = next;
        if (root == NULL) {
                return root;
        }
        next = root->next;
        return RecursiveReverse (root, prev, next); 
}

//
//  Get size
//
int GetSize (node *head) {

        if (!head) {
                return 0;
        }

        node *iter = head;
        int index = 0;

        while (iter) {
                index++;
                iter = iter->next;
        }

        return index;
}

//
//  find the nth element from the end of a singly linked list
//
int GetNthElementFromBack(int n, node *head) {

        if (!head || n == 0) {
                return 0;
        }

        if (n == 1) {
                return head->data;
        }

        //  nth element from end is (size - n)th element from start
        int size = GetSize (head);
        node *iter = head;
        int index = 0;

        while (iter) {
                if (index++ == (size - n)) {
                        break;
                }

                iter = iter->next;
        }

        if (!iter) {
                return 0;
        }
        return iter->data;
}

//
// delete node when only address of the node is given
// and no head is given.
//
void DeleteNthNodeWithoutHead (node *loc) {
        if (loc == nullptr) {
                return; // nothing to delete
        }

        if (loc->next == nullptr) {
                std::cout << "Cannot delete the last node" << std::endl;
                return;
        }

        while (loc->next->next != nullptr) {
                loc->data = loc->next->data;
                 loc = loc->next;
        }

        loc->data = loc->next->data;
        delete loc->next;
        loc->next = nullptr;
}

void Traverse (node *root)
{
        if (root == nullptr) {
                return;
        }

        std::cout << root->data << std::endl;
        return Traverse (root->next);
}

void start(int size) {

        // construct a link list
        node * head = Build (size);
        if (head) {
                Traverse (head);
        }

        DeleteNthNodeWithoutHead(head);
        Traverse (head);
}