#include <iostream>

struct node {
        int data;
        node* left;
        node* right;
};

node* NewNode(int data)
{
        node* newNode = new node;
        newNode->data = data;
        newNode->left = nullptr;
        newNode->right = nullptr;

        return newNode;
}

node* insert(node* head, int data)
{
        if (head == nullptr) {
                head = NewNode(data);
                return head;
        }

        if (data <= head->data) {
                head->left = insert(head->left, data);
        } else {
                head->right = insert(head->right, data);
        }

        return head;
}

node *build123a()
{
        // case 1, calling NewNode() 3 times with different pointers
        node* head = NewNode(2);
        node* lPtr = NewNode(1);
        node* rPtr = NewNode(3);

        head->left = lPtr;
        head->right = rPtr;

        return head;
}

node* build123b()
{
        // case 2, calling NewNode() 3 times with same variable
        node* head = NewNode(2);
        head->left = NewNode(1);
        head->right = NewNode(3);
        return head;
}

node* build123c()
{
        // case 3, calling calling insert 3 times
        node* head = insert(nullptr, 2);
        insert(head, 1);
        insert(head, 3);


        return head;
}

void display(node* head)
{
        if (head == nullptr)
                return;

        if (head->left == nullptr && head->right == nullptr) {
                std::cout << head->data << std::endl;
                return;
        }
        std::cout << head->data << std::endl;
        if (head->left != nullptr)
                display(head->left);
        if (head->right != nullptr)
                display(head->right);
        return;
}

int size(node* head)
{
        if (head == nullptr)
                return 0;

        if (head->left == nullptr && head->right == nullptr)
                return 1;

        int count = 1; // count once for the head
        if (head->left != nullptr)
                count += size(head->left);
        if (head->right != nullptr)
                count += size(head->right);

        return count;
}

void Start()
{
}