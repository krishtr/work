#ifndef CLAIMSRESERVE_H_
#define CLAIMSRESERVE_H_

#include <string>
#include <vector>
#include <map>

class ClaimsReserve {
public:
        ClaimsReserve::ClaimsReserve() : originYear_(0), finalYear_(0) {}
        bool Parse(const std::string& path);
        bool Initialise();
        void PopulateIncrementalValues();
        bool WriteToOutput(const std::string& path);
private:
        struct Product {
                int originYear_, finalYear_;
                std::string product_;
                double incremental_value;
        };
        std::vector<Product> productList_;
        std::map<std::string, std::map<int, std::vector<double>>> map_;
        int originYear_, finalYear_;
        int GetNumDevYears() const { return finalYear_ - originYear_ + 1; }
        void Split (std::vector<std::string>& coll, const std::string& str, char delimiter);
        bool SetOriginYear();
        bool SetFinalYear();
        bool ValidateProduct(const Product& prod);
};

#endif