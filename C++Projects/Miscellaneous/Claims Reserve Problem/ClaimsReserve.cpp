#include <iostream>
#include <fstream>
#include <algorithm>
#include "ClaimsReserve.h"

/*
*       Function to initialise our internal collections and values
*/
bool ClaimsReserve::Initialise() {
        std::cout << "Initialising..." << std::endl;
        if (!SetOriginYear()) return false;
        if (!SetFinalYear()) return false;

        for(auto j : productList_) {
                if ( map_.find(j.product_) != std::end(map_)) continue;
                std::map<int, std::vector<double>> tempMap;
                for(int i = 0; i < GetNumDevYears(); ++i) {
                        tempMap[originYear_ + i] = std::vector<double>(GetNumDevYears() - i, 0.0);
                }
                map_[j.product_] = tempMap;
        }

        return true;
}

/*
*       Find the origin claim year
*/
bool ClaimsReserve::SetOriginYear() {
        auto iter = std::min_element(std::begin(productList_), std::end(productList_), 
                                        [&](const Product& p1, const Product& p2) { 
                                                return p1.originYear_ < p2.originYear_; 
                                        });
        if (iter != std::end(productList_)) {
                if (iter->originYear_ > 0) {
                        originYear_ = iter->originYear_;
                        std::cout << "Origin Year: " << originYear_ << std::endl;
                        return true;
                }
        }
        std::cout << originYear_ << std::endl;
        return false;
}

/*
*       Find the final claim year
*/
bool ClaimsReserve::SetFinalYear() {
        auto miter = std::max_element(std::begin(productList_), std::end(productList_), 
                                        [&](const Product& p1, const Product& p2) { 
                                                return p1.finalYear_ < p2.finalYear_;
                                        });
        if (miter != std::end(productList_)) {
                if (miter->finalYear_ > 0) {
                        finalYear_ = miter->finalYear_;
                        std::cout << "Final Year: " << finalYear_ << std::endl;
                        return true;
                }
        }

        std::cout << finalYear_ << std::endl;
        return false;
}

/*
*       Parse our intermediate collection and write the incremental values to our main
*       collection
*/
void ClaimsReserve::PopulateIncrementalValues() {
        for(auto j : productList_) {
                if (!j.product_.empty()) {
                        if (j.originYear_ > 0) {
                                auto yearDiff = j.finalYear_ - j.originYear_;
                                if ((yearDiff >= 0) && (yearDiff <= GetNumDevYears())) {
                                        map_[j.product_][j.originYear_][yearDiff] = j.incremental_value;
                                } else {
                                        std::cout << "Invalid Value found. Product: " << j.product_
                                                << "Origin Year: " << j.originYear_
                                                << "Final Year: " << j.finalYear_
                                                << "Incremental Value" << j.incremental_value;
                                }
                        }
                } else {
                        std::cout << "Empty Product Value. "
                                        << "Origin Year: " << j.originYear_
                                        << "Final Year: " << j.finalYear_
                                        << "Incremental Value" << j.incremental_value;
                }
        }
}

/*
*       Calculate the cumulative claim value for each year and then write to output
*       input - path: full path of the file to write output to
*               NOTE: the file in the given path will be first cleared and then
*               new values will be written.
*/
bool ClaimsReserve::WriteToOutput(const std::string& path) {
        std::ofstream out;
        out.open(path, std::ofstream::trunc);
        if (!out) {
                std::cout << "ERROR opening " << path << " for writing!" << std::endl;
                return false;
        }
        std::cout << "Writing to output..." << std::endl;
        out << originYear_ << ", " << GetNumDevYears() << std::endl;
        for (const auto& mainIter : map_) {
                out << mainIter.first;
                for (const auto& innerIter : mainIter.second) {
                        auto temp = 0.0;
                        for (const auto& vecIter : innerIter.second) {
                                temp += vecIter;
                                out<< "," << temp;
                        }
                }
                out << std::endl;
        }
        std::cout << "Writing to output...SUCCESS!" << std::endl;
        return true;
}

/*
*       Internal function to split a given string based on a given token
*       input - coll: placeholder vector for capturing the values, 
*               str: string to parse
*               token: delimiter token
*/
void ClaimsReserve::Split (std::vector<std::string>& coll, const std::string& str, char token) {
        auto old_pos = 0;
        auto new_pos = str.find_first_of(token);
        while (new_pos != std::string::npos) {
                coll.push_back(str.substr(old_pos, new_pos - old_pos));

                old_pos = new_pos + 1;
                new_pos = str.find_first_of(token, old_pos);
        }
        coll.push_back(str.substr(old_pos, str.length() - old_pos));
}

/*
*       Validate the input product
*       More validations can be added here
*/
bool ClaimsReserve::ValidateProduct(const Product& prod) {
        if (prod.incremental_value < 0) return false;
        if (prod.originYear_ == 0)      return false;
        if (prod.finalYear_ == 0)       return false;
        if (prod.product_.empty())      return false;

        return true;
}

/*
*       Parse the input file
*       input - full path of the file
*       output - success or failure
*/
bool ClaimsReserve::Parse(const std::string& path) {
        std::ifstream ifs;
        ifs.open(path, std::ifstream::in);
        if (!ifs) {
                std::cout << "ERROR opening " << path << " for reading!" << std::endl;
                return false;
        }
        std::string line;
        std::cout << "Parsing input..." << std::endl;
        while (getline(ifs, line) && ifs.good()) {
                std::cout << line << std::endl;
                while (getline(ifs, line)) {
                        std::cout << line << std::endl;
                        std::vector<std::string> lineOutput;
                        Split(lineOutput, line, ',');
                        if (lineOutput.size() != 4)  return false;
                        Product prod;
                        prod.product_ = lineOutput[0];
                        prod.originYear_ = atoi(lineOutput[1].c_str());
                        prod.finalYear_= atoi(lineOutput[2].c_str());
                        prod.incremental_value = atof(lineOutput[3].c_str());
                        if (ValidateProduct(prod))
                                productList_.push_back(prod);
                        else
                                std::cout << "[WARN]: Validation failed for "
                                                << "Product: " << prod.product_
                                                << "Origin Year: " << prod.originYear_
                                                << "Final Year: " << prod.finalYear_
                                                << "Incremental Value" << prod.incremental_value
                                                << std::endl;
                }
        }
        return true;
}