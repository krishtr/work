#ifndef SYNC_H_
#define SYNC_H_

#pragma warning (disable: 4512)

class Critical_Section
{
public:
   Critical_Section()					{ InitializeCriticalSection(&mutex); }
   ~Critical_Section()					{ DeleteCriticalSection(&mutex); }

   void acquire()						{ EnterCriticalSection(&mutex); }
   void release()						{ LeaveCriticalSection(&mutex); }

private:
   CRITICAL_SECTION		mutex;
};

template<class Key> 
class Lock_Guard
{
public:
   Lock_Guard(Key& a_key) : _key(a_key) { lock(); }
   ~Lock_Guard()						{ unlock(); }

   void lock()							{ _key.acquire(); }
   void unlock()						{ _key.release(); }

private:
   Key&					_key;
};

#endif // SYNC_H_