#include <string>
#include <vector>
#include <algorithm>
#include <functional>

struct Record
{
	int login;
	int volume;
	int cmd;
};

Record arr[5];
std::vector<Record> vec;

arr[0].login = 1;
arr[0].volume = 15;
arr[0].cmd = 0;

arr[1].login = 2;
arr[1].volume = 5;
arr[1].cmd = 0;

arr[2].login = 1;
arr[2].volume = 3;
arr[2].cmd = 3;

arr[3].login = 3;
arr[3].volume = 2;
arr[3].cmd = 3;

arr[4].login = 4;
arr[4].volume = 10;
arr[4].cmd = 1;

class filter : std::unary_function<const Record&, bool>
{
public:
	inline bool operator() (Record const & rec) const
	{
		if ( rec.cmd <= 1 )
			return false;

		return true;
	}
};

vec.reserve(sizeof(arr)/sizeof(arr[0]));
std::remove_copy_if( arr, arr+5, std::inserter(vec, vec.begin()), filter() );

struct SortByProfit : public std::binary_function<Record, Record, bool>
{
	inline bool operator() (const Record & left, const Record & right) const { return left.volume < right.volume; }
};

std::sort( vec.begin(), vec.end(), SortByProfit() );
unsigned int index = 0;
while( index < vec.size() )
{
	if ( vec[index].volume > 14 )
		break;

	index++;
}

if ( index == vec.size() )
{
	std::cout << "No Elements in vector that match the condition" << std::endl;
	exit(0);
}

std::cout << vec[index].volume << std::endl;