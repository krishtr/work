#include <iostream>
#include <sstream>
#include <set>

#include "PatternMatcher.h"

using namespace std;

bool PatternMatcher::WildcardCompare(string str1, const string& pattern, bool allowEverything)
{
    bool negation = false;

    if	(str1.find ("!") == 0)
    {
        //	this is a negation, remove the ! symbol
        str1 = str1.substr(str1.find ("!") + 1);
        negation = true;
    }

    //	Check length and if it is too long it cannot fit
    if	(str1.length() > pattern.length())
    {
        if	(negation)
            return true;
        else
            return false;
    }

    if	(str1.find ("*") == str1.length() - 1)
    {
        //	* occuring at the end, so search beginning
        string pat = str1.substr(0, str1.find ("*"));
        unsigned int pos = pattern.find (pat);
        if	(pos != string::npos)
        {
            //	was able to find it
            if	(pos == 0) //	did you find it in the beginning?
            {
                //	yes
                //	Then, we have a match
                return (negation ? false : true);
            }
            else //	no, else where
            {
                if	(allowEverything)
                {
                    return negation ? true : false;
                }
                else
                {
                    //	invalid state, Senseless state
                    return false;
                }
            }
        }
        else
        {
            //	wasnt able to find it
            return allowEverything ? true : false;
        }
    }
    else
    {
        if	(str1.find ("*") == 0)
        {
            //	* occuring at the beginning, so search at the end
            string pat = str1.substr(str1.find ("*") + 1, str1.length());
            unsigned int pos = pattern.find (pat);
            string tempString = pattern.substr(pattern.length() - pat.length(), pat.length());
            if	(pos != string::npos)
            {
                //	some match found
                if	(pat.compare(tempString) == 0)
                {
                    //	was found at the end
                    return negation ? false : true;
                }
                else
                {
                    if	(allowEverything)
                    {
                        return negation ? true : false;
                    }
                    else
                    {
                        //	invalid state, Senseless state
                        return false;
                    }
                }
            }
            else //	no match at all
            {
                return allowEverything ? true : false;
            }
        }
        else
        {
            //	didnt find *
            //	look for an exact match with the pattern
            if	(str1.compare(pattern) == 0)
            {
                return negation ? false : true;
            }
            else
            {
                if	(allowEverything)
                {
                    return negation ? true : false;
                }
                else
                {
                    //	invalid state, Senseless state
                    return false;
                }
            }
        }
    }
}

bool PatternMatcher::CheckBridgeParameters(const string& configString, const string& pattern)
{
    char text[255];
    std::set<std::string> prohibited, allowed;

    std::istringstream is(configString);
    std::string s;

    //	sprintf (text, "CheckBridgeParameters: Checking for %s, %s", configString.c_str(), pattern.c_str());
    //	g_pProcessor->LogInfo(text);

    while (getline(is, s, ','))
    {
        std::istringstream is(s);
        char c;
        if	(is >> c)
        {
            std::set<std::string>& syms = (c == '!') ? (is.putback(c), prohibited) : (is.putback(c), allowed);
            if	(is >> s)
            {
                syms.insert (s);
            }
        }
    }

    if	(!is.eof())
    {
        sprintf (text, "CheckBridgeParameters: Parsing failed for %s, %s", configString.c_str(), pattern.c_str());
        std::cout << text << std::endl;
    }

    const bool allowEverything = allowed.find ("*") != allowed.end();

    //	search the prohibited list first
    set<string>::iterator end(prohibited.end());
    bool prohibittedResult = false;

    for (set<string>::iterator begin = prohibited.begin(); begin != end; ++begin)
    {
        if	(!WildcardCompare(*begin, pattern, allowEverything))
        {
            //	found a mismatch. No point in searching forward. Break here
            prohibittedResult = false;
            break;
        }
        else
        {
            //	all the ones in this list are allowed
            prohibittedResult = true;
        }
    }

    if	(allowEverything)
    {
        if	( ! prohibited.empty() )
        {
            return prohibittedResult;
        }
        else
        {
            return allowEverything;
        }
    }
    else
    {
        //	we have to browse the allowed collection as well and then take a decision
        set<string>::iterator end(allowed.end());
        bool allowedResult = false;

        for (set<string>::iterator begin = allowed.begin(); begin != end; ++begin)
        {
            if	(WildcardCompare(*begin, pattern, allowEverything))
            {
                //	found a match. No point in searching forward. Break here
                allowedResult = true;
                break;
            }
            else
            {
                //	found a mismatch...search forward. We may find it in the end...
                allowedResult = false;
            }
        }

        if	( ! prohibited.empty() )
        {
            //	sprintf (text, "CheckBridgeParameters: returning %d", prohibittedResult || allowedResult);
            //	g_pProcessor->LogInfo(text);
            return prohibittedResult || allowedResult;
        }
        else
        {
            //	sprintf (text, "CheckBridgeParameters: returning %d", allowedResult);
            //	g_pProcessor->LogInfo(text);
            return allowedResult;
        }
    }
}

void PatternMatcher::PatternTest()
{
    char result;
    std::cout << "P A T T E R N   M A T C H E R" << std::endl;
    std::cout << "-----------------------------" << std::endl;

    do
    {
        std::cout << "Enter a Compare String: ";
        std::string compare_string;
        std::cin >> compare_string;

        std::cout << std::endl;
        std::cout << "Enter a pattern to match: ";
        std::string pattern;
        std::cin >> pattern;

        std::cout << std::endl;
        std::cout << std::endl;

        std::cout << "Result: ";
        std::cout << (PatternMatcher::CheckBridgeParameters(compare_string, pattern) ? "PASS" : "FAIL") << std::endl;
        std::cout << std::endl;
        std::cout << "Continue? (Y/N): ";
        std::cin >> result;
        std::cout << std::endl;
    }
    while (result != 'N' && result != 'n');
}