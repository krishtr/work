#ifndef	SINGLETONINTERFACE_H_
#define SINGLETONINTERFACE_H_

#include "Sync.h"

/// Singleton Interface class

template<class T> struct SingletonMeyersStyle
{
	static T* Create()
	{
		static T instance_;
		return &instance_;
	}
};

template<class T, template<class> class CreationPolicy=SingletonMeyersStyle>
class SingletonInterface
{
public:
	static T& getInstance();
private:
	SingletonInterface() {}
	~SingletonInterface() {}
	SingletonInterface(const SingletonInterface &);
	SingletonInterface & operator=(const SingletonInterface &);

	static T* instance_;
	static Critical_Section lock_;
};

/* Definitions */
template<class T, template<class> class CreationPolicy>
Critical_Section SingletonInterface<T, CreationPolicy>::lock_;

template<class T, template<class> class C>
T* SingletonInterface<T,C>::instance_ = NULL;

template<class T, template<class> class CreationPolicy>
T& SingletonInterface<T,CreationPolicy>::getInstance()
{
	if ( ! instance_ )
	{
		Lock_Guard<Critical_Section> gate(lock_);
		if ( ! instance_ )
		{
			instance_ = CreationPolicy<T>::Create();
		}
	}

	return *instance_;
}

#endif	// SINGLETONINTERFACE_H_