#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>

template<class Input1, class Input2, class Output>
void my_merge( Input1 start1, Input1 end1,
			Input2 start2, Input2 end2,
			Output out)
{
	for (; start1 != end1; ++out) {
		if (start2 == end2) {
			std::copy(start1, end1, out);
			return;
		}
		
		if (*start1 < *start2) {
			*out = *start1;
			++start1;
		} else {
			*out = *start2;
			++start2;
		}
	}
	std::copy(start2, end2, out);
}

void myMerge(int *A, int begin, int mid, int end, int *B)
{
    int i = begin, j = mid;
    for (int k = begin; k < end; ++k) {
        if (i < mid && (A[i] < A[j] || j >= end))
            B[k] = A[i++];
        else
            B[k] = A[j++];
    }
}

void SplitAndMerge(int *A, int first, int last, int *B)
{
    if (last - first < 2) return;
    auto mid = (first + last) / 2;
    SplitAndMerge(B, first, mid, A);
    SplitAndMerge(B, mid, last, A);
    myMerge(A, first, mid, last, B);
}

// sort A of size n into B
void mergeSort(int *A, int n, int *B)
{
    std::copy(A, A+n, B);
    SplitAndMerge(B, 0, n, A);
}

int main() {
    int A[] = {2,7,1,6,3,9,11,89,33,44,23};
    const int size = sizeof(A) / sizeof(A[0]);
    int result[size] = {0};
    mergeSort(A, size, result);
	std::copy(std::begin(result), std::end(result),
		std::ostream_iterator<int>(std::cout, ","));
	return 0;
}
