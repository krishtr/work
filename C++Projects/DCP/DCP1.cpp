#include <iostream>
#include <algorithm>
#include <vector>
#include <unordered_set>

// given: list of numbers, an int k
// return: true/false if any *two* numbers in the list
// add to k
bool isSumExist( const std::vector<int>& vec, int k )
{
    if( vec.size() <= 1 ) return false;
    std::unordered_set<int> lookup;
    for( int i : vec ) {
        if( lookup.find(k-i) != lookup.end() )
            return true;
        lookup.emplace(i);
    }
    return false;
}

int main() {
    std::vector<int> vec { 1,0,8,6,7,9 };
    int k = 0;
    std::cin >> k;
    std::cout << std::boolalpha << isSumExist(vec, k) << '\n';
    return 0;
}
