#include <iostream>
#include <algorithm>
#include <vector>
#include <unordered_set>
#include <iterator>

// given: a vector of numbers
// return: another vector where the entry at index i is the product
// of all numbers in given vector *except* at index i.
std::vector<int> getProductVector( const std::vector<int>& input )
{
    if( input.size() <= 1 ) return input;
    long long product = 1;
    std::vector<int> result;
    if( std::any_of(input.begin(), input.end(), [](int i) { return i == 0; }) ) {
        product = 0;
        for( int i : input )
            if( i != 0 )
                product *= i;
        result.resize(input.size());
        for( int i = 0; i < input.size(); ++i ) {
            if(input[i] == 0) result[i] = product;
            else result[i] = 0;
        }
        return result;
    }
    for( int i : input )
        product *= i;
    std::transform( input.begin(), input.end(), std::back_inserter(result),
                [&](int i) { return product / i; } );
    return result;
}


int main() {
    std::vector<int> vec { 1,0,8,6,7,9 };
    std::copy( vec.begin(), vec.end(), std::ostream_iterator<int>(std::cout, ",") );
    std::cout << '\n';
    auto result = getProductVector( vec );
    std::copy( result.begin(), result.end(), std::ostream_iterator<int>(std::cout, ",") );
    std::cout << '\n';

    return 0;
}
