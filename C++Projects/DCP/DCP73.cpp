#include <iostream>

template<typename T>
class LinkList
{
    public:
        LinkList();
        ~LinkList() = default;
        T front() const;
        T back() const;
        bool empty() const;
        size_t size() const;
        void clear();
        void push_back( const T& value );
        void push_front( const T& value );
        void pop_back();
        void pop_front();
        void display() const;
        void reverse();
    private:
        struct Node {
            T data;
            Node *next;
        };
        Node *buildNode();
        size_t m_size;
        Node *head, *tail;
};

template<typename T>
LinkList<T>::LinkList() : m_size(0), head(nullptr), tail(nullptr) {}

template<typename T>
T LinkList<T>::front() const
{
    if( !head ) throw std::runtime_error( "List is empty!" );
    return head->data;
}

template<typename T>
T LinkList<T>::back() const
{
    if( !head ) throw std::runtime_error( "List is empty" );
    return tail->data;
}

template<typename T>
bool LinkList<T>::empty() const
{
    return !head;
}

template<typename T>
size_t LinkList<T>::size() const { return m_size; }

template<typename T>
void LinkList<T>::clear()
{
    if( empty() ) return;
    Node *prev = head, *curr = head->next;
    while( curr != nullptr ) {
        delete prev;
        --m_size;
        prev = curr;
        curr = curr->next;
    }
    delete prev;
    --m_size;
}

template<typename T>
typename LinkList<T>::Node * LinkList<T>::buildNode()
{
    LinkList<T>::Node *node = new LinkList<T>::Node;
    node->next = nullptr;
    return node;
}

template<typename T>
void LinkList<T>::push_back( const T& value )
{
    if( empty() ) {
        head = buildNode();
        tail = head;
        head->data = value;
        ++m_size;
        return;
    }
    Node *temp = buildNode();
    temp->data = value;
    tail->next = temp;
    tail = tail->next;
    ++m_size;
}

template<typename T>
void LinkList<T>::push_front( const T& value )
{
    if( empty() ) {
        head = buildNode();
        tail = head;
        head->data = value;
        ++m_size;
        return;
    }
    Node *temp = buildNode();
    temp->data = value;
    temp->next = head;
    head = temp;
    ++m_size;
}

template<typename T>
void LinkList<T>::pop_back()
{
    if( empty() ) throw std::runtime_error( "List is empty" );
    Node *temp = head;
    while( temp->next != tail ) temp = temp->next;
    delete temp->next;
    temp->next = nullptr;
    tail = temp;
    --m_size;
}

template<typename T>
void LinkList<T>::pop_front()
{
    if( empty() ) throw std::runtime_error( "List is empty" );
    Node *temp = head;
    head = temp->next;
    temp->next = nullptr;
    delete temp;
    --m_size;
}

template<typename T>
void LinkList<T>::display() const
{
    if( empty() ) return;
    Node *temp = head;
    while( temp != nullptr ) {
        std::cout << temp->data;
        if( temp->next != nullptr )
            std::cout << ", ";
        temp = temp->next;
    }
    std::cout << '\n';
}

template<typename T>
void test( const LinkList<T>& list )
{
    list.display();
    std::cout << list.size() << '\n';
    std::cout << std::boolalpha << list.empty() << '\n';
    std::cout << list.front() << '\n';
    std::cout << list.back() << '\n';
    std::cout << '\n';
}

template<typename T>
void LinkList<T>::reverse()
{
    if( empty() || head == tail ) return;
    Node *prev = nullptr, *curr = head, *next = head->next;
    while( next != nullptr ) {
        curr->next = prev;
        prev = curr;
        curr = next;
        next = curr->next;
    }
    curr->next = prev;
    std::swap(head, tail);
}

int main()
{
    LinkList<int> list;
    list.push_front(1);
    list.push_front(2);
    list.push_front(3);
    list.push_front(4);
    list.push_front(5);
    list.push_front(6);
    list.push_front(7);
    test(list);
    list.reverse();
    test(list);

    return 0;
}
