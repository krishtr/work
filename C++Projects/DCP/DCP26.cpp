#include <iostream>
#include <memory>
#include<cassert>

template<class T>
class LinkList
{
    public:
        struct Node {
            T data;
            std::shared_ptr<Node> next;
            Node(T data) : data(data), next(nullptr) {}
        };

        LinkList(): _head(nullptr), _tail(nullptr), _sz(0) {}
        std::weak_ptr<Node> append(T data);
        std::weak_ptr<Node> deleteKNode(size_t k);
        std::weak_ptr<Node> deleteKNodeFromEnd(size_t k);
        std::weak_ptr<Node> deleteHead();
        std::weak_ptr<Node> deleteTail();
        std::weak_ptr<Node> head() const { return _head; }
        std::weak_ptr<Node> tail() const { return _tail; }
        void print() const;
        size_t size() const { return _sz; }
    private:
        std::shared_ptr<Node>_head, _tail;
        size_t _sz;
};

template<class T>
std::weak_ptr<typename LinkList<T>::Node> LinkList<T>::append(T data)
{
    if (!_head) {
        _head = std::make_shared<LinkList<T>::Node>(data);
        _tail = _head;
        _sz = 1;
        return _head;
    }

    auto newNode = std::make_shared<LinkList<T>::Node>(data);
    _tail->next = newNode;
    _tail = newNode;
    _sz += 1;
    return _head;
}

// given k, delete kth Node from front
template<class T>
std::weak_ptr<typename LinkList<T>::Node> LinkList<T>::deleteKNode(size_t k)
{
    if (k < 0 || k > _sz) throw std::runtime_error("invalid index");
    if (k == 0) return _head;
    if (k == 1) return deleteHead();
    if (k == _sz) return deleteTail();
    std::shared_ptr<typename LinkList<T>::Node> iter, prev;
    prev = _head;
    iter = prev->next;
    k -= 2;
    while (k--) {
        prev = iter;
        iter = prev->next;
    }
    prev->next = iter->next;
    _sz -= 1;
    return _head;
}

template<class T>
std::weak_ptr<typename LinkList<T>::Node> LinkList<T>::deleteKNodeFromEnd(size_t k)
{
    return deleteKNode(_sz - k + 1);
}

template<class T>
std::weak_ptr<typename LinkList<T>::Node> LinkList<T>::deleteHead()
{
    if (!_head) throw std::runtime_error("empty list");
    if (_sz == 1) { --_sz; _head = _tail = nullptr; return _head; }
    _head = _head->next;
    --_sz;
    return _head;
}

template<class T>
std::weak_ptr<typename LinkList<T>::Node> LinkList<T>::deleteTail()
{
    if (!_head) throw std::runtime_error("empty list");
    if (_sz == 1) { --_sz; _head = _tail = nullptr; return _head; }
    std::shared_ptr<typename LinkList<T>::Node> iter = _head;
    while (iter->next != _tail) iter = iter->next;
    iter->next = nullptr;
    _tail = iter;
    --_sz;
    return _head;
}

template<class T>
void LinkList<T>::print() const
{
    if (!_head) return;
    auto temp = _head;
    while (temp->next != nullptr ) {
        std::cout << temp->data << "->";
        temp = temp->next;
    }
    std::cout << temp->data;
    std::cout << '\n';
}

int main() {
    LinkList<int> list;
    for (int i = 1; i <= 5; ++i )
        list.append(i);
    assert(list.size() ==  5);
    assert(list.head().lock()->data == 1);
    assert(list.tail().lock()->data == 5);
    list.deleteHead();
    assert(list.size() == 4);
    assert(list.head().lock()->data == 2);
    assert(list.tail().lock()->data == 5);

    list.deleteTail();
    assert(list.size() == 3);
    assert(list.head().lock()->data == 2);
    assert(list.tail().lock()->data == 4);

    list.append(5);
    list.append(6);
    assert(list.size() == 5);
    assert(list.head().lock()->data == 2);
    assert(list.tail().lock()->data == 6);

    list.deleteKNode(3);
    assert(list.size() == 4);
    assert(list.head().lock()->data == 2);
    assert(list.tail().lock()->data == 6);
    
    list.deleteKNode(4);
    assert(list.size() == 3);
    assert(list.head().lock()->data == 2);
    assert(list.tail().lock()->data == 5);
    
    list.append(7);
    list.append(8);
    list.deleteKNodeFromEnd(5);
    assert(list.size() == 4);
    assert(list.head().lock()->data == 3);
    assert(list.tail().lock()->data == 8);

    list.print();

    return 0;
}
