#include <iostream>
#include <algorithm>
#include <vector>
#include <iterator>
#include <memory>
#include <boost/string/split.hpp>

namespace
{
    std::vector<std::string> parse(const std::string& str)
    {
        std::vector<std::string> results;
        boost::split(results, str, boost::any_of{','});
        return results;
    }
}

// given: a root to a binary tree
// implement: serialize(root) -> tree to string, deserialize(s) -> str back to tree
//

struct Node {
    std::string data;
    std::unique_ptr<Node> left, right;
    Node(const std::string& i, left = nullptr, right = nullptr) 
        : data(i), left(left), right(right) {}
};

void buildTree( std::unique_ptr<Node>& head, const std::string& i )
{
    if( !head ) {
        head = std::make_unique<Node>(i);
        return;
    }
    if( i >= head->data )
        return buildTree(head->right, i);

    return buildTree(head->left, i);
}

void inorderTraverse( std::unique_ptr<Node>& head,
    std::ostream& ss )
{
    if( !head ) return;
    inorderTraverse(head->left);
    ss << head->data << ',';
    inorderTraverse(head->right);
}

std::string serialize(std::unique_ptr<Node>& head)
{
    std::stringstream ss;
    inorderTraverse(head, ss);
    return ss.str();
}

void deserialize(const std::string& str)
{
    std::vector<std::string> nums = parse(str);


}

int main() {
    std::vector<int> vec { 1,0,8,6,7,9 };
    std::copy( vec.begin(), vec.end(), std::ostream_iterator<int>(std::cout, ",") );
    std::cout << '\n';
    auto result = getProductVector( vec );
    std::copy( result.begin(), result.end(), std::ostream_iterator<int>(std::cout, ",") );
    std::cout << '\n';

    return 0;
}
