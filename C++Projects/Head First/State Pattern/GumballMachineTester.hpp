#ifndef GUMBALL_TESTER_H_
#define GUMBALL_TESTER_H_

#include "GumballMachine.h"

class GumballMachineTester
{
public:
    static void run();
};

void GumballMachineTester::run()
{
    GumballMachine machine;
    machine.start(5);

    //  print state
    std::cout << std::endl << machine << std::endl;

    //  test case 1
    std::cout << "You inserted a quarter" << std::endl;
    machine.insertQuarter ();
    std::cout << "You turned the crank" << std::endl;
    machine.turnCrank ();

    //  print state
    std::cout << std::endl << machine << std::endl;

    //  test case 2
    std::cout << "You inserted a quarter" << std::endl;
    machine.insertQuarter ();
    std::cout << "You ejected the quarter" << std::endl;
    machine.ejectQuarter ();
    std::cout << "You turned the crank" << std::endl;
    machine.turnCrank ();

    //  print state
    std::cout << std::endl << machine << std::endl;

    //  test case 3
    std::cout << "You inserted a quarter" << std::endl;
    machine.insertQuarter ();
    std::cout << "You turned the crank" << std::endl;
    machine.turnCrank ();
    std::cout << "You inserted a quarter" << std::endl;
    machine.insertQuarter ();
    std::cout << "You turned the crank" << std::endl;
    machine.turnCrank ();
    std::cout << "You ejected the quarter" << std::endl;
    machine.ejectQuarter ();

    //  print state
    std::cout << std::endl << machine << std::endl;

    //  test case 4
    std::cout << "You inserted a quarter" << std::endl;
    machine.insertQuarter ();
    std::cout << "You inserted a quarter" << std::endl;
    machine.insertQuarter ();
    std::cout << "You turned the crank" << std::endl;
    machine.turnCrank ();
    std::cout << "You inserted a quarter" << std::endl;
    machine.insertQuarter ();
    std::cout << "You turned the crank" << std::endl;
    machine.turnCrank ();
    std::cout << "You inserted a quarter" << std::endl;
    machine.insertQuarter ();
    std::cout << "You turned the crank" << std::endl;
    machine.turnCrank ();

    std::cout << std::endl;
    std::cout << "****REFILLING****" << std::endl;

    //  refill
    machine.refill (10);

    std::cout << std::endl;

    //  print state
    std::cout << std::endl << machine << std::endl;

    //  test case 1
    std::cout << "You inserted a quarter" << std::endl;
    machine.insertQuarter ();
    std::cout << "You turned the crank" << std::endl;
    machine.turnCrank ();

    //  print state
    std::cout << std::endl << machine << std::endl;

    //  test case 2
    std::cout << "You inserted a quarter" << std::endl;
    machine.insertQuarter ();
    std::cout << "You ejected the quarter" << std::endl;
    machine.ejectQuarter ();
    std::cout << "You turned the crank" << std::endl;
    machine.turnCrank ();

    //  print state
    std::cout << std::endl << machine << std::endl;

    //  test case 3
    std::cout << "You inserted a quarter" << std::endl;
    machine.insertQuarter ();
    std::cout << "You turned the crank" << std::endl;
    machine.turnCrank ();
    std::cout << "You inserted a quarter" << std::endl;
    machine.insertQuarter ();
    std::cout << "You turned the crank" << std::endl;
    machine.turnCrank ();
    std::cout << "You ejected the quarter" << std::endl;
    machine.ejectQuarter ();

    //  print state
    std::cout << std::endl << machine << std::endl;

    //  test case 4
    std::cout << "You inserted a quarter" << std::endl;
    machine.insertQuarter ();
    std::cout << "You inserted a quarter" << std::endl;
    machine.insertQuarter ();
    std::cout << "You turned the crank" << std::endl;
    machine.turnCrank ();
    std::cout << "You inserted a quarter" << std::endl;
    machine.insertQuarter ();
    std::cout << "You turned the crank" << std::endl;
    machine.turnCrank ();
    std::cout << "You inserted a quarter" << std::endl;
    machine.insertQuarter ();
    std::cout << "You turned the crank" << std::endl;
    machine.turnCrank ();

    //  print state
    std::cout << std::endl << machine << std::endl;
}

#endif