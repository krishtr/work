#ifndef GUMBALLMACHINE_H_
#define GUMBALLMACHINE_H_

#include <memory>
#include "State.h"

class GumballMachine
{
public:
    GumballMachine();
    ~GumballMachine();
    void start (int count);
    void refill (int count);

    //
    //  Set State types
    //
    void SetNoQuarterState ()   { statePtr_ = noQuarterPtr_; }
    void SetHasQuarterState()   { statePtr_ = hasQuarterPtr_; }
    void SetWinnerState()       { statePtr_ = winnerPtr_; }
    void SetSoldState()         { statePtr_ = soldPtr_; }
    void SetSoldOutState()      { statePtr_ = soldOutPtr_; }

    //
    //  State functions
    //
    void turnCrank()            { statePtr_->turnCrank (); }
    void insertQuarter()        { statePtr_->insertQuarter (); }
    void ejectQuarter()         { statePtr_->ejectQuarter (); }

    //
    //  Access functions
    //
    int GetGumballCount() const     { return gumballCount_; }
    void DecreaseCount(int count)   { gumballCount_ -= count; }
    void SetCount (int value)       { gumballCount_ = value; }

    //
    //  display
    //
    friend std::ostream& operator << (std::ostream& os, const GumballMachine& machine);

private:
    GumballMachine (const GumballMachine&);
    GumballMachine& operator = (const GumballMachine&);

    State* noQuarterPtr_;
    State* hasQuarterPtr_;
    State* winnerPtr_;
    State* soldPtr_;
    State* soldOutPtr_;

    int gumballCount_;
    State* statePtr_;
};

#endif // GUMBALLMACHINE_H_
