#ifndef SOLDSTATE_H_
#define SOLDSTATE_H_

#include "State.h"
#include "GumballMachine.h"

class SoldState : public State
{
public:
    SoldState (GumballMachine* machine) { appPtr_ = machine; }
private:
    SoldState();
    SoldState(const SoldState&);
    SoldState& operator=(const SoldState&);

    GumballMachine* appPtr_;
    void turnCrank ()
    {
        std::cout << "Dispensing gum ball..." << std::endl;
        appPtr_->DecreaseCount (1);
        if  (appPtr_->GetGumballCount () <= 0)
        {
            appPtr_->SetCount(0);
            appPtr_->SetSoldOutState();
        }
        else
            appPtr_->SetNoQuarterState();

    }
};

#endif