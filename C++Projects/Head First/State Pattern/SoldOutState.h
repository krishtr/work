#ifndef SoldOutState_H_
#define SoldOutState_H_

#include "State.h"
#include "GumballMachine.h"

class SoldOutState : public State
{
public:
    SoldOutState (GumballMachine* machine) { appPtr_ = machine; }
private:
    SoldOutState();
    SoldOutState(const SoldOutState&);
    SoldOutState& operator=(const SoldOutState&);

    GumballMachine* appPtr_;
    void insertQuarter() 
    { 
        std::cout << "We are out of gum balls! Sorry!" << std::endl;
        appPtr_->SetHasQuarterState ();
        appPtr_->ejectQuarter ();
    }
    void ejectQuarter () { std::cout << "No Quarter to eject!" << std::endl; }
    void turnCrank () { std::cout << "We are out of gum balls! Sorry!" << std::endl; }
    void display() { std::cout <<  "Machine is sold out!" << std::endl; }
};

#endif