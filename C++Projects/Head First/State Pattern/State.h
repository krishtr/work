#ifndef STATE_H_
#define STATE_H_

#include <iostream>

//  interface State class
class State
{
public:
    virtual void insertQuarter ()   { std::cout << "Busy with previous transaction. Please wait!" << std::endl; }
    virtual void ejectQuarter ()    { std::cout << "Sorry, you already turned the crank" << std::endl; }
    virtual void turnCrank()        = 0;
    virtual void display()          { std::cout << "Machine is waiting for a quarter" << std::endl; }   //  default behavior
};

#endif