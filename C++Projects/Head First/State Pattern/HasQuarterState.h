#ifndef HasQuarterState_H_
#define HasQuarterState_H_

#pragma warning (disable: 4244)

#include <time.h>
#include "State.h"
#include "GumballMachine.h"

class HasQuarterState : public State
{
public:
    HasQuarterState (GumballMachine* machine) { appPtr_ = machine; }
private:
    HasQuarterState();
    HasQuarterState(const HasQuarterState&);
    HasQuarterState& operator=(const HasQuarterState&);

    GumballMachine* appPtr_;
    int count;
    void insertQuarter () { std::cout << "Machine already has a quarter!" << std::endl; }
    void dispense () { std::cout << "Turn the crank to get your gum ball" << std::endl; }
    void ejectQuarter ()
    {
        std::cout << "Quarter returned" << std::endl;
        appPtr_->SetNoQuarterState();
    }
    void turnCrank ()
    {
        if  (appPtr_->GetGumballCount () <= 0)
            appPtr_->SetSoldOutState ();
        else
        {
            srand (time(NULL) + count++);
            int guess = rand() % 10;
            if  (guess == 3)
                appPtr_->SetWinnerState();
            else
                appPtr_->SetSoldState();
        }

        appPtr_->turnCrank ();  //  delegate to implementation of corresponding state
    }
};

#endif HasQuarterState_H_