#ifndef WinnerState_H_
#define WinnerState_H_

#include "State.h"
#include "GumballMachine.h"

class WinnerState : public State
{
public:
    WinnerState (GumballMachine* machine) { appPtr_ = machine; }
private:
    WinnerState();
    WinnerState(const WinnerState&);
    WinnerState& operator=(const WinnerState&);

    GumballMachine* appPtr_;
    void turnCrank () 
    {
        std::cout << "Congratulations! You are a winner!";
        auto flag = appPtr_->GetGumballCount () < 2;
        if  (flag)
            std::cout << " Sorry, 2 gum balls is not available! ";
        
        std::cout << " Dispensing " << (flag ? appPtr_->GetGumballCount () : 2) << " gum ball(s)" << std::endl;

        appPtr_->DecreaseCount (2);
        if  (appPtr_->GetGumballCount () < 0)
        {
            appPtr_->SetCount (0);
            appPtr_->SetSoldOutState();
        }
        else
            appPtr_->SetNoQuarterState();
    }
};

#endif