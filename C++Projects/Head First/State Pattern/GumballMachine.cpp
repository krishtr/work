#include <iostream>
#include "GumballMachine.h"
#include "NoQuarterState.h"
#include "SoldOutState.h"
#include "WinnerState.h"
#include "HasQuarterState.h"
#include "SoldState.h"

GumballMachine::GumballMachine()
    : noQuarterPtr_(0)
    , hasQuarterPtr_(0)
    , winnerPtr_(0)
    , soldPtr_(0)
    , soldOutPtr_(0)
    , gumballCount_(0)
    , statePtr_(0)
{}

GumballMachine::~GumballMachine()
{
    if  (noQuarterPtr_)
    {
        delete noQuarterPtr_;
        noQuarterPtr_ = 0;
    }
    if  (winnerPtr_)
    {
        delete winnerPtr_;
        winnerPtr_ = 0;
    }
    if  (hasQuarterPtr_)
    {
        delete hasQuarterPtr_;
        hasQuarterPtr_ = 0;
    }
    if  (soldPtr_)
    {
        delete soldPtr_;
        soldPtr_ = 0;
    }
    if  (soldOutPtr_)
    {
        delete soldOutPtr_;
        soldOutPtr_ = 0;
    }
    if  (statePtr_) //  have not new'd it, so cannot call delete on it
        statePtr_ = 0;
}

void GumballMachine::start (int count)
{
    noQuarterPtr_   = new NoQuarterState(this);
    winnerPtr_      = new WinnerState(this);
    hasQuarterPtr_  = new HasQuarterState(this);
    soldPtr_        = new SoldState(this);
    soldOutPtr_     = new SoldOutState(this);

    gumballCount_ = count;
    if  (count > 0)
        statePtr_ = noQuarterPtr_;
    else
        statePtr_ = soldOutPtr_;
}

void GumballMachine::refill (int count)
{
    if (count <= 0)
    {
        std::cout << "Invalid number to refill" << std::endl;
        return;
    }

    gumballCount_ += count;
    statePtr_ = noQuarterPtr_;
    std::cout << "Refilled successfully" << std::endl;
}

std::ostream& operator << (std::ostream& os, const GumballMachine& machine)
{
    os << "Inventory: " << machine.gumballCount_ << " gum balls" << std::endl;
    machine.statePtr_->display();

    return os;
}
