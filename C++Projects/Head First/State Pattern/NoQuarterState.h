#ifndef NoQuarterState_H_
#define NoQuarterState_H_

#include "State.h"
#include "GumballMachine.h"

class NoQuarterState : public State
{
public:
    NoQuarterState (GumballMachine* machine) { appPtr_ = machine; }
private:
    NoQuarterState();
    NoQuarterState(const NoQuarterState&);
    NoQuarterState& operator=(const NoQuarterState&);

    GumballMachine* appPtr_;
    void ejectQuarter () { std::cout << "No Quarter to eject!" << std::endl; }
    void turnCrank () { std::cout << "Deposit quarter to get gum ball!" << std::endl; }
    void insertQuarter ()
    {
        std::cout << "Turn the crank to get your gum ball." << std::endl;
        appPtr_->SetHasQuarterState ();
    }
};

#endif NoQuarterState_H_