#include <iostream>
#include <map>
#include <vector>
#include <unordered_set>
#include <algorithm>
#include <string>

struct Order
{
    std::string userId;
    double w,p;
    enum ACTION {SELL, BUY} a;
    Order(const std::string& u = "", double w = 0.0, double p = 0.0, ACTION a = SELL)
        : userId(u), w(w), p(p), a(a) {}
};

struct Object
{
    double net_w;
    std::vector<Order> orders;
    Object() : net_w(0.0) {}
};

// price -> qty
std::map<double, Object> sell_orders;
std::map<double, Object, std::greater<double>> buy_orders;

void addOrder(const Order& o)
{
    if(o.a == Order::SELL) {
        auto iter = sell_orders.find(o.p);
        if(iter == sell_orders.end())
            iter = sell_orders.insert(std::make_pair(o.p, Object())).first;
        iter->second.net_w += -o.w;
        iter->second.orders.push_back(o);
    } else {
        auto iter = buy_orders.find(o.p);
        if(iter == buy_orders.end())
            iter = buy_orders.insert(std::make_pair(o.p, Object())).first;
        iter->second.net_w += o.w;
        iter->second.orders.push_back(o);
    }
}

void removeOrder(const Order& o)
{
    if(o.a == Order::SELL) {
        auto iter = sell_orders.find(o.p);
        if(iter == sell_orders.end()) {
            std::cout << "Order with price: " 
                      << o.p << " not found in the sell order book" 
                      << std::endl;
            return;
        }
        auto find_iter = std::find_if(iter->second.orders.begin(), iter->second.orders.end(),
            [&](const Order& O) {
                return O.p == o.p && O.w == o.w;
            });
        if(find_iter == iter->second.orders.end()) {
            std::cerr << "The right order (SELL, w: " << o.w << "kg, price: £" << o.p
                      << ") was not found to remove" << std::endl;
            return;
        }
        iter->second.orders.erase(find_iter);
        iter->second.net_w += o.w;
    } else {
        auto iter = buy_orders.find(o.p);
        if(iter == buy_orders.end()) {
            std::cout << "Order with price: "
                      << o.p << " not found in the buy order book"
                      << std::endl;
            return;
        }
        auto find_iter = std::find_if(iter->second.orders.begin(), iter->second.orders.end(),
            [&](const Order& O) {
                return O.p == o.p && O.w == o.w;
            });
        if(find_iter == iter->second.orders.end()) {
            std::cerr << "The right order (BUY, w: " << o.w << "kg, price: £" << o.p
                      << ") was not found to remove" << std::endl;
            return;
        }
        iter->second.orders.erase(find_iter);
        iter->second.net_w += -o.w;
    }
}

void displayOrder()
{
    std::cout << "Live SELL Orders" << std::endl;
    for(auto p : sell_orders)
        std::cout << p.second.net_w << "kg for £" << p.first << std::endl;
    std::cout << std::endl;
    std::cout << "Live BUY Orders" << std::endl;
    for(auto p : buy_orders)
        std::cout << p.second.net_w << "kg for £" << p.first << std::endl;
}

int main()
{
    addOrder(Order("1",3.5,306));
    addOrder(Order("2",1.2,310));
    addOrder(Order("3",1.5,307));
    addOrder(Order("4",2.0,306));
    addOrder(Order("1",3.5,306,Order::BUY));
    addOrder(Order("2",1.2,310,Order::BUY));
    addOrder(Order("3",1.5,307,Order::BUY));
    addOrder(Order("4",2.0,306,Order::BUY));
    displayOrder();

    removeOrder(Order("1",3.5,306));
    removeOrder(Order("1",3.4,306));
    
    std::cout << std::endl;
    displayOrder();
    return 0;
}

