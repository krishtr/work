#include <iostream>
#include <vector>

void MaxSubArray()
{
    int T;
    std::cin >> T;
    while (T--) {
        int N;
        std::cin >> N;
        std::vector<int> vec(N,0);
        for (int i = 0; i < N; ++i)
            std::cin >> vec[i];
        int max, cmax, nc_max;
        max = cmax = nc_max  = vec[0];
        for (int i = 1; i < (int)vec.size(); ++i) {
            max = std::max(vec[i] + max, vec[i]);
            cmax = std::max(cmax, max);
            nc_max = std::max(nc_max, std::max(vec[i],vec[i]+nc_max));
        }
        std::cout << cmax << " " << nc_max << std::endl;
    }
}

int main()
{
    MaxSubArray();
    return 0;
}
