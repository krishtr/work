#include <iostream>
#include <string>
#include <algorithm>

void BiggerIsBetter()
{
    int N;
    std::cin >> N;
    while(N--) {
        std::string str;
        std::cin >> str;
        std::string copy(str);
        if (!std::next_permutation(str.begin(), str.end()))
            std::cout << "no answer" << std::endl;
        else
            std::cout << str << std::endl;
    }
}

int main()
{
    BiggerIsBetter();
    return 0;
}
