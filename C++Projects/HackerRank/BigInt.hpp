#include <string>
#include <sstream>
#include <algorithm>
#include <iterator>

class BigInt
{
public:
    explicit BigInt(int n);
    BigInt& operator*=(int n);
    BigInt& operator+=(const BigInt& rhs);
    const BigInt operator+(const BigInt& rhs);
    const BigInt operator*(const BigInt& rhs);
    int operator[](int i) const { return num[i]; }
    void swap(BigInt rhs) { num.swap(rhs.getNum()); }
    void display() const { std::copy(num.rbegin(), num.rend(), std::ostream_iterator<int>(std::cout)); }
    std::vector<int>& getNum() { return num; }
    int size() const { return num.size(); }
private:
    BigInt();
    std::vector<int> num;
};

BigInt::BigInt(int n)
{
    if(n == 0)
        num.push_back(0);
    while(n) {
        num.push_back(n % 10);
        n /= 10;
    }
}

BigInt& BigInt::operator*=(int n)
{
    if(n == 0) {
        this->getNum().clear();
        this->getNum().push_back(0);
        return *this;
    }

    BigInt temp(*this);
    for(int i = 1; i < n; ++i)
        *this += temp;
    return *this;
}

BigInt& BigInt::operator+=(const BigInt& rhs)
{
    std::vector<int> result;
    int carry = 0;
    int max = std::max(rhs.size(), this->size());
    for(int i = 0; i < max; ++i) {
        int temp = (i >= rhs.size() ? 0 : rhs[i]) + 
                   (i >= (int)num.size() ? 0 : num[i]) + 
                   carry;
        if(temp > 9) {
            temp -= 10;
            carry = 1;
        } else
            carry = 0;
        result.push_back(temp);
    }
    if(carry != 0)
        result.push_back(carry);
    num.swap(result);
    return *this;
}

const BigInt BigInt::operator+(const BigInt& rhs)
{
    return BigInt(*this) += rhs;
}

const BigInt BigInt::operator*(const BigInt& rhs)
{
    BigInt result(0);
    for(int i = 0; i < rhs.size(); ++i) {
        *this *= rhs[i] * std::pow(10,i);
        result += *this;
    }
    this->swap(result); 
    return *this;
}
