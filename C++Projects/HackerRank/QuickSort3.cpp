#include <iostream>
#include <vector>
#include <iterator>

// running time comparision
//
void display(const std::vector<int>& vec)
{
    std::copy(vec.begin(), vec.end(), std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;
}

int InsertionSort(std::vector<int>& vec)
{
    int curr = 1, shifts = 0;
    while (curr < (int)vec.size()) {
        int p = curr, n = p - 1;
        int elem = vec[curr];
        while (vec[n] >= elem && n >= 0) {
            if (vec[p] != vec[n]) {
                vec[p] = vec[n];
                ++shifts;
            }
            --p;
            n = p - 1;
        }
        vec[p] = elem;
        ++curr;
    }
    //display(vec);
    return shifts;
}

int partition(std::vector<int>& vec, int low, int high, int& swaps)
{
    int i = low;
    for (int j = low; j < high; ++j) {
        if (vec[j] < vec[high]) {
            std::swap(vec[j], vec[i]);
            swaps += 1;
            i += 1;
        }
    }
    std::swap(vec[i], vec[high]);
    swaps += 1;
    return i;
}

void QuickSort(std::vector<int>& vec, int low, int high, int& swaps)
{
    if (low >= high)
        return;
    int p = partition(vec, low, high, swaps);;
    QuickSort(vec, low, p - 1, swaps);
    QuickSort(vec, p + 1, high, swaps);
}

int main()
{
    int N;
    std::cin >> N;
    std::vector<int> vec(std::istream_iterator<int>(std::cin),
                         std::istream_iterator<int>());
    std::vector<int> copy(vec.begin(), vec.end());
    int shifts = InsertionSort(vec);
    //std::cout << shifts << std::endl;
    int swaps = 0;
    QuickSort(copy, 0, copy.size() - 1, swaps);
    //display(copy);
    //std::cout << swaps << std::endl;
    std::cout << shifts - swaps << std::endl;
    return 0;
}
