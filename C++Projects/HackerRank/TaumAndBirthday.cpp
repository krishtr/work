#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

void TaumAndBirthday()
{
    int t;
    std::cin >> t;
    std::vector<uint64_t> results;
    for(int i = 0; i < t; ++i) {
        uint64_t b,w;
        std::cin >> b >> w;
        uint64_t x,y,z;
        std::cin >> x >> y >> z;

        // buy b black, buy w white, no conversion
        uint64_t amount1 = b*x + w*y;
        // buy b black, w black, w conversions
        uint64_t amount2 = b*x + w*x + w*z;
        // buy w black, w white,
        uint64_t amount3 = b*y + w*y + b*z;
        results.push_back(std::min(amount1, std::min(amount2, amount3)));
    }
    std::copy(results.begin(), results.end(), std::ostream_iterator<uint64_t>(std::cout, "\n"));
}

int main()
{
    TaumAndBirthday();
    return 0;
}