#include <iostream>
#include <string>

// abba
// abpba
bool isPalindrome(const std::string& str)
{
    auto first = str.begin();
    auto last = str.end() - 1;
    for(; first < last; ++first, --last) {
        if(*first != *last)
            return false;
    }
    return true;
}

int PalindromeIndex(const std::string& str)
{
    const int len = str.length();
    int i = 0, j = len - 1;
    while(i < j && str[i] == str[j]) i++, --j;
    if(i < j) {
        std::string temp = str.substr(0, i) + str.substr(i+1);
        if(isPalindrome(temp)) return i;
        temp = str.substr(0,j) + str.substr(j+1);
        if(isPalindrome(temp)) return j;
    }

    return -1;
}

void PalindromeIndexHandler()
{
    int T;
    std::cin >> T;
    while(T--) {
        std::string str;
        std::cin >> str;
        std::cout << PalindromeIndex(str) << std::endl;
    }
}

int main()
{
    PalindromeIndexHandler();
    return 0;
}