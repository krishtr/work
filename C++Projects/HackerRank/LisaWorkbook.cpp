#include <iostream>

void LisaWorkbook()
{
    int N,K;
    std::cin >> N >> K;
    int result;
    int page = 1;
    while(N--) {
        int n; // number of problems per chapter
        std::cin >> n;
        for(int i = 1; i <= n; ++i) { // i=problem id per page
            if(page == i)
                ++result;
            if(i % K == 0) 
                ++page;
        }
        if(n % K != 0)
            ++page;
    }
    std::cout << result << std::endl;
}

int main()
{
    LisaWorkbook();
    return 0;
}
