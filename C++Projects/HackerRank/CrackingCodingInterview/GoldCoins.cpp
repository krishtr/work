#include <iostream>
#include <cmath>

/**

The pattern of goldcoins is 1 2 2 3 3 3 4 4 4 4...
Hence the total coins = 1^2 + 2^2 + 3^2 + 4^2 +...

Each of the number in the pattern gives us the number of gold coins given **per day**

Method is to first find the relationship between the total number of days (n) and the
number of gold coins received on the nth day. Once we have this number, we can then
use the sum of squares formula to get our total number (after some adjustment, explained below).

We can find out the relationship by using the sum of the n numbers formula as follows:
Sum of m numbers = m(m+1) / 2. In our case, the sum would be the number of days (n) and 
m would be the number of gold coins received on nth day. For example:
If n = 6
6 = m(m+1)/2 => 12 = m(m+1) => m = 3 This shows that on the 6th day, he had been receiving 3 coins.

Now that we know what the number of gold coins is - the total should be 1^2 + 2^2 + 3^2 = 14

However, we could have a case where the given number of days (n) would not evenly divide to give us a perfect 'm'

Example: n = 7
7 = m(m+1) / 2 => m(m+1) = 14 // does not evenly divide

In these cases, we find out the nearest number that would evenly divide, which we can find by finding the
integer portion of the squareroot. So, int(sqrt(14)) ~= 3. And then adjust the gold coins for the difference.
So in case of 7:
1. we get m = 3, 
2. find the total number of gold coins for this number, 1^2 + 2^2 + 3^2 = 14
3. Find the difference we have to account for. 7 - (sum of m numbers were m = 3) => 7 - 6 = 1
4. So for 1 day the knight received 4 gold coins (the  next number after 3).

Therefore total gold coins = 1^2 + 2^2 + 3^2 + 4 = 18

However, we have another special case to consider. What if the difference to calculate the adjustment is negative?
This is evident in case n = 100
Following our process
1. Calculate m. 100 = m(m+1) / 2 => m ~= 14
2. Calculate sum of m numbers. 1+2+3...14 = 14*15 / 2 = 105 which is more than 100.
3. If we consider m = 14, this will give us a negative difference (100 - 105). So we need to use 1 less than 14 = 13
4. This means that the knight received gold coins on all days for (13*14 / 2) = 91 days.
5. From days 91 -> 100, he received 14 gold coins, ie 9 days
6. Therefore, total gold coins = 1^2 + 2^2 +...+ 13^2 + 14*9 = 945

Formulas used
Sum of m natural numbers = m(m+1) / 2
Sum of squares of m numbers = m(m+1)(2m+1) / 6

*/

int main()
{
    int n = 0;
    std::cin >> n;
    while (n != 0) {
        int sqrt = std::sqrt(2 * n);
        int delta = n - sqrt * (sqrt + 1) * 0.5;
        if (delta < 0) {
            sqrt = sqrt - 1;
            delta = n - sqrt * (sqrt + 1) * 0.5;
        }
        int result = sqrt * (sqrt + 1) * (2 * sqrt + 1) * 1/6;
        if (delta != 0)
            result += delta * (sqrt + 1);
        std::cout << n << " " << result << '\n';
        std::cin >> n;
    }
    return 0;
}
