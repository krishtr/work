#include <iostream>
#include <stack>

using namespace std;

bool is_balanced(string expr) {
    bool result = false;
    std::stack<char> st;
    for(auto iter = expr.begin(); iter != expr.end(); ++iter) {
        const char c = *iter;
        if(c == '(' || c == '{' || c == '[')
            st.push(c);
        else {
            if(!st.empty()) {
                auto top = st.top();
                if((top == '(' && c == ')') || (top == '[' && c == ']') || (top == '{' && c == '}'))
                    st.pop();
                else return false;
            } else return false;
        }
    }
    if(st.empty())
        result = true;
    return result;
}

int main(){
    int t;
    cin >> t;
    for(int a0 = 0; a0 < t; a0++){
        string expression;
        cin >> expression;
        bool answer = is_balanced(expression);
        if(answer)
            cout << "YES\n";
        else cout << "NO\n";
    }
    return 0;
}

