#include <iostream>
#include <string>
#include <unordered_map>

int main()
{
    int N;
    std::cin >> N;
    std::string s1, s2;
    std::unordered_map<std::string, int> map;
    while(N--) {
        std::cin >> s1 >> s2;
        if(s1 == "add") {
            for(uint l = 1; l <= s2.size(); ++l)
                map[s2.substr(0,l)]++;
        } else if(s1 == "find") {
            auto iter = map.find(s2);
            std::cout << (iter == map.end() ? 0 : iter->second) << '\n';
        }
    }
    return 0;
}
