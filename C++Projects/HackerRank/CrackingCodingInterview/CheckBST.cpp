#include <algorithm>

struct Node {
	int data;
	Node *left, *right;
};


int findMin(Node *root) {
    if(!root->left && !root->right) return root->data;
    if(!root->left) std::min(root->data, findMin(root->right));
    if(!root->right) std::min(root->data, findMin(root->left));
    return std::min(root->data, std::min(findMin(root->left), findMin(root->right)));
}

int findMax(Node *root) {
    if(!root->left && !root->right) return root->data;
    if(!root->left) std::max(root->data, findMax(root->right));
    if(!root->right) std::max(root->data, findMax(root->left));
    return std::max(root->data, std::max(findMax(root->left), findMax(root->right)));
}

bool checkBST(Node* root) {
    if(!root) return true;
    if(!root->left && !root->right) return true;
    if(!root->left) { 
        auto r = findMin(root->right);
        return root->data < r && checkBST(root->right);
    }
    if(!root->right) {
        auto l = findMax(root->left);
        return root->data > l && checkBST(root->left);
    }
    
    auto l = findMax(root->left);
    auto r = findMin(root->right);
    return root->data > l && root->data < r && checkBST(root->left) && checkBST(root->right);
}

int main()
{
}
