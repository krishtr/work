#include <cmath>
#include <cstdio>
#include <vector>
#include <set>
#include <iostream>
#include <algorithm>
#include <iterator>
#include <functional>

using namespace std;

/**
  1. Keep a running median, initially that will be zero.
  2. Maintain 2 heaps - max heap and min heap
  3. If the incoming element is less than running median, insert it to max heap
  however, if the insertion causes the heap to have more than 1 element diff
  between the 2 heaps, rebalance it by moving the top elem from max to min.
  Then insert the new element in max.
  4. If the incoming element is more than running median, insert it to min heap
  and follow the same advice about rebalancing
  5. If the size of the 2 heaps are the same, the median is the average of top elem
  else the median is the top elem of the heap with more size
 */

void choose(const std::vector<int>& max, const std::vector<int>& min, double& median)
{
    if(max.size() == min.size())
        median = (max[0] + min[0]) * 0.5;
    else
        median = static_cast<double>((max.size() > min.size()) ? max[0] : min[0]);

    printf("%.1f\n", median);
}

void balance(std::vector<int>& from, std::vector<int>& to,
             const std::function<bool(int,int)>& op1,
             const std::function<bool(int,int)>& op2)
{
    auto elem = from[0];
    std::pop_heap(from.begin(), from.end(), op1);
    from.pop_back();
    to.push_back(elem);
    std::push_heap(to.begin(), to.end(), op2);
}

void solve(const std::vector<int>& vec)
{
    auto maxop = [](int a, int b) { return a > b; }; // min heap
    auto minop = [](int a, int b) { return a < b; }; // max heap

    double median = 0.0;
    std::vector<int> max, min;
    if(vec[0] < median) {
        max.push_back(vec[0]);
        std::make_heap(max.begin(), max.end(), minop);
    } else {
        min.push_back(vec[0]);
        std::make_heap(min.begin(), min.end(), maxop);
    }
    choose(max,min,median);

    for(uint i = 1; i < vec.size(); ++i) {
        if(vec[i] > median) { // min heap
            if(min.size() > max.size())
                balance(min, max, maxop, minop);
            min.push_back(vec[i]);
            std::push_heap(min.begin(), min.end(), maxop);
        } else { // max heap
            if(max.size() > min.size())
                balance(max, min, minop, maxop);
            max.push_back(vec[i]);
            std::push_heap(max.begin(), max.end(), minop);
        }
        choose(max,min,median);
    }
}

// insertion sort approach
void solve1(const std::vector<int>& vec)
{
    std::vector<int> result;
    for(auto i : vec) {
        auto it = std::lower_bound(result.begin(), result.end(), i);
        result.insert(it, i);
        const int mid = result.size() / 2;
        if(result.size() % 2 == 0)
            printf("%.1f\n", (result[mid] + result[mid - 1]) * 0.5);
        else
            printf("%.1f\n", static_cast<double>(result[mid]));
    }
}

void median(std::vector<int>& vec, int num)
{
    auto it = std::lower_bound(vec.begin(), vec.end(), num);
    vec.insert(it, num);
    const unsigned int size = vec.size();
    const uint mid = size / 2;
    if(size % 2 == 0)
        std::cout << (int)((vec[mid] + vec[mid - 1]) * 0.5) << '\n';
    else
        std::cout << vec[mid] << '\n';
}

int main(){
    int n;
    cin >> n;
    std::vector<int> a;
    std::vector<int> b(n);
    for(int a_i = 0; a_i < n; ++a_i) {
        int i = 0;
        std::cin >> i;
        b[a_i] = i;
        median(a, i);
    }
    std::cout << '\n';
    solve1(b);
    return 0;
}

