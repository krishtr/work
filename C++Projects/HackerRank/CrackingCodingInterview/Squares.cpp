#include <iostream>
#include <unordered_set>
#include <vector>
#include <utility>

/**

Our method here is as follows
1. store all the coordinates. Use a hashset so that lookup is quick
2. Walk through all the coordinates, for every pair of coordinates, we assume that
the 2 coordinates makeup the diagonal corners of the square.
3. Using this information and the formula for diagonal center point and half diagonal,
we calculate the other 2 coordinate points that make up the other diagonal.
4. If these 2 coordinate points are found in our hashset, that means we have a square and we increment our count.
5. Since for every square we find, we have 2 diagonals and we would have found incremented the count twice.
6. Hence, before we report the count, we halve it and then report it.

Disadvantage: This approach has O(n^2) complexity. In addition we have a space penalty due to the extra memory required
for hashset and vector.

Although this approach seems to be giving the right answer, I was only able to find this approach in the time provided.
There may be more efficient approaches to solve this problem.

*/

int main()
{
    int n = 0;
    std::cin >> n;
    while (n != 0) {
        std::unordered_set<std::string> lookup;
        std::vector<std::pair<int,int>> vec;
        for (int i = 0; i < n; ++i) {
            double x, y;
            std::cin >> x >> y;
            char buff[14]; 
            snprintf(buff, 15, "%.1f%.1f", x, y);
            lookup.insert(buff);
            vec.push_back(std::make_pair(x,y));
        }
        int count = 0;
        for (uint i = 0; i < vec.size() - 1; ++i) {
            for (uint j = i + 1; j < vec.size(); ++j) {
                int x1 = vec[i].first, y1 = vec[i].second;
                int x2 = vec[j].first, y2 = vec[j].second;

                // assume (x1,y1) and (x2,y2) are diagnol points of the square
                // find center of diagnol
                auto xc = (x1 + x2) * 0.5, yc = (y1 + y2) * 0.5;

                // find half diagnol
                auto xd = (x1 - x2) * 0.5, yd = (y1 - y2) * 0.5;

                // the other 2 points of the diagnols (x3,y3) (x4,y4)
                auto x3 = xc - yd, y3 = yc + xd;
                auto x4 = xc + yd, y4 = yc - xd;

                // construct the keys and check if they are present
                char buff3[14], buff4[14];
                snprintf(buff3, 15, "%.1f%.1f", x3, y3);
                //std::cout << "buff3: " << buff3 << '\n';
                snprintf(buff4, 15, "%.1f%.1f", x4, y4);
                //std::cout << "buff4: " << buff4 << '\n';
                if (lookup.find(buff3) != lookup.end()
                 && lookup.find(buff4) != lookup.end()) {
                    ++count;
                }
            }
        }
        
        // for every square, we would have calculated diagnols twice
        std::cout << count * 0.5 << '\n';
        std::cin >> n;
    }
    return 0;
}
