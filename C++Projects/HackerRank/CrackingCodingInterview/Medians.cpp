#include <iostream>
#include <vector>
#include <algorithm>

/**

This problem expects us to calculate the running median.

Instead of repeatedly sorting the sequence everytime an element is added and then calculating
the median, we can instead use insertion sort to figure out the ideal location to store the
value and then calculate the running median.

*/

void solve(const std::vector<int>& vec)
{
    std::vector<int> result;
    for(auto i : vec) {
        auto it = std::lower_bound(result.begin(), result.end(), i);
        result.insert(it, i);
        const int mid = result.size() / 2;
        if(result.size() % 2 == 0)
            std::cout << static_cast<int>((result[mid] + result[mid - 1]) * 0.5) << '\n';
        else
            std::cout << result[mid] << '\n';
    }
}

int main(){
    std::vector<int> b;
    int n = 0;
    while (std::cin >> n) {
        b.push_back(n);
    }
    solve(b);
    return 0;
}

