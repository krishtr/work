#include <iostream>
#include <algorithm>

/**

This is a classic knapsack problem where we have the requirement that given objects with
a price and weight and a constraint of max weight that can be carried what items can we pick
to maximise the price keeping the weight limited to the constraint.

This can be solved using Dynamic programming recursion method. However, even though this gives
us the answer, it would not be as efficient (complexity exponential -> O(2^n)) as it could since 
in the recursion tree that will be many subbranches which will be calculated again and again.

The typical solution to this would be memoisation where we store the results from intermediate steps
and then before calculation of a new branch, we first check if we have already computed it before
and use the cached value if we have cached it.

Our method is as follows -

For every item (price and weight combination), we calculate all possible ideal solutions (ie for a given
price, weight and constraint the ideal value will be the max price possible) and cache
them. Here we have been given an upper limit of constrained weight (30) which helps

*/

/*
int maximise(int *price, int *weight,
             int constraint, int n)
{
    if (n == 0 || constraint == 0) return 0;
    if (weight[n - 1] > constraint)
        return maximise(price, weight, constraint, n - 1);

    return std::max( price[n - 1] + maximise(price, weight, constraint - weight[n - 1], n - 1),
                     maximise(price, weight, constraint, n - 1));
}
*/

int main()
{
    int T = 0;
    std::cin >> T;
    while (T--) {
        int obj = 0;
        std::cin >> obj;
        int P[1001], W[1001];
        std::fill(std::begin(P), std::end(P), 0);
        std::fill(std::begin(W), std::end(W), 0);
        for (int i = 0; i < obj; ++i)
            std::cin >> P[i] >> W[i];
        int lookup[31];
        std::fill(std::begin(lookup), std::end(lookup), 0);
        for (int i = 0; i < obj; ++i) {
            for (int j = 30; j >= 0; --j) {
                if (W[i] <= j)
                    lookup[j] = std::max(lookup[j], P[i] + lookup[j - W[i]]);
            }
        }
        auto constraints = 0;
        std::cin >> constraints;
        auto result = 0;
        while (constraints--) {
            int c = 0;
            std::cin >> c;
            result += lookup[c];
        }
        std::cout << result << '\n';
    }
    return 0;
}
