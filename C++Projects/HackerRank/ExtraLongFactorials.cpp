#include <iostream>
#include "BigInt.hpp"

void ExtraLongFactorials()
{
    int n;
    std::cin >> n;
    BigInt result(n);
    for(int i = 1; (n-i) >= 1; ++i) {
        result *= (n-i);
    }
    result.display();
}

int main()
{
    //ExtraLongFactorials();
    int A,B;
    std::cin >> A >> B;
    BigInt a(A), b(B);
    BigInt result(a*b);
    result.display();
    return 0;
}
