#include <iostream>
#include <vector>
#include <iterator>

void display(std::vector<int>& vec)
{
    std::copy(vec.begin(), vec.end(), std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;
}

int partition(std::vector<int>& vec, int l, int h)
{
    int pivot = vec[h];
    int low = l;
    for (int j = l; j < h; ++j) {
        if (vec[j] < pivot) {
            std::swap(vec[j], vec[low]);
            low += 1;
        }
    }
    std::swap(vec[h], vec[low]);
    display(vec);
    return low;
}

void QuickSort(std::vector<int>& vec, int l, int h)
{
    if (l >= h)
        return;
    int p = partition(vec, l, h);
    QuickSort(vec, l, p-1);
    QuickSort(vec, p+1, h);
}

int main()
{
    int N;
    std::cin >> N;
    std::vector<int> vec(std::istream_iterator<int>(std::cin),
                         std::istream_iterator<int>());
    QuickSort(vec, 0, vec.size() - 1);
    return 0;
}
