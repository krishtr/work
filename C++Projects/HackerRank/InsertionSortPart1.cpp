#include <iostream>
#include <vector>
#include <iterator>

void InsertionSort()
{
    int N;
    std::cin >> N;
    std::vector<int> vec(std::istream_iterator<int>(std::cin),
                         std::istream_iterator<int>());
    auto curr = vec.rbegin();
    auto elem = *curr;
    auto next = std::next(curr);
    while(next != vec.rend()) {
        if(*next <= elem) {
            *curr = elem;
            break;
        } else {
            *curr = *next;
            ++curr;
            ++next;
        }
        std::copy(vec.begin(), vec.end(), std::ostream_iterator<int>(std::cout, " "));
        std::cout << std::endl;
    }
    if(*curr > elem)
        *curr = elem;
    std::copy(vec.begin(), vec.end(), std::ostream_iterator<int>(std::cout, " "));
}

int main()
{
    InsertionSort();
    return 0;
}

