#include <iostream>

void LibraryFine()
{
    //
    // If the book is returned on or before the expected return date, no fine will be charged. In other words, the fine is 0.
    // If the book is returned in the same calendar month as the expected return date, the fine = 15 Hackos × the number of late days.
    // If the book is not returned in the same calendar month but in the same calendar year as the expected return date, the fine = 500 Hackos×the number of late months.
    // If the book is not returned in the same calendar year, the fine is fixed at 10000 Hackos.
    //
    int d1,m1,y1;
    std::cin >> d1 >> m1 >> y1; // actual
    int d2,m2,y2;
    std::cin >> d2 >> m2 >> y2; // expected
    long fine;
    if(y1 > y2)
        fine = 10000;
    else if(y1 < y2)
        fine = 0;
    else {
       if(m1 > m2)
          fine = 500*(m1-m2);
       else if(m1 < m2)
          fine = 0;
       else {
           if(d1 > d2)
               fine = 15*(d1-d2);
           else
               fine = 0;
       }
    }
    std::cout << fine << std::endl;
}

int main()
{
    LibraryFine();
    return 0;
}