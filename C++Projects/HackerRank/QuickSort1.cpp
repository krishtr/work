#include <iostream>
#include <vector>
#include <iterator>

void display(const std::vector<int>& vec)
{
    std::copy(vec.begin(), vec.end(), std::ostream_iterator<int>(std::cout, " "));
}

void QuickSort()
{
    int N;
    std::cin >> N;
    std::vector<int> vec(std::istream_iterator<int>(std::cin),
                         std::istream_iterator<int>());
    const int pivot = vec[0];
    std::vector<int> left, right;
    left.reserve(vec.size());
    right.reserve(vec.size());
    auto iter = vec.begin(), end = vec.end();
    for (; iter != end; ++iter) {
        if (*iter < pivot)
            left.push_back(*iter);
        else
            right.push_back(*iter);
    }
    display(left);
    display(right);
    std::cout << std::endl;
}

int main()
{
    QuickSort();
    return 0;
}
