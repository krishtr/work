#include <iostream>
#include <algorithm>

int main()
{
    int N;
    std::cin >> N;
    std::vector<int> vec(N);
    for(int i = 0; i < N; ++i)
        std::cin >> vec[i];
    if(vec.size() < 3)
        std::cout << "NO" << std::endl;
    else {
        int count = 0;
        for(uint i = 0; i < vec.size()-1; ++i) {
            if(vec[i]%2 != 0) {
                count += 2;
                vec[i]++;
                vec[i+1]++;
            }
        }
        if(!std::any_of(vec.begin(), vec.end(), [](int i){ return i%2 != 0; }))
            std::cout << count << std::endl;
        else
            std::cout << "NO" << std::endl;
    }
    return 0;
}
