#include <iostream>
#include <vector>
#include <iterator>

void display(std::vector<int>& vec)
{
    std::copy(vec.begin(), vec.end(), std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;
}

void InsertionSort()
{
    int N;
    std::cin >> N;
    std::vector<int> vec(std::istream_iterator<int>(std::cin),
                         std::istream_iterator<int>());
    int curr = 1 ;
    while(curr < (int)vec.size()) {
        int p = curr, n = p-1;
        int elem = vec[curr];
        while(vec[n] >= elem && n >= 0) {
            vec[p] = vec[n];
            --p;
            n = p-1;
        }
        vec[p] = elem;
        ++curr;
       display(vec); 
    }
}

int main()
{
    InsertionSort();
    return 0;
}
