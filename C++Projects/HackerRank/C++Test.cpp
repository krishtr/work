#include <iostream>
#include <fstream>
#include <stdint.h>
#include <sstream>
#include <math.h>
#include <map>
#include <unordered_map>
#include <list>
#include <set>
#include <algorithm>
#include <iterator>
#include <memory>
#include <vector>
#include <bitset>
#include <future>
#include <mutex>


class Person : public std::enable_shared_from_this<Person>
{
private:
    std::string name;
public:
    std::shared_ptr<Person> mom;
    std::shared_ptr<Person> dad;
    std::vector<std::weak_ptr<Person>> kids;
    Person( const std::string& n,
            std::shared_ptr<Person> m = nullptr,
            std::shared_ptr<Person> d = nullptr )
        : name(n), mom(m), dad(d)
    {}
    std::string getName() const { return name; }
    ~Person() { std::cout << name << " destroyed" << std::endl; }
    void setParentsAndKids( std::shared_ptr<Person> mom = nullptr,
                            std::shared_ptr<Person> dad = nullptr )
    {
        if (mom != nullptr)
            mom->kids.push_back(shared_from_this());
        if (dad != nullptr)
            dad->kids.push_back(shared_from_this());
    }
};

std::shared_ptr<Person> initFamily(const std::string& name)
{
    std::shared_ptr<Person> mom = std::make_shared<Person>(name + "'s mom");
    std::shared_ptr<Person> dad = std::make_shared<Person>(name + "'s dad");
    std::shared_ptr<Person> kid = std::make_shared<Person>(name, mom, dad);
    kid->setParentsAndKids(mom, dad);
    //kid->mom->kids.push_back(kid);
    //kid->dad->kids.push_back(kid);
    std::cout << name <<  "'s Family initialized" << std::endl;
    return kid;
}
std::mutex printMutex;

void print(const std::string& s)
{
    std::lock_guard<std::mutex> l(printMutex);
    for (auto c : s)
        std::cout.put(c);
    std::cout << std::endl;
}


int binaryGap(int num)
{
    int max_length = 0;
    int zero_length = 0;
    int mask = 1;
    bool start = false;
    while(num) {
        int bit = num & mask;
        if (bit == 1) {
            if (!start)
                start = true;
            else {
                zero_length = 0;
            }
        } else {
            if (start) {
                ++zero_length;
                max_length = std::max(zero_length, max_length);
            }
        }
        num /= 2;
    }

    return max_length;
}

void printBinaryRep(int num)
{
    std::vector<int> bin;
    while(num) {
        bin.push_back(num & 1);
        num >>= 2;
    }
    std::reverse(bin.begin(), bin.end());
    std::copy(bin.begin(), bin.end(), std::ostream_iterator<int>(std::cout));
    std::cout << std::endl;
}

void rotateArray(std::vector<int>& A, int K)
{
    // [3,8,4,6,9] ->(1) [9,3,8,4,6]
    if (K % A.size() == 0)
        return;
    K = K % A.size();
    std::vector<int> result;
    result.reserve(A.size());
    std::vector<int>::reverse_iterator iter = A.rbegin();
    while (--K) {
        ++iter;
    }

    std::vector<int>::iterator pin = (++iter).base();
    std::vector<int>::iterator it = pin;
    for (; it != A.end(); ++it)
        result.push_back(*it);

    std::copy(A.begin(), pin, std::back_inserter(result));
    A.swap(result);
}

void getBinary(std::vector<int>& bin, int N)
{
    while(N) {
        bin.push_back(N & 1);
        N = N >> 1;
    }
    std::reverse(bin.begin(), bin.end());
    std::copy(bin.begin(), bin.end(), std::ostream_iterator<int>(std::cout));
}

// 1001100110
int binaryPeriod(int N)
{
    std::vector<int> bin;
    getBinary(bin, N);
    int Q = bin.size();
    for(int P = 1; P <= Q/2; ++P) {
        bool match = true;
        for(int K = 0; K < (Q-P); ++K) {
            if(bin[K] != bin[K + P]) {
                match = false;
                break;
            }
        }
        if (match)
            return P;
    }
    return -1;
}

//int findSquares(int A, int B)
//{

//}

int findUnpairedValue(std::vector<int>& A)
{
    if (A.size() == 1)
        return A[0];

    std::vector<int>::iterator iter = A.begin();
    int value = *iter;
    ++iter;
    for(; iter != A.end(); ++iter)
        value = value ^ *iter;

    return value;
}

template<class T>
void display(std::vector<T>& A)
{
    std::copy(A.begin(), A.end(), std::ostream_iterator<T>(std::cout, ", "));
}

int squareCount(int A, int B)
{
    if (A < 0)
        A = 0;
    if (B < 0)
        B = 0;
    if (A == B)
        return 0;
    int max = std::max(A,B);
    int min = std::min(A,B);
    int count = 0;
    for(int i = sqrt(double(min)); i <= sqrt(double(max)); ++i) {
        if((i * i) >= min && (i*i) <= max)
           ++count;
    }
    return count;
}

bool isTriangle(int a, int b, int c)
{
    return (a+b>c && a+c>b && b+c>a);
}

int triangleTripletCount(std::vector<int>& A)
{
    //
    // 12,10,8,5,2,1
    // 12,10,8
    // 12,10,5
    // 12,8,5
    // 10,8,5
    //
    if(A.size() < 3)
        return 0;
    int count = 0;
    std::sort(A.begin(), A.end());
    for(uint i = 0, j = 0, k = 0; i < A.size()-2; ++i) {
        j = i + 1;
        k = j + 1;
        while(j < A.size()) {
            if(isTriangle(A[i],A[j],A[k])) {
                ++count;
                ++k;
            } else {
                ++j;
                k = j+1;
            }
        }
    }
    return count;
}

struct City
{
    int value;
    bool isVisited, isLeaf;
    City() : value(-1), isVisited(false), isLeaf(false) {}
    City(int val) : value(val), isVisited(false), isLeaf(false) {}
    std::vector<City> links;
};
std::ostream& operator<<(std::ostream& os, const City& c)
{
    os << "(" << c.value << "," << c.isLeaf << ")";
    return os;
}

std::vector<City> graph;

void parseInput(std::vector<uint>& A)
{
    graph.resize(A.size());
    for(uint i = 0; i < A.size(); ++i) {
        if(i != A[i]) {
            graph[i].links.push_back(City(A[i]));
            graph[A[i]].links.push_back(City(i));
        }
        graph[i].value = i;
    }

    // mark leafs
    for(uint i = 0; i < graph.size(); ++i) {
        if(graph[i].links.size() == 1)
            graph[i].isLeaf = true;
    }
}

/*
std::vector<int> maxCounters(int M, std::vector<int>& A)
{
    std::cout << "Begin function" << std::endl;
    std::vector<int> result(M, 0);
    int prev_max = 0, max = 0;
    for(auto val : A) {
        if(val == M+1) { // max it
            prev_max = max;
        } else { // increment the counter
            if(result[val-1] < prev_max)
                result[val-1] = prev_max;
            result[val-1] += 1;
            max = std::max(result[val-1], max);
        }
        std::copy(result.begin(), result.end(), std::ostream_iterator<int>(std::cout, ", "));
        std::cout << "\tprev_max: " << prev_max << ", max: " << max << std::endl;
    }
    for(uint i = 0; i < result.size(); ++i)
        if(result[i] < prev_max)
            result[i] = prev_max;

    return result;
}
*/

int K(std::vector<int>&A, int K)
{
    std::vector<int>::iterator iter = A.begin(), end = A.end();
    std::map<int, int> cache;
    for(; iter != end; ++iter) {
        int comp = K - *iter;
        int temp = cache.find(comp) != cache.end()
                    ? cache[comp] : 0;
        cache[comp] = ++temp;
    }

    int count = 0;
    iter = A.begin();
    for(; iter != end; ++iter) {
        count += cache.find(*iter) != cache.end() ? cache[*iter] : 0;
    }

    return count;
}

//
// given link-list of integers,
// return the integer which has
// max repetitions, if 2 or more
// integers have same repetition
// return the first one
//
void MaxCharRepetitions(std::list<char> &list)
{
    if(list.size() == 0)
        return;
    std::vector<int> vec(256, 0);
    auto iter = list.begin();
    auto end = list.end();
    for(; iter != end; ++iter)
        vec[*iter]++;
    auto maxIter = std::max_element(vec.begin(), vec.end(), std::less<int>());
    std::cout << "character " << char(maxIter - vec.begin())
        << " occurs " << *maxIter << " times." << std::endl;
}

// 90.56 -> 90
// 909 -> 909
// -90 -> -90

int myatoi(const char *str)
{
    int result = 0;
    if(!str)
        return result;
    if((str[0] != '-' || str[0] != '+') && !isdigit(str[0]))
        return result;
    int i = (str[0] == '-' || str[0] == '+') ? 1 : 0;
    for(; isdigit(str[i]) && str[i] != '\0'; ++i) {
        result = 10 * result + (str[i] - '0');
    }

    return (str[0] == '-') ? -result : result;
}

typedef std::vector<std::vector<long long int>> VecVec;
void UnRoll(VecVec& matrix, std::vector<long long int>& roll)
{
    //for(int
}

void MatrixRotation()
{
    long long int M,N,R;
    std::cin >> M >> N >> R;
    VecVec input(M, std::vector<long long int>(N,0));
    std::cout << "Number of rows: " << input.size() << std::endl;
    std::cout << "Number of cols: " << input[0].size() << std::endl;
    for(long long int i = 0; i < M; ++i) {
        for(long long int j = 0; j < N; ++j) {
            long long int m;
            std::cin >> m;
            input[i][j] = m;
        }
    }
    for(auto vec : input) {
        std::copy(vec.begin(), vec.end(), std::ostream_iterator<long long int>(std::cout, " "));
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

void AshtonAndString()
{
    int T;
    std::cin >> T;
    while(T--) {
        std::string str;
        std::cin >> str;
        uint K;
        std::cin >> K;
        std::vector<std::string> results;
        const int len = str.length();
        results.reserve(len*(len+1)*0.5);
        findAllSubstrings(str, results);
        //std::copy(results.begin(), results.end(), std::ostream_iterator<std::string>(std::cout,","));
        //std::cout << std::endl;
        std::sort(results.begin(), results.end());
        //std::copy(results.begin(), results.end(), std::ostream_iterator<std::string>(std::cout,","));
        //std::cout << std::endl;
        results.erase(std::unique(results.begin(), results.end()), results.end());
        //std::copy(results.begin(), results.end(), std::ostream_iterator<std::string>(std::cout,","));
        //std::cout << std::endl;
        const int newlen = results.size();
        int runningLength = 0;
        for(int i = 0; i < newlen; ++i) {
            const std::string temp(results[i]);
            if(K <= runningLength+temp.length()) {
                K -= runningLength;
                std::cout << temp[K - 1] << std::endl;
                break;
            } else {
                runningLength += temp.length();
            }
        }
    }
}

int main()
{

    /**
    auto p = initFamily("Krish");
    std::cout << "Krish's use_count: " << p.use_count() << std::endl;
    std::cout << "Krish's mom's child: " << p->mom->kids[0].lock()->getName() << std::endl;

    p = initFamily("Aarthi");
    **/

    /**
    std::thread::yield();

    auto f1 = std::async(std::launch::async, print, "Hello, from first thread");
    auto f2 = std::async(std::launch::async, print, "Hello, from second thread");
    print("Hello, from the main thread");

    **/
    /**
    std::cout << "Enter a num: ";
    int i = 0, j = 0;
    std::cin >> i;
    std::cin >> j;
    //auto result = binaryPeriod(i);
    auto result = squareCount(i,j);
    std::cout << std::endl;
    std::cout << "Result: " << result << std::endl;
    **/

    //std::vector<int> A {0, 1000000000};
    //ExtraLongFactorials();
    /*
    std::list<char> list {'1','1','1','2','2','2','3'};
    MaxCharRepetitions(list);
    std::string numstr;
    std::cin >> numstr;
    std::cout << "atoi: " << atoi(numstr.c_str()) << std::endl;
    std::cout << "myatoi: " << myatoi(numstr.c_str()) << std::endl;
    char *str = "123a";
    std::cout << "atoi: " << atoi(str) << std::endl;
    std::cout << "myatoi: " << myatoi(str) << std::endl;
    */
    //MatrixRotation();
    AshtonAndString();
    return 0;
}
