#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

void ManasaStones()
{
    int t;
    std::cin >> t;
    std::vector<std::vector<int> > results(t);
    for(int i = 0; i < t; ++i) {
        int n;
        std::cin >> n;
        int a,b;
        std::cin >> a >> b;
        int first = a*(n-1);
        results[i].push_back(first);
        for(int j = 1; j < n; ++j) {
            first += (b-a);
            results[i].push_back(first);
        }
    }
    for(uint i = 0; i < results.size(); ++i) {
        std::copy(results[i].begin(), results[i].end(), std::ostream_iterator<int>(std::cout, " "));
        if(i != results.size() - 1)
            std::cout << std::endl;
    }
}

int main()
{
    ManasaStones();
    return 0;
}