#include <iostream>
#include <bitset>
#include <vector>

void ACMTeam()
{
    int n,m;
    std::cin >> n >> m;
    std::vector<std::string> inputs(n);
    for(int i = 0; i < n; ++i)
        std::cin >> inputs[i];

    int max = 0, numTeamMax = 0;
    for(int i = 0; i < n; ++i) {
        std::bitset<500> a(inputs[i]);
        for(int j = i+1; j < n; ++j) {
            std::bitset<500> b(inputs[j]);
            int count = (a|b).count();
            if(count > max) {
                max = count;
                numTeamMax = 1;
            } else if(count == max)
                numTeamMax++;
        }
    }

    std::cout << max << std::endl;
    std::cout << numTeamMax << std::endl;
}

int main()
{
    ACMTeam();
    return 0;
}