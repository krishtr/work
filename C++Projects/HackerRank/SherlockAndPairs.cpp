#include <iostream>
#include <unordered_map>

void SherlockAndPairs()
{
    long long int T;
    std::cin >> T;
    while (T--) {
        long long int N;
        std::cin >> N; 
        // n Choose 2 * 2, *2 because (1,0),(0,1) 2 combos for same pair
        std::unordered_map<long long int,long long int> map;
        long long int k;
        while (N--) {
            std::cin >> k;
            map[k]++;
        }
        long long int count = 0;
        for (auto p : map) {
            const long long int n = p.second;
            if (n > 1)
                count += n * (n-1);
        }
        std::cout << count << std::endl;
    }
}

int main()
{
    SherlockAndPairs();
    return 0;
}
