#include <iostream>
#include <iterator>

int main()
{
    int N;
    std::cin >> N;
    int arr[100] = {0,};
    while (N--) {
        int i;
        std::cin >> i;
        arr[i]++;
    }
    std::copy(std::begin(arr), std::end(arr), std::ostream_iterator<int>(std::cout, " "));
    return 0;
}
