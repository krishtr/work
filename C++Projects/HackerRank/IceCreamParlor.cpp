#include <iostream>
#include <vector>
#include <map>

void IceCream()
{
    int T;
    std::cin >> T;
    while (T--) {
        int M,N;
        std::cin >> M >> N;
        std::vector<int> vec(N,0);
        for (int i = 0; i < N; ++i)
            std::cin >> vec[i];
        std::map<int,int> cache;
        for (int i = 0; i < (int)vec.size(); ++i) {
            std::map<int, int>::const_iterator iter;
            if ((iter = cache.find(vec[i])) != cache.end()) {
                std::cout << iter->second << " " << i + 1 << std::endl;
                break;
            }
            const int complement = M - vec[i];
            cache[complement] = i + 1;
        }
    }
}

int main()
{
    IceCream();
    return 0;
}
