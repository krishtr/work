#include <iostream>
#include <vector>
#include <iterator>

void display(const std::vector<int>& vec)
{
    std::copy(vec.begin(), vec.end(), std::ostream_iterator<int>(std::cout, " "));
}

void partition(std::vector<int>& vec)
{
    if (vec.size() == 0 || vec.size() == 1)
        return;
    const int pivot = vec[0];
    std::vector<int> left, right, equal;
    left.reserve(vec.size());
    right.reserve(vec.size());
    equal.reserve(vec.size());
    auto iter = vec.begin(), end = vec.end();
    for (; iter != end; ++iter) {
        if (*iter < pivot)
            left.push_back(*iter);
        else if (*iter > pivot)
            right.push_back(*iter);
        else 
            equal.push_back(*iter);
    }
    partition(left);
    partition(right);

    vec.swap(left);
    vec.insert(vec.end(), equal.begin(), equal.end());
    vec.insert(vec.end(), right.begin(), right.end());
    display(vec);
    std::cout << std::endl;
}

void QuickSort()
{
    int N;
    std::cin >> N;
    std::vector<int> vec(std::istream_iterator<int>(std::cin),
                         std::istream_iterator<int>());
    partition(vec);
    std::cout << std::endl;
}

int main()
{
    QuickSort();
    return 0;
}
