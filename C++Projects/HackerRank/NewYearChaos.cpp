#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

template<typename ForwardIterator>
int applyTransform(ForwardIterator first, ForwardIterator last)
{
    auto iter = last - 1;
    int swaps = 0;
    while(iter >= first) {
        std::iter_swap(iter--, last--);
        swaps++;
    }
    return swaps;
}

void NewYearChaos()
{
    int T;
    std::cin >> T;
    while (T--) {
        int N;
        std::cin >> N;
        std::vector<int> vec(N,0), input(N,0);
        for (int i = 0; i < N; ++i) {
            std::cin >> input[i];
            vec[i] = i+1;
        }
        //
        // input: input array
        // vec: array to change and compare 
        int count = 0;
        bool found = true;
        for (int i = 0; i < (int)input.size() - 1; ++i) {
            if (vec[i] == input[i])
                continue;
            auto iter = std::find(vec.begin() + i, vec.end(), input[i]);
            if (iter == vec.end()) { // shouldnt be here
                found = false;
                break;
            }
            if (std::distance(vec.begin() + i, iter) > 2) {
                std::cout << "Too chaotic" << std::endl;
                found = false;
                break;
            }
            count += applyTransform(vec.begin() + i, iter);
        }
        if (found)
            std::cout << count << std::endl;
    }
}

int main()
{
    NewYearChaos();
    return 0;
}
