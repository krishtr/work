#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

void SherlockAndArray()
{
    int T;
    std::cin >> T;
    while (T--) {
        int N;
        std::cin >> N;
        std::vector<int> vec(N,0);
        for (int i = 0; i < N; ++i)
            std::cin >> vec[i];
            
        int total = 0;
        bool found = false;
        total = std::accumulate(vec.begin(), vec.end(), total);
        int lsum = 0, rsum = 0;
        for (int i = 0; i < (int)vec.size(); ++i) {
            rsum = total - lsum - vec[i];
            if (lsum == rsum) {
                std::cout << "YES" << std::endl;
                found = true;
                break;
            }
            lsum += vec[i];
        }
        if (!found)
            std::cout << "NO" << std::endl;
    }
}

int main()
{
    SherlockAndArray();
    return 0;
}
