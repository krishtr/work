#!/usr/bin/python

def do(A, B, N):
    for i in xrange(N-2):
        C = B * B + A
        A = B
        B = C
    print C

if __name__ == "main":
    do(0,1,10)
