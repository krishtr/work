#include <iostream>
#include <algorithm>
#include <iterator>

int main()
{
    int n,k,q;
    std::cin >> n >> q >> k;
    std::vector<int> list(n);
    for(int i = 0; i < n; ++i)
        std::cin >> list[i];

    // rotate k times
    // 1 2 3
    std::vector<int> result(n);
    q %= n;
    int index = n - q;
    std::copy(list.begin() + index, list.end(), result.begin());
    std::copy(list.begin(), list.begin() + index, result.begin() + q);
    for(int i = 0; i < k; ++i) {
        int idx;
        std::cin >> idx;
        std::cout << result[idx] << std::endl;
    }

    //std::copy(result.begin(), result.end(), std::ostream_iterator<int>(std::cout, " "));
    //std::cout << std::endl;
    return 0;
}
