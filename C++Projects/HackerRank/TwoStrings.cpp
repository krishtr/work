#include <iostream>
#include <string>
#include <unordered_set>

// A.size() <= B.size()
bool isPresent(const std::string& A, const std::string& B)
{
    std::unordered_set<char> cache;
    for(auto c : B)
        cache.insert(c);
    for(auto c : A) {
        if(cache.find(c) != cache.end())
                return true;
    }
    return false;
}

void TwoStrings()
{
    int T;
    std::cin >> T;
    while(T--) {
        std::string A,B;
        std::cin >> A >> B;
        bool result = (A.size() <= B.size()) ? isPresent(A,B) : isPresent(B,A);
        std::cout << (result ? "YES" : "NO") << std::endl;
    }
}

int main()
{
    TwoStrings();
    return 0;
}

