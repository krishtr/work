#include <iostream>
#include <vector>
#include <algorithm>

bool isAnagram(const std::string& str1, const std::string& str2)
{
   int count1[26] = {0,}, count2[26] = {0,};
   const int len1 = str1.length();
   for(int i = 0; i < len1; ++i)
       count1[str1[i] - 'a']++;
   const int len2 = str2.length();
   for(int i = 0; i < len2; ++i)
       count2[str2[i] - 'a']++;
   for(int c = 0; c < 26; ++c)
       if(count1[c] != count2[c])
           return false;
   return true;
}

void findAllSubstrings(const std::string& str, std::vector<std::string>& results)
{
    const uint len = str.length();
    for(uint pos = 0; pos < len; ++pos)
        for(uint l = 1; l <= (len-pos); ++l)
            results.push_back(str.substr(pos, l));
}

void SherlockAndAnagram()
{
    int T;
    std::cin >> T;
    while(T--) {
        std::string str;
        std::cin >> str;
        std::vector<std::string> results;
        const int n = str.length();
        results.reserve(0.5*n*(n+1));
        findAllSubstrings(str, results);
        const auto len = results.size();
        int count = 0;
        std::sort(results.begin(), results.end(),
                [](const std::string& s1, const std::string& s2)
                { return s1.length() < s2.length(); });
        for(uint i = 0; i < len; ++i)
            for(uint j = i+1; j < len && results[i].length() == results[j].length() ; ++j)
                if(isAnagram(results[i], results[j]))
                    ++count;
        std::cout << count << std::endl;
    }
}

int main()
{
    SherlockAndAnagram();
    return 0;
}