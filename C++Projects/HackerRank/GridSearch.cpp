#include <iostream>
#include <vector>
#include <string>

void GridSearch()
{
    int t;
    std::cin >> t;
    for(int i = 0; i < t; ++i) {
        int R, C;
        std::cin >> R >> C;
        std::vector<std::string> G(R);
        for(int i = 0; i < R; ++i)
            std::cin >> G[i];
        int r, c;
        std::cin >> r >> c;
        std::vector<std::string> P(r);
        for(int i = 0; i < r; ++i)
            std::cin >> P[i];

        bool found = false, done = false;
        for(int i = 0; i <= R-r; ++i) {
            size_t pos = G[i].find(P[0]);
            while(pos != std::string::npos) {
                found = true;
                for(uint j = 1; j < P.size(); ++j) {
                    if(G[i+j].substr(pos, P[j].size()) != P[j]) {
                        found = false;
                        break;
                    }
                }
                if(found) {
                    std::cout << "YES" << std::endl;
                    done = true;
                    break;
                }
                pos = G[i].find(P[0], pos+1);
            }
        }
        if(!found && !done)
            std::cout << "NO" << std::endl;
    }
}

int main()
{
    GridSearch();
    return 0;
}