#include <iostream>
#include <string>

// 06:19:54PM -> 18:19:54
void ConvertTime()
{
    std::string time;
    std::cin >> time;
    if(time.size() != 10) {
        std::cout << "Data not in the right format" << std::endl;
        return;
    }
    std::string hh(time.substr(0,2)),mm(time.substr(3,2)),ss(time.substr(6,2));
    bool isAm = (time[8] == 'A');
    if(isAm)
        std::cout << ((hh == "12") ? "00" : hh);
    else
        std::cout << ((hh == "12") ? 12 : (atoi(hh.c_str()) + 12));

    std::cout << ":" << mm << ":" << ss;
}

int main()
{
    ConvertTime();
    return 0;
}