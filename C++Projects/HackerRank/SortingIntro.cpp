#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
#include <iterator>

using namespace std;

void IntroSorting()
{
    int V, n;
    std::cin >> V >> n;
    std::vector<int> vec(std::istream_iterator<int>(std::cin), std::istream_iterator<int>());
    int first = 0;
    int last = n - 1;
    int mid = 0;
    while(first <= last) {
        mid = (last+first) >> 1;
        if(V < vec[mid]) 
            last = mid - 1;
        else if(V > vec[mid]) 
            first = mid + 1;
        else {
            std::cout << mid << std::endl;
            break;
        }
    }
}
    


int main() {
    IntroSorting();
    return 0;
}

