#include <iostream>
#include <string>

void SherlockAndBeast()
{
    int T;
    std::cin >> T;
    while(T--) {
        int N;
        std::cin >> N;
        int y = N;
        bool found = false;
        if(N < 3) {
            std::cout << -1 << std::endl;
            continue;
        }
        if(N % 15 == 0 || N % 3 == 0) {
            y = 0;
        } else {
            // 1. find largest multiple of 3 < N
            // x = number of 5's N-x = number of 3's
            for(int x = N - (N % 3); x > 0; x -= 3) {
                if((N-x) % 5 == 0) {
                    y = N-x;
                    found = true;
                    break;
                }
            }
            if(!found) {
                if(N % 5 != 0) {
                    std::cout << -1 << std::endl;
                    continue;
                }
            }
        }
        const std::string temp(std::string(N-y,'5')+std::string(y,'3'));
        std::cout << temp << std::endl;
    }
}

int main()
{
    SherlockAndBeast();
    return 0;
}