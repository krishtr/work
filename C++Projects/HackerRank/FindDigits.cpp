#include <iostream>
#include <string>
#include <stdlib.h>

int main()
{
    int T;
    std::cin >> T;
    while(T--) {
        std::string num;
        std::cin >> num;
        long long N = strtoll(num.c_str(), nullptr, 10);
        int count = 0;
        for(auto c : num) {
            if(c == '0') continue;
            char ch[] = {c};
            int n = atoi(ch);
            if(N % n == 0) ++count;
        }
        std::cout << count << std::endl;
    }
    return 0;
}
