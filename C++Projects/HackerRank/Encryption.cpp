#include <iostream>
#include <cmath>

void Encryption()
{
    std::string s;
    std::cin >> s;
    double len = std::sqrt(s.length());
    int r = std::floor(len);
    int c = std::ceil(len);
    if(uint(r*c) < s.length())
        r = c;
    for(int i = 0; i < c; ++i) {
        for(int j = 0, k = 0; j < r; k = k+c, ++j) {
            if(uint((i+1)*(j+1)) > s.length())
                continue;
            if(i+k < (int)s.size())
                std::cout << s[i+k];
        }
        if(i != (c-1))
            std::cout << " ";
    }
    std::cout << std::endl;
}

int main()
{
    Encryption();
    return 0;
}
