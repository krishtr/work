#include <iostream>
#include <string>

void CavityMap()
{
    int n;
    std::cin >> n;
    std::vector<std::string> grid(n);
    for(int i = 0; i < n; ++i)
        std::cin >> grid[i];
    std::cout << grid[0] << std::endl;
    for(int i = 1; i < n-1; ++i) {
        std::string temp(grid[i]);
        for(int j = 1; j < n-1; ++j) {
            if(grid[i][j] > grid[i][j-1] && grid[i][j] > grid[i][j+1]
            && grid[i][j] > grid[i-1][j] && grid[i][j] > grid[i+1][j])
                temp[j] = 'X';
        }
        std::cout << temp << std::endl;
    }
    if(n > 1)
        std::cout << grid[n-1] << std::endl;
}

int main()
{
    CavityMap();
    return 0;
}