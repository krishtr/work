#include <iostream>
#include <math.h>

int digits(int N)
{
    int count = 0;
    while(N) N /= 10, ++count;
    return count;
}

bool isKaprekar(long long int i)
{
    const long long int N = i*i;
    const long long int d = digits(i);
    const long long int r = N % (int) pow(10,d);
    const long long int l = (N - r) / (int)pow(10,d);
    return (r+l) == i;
}

int main() {
    int p,q;
    std::cin >> p >> q;
    long long int count = 0;
    for(long long int i = p; i <= q; ++i) {
        if(isKaprekar(i))
            std::cout << i << " ", ++count;
    }
    if(count == 0)
        std::cout << "INVALID RANGE" << std::endl;
   
    return 0;
}
