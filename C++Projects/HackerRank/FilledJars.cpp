#include <iostream>

void FilledJars()
{
    long long int N, M;
    std::cin >> N >> M;
    long long int result = 0;
    for(int i = 0; i < M; ++i) {
        long long int a,b,k;
        std::cin >> a >> b >> k;
        result += (b-a+1)*k;
    }
    std::cout << (long long int)(result / N) << std::endl;
}

int main()
{
    FilledJars();
    return 0;
}