#include <vector>
#include <sstream>
#include <iterator>
#include <algorithm>

typedef unsigned long long int ull;

namespace
{
    void makeSpace(std::vector<ull>& vec)
    {
        vec.resize(vec.size() + 1);
        for(int i = vec.size() - 2, j = vec.size() - 1; i >= 0; --i, --j)
            vec[j] = vec[i];
    }
}

struct BigNum
{
    BigNum() = default;
    BigNum(const BigNum& rhs) = default;
    BigNum operator*(const BigNum& rhs) const;
    BigNum operator*(ull n) const;
    BigNum operator+(const BigNum& rhs) const;
    BigNum(std::vector<ull>&& rhs) : vec(std::move(rhs)) {}
    std::string toString() const; // for testing
    std::vector<ull> vec;
};

std::string BigNum::toString() const
{
    std::ostringstream ss;
    std::copy(vec.rbegin(), vec.rend(), std::ostream_iterator<ull>(ss));
    return ss.str();
}

BigNum BigNum::operator+(const BigNum& rhs) const
{
    if(rhs.vec.size() == 1 && rhs.vec[0] == 0)
        return *this;
    if(vec.size() == 1 && vec[0] == 0)
        return rhs;
    const ull max = std::max(vec.size(), rhs.vec.size());
    const ull rsize = (ull)rhs.vec.size();
    const ull size = (ull)vec.size();
    ull carry = 0;
    BigNum result;
    for(ull i = 0; i < max; ++i) {
        const ull n1 = (i >= size) ? 0 : vec[i];
        const ull n2 = (i >= rsize) ? 0 : rhs.vec[i];
        const ull temp = n1 + n2 + carry;
        result.vec.push_back(temp % 10);
        carry = temp / 10;
    }
    if(carry > 0)
        result.vec.push_back(carry);
    return result;
}

// 123*6 = 738
BigNum BigNum::operator*(ull n) const
{
    if(n == 0)
        return BigNum(std::vector<ull>{0});
    if(n == 1)
        return *this;
    BigNum result;
    ull carry = 0;
    const ull size = (ull)vec.size();
    for(ull i = 0; i < size; ++i) {
        const ull temp = vec[i] * n + carry;
        result.vec.push_back(temp % 10);
        carry = temp / 10;
    }
    if(carry > 0)
        result.vec.push_back(carry);
    return result;
}

// 123*56 = 123*6 + 1230*5
BigNum BigNum::operator*(const BigNum& rhs) const
{
    BigNum result(std::vector<ull>{0});
    BigNum copy(*this);
    BigNum placeholder;
    const ull rsize = (ull)rhs.vec.size();
    for(ull i = 0; i < rsize; ++i) {
        result = result + (copy * rhs.vec[i]);
        //copy.vec.push_back(0);
        makeSpace(copy.vec);
        copy.vec[0] = 0;
    }
    return result;
}

