#include <iostream>
#include <math.h>

int main()
{
    int N;
    std::cin >> N;
    while(N--) {
        int px, py, qx, qy, rx, ry;
        std::cin >> px >> py >> qx >> qy;
        if(qx == 0 && qy == 0) {
            rx = -px;
            ry = -py;
        } else if (qx == px) {
            rx = qx;
            const int d = qy - py;
            ry = qy + d;
        } else if (qy == py) {
            ry = qy;
            const int d = qx - px;
            rx = qx + d;
        } else {
            const double M = (qy - py) / (qx - px); // slope
            const double D = sqrt(pow((qx - px),2) + pow((qy - py),2));
            rx = (2*D / sqrt(M*M + 1)) + px;
            ry = M*(rx - px) + py;
        }
        std::cout << rx << " " << ry << std::endl;        
    }
    return 0;
}
