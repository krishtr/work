#include <iostream>
#include <vector>
#include <cassert>
#include <ctime>
#include "BigNum.hpp"

// 1, 2, 3 * 9 = 1,1,0,7
void VecMult(std::vector<unsigned long long int>& vec, unsigned long long int n)
{
    unsigned long long int carry = 0;
    const unsigned long long int size = vec.size();
    vec.resize(size + 1);
    for(unsigned long long int i = size - 1; i >= 0; --i) {
        const unsigned long long int ans = vec[i] * n + carry;
        vec[i] = ans % 10;
        carry = ans / 10;
    }
    if(carry > 0)
        vec[0] += carry;
}

void RunModFibo()
{
    unsigned long long int a,b,N;
    std::cin >> a >> b >> N;
    auto begin = clock();
    BigNum C, A(std::vector<unsigned long long int>{a}), B(std::vector<unsigned long long int>{b});
    unsigned long long int count = 2; // we already know a and b
    while(true) {
        C = B * B + A;
        if(++count == N) break;
        A = B;
        B = C;
    }
    std::cout << C.toString() << std::endl;
    auto end = clock();
    auto elapsed_sec = double(end - begin) / CLOCKS_PER_SEC;
    std::cout << "program takes: " << elapsed_sec << " secs" << std::endl;
}

/*
void testBigNumAddition()
{
    BigNum vec1 {std::vector<unsigned long long int>{0,0,5,1}};
    BigNum vec2 {std::vector<unsigned long long int>{0,5}};
    BigNum vec3 = vec1 + vec2;
    assert(vec3.toString() == "1550");
    vec1 = std::vector<unsigned long long int>{0};
    vec3 = vec1 + vec2;
    assert(vec3.toString() == "50");
    vec1 = std::vector<unsigned long long int>{0,5};
    vec3 = vec1 + vec2;
    assert(vec3.toString() == "100");
    vec2 = {0};
    vec1 = {0};
    vec3 = vec1 + vec2;
    assert(vec3.toString() == "0");
    vec1 = {9,9,9,9};
    vec2 = {9,9,9,9};
    vec3 = vec1 + vec2;
    assert(vec3.toString() == "19998");
    vec1 = {9,9,9,9,9,9,9,9,9,9,9,9};
    vec2 = {9,9,9,9,9,9,9,9,9,9,9,9};
    vec3 = vec1 + vec2;
    assert(vec3.toString() == "1999999999998");
    std::cout << "All tests passed" << std::endl;
}

void testNormalMultiplication()
{
    BigNum vec1 = std::vector<unsigned long long int>{3,2,1};
    unsigned long long int n = 6;
    BigNum vec3 = vec1 * n;
    assert(vec3.toString() == "738");
    n = 0;
    vec3 = vec1 * n;
    assert(vec3.toString() == "0");
    vec1 = {1};
    n = 2;
    vec3 = vec1 * n;
    assert(vec3.toString() == "2");
    vec1 = {0,5};
    n = 2;
    vec3 = vec1 * n;
    assert(vec3.toString() == "100");
    vec1 = {0,0,1};
    n = 100;
    vec3 = vec1 * n;
    assert(vec3.toString() == "10000");
    vec1 = {0,0,4};
    n = 3;
    vec3 = vec1 * n;
    assert(vec3.toString() == "1200");
    //vec1 = {4,0,0};
    //n = 2147483647;
    //vec3 = vec1 * n;
    //std::cout << vec3.toString() << std::endl;
    //assert(vec3.toString() == "858993458800");
    std::cout << "All tests passed" << std::endl;
}

void testBigNumMultiplication()
{
    BigNum vec1 = std::vector<unsigned long long int>{0,0,1};
    BigNum vec2 = std::vector<unsigned long long int>{0,1};
    BigNum result = vec1 * vec2;
    assert(result.toString() == "1000");
    vec1 = {0};
    result = vec1 * vec2;
    assert(result.toString() == "00");
    vec1 = {0,0,1};
    vec2 = {0};
    result = vec1 * vec2;
    assert(result.toString() == "0");
    result = vec1 * vec1;
    assert(result.toString() == "10000");
    vec1 = {2,1,1,2,8,3,3,4,2,1,8,2,6,9,0,3,1,6,6,6,2,4,8};
    result = vec1 * vec1;
    assert(result.toString() == "7100862082718357559748563880517485796441580544");
    std::cout << "All tests passed" << std::endl;
}

std::string toString(const std::vector<unsigned long long int>& vec)
{
    std::ostringstream os;
    std::copy(vec.begin(), vec.end(), std::ostream_iterator<unsigned long long int>(os));
    return os.str();
}

void testVectMult()
{
    std::vector<unsigned long long int> vec {1,2,3};
    unsigned long long int n = 9;
    VecMult(vec,n);
    std::cout << toString(vec) << std::endl;
    assert(toString(vec) == "1107");
    std::cout << "All tests passed" << std::endl;
}
*/
int main()
{
    //testVectMult();
    RunModFibo();
    //testBigNumAddition();
    //testNormalMultiplication();
    //testBigNumMultiplication();
    return 0;
}
