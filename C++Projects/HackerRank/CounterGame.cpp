#include <iostream>
#include <bitset>
#include <algorithm>

int main()
{
    int T;
    std::cin >> T;
    while(T--) {
        unsigned long long int N;
        std::cin >> N;
        int counter = 1;
        while(N != 1) {
            std::bitset<64> bits(N);
            if(bits.count() == 1) { // power of 2
                N >>= 1;
            } else {
                // reduce N by highest power of 2 < N
            }
        }
    }
    return 0;
}
