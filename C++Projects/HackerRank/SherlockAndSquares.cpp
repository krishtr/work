#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

void SherlockAndSquares()
{
    int T;
    std::cin >> T;
    long long int A,B;
    std::vector<int> results;
    for(int i = 0; i < T; ++i) {
        std::cin >> A >> B;
        int result = 0;
        if(A == B) result = 0;
        for(int j = sqrt(A); j <= sqrt(B); ++j)
            if((j*j) >= A && (j*j) <= B)
                ++result;
        results.push_back(result);
    }
    std::copy(results.begin(), results.end(), std::ostream_iterator<int>(std::cout, "\n"));
}

int main()
{
    SherlockAndSquares();
    return 0;
}