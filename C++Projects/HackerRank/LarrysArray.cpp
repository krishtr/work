#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>

void display(const std::vector<int>& vec)
{
    std::copy(vec.begin(), vec.end(), std::ostream_iterator<int>(std::cout, " "));
    std::cout << std::endl;
}

// 1 4 5 6 7 8 3

bool isSortable(const std::vector<int>& sorted,
                std::vector<int>& vec)
{
    if (sorted.size() != vec.size())
        return false;
    int wall = 0;
    while (wall <= (int)vec.size() - 3) {
        //std::cout << "iteration: " << wall << std::endl;
        //std::cout << "----------------------------------" << std::endl;
        // there is still a min element out there so we need to repeat the whole process again
        while (std::distance(vec.begin()+wall, std::min_element(vec.begin()+wall, vec.end())) > 0) { 
            for (int first = 0; first <= (int)vec.size() - 3; ++first) {
                auto start = vec.begin() + first;
                auto iter = std::min_element(start, start+3);
                if (std::distance(start, iter) == 0)
                    continue;
                else 
                    std::rotate(start, iter, start+3);
                //display(vec); // display after every iteration
            }
        }
        if (std::equal(vec.begin(), vec.end(), sorted.begin()))
            return true;
        ++wall;
    }
    return false;
}

void LarrysArray()
{
    int T;
    std::cin >> T;
    while(T--) {
        int N;
        std::cin >> N;
        std::vector<int> vec(N,0);
        for(int i = 0; i < N; ++i) {
            int t;
            std::cin >> t;
            vec[i] = t;
        }
        std::vector<int> sorted(vec);
        std::sort(sorted.begin(), sorted.end());
        std::cout << (isSortable(sorted, vec) ? "YES" : "NO") << std::endl;
    }
}

int main()
{
    LarrysArray();
    return 0;
}
