#include <iostream>
#include <string>
#include <unordered_map>

void TimeInWords()
{
    std::unordered_map<int, std::string> hours {
        {1, "one"},{2, "two"},{3, "three"},{4, "four"},{5, "five"}, {6, "six"},
        {7, "seven"},{8, "eight"},{9, "nine"},{10, "ten"},{11, "eleven"}, {12, "twelve"}
    };
    std::unordered_map<int, std::string> minutes {
        {1,"one"}, {2,"two"},{3,"three"},{4,"four"},{5,"five"},{6,"six"},
        {7,"seven"}, {8,"eight"},{9,"nine"},{10,"ten"},{11,"eleven"},{12,"twelve"},
        {13,"thirteen"},{14,"fourteen"},{15,"quarter"},{16,"sixteen"},{17,"seventeen"},{18,"eighteen"},
        {19,"nineteen"}, {20,"twenty"},{21,"twenty one"},{22,"twenty two"},{23,"twenty three"},{24,"twenty four"},
        {25,"twenty five"}, {26,"twenty six"},{27,"twenty seven"},{28,"twenty eight"},{29,"twenty nine"},{30,"half"},
        {31,"twenty nine"}, {32,"twenty eight"},{33,"twenty seven"},{34,"twenty six"},{35,"twenty five"},{36,"twenty four"},
        {37,"twenty three"}, {38,"twenty two"},{39,"twenty one"},{40,"twenty"},{41,"nineteen"},{42,"eighteen"},
        {43,"seventeen"}, {44,"sixteen"},{45,"quarter"},{46,"fourteen"},{47,"thirteen"},{48,"twelve"},
        {49,"eleven"}, {50,"ten"},{51,"nine"},{52,"eight"},{53,"seven"},{54,"six"},
        {55,"five"}, {56,"four"},{57,"three"},{58,"two"},{59,"one"}
    };
    int h,m;
    std::cin >> h >> m;
    std::string min = (m == 1 || m == 59) ? " minute" : " minutes";
    if(m > 30) {
        if(m == 45)
            std::cout << minutes[m] << " to " << hours[h+1];
        else
            std::cout << minutes[m] << min << " to " << hours[h+1];
    } else {
        if(m == 0)
            std::cout << hours[h] << " o' clock";
        else if(m == 15 || m == 30)
            std::cout << minutes[m] << " past " << hours[h];
        else
            std::cout << minutes[m] << min << " past " << hours[h];
    }
    std::cout << std::endl;
}

int main()
{
    TimeInWords();
    return 0;
}