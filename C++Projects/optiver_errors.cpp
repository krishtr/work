#include <bits/stdc++.h>
using namespace std;

namespace {
    
    constexpr int GRAPH_SZ       = 26;
    constexpr int VALID_INPUT_SZ = 5;
    
    struct Node {
        char data;
        vector<char> children;
        vector<char> parents;
    };
    
    using Graph = vector<Node>;
    unordered_set<string> input_lookup; // for E2
    vector<string> errors; // for storing errors-take the first in multiple errors
    
    Graph graph(GRAPH_SZ, Node());
    
    bool parseInput(const string& input_) 
    {
        // we expect the input format to be (A,B)
        if(input_.size() != VALID_INPUT_SZ) {
            errors.emplace_back("E1");
            return false;
        }
        
        if(input_[0] != '(' || input_[4] != ')' || input_[2] != ',') {
            errors.emplace_back("E1");
            return false;
        }
        
        char parent = input_[1];
        char child  = input_[3];
        
        graph[parent - 'A'].data = parent;
        graph[parent - 'A'].children.push_back(child);
        
        graph[child - 'A'].data = child;
        graph[child - 'A'].parents.push_back(parent);
        
        return true;
    }
    
    void printSExp(const Graph& graph_, char root_)
    {
        cout << "(" << root_;
        auto& children = graph_[root_ - 'A'].children;
        const auto child_sz = children.size();
        if(child_sz == 1) {
            printSExp(graph_, children[0]);
        } else if(child_sz == 2) {
            if(children[0] < children[1]) {
                printSExp(graph_, children[0]);
                printSExp(graph_, children[1]);
            } else {
                printSExp(graph_, children[1]);
                printSExp(graph_, children[0]);
            }
        }
        cout << ")";
    }
}

int main() 
{
    ios::sync_with_stdio(0);
    cin.tie(0);
    string input;
    while(cin >> input) {
        if(input_lookup.count(input) == 1) {
            errors.emplace_back("E2"); // we have already seen this pair before
            continue;
        }
        
        if(!parseInput(input)) {
            continue;
        }
        input_lookup.insert(input);
    }
    
    /* at this stage if the errors are not empty, we already know that its filled
       with either E1 or E2 errors. So there is no need to even check for the higher
       error codes since we only need to print the first listed error
   */
    
    // E3
    if(errors.empty() && any_of(begin(graph), end(graph), [](const Node& n_) {
        return isalpha(n_.data) && isupper(n_.data) && n_.children.size() > 2;
    })) {
        errors.emplace_back("E3");
    }
    
    // E4
    if(errors.empty() && count_if(begin(graph), end(graph), [](const Node& n_) {
        return isalpha(n_.data) && isupper(n_.data) && n_.parents.empty();
    }) > 1) {
        errors.emplace_back("E4");
    }
    
    // E5
    if(errors.empty() && any_of(begin(graph), end(graph), [](const Node& n_) {
        return isalpha(n_.data) && isupper(n_.data) && n_.parents.size() > 1;
    })) {
        errors.emplace_back("E5");
    }
    
    // another case of E5 when there is no root in the structure: (A,B) (B,A)    
    if(errors.empty()) {
        bool no_root = true;
        for(const auto& n_ : graph) {
            if(isalpha(n_.data) && isupper(n_.data)) {
                no_root &= !n_.parents.empty();
            }
        }
        if(no_root) {
            errors.emplace_back("E5");
        }
    }
    
    // check errors
    if(!errors.empty()) {
        if(errors.size() > 1) {
            std::sort(begin(errors), end(errors));
        }
        cout << errors[0] << '\n';
        return 0;        
    }
    
    // Good Case
    const auto& it = find_if(begin(graph), end(graph), [](const Node& n_) {
        return isalpha(n_.data) && isupper(n_.data) && n_.parents.empty();
    });
    
    printSExp(graph, it->data);
    return 0;
}