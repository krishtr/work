/*
2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.

What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
*/

#include <math.h>
#include <vector>

bool IsPrime(long num)
{
	if ( num == 2 )
		return true;

	if ( num % 2 == 0 )
		return false;

	for ( int i = 3; i*i <= num; i += 2 )
		if ( num % i == 0 )
			return false;

	return true;
}

// returns highest power of x <= num
int HighestPower(int x, int num)
{
	for( int i = 1; i < num; i++ )
		if ( pow(double(x), i) >= num )
			return i - 1;
}

void ProjectEulerProblem5( int num )
{
	// find the list of primes from 1 to num and add it to the vector
	std::vector<int> primeList;
	for( int i = 2; i <= num; i++ )
	{
		if ( IsPrime(i) )
			primeList.push_back(i);
	}

	// find the highest prime factor in the range - if you find it, replace
	// the occurance of the factor with the result
	std::vector<int>::iterator iter(primeList.begin());
	std::vector<int>::iterator end(primeList.end());
	int power = 0;
	for( ; iter != end; ++iter )
	{
		if ( (power = HighestPower(*iter, num)) > 1 )
			*iter = pow(double(*iter), power);
	}

	// find the product of all the numbers
	unsigned __int64 product = 1;
	iter = primeList.begin();
	while( iter != end )
		product *= *iter++;

	std::cout << product << std::endl;
}