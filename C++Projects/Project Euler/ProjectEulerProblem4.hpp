/*
*	A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 � 99.
*	Find the largest palindrome made from the product of two 3-digit numbers.
*
*/
#include <iostream>
#include <string>

bool IsPalindrome( std::string num ) {
        auto iter = std::begin(num);
        auto end = std::end(num);
        while (iter != end) {
                if (*iter != *end) {
                        return false;
                }
                ++iter;
                --end;
        }
        return true;
}

std::string Stringize( __int64 num )
{
        char text[32];
        sprintf( text, "%I64u", num );
        return std::string(text);
}

void ProjectEulerProblem4()
{
        __int64 max = 0, iterations = 0;
        for( int i = 999; i > 99; i-- )
        {
                for( int j = 990; j > 99 && (i*j > max); j -= 11 )
                {
                        iterations++;
                        if ( IsPalindrome( Stringize(i*j) ) )
                        {
                                if ( i*j > max )
                                        max = i*j;
                        }
                }
        }

        std::cout << max << std::endl;
        std::cout << iterations << std::endl;
}