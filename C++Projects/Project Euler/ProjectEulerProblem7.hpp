/*
By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6^(th) prime is 13.

What is the 10001^(st) prime number?

*/

#include <iostream>

bool IsPrime(long num)
{
	if ( num % 2 == 0 )
		return false;

	for ( int i = 3; i*i <= num; i += 2 )
		if ( num % i == 0 )
			return false;

	return true;
}

void ProjectEulerProblem7( int index )
{
	// which prime number do you want? (index)
	int idx = 0, i = 0;
	while(true)
	{
		i++;
		if ( IsPrime(i) )
		{
			idx++;
			if ( idx == index )
			{
				std::cout << index << "st prime number is: " << i << std::endl;
				break;
			}
		}
	}
}