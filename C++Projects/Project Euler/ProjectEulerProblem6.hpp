/*
The sum of the squares of the first ten natural numbers is,
1^(2) + 2^(2) + ... + 10^(2) = 385

The square of the sum of the first ten natural numbers is,
(1 + 2 + ... + 10)^(2) = 55^(2) = 3025

Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.

Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.

Solution:
Sum of n squares = (1/6)*n*(n+1)*(2n+1)			=>	(1)
Squares of sum of n numbers = [(1/2)*n*(n+1)]^2	=>	(2)

What we want is - (2) - (1)
which after simplifying comes to (1/12) * n * (n+1) * (3n^2 - n - 2)
*/

#include <iostream>
#include <string>
#include <math.h>

std::string Stringize( double num )
{
	char text[32];
	sprintf( text, "%0.f", num );
	return std::string(text);
}

void ProjectEulerProblem6( int n )
{
	double result = (1.0/12.0) * n * (n+1) * (3*pow(double(n),2) - n - 2);
	std::cout << Stringize(result) << std::endl;
}