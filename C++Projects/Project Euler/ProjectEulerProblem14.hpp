/*
   The following iterative sequence is defined for the set of positive integers:

   n-> n/2 (n is even)
   n-> 3n + 1 (n is odd)

   Using the rule above and starting with 13, we generate the following sequence:

   13  40  20  10  5  16  8  4  2  1
   It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.

   Which starting number, under one million, produces the longest chain?

NOTE: Once the chain starts the terms are allowed to go above one million.

Answer: Number: 837799, Number of terms: 525
 */

#include <iostream>
#include <thread>

int CalculateNumberOfTerms(unsigned int n) {
    if (n == 1) {
        return 1;
    }

    int count = 1;
    while (n != 1) {
        if ((n & 1) == 1) { // note: the same as n % 2 != 0, ie odd check
            n += (n<<1) + 1;
        } else {
            n >>= 1;
        }
        ++count;
    }


    return count;
}

void Start() {
    auto diff = clock();
    int max = 0, result = 0, temp = 0;
    for (int i = 1; i <= 1000000; ++i) {
        temp = CalculateNumberOfTerms(i);
        if ( temp > max) {
            max = temp;
            result = i;
        }
    }

    std::cout << "time : " << clock() - diff << std::endl;
    std::cout << "Number: " << result << ", Number of terms: " << max << std::endl;
}
