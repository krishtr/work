//
//      The sequence of triangle numbers is generated by adding the natural numbers. So the 7th triangle number would be 1 + 2 + 3 + 4 + 5 + 6 + 7 = 28. The first ten terms would be:
//
//      1, 3, 6, 10, 15, 21, 28, 36, 45, 55, ...
//
//      Let us list the factors of the first seven triangle numbers:
//
//      1:  1
//      3:  1,3
//      6:  1,2,3,6
//      10: 1,2,5,10
//      15: 1,3,5,15
//      21: 1,3,7,21
//      28: 1,2,4,7,14,28
//      We can see that 28 is the first triangle number to have over five divisors.
//
//      What is the value of the first triangle number to have over five hundred divisors?
//

#include <iostream>
#include <string>

int GetTriangleNumber (int num) {
        return (num * (num + 1)) >> 1;
}

//
// Return number of divisors
//
int NumberOfDivisors(int num) {
        unsigned int count = 0;
        for (int i = 1; i <= std::sqrt(num); ++i) {
                if ((num % i) == 0) {
                        count += 2;
                }
        }

        return count;
}

void ProjectEulerProblem12() {

        // std::cout << NumberOfDivisors(GetTriangleNumber(10)) << std::endl;
        
        unsigned int num = 0, triangleNumber = 0;
        auto time = clock();
        while (true) {
                triangleNumber = GetTriangleNumber(num++);
                if (NumberOfDivisors(triangleNumber) > 500) {
                        break;
                }
        }
        std::cout << std::endl;
        std::cout << clock() - time << std::endl;
        std::cout << triangleNumber << std::endl;
}