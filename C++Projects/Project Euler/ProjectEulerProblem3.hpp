/*
*	Problem - The prime factors of 13195 are 5, 7, 13 and 29.
*	What is the largest prime factor of the number 600851475143 ?
*
*	Proposed Algorithm-
*	1. Walk through all the factors from 1 to floor(sqrt(n)).
*	2. if IsPrime(i) then remember it.
*
*	Algorithm for Prime-
*	1. If num is even return false
*	2. Walk from i = 1 to num; i += 2 (only odds)
*		a. if num % i = 0; return false;
		b. else continue.
	3. return true;
*/

bool IsPrime(long num)
{
	if ( num == 2 )
		return true;

	if ( num % 2 == 0 )
		return false;

	for ( int i = 3; i*i <= num; i += 2 )
		if ( num % i == 0 )
			return false;

	return true;
}

void ProjectEulerProblem3()
{
	unsigned long long num = 600851475143;
	unsigned long long max = 0;

	for ( int i = 1; i*i <= num; i++ )
		if ( num % i == 0 && IsPrime(i) )
			max = i;

	std::cout << max << std::endl;
}