/*
A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
a^(2) + b^(2) = c^(2)

For example, 3^(2) + 4^(2) = 9 + 16 = 25 = 5^(2).

There exists exactly one Pythagorean triplet for which a + b + c = 1000.
Find the product abc.

*/

#include <iostream>
#include <math.h>

void ProjectEulerProblem9()
{
	int iter = 0;
	for( int i = 1; i <= 500; i++ )
	{
		for( int j = 1; j <= 500; j++ )
		{
			iter++;
			if ( (pow(double(i),2) + pow(double(j),2) == pow(double(1000-i-j),2)))
			{
				std::cout << "Product = " << i*j*(1000-i-j) << ". Numbers are: " << i << ", " << j << ", " << (1000-i-j) << ". Iterations: " << iter << std::endl;
				goto exit;
			}
		}
	}

exit:
	return;
}