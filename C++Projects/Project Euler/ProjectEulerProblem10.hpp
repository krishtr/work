/*
The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.

Find the sum of all the primes below two million.
*/

#include <iostream>

bool IsPrime(unsigned __int64 num)
{
	if ( num == 2 )
		return true;

	if ( num % 2 == 0 )
		return false;

	for ( unsigned __int64 i = 3; i*i <= num; i += 2 )
		if ( num % i == 0 )
			return false;

	return true;
}

void ProjectEulerProblem10(int num)
{
	unsigned __int64 sum = 0;
	for ( unsigned __int64 i = 2; i <= num; i++ )
	{
		if  (IsPrime(i))
			sum += i;
	}

	std::cout << sum << std::endl;
}