#include <iostream>
#include <algorithm>
#include <string>
#include <queue>

struct Node {
        Node(std::string i) : data(i), level(-1), left(nullptr), right(nullptr) {}
        std::string data;
		int level;
        Node* left;
        Node* right;
};

Node* GetMeNode(std::string data) {
        Node *node = new Node(data);
        return node;
}

Node *loc1, *loc2;

Node* StaticConstructBinaryTree() {
        Node *root = GetMeNode("F");
        root->left = GetMeNode("B");
		loc2 = root->left;
        root->right = GetMeNode("G");

        root->left->left = GetMeNode("A");
		loc1 = root->left->left;
        root->left->right = GetMeNode("D");
        root->left->right->left = GetMeNode("C");
        root->left->right->right = GetMeNode("E");

        root->right->right = GetMeNode("I");
        root->right->right->left = GetMeNode("H");

        return root;
}

void InfixTraverseBinaryTree(Node *head) {
        if (head == nullptr) return;

        InfixTraverseBinaryTree(head->left);
        std::cout << head->data << ", ";
        InfixTraverseBinaryTree(head->right);
}

void PrefixTraverseBinaryTree(Node *head) {

        if (head == nullptr) return;
        
        std::cout << head->data << ", ";
        PrefixTraverseBinaryTree(head->left);
        PrefixTraverseBinaryTree(head->right);
}

void PostfixTraverseBinaryTree(Node *head) {

        if (head == nullptr) return;
        
        PostfixTraverseBinaryTree(head->left);
        PostfixTraverseBinaryTree(head->right);
        std::cout << head->data << ", ";
}

void BreadthFirstTraversal(Node *head) {

        if (head == nullptr) return;

		int level = 0;
        std::queue<Node*> queue_;
		head->level = level;
        queue_.push(head);
        while (!queue_.empty()) {
                Node *root = queue_.front();
                //std::cout << "(" << root->data << "," << root->level << ")" << ", ";
                if (!(root->left == nullptr)) {
						root->left->level = root->level + 1;
                        queue_.push(root->left);
				}
                if (!(root->right == nullptr)) {
						root->right->level = root->level + 1;
                        queue_.push(root->right);
				}

                queue_.pop();
        }
}

struct EulerData {
		Node *location;
		int level;
		EulerData(Node *loc, int l) : location(loc), level(l) {}
};

std::vector<EulerData> eulereanVector;
void ConstructEulerianPath(Node *root) {

        if (root == nullptr) return;
		eulereanVector.push_back(EulerData(root, root->level));
        if (!(root->left == nullptr)) {
                ConstructEulerianPath(root->left);
				eulereanVector.push_back(EulerData(root, root->level));
        }

        if (!(root->right == nullptr)) {
                ConstructEulerianPath(root->right);
				eulereanVector.push_back(EulerData(root, root->level));
        }
}

int LCA() {

		Node *root = StaticConstructBinaryTree();

		//
		//		1. Populate level information for each node
		//		2. Construct a vector that represents the Eulerean 
		//		   path of the levels of the tree
		//		3. Given 2 locations of the tree for which LCA needs to be found
		//		4. Locate the 2 locations on the Eulerean vector.
		//		5. Find the minimum value between the 2 locations in the vector
		//		6. Result will give the location of the LCA
		//
		BreadthFirstTraversal(root); // populate the level numbers
        ConstructEulerianPath(root); // construct vector of levels by traversing the Euler path

		auto start = std::find_if(eulereanVector.begin(), eulereanVector.end(), 
				[=](EulerData data) { return data.location == loc1; });
		if (start == eulereanVector.end()) {
				std::cout << "Cannot find %s in the tree" << loc1->data.c_str() 
						<< std::endl;
				return -1;
		}

		auto end = std::find_if(eulereanVector.begin(), eulereanVector.end(), 
				[=](EulerData data) { return data.location == loc2; });
		if (end == eulereanVector.end()) {
				std::cout << "Cannot find %s in the tree" << loc2->data.c_str() 
						<< std::endl;
				return -1;
		}

		if (end - start < 0) {
				std::cout << "Invalid locations, swapping them" << std::endl;
				std::swap(start, end);
		}

		auto resultIter = std::min_element(start, end, 
				[](EulerData lhs, EulerData rhs){ return lhs.level < rhs.level; });

		if (resultIter == end) {
				std::cout << "Result: (" << "DNE" << "," << "-1" << ")" << std::endl;
				return -1;
		}
		std::cout << "Result: (" << resultIter->location->data
				<< "," << resultIter->location->level << ")" << std::endl;
}

int TreeStart() {        
}