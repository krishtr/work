
// https://www.codewars.com/kata/build-tower/train/cpp
std::vector<std::string> buildTower(int floors)
{
    std::vector<std::string> results;
    results.reserve(floors);
    std::string floor(2 * floors - 1, ' ');
    const auto mid = floors - 1;
    for (int i = 0; i < floors; ++i) {
        if (i > 0)
            floor[mid + i] = '*';
        floor[mid - i] = '*';
        results.emplace_back(floor);
    }
    return results;
}

// https://www.codewars.com/kata/sum-of-odd-numbers/train/cpp
long long rowSumOddNumbers(unsigned n)
{
    long long result = 0;
    for (unsigned i = n * (n - 1) + 1, count = 0; count < n; ++count, i += 2) {
        result += i;
    }
    return result;
}

// https://www.codewars.com/kata/sum-of-the-first-nth-term-of-series/train/cpp
std::string seriesSum(int n)
{
    // denominator of last term is 3 * (n-1) + 1 = 3n - 2
    // Sum(i = 1 to n) { 1 / (3i-2) }
    double result = 0.0;
    for (int i = 1; i <= n; ++i) {
        result += 1.0 / (3 * i - 2);
    }
    std::stringstream ss;
    ss << std::fixed << std::setprecision(2) << result;
    return ss.str();
}

// https://www.codewars.com/kata/principal-diagonal-vs-secondary-diagonal/train/cpp
std::string diagonal(const std::vector<std::vector<int>>& matrix)
{
    if (matrix.empty())
        return "";
    long long p = 1, s = 0;
    int index = 0;
    const size_t sz = matrix[1].size();
    for (auto v : matrix) {
        p += v[index];
        s += v[sz - index - 1];
        ++index;
    }
    const long long diff = p - s;
    return diff == 0
        ? "Draw!"
        : diff < 0 ? "Secondary diagonal win!" : "Principal diagonal win!";
}

// https://www.codewars.com/kata/complementary-dna/train/cpp
std::string DNAStrand(const std::string& dna)
{
    // A <-> T; C <-> G
    std::string result(dna);
    std::transform(result.begin(), result.end(), result.begin(), [](char c) {
        switch (c) {
        case 'C':
            return 'G';
        case 'G':
            return 'C';
        case 'A':
            return 'T';
        case 'T':
            return 'A';
        default:
            return ' ';
        }
    });
    return result;
}

// https://www.codewars.com/kata/consecutive-strings/train/cpp
std::string longestConsec(std::vector<std::string>& strarr, size_t k)
{
    const auto sz = strarr.size();
    if (strarr.empty() || k > sz || k <= 0)
        return {};
    std::string result;
    for (size_t i = 0; i + k <= sz; ++i) {
        std::stringstream ss;
        std::copy(&strarr[i], &strarr[i + k], std::ostream_iterator<std::string>(ss, ""));
        if (ss.str().size() > result.size()) {
            result = ss.str();
        }
    }
    return result;
}

// https://www.codewars.com/kata/are-they-the-same/train/cpp
bool comp(std::vector<int> lhs, std::vector<int> rhs)
{
    // sort lhs, square it and then find the diff with rhs
    std::transform(lhs.begin(), lhs.end(), lhs.begin(),
        [](int i) { return i * i; } );
    std::sort(lhs.begin(), lhs.end());
    std::sort(rhs.begin(), rhs.end());
    std::vector<int> temp;
    std::set_difference(rhs.begin(), rhs.end(),
        lhs.begin(),lhs.end(),
        std::back_inserter(temp));
    return temp.empty();
}

// https://www.codewars.com/kata/sum-of-numbers-from-0-to-n/train/cpp
std::string showSequence(int n)
{
    // given N: print 0+1+2....+N = <ans>
    std::stringstream ss;
    if(n < 0) ss << n << "<0";
    else if(n == 0) ss << n << "=0";
    else {
        long long result = n * (n + 1) * 0.5;
        for(int i = 0; i <= n; ++i) {
            ss << i;
            if(i != n) ss << "+"; 
        }
        ss << " = " << result;
    }
    return ss.str();
}

namespace {
    const std::unordered_map<char, int> lookup {
        {'a',1}, {'b',2}, {'c',3}, {'d',4}, {'e',5},
        {'f',6}, {'g',7}, {'h',8}, {'i',9}, {'j',10},
        {'k',11}, {'l',12}, {'m',13}, {'n',14}, {'o',15},
        {'p',16}, {'q',17}, {'r',18}, {'s',19}, {'t',20},
        {'u',21}, {'v',22}, {'w',23}, {'x',24}, {'y',25},
        {'z',26}
    };
}

// https://www.codewars.com/kata/digital-cypher/train/cpp
std::vector<int> encode(const std::string& str, int n)
{
    std::vector<int> result;
    for(const auto& c : str) {
        result.emplace_back(lookup.at(c));
    }
    size_t index = 0;
    const std::string key = std::to_string(n);
    for(auto & elem : result) {
        elem += key[index++] - '0';
        if(index == key.size()) index = 0;
    }
    return result;
}

// https://www.codewars.com/kata/abbreviate-a-two-word-name/train/cpp
std::string abbrevName(const std::string& name)
{
    /*
    std::transform(name.begin(), name.end(), name.begin(), 
        [](char c) { return std::toupper(c); });
    std::istringstream ss(name);
    std::vector<std::string> names;
    std::transform(std::istream_iterator<std::string>(ss),
        std::istream_iterator<std::string>(),
        std::back_inserter(names),
        [](const std::string& str) { return str; });
    return std::string(1,names[0][0]) + "." + std::string(1,names[1][0]);
    */
    std::istringstream ss(name);
    std::vector<std::string> names;
    std::copy(std::istream_iterator<std::string>(ss),
        std::istream_iterator<std::string>(),
        std::back_inserter(names));
    return { static_cast<char>(std::toupper(names[0][0])), 
             '.', 
             static_cast<char>(std::toupper(names[1][0]))
           };
}

using ull = unsigned long long;
std::string toString(const std::vector<std::vector<ull>>& vec) {
    std::stringstream ss;
    for(const auto& v : vec) {
        ss << "(";
        for(size_t j = 0; j < v.size(); ++j) {
            ss << v[j];
            if(j != v.size() - 1) ss << ",";
        }
        ss << ")";
    }
    return ss.str();
}

// https://www.codewars.com/kata/common-denominators/train/cpp
std::string convertFrac(std::vector<std::vector<ull>> &lst) {
    ull lcm = 1;
    for(const auto& vec: lst) { lcm = std::lcm(vec[1], lcm); }
    for(auto& vec : lst) {
        vec[0] *= lcm / vec[1];
        vec[1] = lcm;
    }
    return toString(lst);
}

using ll = long long;

// https://www.codewars.com/kata/square-into-squares-protect-trees/train/cpp
std::vector<ll> decompose(ll num) // saw a python solution online and converted that to C++
{ // need to attempt this all by myself

/* Looks like we can first come up with a recursive solution
   and try and "unroll" that into a solution with normal
   loops
*/
    ll goal = 0;
    std::vector<ll> results;
    results.emplace_back(num);
    while(!results.empty()) {
        ll current = results.back();
        results.pop_back();
        goal += current * current;
        for(ll i = current - 1; i > 0; --i) {
            if(goal - (i*i) >=0) {
                goal -= i*i;
                results.emplace_back(i);
                if(goal == 0) {
                    std::sort(results.begin(), results.end());
                    return results;
                }
            }
        }
    }
    return results;
}

// https://www.codewars.com/kata/ball-upwards/train/cpp
int maxBall(int v0)
{
    if(v0 == 0) return 0;
    // convert to m/s
    const double v = static_cast<double>(v0) * (5/18.0);
    const double result = (v * 10.0) / 9.81;
    return std::round(result);
}

// https://www.codewars.com/kata/help-your-granny/train/cpp
typedef std::unordered_map<std::string, double> stringdblmap;
int tour(const std::vector<std::string> &arrFriends, 
         const std::vector<std::vector<std::string>> &ftwns,
         const stringdblmap &h)
{
    static stringdblmap ftodistmap;
    double total = 0.0;
    for(size_t i = 0; i < arrFriends.size(); ++i) {
        const std::string& fr = arrFriends[i];
        const auto iter = std::find_if(ftwns.begin(), ftwns.end(),
                                [&](const std::vector<std::string>& vec) {
                                        const auto& it = std::find(vec.begin(), vec.end(), fr);
                                        return it != vec.end(); 
                                 });
        double dist = 0.0;
        if(iter != ftwns.end()) {
            std::string twn;
            if(iter->size() > 1) {
                assert((*iter)[0] == fr);
                twn = (*iter)[1];
            }
            const auto it = h.find(twn);
            if(it != h.end()) dist = it->second;
        }
        ftodistmap[fr] = dist;
        if(i == 0) {
            total += dist;
            continue;
        }
        const auto prevDist = ftodistmap[arrFriends[i-1]];
        if(iter == ftwns.end()) {
            if(i == arrFriends.size() - 1) dist = prevDist;
            else continue;
        }
        total += std::sqrt(dist*dist - prevDist*prevDist);
        if(i == (arrFriends.size() - 1))
            total += dist;
    }
    return total;
}

constexpr double PI = 3.14159265;

double foo(double x)
{
    double sine = std::sin(x);
    return 1.5 *  std::pow(sine, 3);
}

// https://www.codewars.com/kata/simpsons-rule-approximate-integration/train/cpp
double simpson(int n)
{
    constexpr double b = PI;
    constexpr double a = 0.0;
    double h = (b - a) / static_cast<double>(n);
    double result = foo(a) + foo(b);
    double total = 0.0;
    for(int i = 1; i <= n/2; ++i) {
        total += foo(a + (2 * i - 1) * h);
    }
    result += 4 * total;
    total = 0.0;
    for(int i = 1; i <= (n/2) - 1; ++i) {
        total += foo(a + 2 * i * h);
    }
    result += 2 * total;
    return result * h / 3.0;
}

// https://www.codewars.com/kata/pi-approximation/train/cpp
#define M_PI 3.14159265358979323846  /* pi */
std::string iterPi(double epsilon)
{
    long long iterations = 0;
    double result = 0.0;
    while(std::abs(M_PI - (4.0 * result)) > epsilon) {
        result += std::pow(-1, iterations) * 1.0 / (2.0 * iterations + 1.0);
        ++iterations;
    }
    std::stringstream ss;
    ss << "[" << iterations << ", " 
       << std::fixed << std::setprecision(10) 
       << 4 * result << "]";
    return ss.str();
}

// https://www.codewars.com/kata/delete-occurrences-of-an-element-if-it-occurs-more-than-n-times/train/cpp
std::vector<int> deleteNth(std::vector<int> arr, int n)
{
    static int lookup[255];
    std::fill(std::begin(lookup), std::end(lookup), 0);
    std::vector<int>::iterator iter = arr.begin(), end = arr.end();
    for(; iter != end;) {
        lookup[*iter]++;
        if(lookup[*iter] > n) {
            iter = arr.erase(iter);
            end = arr.end();
        } else ++iter;
    }
    return arr;
}

// https://www.codewars.com/kata/phone-directory/train/cpp
std::string phone(const std::string& orgdr, std::string num)
{
    const auto getName = [](const std::string& line) {
        std::regex rgx("\\<(\\w.*)\\>");
        std::smatch match;
        if(std::regex_search(line.begin(), line.end(), match, rgx))
            return match[1].str();
        return std::string();
    };
    const auto getAddress = [&](std::string line, const std::string& name) {
        const std::string fullname = "<" + name + ">";
        size_t pos = line.find(fullname);
        if(pos != std::string::npos)
            line.erase(pos, fullname.size());
        const std::string fullnum = "+" + num;
        pos = line.find(fullnum);
        if(pos != std::string::npos)
            line.erase(pos, fullnum.size());
        std::replace_if(line.begin(), line.end(), [](char c) { return ispunct(c) && c != '-' && c != '.'; }, ' ');
        std::stringstream ss(line), out;
        std::string word;
        while(ss >> word) {
            out << word << " ";
        }
        line = out.str();
        line.erase(std::find_if(line.rbegin(), line.rend(), 
                        [](char c) { return !isspace(c); }).base(),
                   line.end());
        return line;
    };
    std::istringstream ss(orgdr);
    std::string name, address, line;
    size_t matches = 0;
    while(std::getline(ss, line)) {
        const size_t pos = line.find(num);
        if(pos == std::string::npos) continue;
        ++matches;
        if(!(matches > 1)) {
            name = getName(line);
            address = getAddress(line, name);
        }
    }
    std::ostringstream os;
    if(matches == 0) {
        os << "Error => Not found: " << num;
    } else if(matches > 1) {
        os << "Error => Too many people: " << num;
    } else {
        os << "Phone => " << num << ", Name => " << name << ", Address => " << address;
    }    
    return os.str();
}

static std::unordered_map<long long, std::vector<long long>>
lookup = { {5, {6,5,4,3,2,1}}, {4, {4,3,2,1}}, {} };

std::vector<long long> get_prod_vect(long long n)
{
    if(n == 3) return { 3, 2, 1 };
    return {};
}

// https://www.codewars.com/kata/getting-along-with-integer-partitions/train/cpp


// https://www.codewars.com/kata/is-my-friend-cheating/train/cpp
using ll = long long;
std::vector<std::vector<ll>> removNb(ll n)
{
    const ll S = n * (n + 1) * 0.5;
    std::vector<std::vector<ll>> results;
    for(int i = n; i >= 1; --i) {
        const double b = (S - i) / (1.0 + i);
        if(b <= n && (b - static_cast<int>(b)) == 0.0) {
            results.emplace_back(std::vector<ll> {i, b});
        }
    }
    return results;
}

namespace
{
    const std::unordered_map<std::string, long> parse_lookup {
        {"zero",0},{"one",1},{"two",2},{"three",3},{"four",4},{"five",5},
        {"six",6},{"seven",7},{"eight",8},{"nine",9},{"ten",10},{"eleven",11},
        {"twelve",12},{"thirteen",13},{"fourteen",14},{"fifteen",15},{"sixteen",16},{"seventeen",17},
        {"eighteen",18},{"nineteen",19},{"twenty",20},{"thirty",30},{"forty",40},{"fifty",50},
        {"sixty",60},{"seventy",70},{"eighty",80},{"ninety",90},{"hundred",100},{"thousand",1000},
        {"million",1000000}
    };
}

// https://www.codewars.com/kata/parseint-reloaded/train/cpp
long parse_int(std::string number)
{
    std::replace(number.begin(), number.end(), '-', ' ');
    std::stringstream ss(number);
    std::string word;
    std::vector<std::string> words;
    while(ss >> word) { words.emplace_back(word); }
    std::stack<long> nums;
    for(const auto& word : words) {
        if(word == "and") continue;
        const long num = parse_lookup.find(word)->second; // cannot be invalid word
        if(word == "hundred") {
            const long temp = nums.top();
            nums.pop();
            nums.push(temp * num);
        } else if(word == "thousand" || word == "million") {
            long total = 0;
            while(!nums.empty()) {
                total += nums.top();
                nums.pop();
            }
            nums.push(total * num);
        } else { nums.push(num); }
    }
    long result = 0;
    while(!nums.empty()) {
        result += nums.top();
        nums.pop();
    }
    return result;
}

// https://www.codewars.com/kata/snail/train/cpp
std::vector<int> snail(std::vector<std::vector<int>> array) {
    const size_t N = array[0].size();
    std::vector<int> results;
    int A = 0, B = N - 1, X = 0, Y = N - 1;
    while(A <= B) {
        for(int i = A; i <= B; ++i)
            results.emplace_back(array[X][i]);
        ++X;
        for(int i = X; i <= Y; ++i)
            results.emplace_back(array[i][B]);
        --B;
        for(int i = B; i >= A; --i)
            results.emplace_back(array[Y][i]);
        --Y;
        for(int i = Y; i >= X; --i)
            results.emplace_back(array[i][A]);
        ++A;
    }
    return results;
}

// https://www.codewars.com/kata/rail-fence-cipher-encoding-and-decoding/train/cpp
std::string encode_rail_fence_cipher(std::string str, int n) {
    if(str.empty()) return str;
    std::string result;
    result.reserve(str.size());
    const int first_rail = 0, last_rail = n - 1;
    int step = 0, counter = 1;
    for(int i = first_rail; i <= last_rail; ++i) {
        size_t j = i;
        while(j < str.size()) {
            if(i == first_rail || i == last_rail) {
                step = (n - 1) * 2;
            } else {
                if((counter & 1) == 1)
                    step = (last_rail - i) * 2;
                else
                    step = (i - first_rail) * 2;
            }
            result.push_back(str[j]);
            j += step;
            ++counter;
        }
        counter = 1;
    }
    return result;
}

std::string decode_rail_fence_cipher(std::string str, int n) {
    if(str.empty()) return str;
    std::string result;
    result.resize(str.size());
    const int first_rail = 0, last_rail = n - 1;
    int step = 0, counter = 1, index = 0;
    for(int i = first_rail; i <= last_rail; ++i) {
        size_t j = i;
        while(j < str.size()) {
            if(i == first_rail || i == last_rail) {
                step = (n - 1) * 2;
            } else {
                if((counter & 1) == 1)
                    step = (last_rail - i) * 2;
                else
                    step = (i - first_rail) * 2;
            }
            result[j] = str[index++];
            j += step;
            ++counter;
        }
        counter = 1;
    }
    return result;
}

// https://www.codewars.com/kata/sum-of-intervals/train/cpp
using pair = std::pair<int,int>;
int sum_intervals(std::vector<pair> vec) {
    std::sort(vec.begin(), vec.end(), 
        [](const pair& lhs, const pair& rhs) { return lhs.first < rhs.first; });
    vec.erase(std::unique(vec.begin(),vec.end(), [](const pair& lhs, const pair& rhs)
        { return lhs.first == rhs.first && lhs.second == rhs.second; }), vec.end());
    int result = 0;
    auto iter = vec.begin(), next = vec.begin();
    auto max_second = iter->second;
    while(iter != vec.end()) {
        while(next != vec.end() 
            && (next->second <= max_second || next->first <= max_second)) {
            max_second = std::max(next->second, max_second);
            ++next;
        }
        result += max_second - iter->first;
        iter = next;
        max_second = iter->second;
    }
    return result;
}

// https://www.codewars.com/kata/adding-big-numbers/train/cpp
std::string add(std::string a, std::string b) {
    std::string& first = (a.size() >= b.size()) ? a : b;
    const std::string& second = (a.size() < b.size()) ? a : b;
    first.erase(0, std::min(first.find_first_not_of('0'), first.size() - 1));
    auto fiter = first.crbegin(), siter = second.crbegin();
    int carry = 0;
    std::string result;
    for(; siter != second.crend(); ++siter, ++fiter) {
        int temp = (*siter - '0' + *fiter - '0') + carry;
        result.push_back(temp % 10 + '0');
        carry = temp / 10;
    }
    while(fiter != first.crend()) {
        int temp = *fiter++ - '0' + carry;
        result.push_back(temp % 10 + '0');
        carry = temp / 10;
    }
    if(carry > 0) result.push_back(carry + '0');
    std::reverse(result.begin(), result.end());
    return result;
}