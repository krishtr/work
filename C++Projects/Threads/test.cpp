#include <iostream>
#include <queue>
#include <condition_variable>
#include <fstream>
#include <thread>
#include <mutex>
#include <chrono>
#include <future>
#include <iterator>
#include <list>
#include <algorithm>

template<class T>
class threadsafe_queue
{
private:
    std::queue<int> data_queue;
    std::condition_variable cond;
    std::mutex mut;
public:
    void push(const T& t)
    {
        std::lock_guard<std::mutex> lock(mut);
        data_queue.push(t);
        cond.notify_one();
    }
    void wait_pop(T& value)
    {
        std::unique_lock<std::mutex> lock(mut);
        cond.wait(lock, [this] { return !data_queue.empty(); });
        value = data_queue.front();
        data_queue.pop();
        lock.unlock();
    }
};

threadsafe_queue<int> data_queue;
void producer()
{
    std::cout << "producer..." << '\n';
    for(int i = 0; i < 500; ++i) {
        if(i % 5 == 0) data_queue.push(i);
        std::this_thread::sleep_for(std::chrono::milliseconds(50));
    }
}

void consumer()
{
    std::cout << "consumer..." << '\n';
    while(true) {
        int data = 0;
        data_queue.wait_pop(data);
        std::cout << "Got multiple of 5: " << data << '\n';
        if(data == 495) break;
    }
}

int calculateVal(const std::string& filename)
{
    std::fstream file(filename);
    int checksum = 0;
    std::string line;
    while(std::getline(file, line))
        for(auto c : line)
            checksum += c;
    return checksum;
}

std::vector<std::packaged_task<int(const std::string&)>> tasks;
std::mutex m;
auto getFuture(const std::function<int(const std::string&)>& f)
{
    std::packaged_task<int(const std::string&)> task(f);
    auto fut = task.get_future();
    std::lock_guard<std::mutex> lock(m);
    tasks.push_back(std::move(task));
    return fut;
}

void processTasks(const std::vector<std::string>& filenames)
{
    for(auto file: filenames) {
        std::packaged_task<int(const std::string&)> task;        
        {
            std::lock_guard<std::mutex> lock(m);
            if(tasks.empty()) continue;
            task = std::move(tasks.back());
            tasks.pop_back();
        }
        task(file);
    }
}

template<typename T>
std::list<T> parallel_qsort(std::list<T> input)
{
    if (input.empty()) return input;
    std::list<T> result;
    result.splice(result.begin(), input, input.begin());
    const T& pivot = *result.begin();
    auto divide_point = std::partition(input.begin(), input.end(),
                                [&](const T& t) { return t < pivot; });
    std::list<T> lower;
    lower.splice(lower.begin(), input, input.begin(), divide_point);
    auto lower_part = std::async(&parallel_qsort<T>, std::move(lower));
    auto higher_part { parallel_qsort(std::move(input)) };
    result.splice(result.begin(), lower_part.get());
    result.splice(result.end(), higher_part);
    return result;
}

int main()
{
    std::list<int> list {-1,2,3,-3,0,100,205,3,7,356,34,87,5,3,8,9,6,66,78,8,3};
    std::copy(list.begin(), list.end(), std::ostream_iterator<int>(std::cout, ","));
    std::cout << '\n';
    std::list<int> result {parallel_qsort(list)};
    std::copy(result.begin(), result.end(), std::ostream_iterator<int>(std::cout, ","));
    std::cout << '\n';
    return 0;
}
