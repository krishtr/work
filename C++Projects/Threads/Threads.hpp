#include <iostream>
#include <thread>
#include <memory>
#include <mutex>
#include <queue>
#include <map>
#include <condition_variable>
#include <string>

namespace LockGuardAdoptLock {
    class X {
        private:
            std::string str;
            std::mutex m;
        public:
            X(const std::string& s) : str(s) {}
            friend void swap (X& lhs, X& rhs) {
                if (&lhs == &rhs) {
                    return;
                }
                std::lock (lhs.m, rhs.m); // locks both the mutexes at the same time

                // transfer the ownership to lock_guard with adopt_lock
                std::lock_guard<std::mutex> (lhs.m, std::adopt_lock);
                std::lock_guard<std::mutex> (rhs.m, std::adopt_lock);

                std::swap (lhs.str, rhs.str);
            }

            void display() const { std::cout << str << std::endl; }

    };

    void swap (X& lhs, X& rhs);
}

namespace UniqueLockDeferLock {
    class X {
        public:
            X (const std::string& s) : str(s) {}
            friend void swap(X& lhs, X& rhs) {
                if (&lhs == &rhs) {
                    return;
                }

                // I'm not going to lock now, I'll lock it later
                std::unique_lock<std::mutex> lock_a (lhs.m, std::defer_lock);
                std::unique_lock<std::mutex> lock_b (rhs.m, std::defer_lock);

                // ok, I'll lock it now
                std::lock (lock_a, lock_b);

                std::swap (lhs.str, rhs.str);
            }
            void display() const { std::cout << str << std::endl; }
        private:
            std::string str;
            std::mutex m;
    };

    void swap(X& lhs, X& rhs);
}

namespace UniqueLockOwnerShipTransfer {
    class test {
        public:
            std::unique_lock<std::mutex> get_lock();
            void InitialiseMap();
            void InsertElement(int key, const std::string& value);
        private:
            std::map<int, std::string> map_;
            std::mutex m;
    };

    void test::InitialiseMap() {
        if (!map_.empty()) {
            return;
        }

        map_.insert (std::make_pair(1, "One"));
        map_.insert (std::make_pair(2, "Two"));
        map_.insert (std::make_pair(3, "Three"));
        map_.insert (std::make_pair(4, "Four"));
        map_.insert (std::make_pair(5, "Five"));
    }

    void test::InsertElement(int key, const std::string& value) {
        auto iter = map_.lower_bound(key);
        if (iter == std::end(map_)) {
            map_.insert (std::make_pair(key, value)); // insert if not found
        } else {
            iter->second = value; // update if found
        }
    }

    std::unique_lock<std::mutex> test::get_lock() {
        std::unique_lock<std::mutex> lock(m); // lock it
        InitialiseMap();
        return lock;
    }
}
struct Person {
    explicit Person (const std::string& n, int a) : name(n), age(a) {}
    std::string name;
    int age;
};

void ThreadFunction() {
    std::mutex personMutex;
    std::map<std::string, Person> map_;

    typedef UniqueLockOwnerShipTransfer::test ULTest;
    ULTest t;

    std::unique_lock<std::mutex> lock_a (personMutex, std::defer_lock);
    std::unique_lock<std::mutex> lock_b (t.get_lock());
    lock_b.unlock();
    std::lock (lock_a, lock_b);

    // modify both the maps
    t.InsertElement(10, "Ten");
    map_.insert(std::make_pair("Krish", Person("Krish", 33)));
}

//
// Exercise to write a Gateway class that controls access to protected data
// Here, if you need to work on protected data, you need to get an instance
// to work on the data. This instance can only be got from the GetGateKeeperObject().
// You cannot create an object of this class as the ctor is private. When the instance
// is created the lock is active and when the scope exits, the object is destroyed,
// unlocking the lock along with it. Move semantics is coded to make it move friendly
//
namespace GatewayClassUniqueLock {
    class GateKeeper {
        public:
            ~GateKeeper();
            static GateKeeper GetGateKeeperObject();
            void WorkOnProtectedData();
            GateKeeper(GateKeeper&& other);
            GateKeeper& operator=(GateKeeper&&);
        private:
            GateKeeper();
            GateKeeper(const GateKeeper&);
            GateKeeper& operator=(const GateKeeper&);

            std::mutex m;
            std::unique_lock<std::mutex> lock;
    };

    GateKeeper::GateKeeper() : lock (m) {
        std::cout << "Locking unique_lock" << std::endl;
    }
    GateKeeper::~GateKeeper() {
        if (lock.owns_lock()) {
            lock.unlock();
            std::cout << "Unlocking unique_lock" << std::endl;
        }
    }
    GateKeeper::GateKeeper(GateKeeper&& other) : lock(std::move(other.lock)) 
    {}
    GateKeeper& GateKeeper::operator=(GateKeeper&& other) {
        if (this != &other) {
            if (lock.owns_lock()) {
                lock.unlock();
            }
            lock = std::move(other.lock);
        }
        return *this;
    }

    GateKeeper GateKeeper::GetGateKeeperObject() {
        return GateKeeper();
    }
    void GateKeeper::WorkOnProtectedData() {
        std::cout << "WorkOnProtectedData" << std::endl; 
    }
}

namespace CallOnceTest {
    // common data
    struct HeavyObject {
        void do_something() {}
    };
    std::shared_ptr<HeavyObject> resource_ptr;

    // original problem - Double check locking
    namespace DoubleCheckLockingExample {
        std::mutex m;
        void Initialise() {
            if (!resource_ptr) { // check 1
                std::lock_guard<std::mutex> lock(m);
                if (!resource_ptr) { // check 2 (Double check)
                    resource_ptr.reset(new HeavyObject());
                }
            }
            resource_ptr->do_something();
        }
    }

    // solution using call_once - functor (initialisation routine here) will be called exactly once
    namespace CallOnceExample {
        // dont use a mutex, instead use call_once
        std::once_flag resource_flag;
        void Initialise() {
            //
            // if resource flag is not set, call the functor
            // else skip it and directly call do_something()
            //
            std::call_once(resource_flag, []() { resource_ptr.reset(new HeavyObject()); });
            resource_ptr->do_something();
        }
    }
}

namespace ConditionalVariableExample {
    struct DataChunk {
    };
    std::mutex m;
    std::queue<DataChunk> data_queue;
    std::condition_variable dataCondition;
    bool DataToPrepare() {
        return true;
    }
    DataChunk PrepareData() {
        return DataChunk();
    }
    void DataPreperationThread() {
        while (DataToPrepare()) {
            const DataChunk data = PrepareData();
            std::lock_guard<std::mutex> lock(m); // use lock_guard here
            data_queue.push(data);
            dataCondition.notify_one();
        }
    }

    void ProcessData(const DataChunk&) {
    }
    bool LastChunkToProcess() {
        return true;
    }
    void DataProcessingThread() {
        while (true) {
            std::unique_lock<std::mutex> lock (m); // use unique_lock here

            dataCondition.wait(lock, []() { return !data_queue.empty(); });
            const DataChunk data = data_queue.front();
            data_queue.pop();
            lock.unlock();

            // no need to keep the mutex locked during the processing
            ProcessData(data);
            if (LastChunkToProcess()) {
                break;
            }
        }
    }

}

void Start() {
}
