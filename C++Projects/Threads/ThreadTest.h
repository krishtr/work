#ifndef THREADTEST_H_
#define THREADTEST_H_

#define ITERATIONS 5

#include <thread>
#include "LockedQueue.h"

struct WorkItem {
    std::string symbol;
    double bid, ask;
    WorkItem(std::string s, double b, double a)
        : symbol(s)
          , bid(b)
          , ask(a)
    {}
};

class ThreadTest {
    public:
        ThreadTest();
        virtual ~ThreadTest();

        void start();
        void join();
        virtual void run() = 0;

        std::thread::id self() const { return id_; }
        void SetId(std::thread::id id) { id_ = id; }

        void SetRunning(bool flag) { isRunning = flag; }
        bool GetRunning() const { return isRunning; }

    private:
        ThreadTest& operator=(const ThreadTest&) = delete;
        ThreadTest& ThreadTest(const ThreadTest&) = delete;
        std::thread* thread_;
        std::thread::id id_;
        bool isRunning;
};

class ConsumerThread : public ThreadTest {
    public:
        explicit ConsumerThread(LockedQueue<WorkItem*>& q);
        void run();
    private:
        LockedQueue<WorkItem*>& queue_;
        ConsumerThread& operator=(const ConsumerThread&);
};

class ProducerThread : public ThreadTest {
    public:
        explicit ProducerThread(LockedQueue<WorkItem*>& q);
        void run();
    private:
        LockedQueue<WorkItem*>& queue_;
        ProducerThread& operator=(const ProducerThread&);
};

#endif
