#include <iostream>
#include <string>
#include "ThreadTest.h"


ThreadTest::ThreadTest()
    : thread_(nullptr)
      , isRunning(false)
{}

ThreadTest::~ThreadTest() {
    if (thread_ && isRunning) {
        SetRunning(false);
        thread_->join();
    }

    if (thread_) {
        std::cout << "Destroying " << self() 
            << "..." << std::endl;
        delete thread_;
    }
}

static void runThread(void * args) {
    auto ptr = (static_cast<ThreadTest*>(args));
    ptr->SetId(std::this_thread::get_id());
    ptr->SetRunning(true);
    ptr->run();
}

void ThreadTest::start() {
    thread_ = new std::thread(runThread, this);
}

void ThreadTest::join() {
    if (thread_ && thread_->joinable()) {
        thread_->join();
        SetRunning(false);
    }
}
std::mutex m;
ConsumerThread::ConsumerThread(LockedQueue<WorkItem*>& q) : queue_(q) {}
void ConsumerThread::run() {
    for (;;) {
        auto item = queue_.RemoveItem();
        {
            std::lock_guard<std::mutex> lock_(m);
            std::cout << "{" << self() << "}" << ": Symbol(" << "): " << item->symbol
                << ", (Bid, Ask): [" << item->bid << ", " << item->ask << "]";
            std::cout << std::endl;
        }
        delete item;
        std::this_thread::sleep_for(std::chrono::milliseconds(2000));
        if (queue_.size() == 0) break;
    }
}

ProducerThread::ProducerThread(LockedQueue<WorkItem*>& q) : queue_(q) {}
void ProducerThread::run() {
    WorkItem* item;
    for(int i = 1; i <= ITERATIONS; ++i) {
        item = new WorkItem("EUR/USD", 1.34567, 1.34678);
        queue_.AddItem(item);
        item = new WorkItem("GBP/USD", 1.54567, 1.54678);
        queue_.AddItem(item);
        item = new WorkItem("USD/JPY", 78.289, 78.346);
        queue_.AddItem(item);
        std::this_thread::sleep_for(std::chrono::milliseconds(1000));
    }
    std::cout << "Producer Thread done..." << std::endl;

}
