#include <stdexcept>
#include <thread>
#include <mutex>

class HierarchicalLock {
public:
		explicit HierarchicalLock (unsigned long value) : hierarchical_value (value) {}
		void lock();
		void unlock();
		bool try_lock();
		void update_hierarchical_value();
		void check_violation();

private:
		std::mutex internal_mutex;
		unsigned long prev_hierarchical_value;
		unsigned long hierarchical_value;
		static unsigned long this_hierarchical_value;
};

unsigned long HierarchicalLock::this_hierarchical_value (ULONG_MAX);

void HierarchicalLock::check_violation() {
		if (this_hierarchical_value <= hierarchical_value) {
				throw std::logic_error("Mutex violation");
		}
}

void HierarchicalLock::update_hierarchical_value() {
		prev_hierarchical_value = this_hierarchical_value;
		this_hierarchical_value = hierarchical_value;
}
void HierarchicalLock::lock() {
		check_violation();
		internal_mutex.lock();
		update_hierarchical_value();
}

void HierarchicalLock::unlock() {
		this_hierarchical_value = prev_hierarchical_value;
		internal_mutex.unlock();
}

bool HierarchicalLock::try_lock() {
		check_violation();
		if (!internal_mutex.try_lock()) {
				return false;
		}
		update_hierarchical_value();
		return true;
}