#include <bits/stdc++.h>

#include <boost/interprocess/file_mapping.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <boost/algorithm/string/trim.hpp>
#include <boost/lexical_cast.hpp>

static int kOutputSpeedUp = []() {
    std::ios::sync_with_stdio(false);
    std::cin.tie(0);
    return 0;
}();

namespace
{
    bool is_range_int(long long x)
    {
        return std::numeric_limits<int>::min() <= x && x <= std::numeric_limits<int>::max();
    }
} // namespace

class Person : public std::enable_shared_from_this<Person>
{
private:
    std::string name;

public:
    std::shared_ptr<Person> mom, dad;
    std::vector<std::weak_ptr<Person>> kids;
    Person(const std::string &n, std::shared_ptr<Person> m = nullptr,
           std::shared_ptr<Person> d = nullptr)
        : name(n), mom(m), dad(d)
    {
    }
    std::string getName() const { return name; }
    ~Person() { std::cout << name << " destroyed" << '\n'; }
    void setParentsAndKids(std::shared_ptr<Person> mom = nullptr,
                           std::shared_ptr<Person> dad = nullptr)
    {
        if (mom != nullptr)
            mom->kids.push_back(shared_from_this());
        if (dad != nullptr)
            dad->kids.push_back(shared_from_this());
    }
};

std::shared_ptr<Person> initFamily(const std::string &name)
{
    auto mom = std::make_shared<Person>(name + "'s mom");
    auto dad = std::make_shared<Person>(name + "'s dad");
    auto kid = std::make_shared<Person>(name, mom, dad);
    kid->setParentsAndKids(mom, dad);
    // kid->mom->kids.push_back(kid);
    // kid->dad->kids.push_back(kid);
    std::cout << name << "'s Family initialized" << '\n';
    return kid;
}
std::mutex printMutex;

void print(const std::string &s)
{
    std::lock_guard<std::mutex> l(printMutex);
    for (auto c : s)
        std::cout.put(c);
    std::cout << '\n';
}

int binaryGap(int num)
{
    int max_length = 0;
    int zero_length = 0;
    int mask = 1;
    bool start = false;
    while (num)
    {
        int bit = num & mask;
        if (bit == 1)
        {
            if (!start)
                start = true;
            else
            {
                zero_length = 0;
            }
        }
        else
        {
            if (start)
            {
                ++zero_length;
                max_length = std::max(zero_length, max_length);
            }
        }
        num /= 2;
    }

    return max_length;
}

void printBinaryRep(int num)
{
    std::vector<int> bin;
    while (num)
    {
        bin.push_back(num & 1);
        num >>= 2;
    }
    std::reverse(bin.begin(), bin.end());
    std::copy(bin.begin(), bin.end(), std::ostream_iterator<int>(std::cout));
    std::cout << '\n';
}

void rotateArray(std::vector<int> &A, int K)
{
    // [3,8,4,6,9] ->(1) [9,3,8,4,6]
    if (K % A.size() == 0)
        return;
    K = K % A.size();
    std::vector<int> result;
    result.reserve(A.size());
    std::vector<int>::reverse_iterator iter = A.rbegin();
    while (--K)
    {
        ++iter;
    }

    std::vector<int>::iterator pin = (++iter).base();
    std::vector<int>::iterator it = pin;
    for (; it != A.end(); ++it)
        result.push_back(*it);

    std::copy(A.begin(), pin, std::back_inserter(result));
    A.swap(result);
}

void getBinary(std::vector<int> &bin, int N)
{
    while (N)
    {
        bin.push_back(N & 1);
        N = N >> 1;
    }
    std::reverse(bin.begin(), bin.end());
    std::copy(bin.begin(), bin.end(), std::ostream_iterator<int>(std::cout));
}

// 1001100110
int binaryPeriod(int N)
{
    std::vector<int> bin;
    getBinary(bin, N);
    int Q = bin.size();
    for (int P = 1; P <= Q / 2; ++P)
    {
        bool match = true;
        for (int K = 0; K < (Q - P); ++K)
        {
            if (bin[K] != bin[K + P])
            {
                match = false;
                break;
            }
        }
        if (match)
            return P;
    }
    return -1;
}

// int findSquares(int A, int B)
//{

//}

int findUnpairedValue(std::vector<int> &A)
{
    if (A.size() == 1)
        return A[0];

    std::vector<int>::iterator iter = A.begin();
    int value = *iter;
    ++iter;
    for (; iter != A.end(); ++iter)
        value = value ^ *iter;

    return value;
}

template <class T>
void display(const T &A)
{
    typedef decltype(*(A.begin())) t;
    std::copy(A.begin(), A.end(), std::ostream_iterator<t>(std::cout, ", "));
    std::cout << '\n';
}

int squareCount(int A, int B)
{
    if (A < 0)
        A = 0;
    if (B < 0)
        B = 0;
    if (A == B)
        return 0;
    int max = std::max(A, B);
    int min = std::min(A, B);
    int count = 0;
    for (int i = sqrt(double(min)); i <= sqrt(double(max)); ++i)
    {
        if ((i * i) >= min && (i * i) <= max)
            ++count;
    }
    return count;
}

bool isTriangle(int a, int b, int c)
{
    return (a + b > c && a + c > b && b + c > a);
}

int triangleTripletCount(std::vector<int> &A)
{
    //
    // 12,10,8,5,2,1
    // 12,10,8
    // 12,10,5
    // 12,8,5
    // 10,8,5
    //
    if (A.size() < 3)
        return 0;
    int count = 0;
    std::sort(A.begin(), A.end());
    for (uint i = 0, j = 0, k = 0; i < A.size() - 2; ++i)
    {
        j = i + 1;
        k = j + 1;
        while (j < A.size())
        {
            if (isTriangle(A[i], A[j], A[k]))
            {
                ++count;
                ++k;
            }
            else
            {
                ++j;
                k = j + 1;
            }
        }
    }
    return count;
}

struct City
{
    int value;
    bool isVisited, isLeaf;
    City()
        : value(-1), isVisited(false), isLeaf(false)
    {
    }
    City(int val)
        : value(val), isVisited(false), isLeaf(false)
    {
    }
    std::vector<City> links;
};
std::ostream &operator<<(std::ostream &os, const City &c)
{
    os << "(" << c.value << "," << c.isLeaf << ")";
    return os;
}

std::vector<City> graph;

void parseInput(std::vector<uint> &A)
{
    graph.resize(A.size());
    for (uint i = 0; i < A.size(); ++i)
    {
        if (i != A[i])
        {
            graph[i].links.push_back(City(A[i]));
            graph[A[i]].links.push_back(City(i));
        }
        graph[i].value = i;
    }

    // mark leafs
    for (uint i = 0; i < graph.size(); ++i)
    {
        if (graph[i].links.size() == 1)
            graph[i].isLeaf = true;
    }
}

/*
std::vector<int> maxCounters(int M, std::vector<int>& A)
{
    std::cout << "Begin function" << '\n';
    std::vector<int> result(M, 0);
    int prev_max = 0, max = 0;
    for(auto val : A) {
        if(val == M+1) { // max it
            prev_max = max;
        } else { // increment the counter
            if(result[val-1] < prev_max)
                result[val-1] = prev_max;
            result[val-1] += 1;
            max = std::max(result[val-1], max);
        }
        std::copy(result.begin(), result.end(),
std::ostream_iterator<int>(std::cout, ", "));
        std::cout << "\tprev_max: " << prev_max << ", max: " << max << '\n';
    }
    for(uint i = 0; i < result.size(); ++i)
        if(result[i] < prev_max)
            result[i] = prev_max;

    return result;
}
*/

int K(std::vector<int> &A, int K)
{
    std::vector<int>::iterator iter = A.begin(), end = A.end();
    std::map<int, int> cache;
    for (; iter != end; ++iter)
    {
        int comp = K - *iter;
        int temp = cache.find(comp) != cache.end() ? cache[comp] : 0;
        cache[comp] = ++temp;
    }

    int count = 0;
    iter = A.begin();
    for (; iter != end; ++iter)
    {
        count += cache.find(*iter) != cache.end() ? cache[*iter] : 0;
    }

    return count;
}

//
// given link-list of integers,
// return the integer which has
// max repetitions, if 2 or more
// integers have same repetition
// return the first one
//
void MaxCharRepetitions(std::list<char> &list)
{
    if (list.size() == 0)
        return;
    std::vector<int> vec(256, 0);
    auto iter = list.begin();
    auto end = list.end();
    for (; iter != end; ++iter)
        vec[*iter]++;
    auto maxIter = std::max_element(vec.begin(), vec.end(), std::less<int>());
    std::cout << "character " << char(maxIter - vec.begin()) << " occurs "
              << *maxIter << " times." << '\n';
}

// 90.56 -> 90
// 909 -> 909
// -90 -> -90

int myatoi(const char *str)
{
    int result = 0;
    if (!str)
        return result;
    if ((str[0] != '-' || str[0] != '+') && !isdigit(str[0]))
        return result;
    int i = (str[0] == '-' || str[0] == '+') ? 1 : 0;
    for (; isdigit(str[i]) && str[i] != '\0'; ++i)
    {
        result = 10 * result + (str[i] - '0');
    }

    return (str[0] == '-') ? -result : result;
}

typedef std::vector<std::vector<long long int>> VecVec;
void UnRoll(VecVec &matrix, std::vector<long long int> &roll)
{
    // for(int
}

void MatrixRotation()
{
    long long int M, N, R;
    std::cin >> M >> N >> R;
    VecVec input(M, std::vector<long long int>(N, 0));
    std::cout << "Number of rows: " << input.size() << '\n';
    std::cout << "Number of cols: " << input[0].size() << '\n';
    for (long long int i = 0; i < M; ++i)
    {
        for (long long int j = 0; j < N; ++j)
        {
            long long int m;
            std::cin >> m;
            input[i][j] = m;
        }
    }
    for (auto vec : input)
    {
        std::copy(vec.begin(), vec.end(),
                  std::ostream_iterator<long long int>(std::cout, " "));
        std::cout << '\n';
    }
    std::cout << '\n';
}

void AshtonAndString()
{
    int T;
    std::cin >> T;
    while (T--)
    {
        std::string str;
        std::cin >> str;
        uint K;
        std::cin >> K;
        std::vector<std::string> results;
        const int len = str.length();
        results.reserve(len * (len + 1) * 0.5);
        // findAllSubstrings(str, results);
        std::sort(results.begin(), results.end());
        results.erase(std::unique(results.begin(), results.end()), results.end());
        const int newlen = results.size();
        int runningLength = 0;
        for (int i = 0; i < newlen; ++i)
        {
            const std::string temp(results[i]);
            if (K <= runningLength + temp.length())
            {
                K -= runningLength;
                std::cout << temp[K - 1] << '\n';
                break;
            }
            else
            {
                runningLength += temp.length();
            }
        }
    }
}

template <typename FwdIter, typename T>
bool bin_search(FwdIter lhs, FwdIter rhs, const T &value)
{
    bool result = false;
    while (lhs < rhs)
    {
        auto mid = lhs + (rhs - lhs) / 2;
        if (mid == lhs)
        {
            result = *lhs == value;
            break;
        }
        if (*mid > value)
            rhs = mid;
        else if (*mid < value)
            lhs = mid;
        else
        {
            result = true;
            break;
        }
    }
    return result;
}

void printKMax(int arr[], int n, int k)
{
    if (n == 1)
    {
        std::cout << arr[0] << '\n';
        return;
    }
    if (k == 1)
    {
        std::copy(&arr[0], &arr[n], std::ostream_iterator<int>(std::cout, " "));
        return;
    }
    if (k >= n)
    {
        std::cout << *std::max_element(&arr[0], &arr[n]) << '\n';
        return;
    }
    auto first = &arr[0], end = &arr[n], last = &arr[k];
    while (true)
    {
        auto mi = std::max_element(first, last);
        if (mi == end)
            break;
        auto max = *mi;
        std::cout << max << " ";
        if (first == mi)
        {
            ++first;
            last = first + k;
        }
        else
        {
            auto next = last;
            while (first != mi && next != end)
            {
                ++first;
                max = std::max(max, *next);
                std::cout << max << " ";
                ++next;
            }
            if (next >= end)
                break;
            ++first;
            last = first + k;
        }
    }
    /*
auto mi = std::max_element(first, last);
if(mi != end) {
auto max = *mi;
std::cout << max << " ";
}
*/
    std::cout << '\n';
}

void printAlternate(const std::string &str)
{
    // 12345 -> 51423
    if (str.empty())
        return;
    if (str.size() == 1)
    {
        std::cout << str << '\n';
        return;
    }
    auto last = str.end() - 1;
    auto first = str.begin();
    while (first <= last)
    {
        if (first == last)
            std::cout << *first;
        else
            std::cout << *last << *first;

        --last;
        ++first;
    }
    std::cout << '\n';
}

// coins is the denominations available in decreasing order
// amount is the amount for which the result needs to be found
// result is a collection to hold the result: result will hold
// the number of coins whose index maps to the index of denomination
void solve(const std::vector<int> &coins, int amount,
           std::vector<int> &result)
{
    if (coins.empty() || amount <= 0)
        return;
    result.resize(coins.size());
    std::fill(result.begin(), result.end(), 0);
    while (amount > 0)
    {
        for (size_t i = 0; i < coins.size(); ++i)
        {
            auto amt = amount - coins[i];
            if (amt < 0)
                continue;
            result[i]++;
            amount -= coins[i];
            break;
        }
    }
}

void solve_faster(const std::vector<int> &coins, int amount,
                  std::vector<int> &result)
{
    if (coins.empty() || amount <= 0)
        return;
    result.resize(coins.size());
    std::fill(result.begin(), result.end(), 0);
    for (size_t i = 0; i < coins.size(); ++i)
    {
        auto n = std::floor(amount / coins[i]);
        amount -= n * coins[i];
        result[i] = n;
    }
}

int reverse_int(int x)
{
    if (x == 0)
        return 0;
    const bool neg = x < 0 ? true : false;
    long long result = 0;
    x = std::abs(x);
    while (x)
    {
        result = result * 10 + x % 10;
        x /= 10;
    }
    return !is_range_int(result) ? 0 : neg ? -result
                                           : result;
}

int myatoi(const std::string &str)
{
    if (str.empty())
        return 0;
    const char *iter = &str[0];
    const char *end = &str[str.size()];
    while (std::isspace(*iter))
        ++iter;
    int neg = 1;
    if (*iter == '-' || *iter == '+')
    {
        neg = *iter == '-' ? -1 : 1;
        ++iter;
    }
    long long result = 0;
    while (std::isdigit(*iter) && iter < end)
    {
        result = result * 10 + neg * (*iter++ - '0');
        if (result > INT_MAX)
            return INT_MAX;
        if (result < INT_MIN)
            return INT_MIN;
    }
    return result;
}

struct ListNode
{
    int val;
    ListNode *next;
    ListNode(int x)
        : val(x), next(nullptr)
    {
    }
};

int getListNodeLength(ListNode *head)
{
    if (head == nullptr)
        return 0;
    int count = 0;
    while (head != nullptr)
    {
        ++count;
        head = head->next;
    }
    return count;
}

// 1->2->3->4->5
// del 4
// 1->2->3->5
ListNode *removeNthFromEnd(ListNode *head, int n)
{
    if (head == nullptr)
        return head;
    if (n <= 0)
        return head;
    const int len = getListNodeLength(head);
    int jumps = len - n;
    if (jumps <= 0)
        return head->next;
    jumps -= 1;
    ListNode *temp = head;
    while (jumps != 0)
    {
        temp = temp->next;
        --jumps;
    }
    temp->next = temp->next->next;
    return head;
}

ListNode *removeNthFromEndOnePass(ListNode *head, int n)
{
    if (head == nullptr || n <= 0)
        return head;
    ListNode *first = head;
    ListNode *second = head;
    for (int i = 1; i <= n + 1; ++i)
        first = first->next;
    while (first != nullptr)
    {
        first = first->next;
        second = second->next;
    }
    second->next = second->next->next;
    return head;
}

// not allowed to use extra space!
bool isPalindrome(int x)
{
    if (x < 0)
        return false;
    if (x < 10)
        return true;
    int lhs = 1000000000;
    while ((x / lhs) == 0)
        lhs /= 10;
    int rhs = 10;
    while (lhs >= rhs)
    {
        if ((x / lhs) % 10 != ((x % rhs) / (rhs / 10)))
            return false;
        lhs /= 10;
        rhs *= 10;
    }
    return true;
}

// using int
bool isPalindromeNum(int x)
{
    if (x < 0)
        return false;
    x = std::abs(x);
    int temp = x;
    long long result = 0;
    while (x)
    {
        result = result * 10 + x % 10;
        x /= 10;
    }
    return result == temp;
}

struct test
{
    static int si;
    int i = 100;
};

int test::si = 10;

std::string intToRoman(int n)
{
    static std::map<int, std::string> map{
        {1, "I"}, {4, "IV"}, {5, "V"}, {9, "IX"}, {10, "X"}, {40, "XL"}, {50, "L"}, {90, "XC"}, {100, "C"}, {400, "CD"}, {500, "D"}, {900, "CM"}, {1000, "M"}};

    std::stringstream ss;
    std::map<int, std::string>::const_iterator iter;
    while (n > 0)
    {
        iter = map.lower_bound(n);
        if (!(iter != map.end() && n == iter->first))
        {
            if (iter != map.begin())
                iter = std::prev(iter);
        }
        ss << iter->second;
        n -= iter->first;
    }
    return ss.str();
}

template <typename T>
std::vector<std::pair<T, size_t>>
mostFrequentInRange(const std::vector<T> &range)
{
    if (range.empty())
        throw std::runtime_error("Input is empty");

    std::vector<T> copy(range.begin(), range.end());
    std::sort(copy.begin(), copy.end());

    std::vector<std::pair<T, size_t>> results;

    T prev_t = copy[0];
    size_t max = 0, curr = 0;
    for (const auto &elem : copy)
    {
        if (elem != prev_t)
        {
            if (curr == max)
            {
                results.emplace_back(std::make_pair(prev_t, curr));
            }
            else if (curr > max)
            {
                results.clear();
                results.emplace_back(std::make_pair(prev_t, curr));
                max = curr;
            }
            curr = 1;
            prev_t = elem;
        }
        else
        {
            ++curr;
        }
    }
    return results;
}

template <typename T>
std::vector<std::pair<T, size_t>> mostFreqInRange(const std::vector<T> &vec)
{
    if (vec.empty())
        throw std::runtime_error("Input is empty");
    std::map<T, size_t> map;
    std::for_each(vec.begin(), vec.end(), [&](const T &t) { map[t]++; });

    using vt = typename decltype(map)::value_type;
    auto iter = std::max_element(
        map.begin(), map.end(),
        [](const vt &lhs, const vt &rhs) { return lhs.second < rhs.second; });
    std::vector<std::pair<T, size_t>> results;
    std::copy_if(map.begin(), map.end(), std::back_inserter(results),
                 [&](const vt &lhs) { return lhs.second == iter->second; });
    return results;
}

// print histogram
// only letters, no spaces, digits and other symbols
// total is only the total of the letters and not the text size
void textHistogram(const std::string &text)
{
    if (text.empty())
        return;

    char arr[256] = {0};
    std::fill(std::begin(arr), std::end(arr), 0);
    std::for_each(text.begin(), text.end(), [&](char c) {
        if (std::isalpha(c))
            arr[static_cast<int>(c)]++;
    });

    int total = std::accumulate(std::begin(arr), std::end(arr), 0);

    // print and calculate the histogram
    std::stringstream ss;
    for (int i = 0; i < 256; ++i)
    {
        int count = (double(arr[i]) / double(total)) * 100.0;
        if (count > 0)
        {
            ss << char(i) << "\t|";
            while (count--)
                ss << "*";
            ss << '\n';
        }
    }
    std::cout << ss.str() << '\n';
}

// MCMXC
int romanToInt(const std::string &str)
{
    static std::unordered_map<std::string, int> map{
        {"I", 1}, {"IV", 4}, {"V", 5}, {"IX", 9}, {"X", 10}, {"XL", 40}, {"L", 50}, {"XC", 90}, {"C", 100}, {"CD", 400}, {"D", 500}, {"CM", 900}, {"M", 1000}};

    size_t total = 0;
    for (size_t i = 0; i < str.length();)
    {
        std::string sub = str.substr(i, 2);
        auto iter = map.find(sub);
        if (iter != map.end())
        {
            total += iter->second;
            i += 2;
        }
        else
        {
            sub = str.substr(i, 1);
            iter = map.find(sub);
            if (iter == map.end())
                throw std::runtime_error("invalid roman string");
            total += iter->second;
            i += 1;
        }
    }
    return total;
}

struct Name
{
    std::string first;
    std::string last;
};

std::ostream &operator<<(std::ostream &os, const Name &p)
{
    os << p.last << ", " << p.first;
    return os;
}
struct NameSort
{
    bool operator()(const Name &lhs, const Name &rhs) const
    {
        return (lhs.last < rhs.last || (lhs.last == rhs.last && lhs.first < rhs.first));
    }
};

void removeNthElement(int N, std::vector<int> &vec)
{
    int count = 0;
    auto iter = std::find_if(vec.begin(), vec.end(), [&](int) { return ++count == N; });
    if (iter == vec.end())
        return;
    vec.erase(iter);
}

bool doesWordExist(char arr[][4], size_t cols, size_t rows,
                   const std::string &target)
{
    std::string word;
    for (size_t i = 0; i < rows; ++i)
        std::transform(std::begin(arr[i]), std::end(arr[i]),
                       std::back_inserter(word), [](char c) { return c; });
    std::cout << word << '\n';
    size_t pos = 0;
    for (char c : target)
    {
        pos = word.find(c, pos);
        if (pos == std::string::npos)
            return false;
    }

    return true;
}

template <typename T>
std::vector<std::pair<T, T>> zip(const std::vector<T> &lhs,
                                 const std::vector<T> &rhs)
{
    if (lhs.empty() || rhs.empty())
        return {};

    // find the collection with the lesser size
    size_t count = lhs.size() < rhs.size() ? lhs.size() : rhs.size();
    std::vector<std::pair<T, T>> result;
    result.reserve(count);
    for (size_t i = 0; i < count; ++i)
    {
        result.emplace_back(std::make_pair(lhs[i], rhs[i]));
    }
    return result;
}

//
// ['a','a','a','b','b','c','c','d'] => ['a','3','b','2','c','2','d']
int compress( std::vector<char>& chars )
{
    if( chars.size() <= 1 ) return chars.size();
        std::vector<char> s;
        int prev = 0;
        int next = 1;
        int end = chars.size();
        while(next != end) {
            for(; next < end && chars[next] == chars[prev]; ++next) {}
            s.emplace_back(chars[prev]);
            if( next - prev > 9 ) {
                const std::string& count = std::to_string( next - prev);
                for(char ch : count) {
                    s.push_back(ch);
                }
            } else {
                if(next - prev > 1) {
                    s.emplace_back((next - prev) + '0');
                }
            }
            prev = next;
        }
        chars.swap(s);
        return chars.size();
}

template<typename ...T>
auto average(T ...args)
{
    constexpr auto num = double{ sizeof...(args) };
    return (... + args) / num;
}

int main()
{
    std::vector<char> vec { 'a','a' };
    std::copy( vec.begin(), vec.end(), std::ostream_iterator<char>(std::cout, ","));
    std::cout << std::endl;
    std::cout << compress(vec) << std::endl;
    std::copy( vec.begin(), vec.end(), std::ostream_iterator<char>(std::cout, ","));
    std::cout << std::endl;
    return 0;
}