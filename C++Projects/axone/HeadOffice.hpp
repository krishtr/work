#ifndef HEADOFFICE_HPP_
#define HEADOFFICE_HPP_

#pragma once

#include <map>
#include <set>
#include <string>
#include <vector>

class HeadOffice {
private:
    std::map<std::string, std::vector<int>> branch_table;

public:
    using bt = decltype(branch_table);
    HeadOffice() {}
    void add_product_codes(const std::string& branch, int code);
    void add_product_vec( const std::string& branch, std::vector<int> vec );
    std::set<int> get_unique_prod_codes() const;
    std::vector<int> get_popular_products() const;
    std::string to_string() const;

    friend std::ostream& operator<<(std::ostream&, const HeadOffice&);
};

inline std::ostream& operator<<(std::ostream& os, const HeadOffice& off)
{
    os << off.to_string();
    return os;
}
#endif
