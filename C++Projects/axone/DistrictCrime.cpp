#include <algorithm>
#include <iterator>
#include <sstream>
#include <vector>
#include "DistrictCrime.hpp"

namespace
{
    template<template<class...> class Coll, typename T>
    std::string join( const Coll<T>& vec, const char* sep )
    {
        std::stringstream ss;
        std::copy( vec.begin(), vec.end(), std::ostream_iterator<T>(ss, sep) );
        return ss.str();
    }
}

void DistrictCrime::addToCrimeList( const std::string& district, int code )
{
    crimeTable[district].emplace( code );
}

void DistrictCrime::addCrimeSet( const std::string& district, std::set<int> s )
{
    crimeTable[district] = std::move(s);
}

std::vector<int> DistrictCrime::crimeList() const
{
    std::vector<int> results; // list of all the crime codes
    std::for_each( crimeTable.begin(), crimeTable.end(),
        [&](const decltype(crimeTable)::value_type& p) {
            std::copy( p.second.begin(), p.second.end(),
                std::back_inserter(results) );
        });
    return results;
}

std::vector<int> DistrictCrime::highlyLocalizedCrimeTypes() const
{
    const auto& vec = crimeList();
    std::map<int,int> lookup;
    for(auto i : vec) lookup[i]++;
    std::vector<int> results;
    std::transform( lookup.begin(), lookup.end(), std::back_inserter(results),
        [](const std::pair<int,int>& p) { return p.second == 1 ? p.first : -1; } );
    results.erase( std::remove(results.begin(), results.end(), -1), results.end() );
    return results;
}

std::string DistrictCrime::toString() const
{
    std::stringstream ss;
    ss << '\n';
    for( auto p : crimeTable ) {
        ss << p.first << "-> " << join<std::set, int>( p.second, "," ) << '\n';
    }
    return ss.str();
}
