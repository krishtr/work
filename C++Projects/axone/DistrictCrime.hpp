#ifndef DISTRICTCRIME_HPP_
#define DISTRICTCRIME_HPP_

#include <map>
#include <set>
#include <string>

class DistrictCrime
{
public:
    DistrictCrime() {}
    std::vector<int> crimeList() const;
    std::vector<int> highlyLocalizedCrimeTypes() const;
    void addToCrimeList( const std::string&, int );
    void addCrimeSet( const std::string&, std::set<int> );

    std::string toString() const;
    friend std::ostream& operator<<( std::ostream& os, const DistrictCrime& );
private:
    std::map<std::string, std::set<int>> crimeTable;
};

inline std::ostream& operator<<( std::ostream& os, const DistrictCrime& d )
{
    os << d.toString() << '\n';
    return os;
}

#endif
