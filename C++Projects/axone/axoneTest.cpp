#include <iostream>
#include "HeadOffice.hpp"
#include "DistrictCrime.hpp"

int testHeadOffice()
{
    HeadOffice off;
    off.add_product_vec( "Chelsea", {23, 8, 77, 13, 23, 7} );
    off.add_product_vec( "Maida Vale", {76, 14, 11, 7, 11, 24, 8, 9} );
    off.add_product_vec( "Belgravia", {17, 2, 6, 9, 14, 8} );
    off.add_product_vec( "Knightsbridge", {31, 7, 9, 16, 32, 31} );

    std::cout << off << '\n';

    std::cout << "get unique product codes - " << '\n';
    for(auto i: off.get_unique_prod_codes())
        std::cout << i << ",";
    std::cout << '\n';

    std::cout << "get popular products - " << '\n';
    for(auto i : off.get_popular_products())
        std::cout << i << ",";
    std::cout << '\n';
    return 0;
}

int testDistrictCrime()
{
    DistrictCrime crime;
    crime.addCrimeSet( "Little Newark", {8, 77, 93} );
    crime.addCrimeSet( "Springfield Heights", {8, 14, 15, 31, 33, 47, 77} );
    crime.addCrimeSet( "Squidport", {5, 8, 14, 27, 31, 47, 93} );
    crime.addCrimeSet( "Waterfront", {2} );

    std::cout << crime << '\n';

    std::cout << "Highly localized crime types - " << '\n';
    for( auto i : crime.highlyLocalizedCrimeTypes() ) {
        std::cout << i << ",";
    }
    std::cout << std::endl;
    return 0;
}

int main()
{
    return testDistrictCrime();
}
