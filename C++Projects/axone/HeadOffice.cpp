#include <sstream>
#include <algorithm>
#include <iterator>
#include "HeadOffice.hpp"

namespace 
{
    template<typename T>
    std::string join(const std::vector<T>& vec, const char* sep)
    {
        std::stringstream ss;
        std::copy(vec.begin(), vec.end(), std::ostream_iterator<T>(ss, sep));
        return ss.str();
    }
}

void HeadOffice::add_product_vec( const std::string& branch, std::vector<int> vec )
{
    branch_table[branch] = std::move(vec);
}


void HeadOffice::add_product_codes(const std::string& branch, int code)
{
    branch_table[branch].emplace_back(code);
}

std::set<int> HeadOffice::get_unique_prod_codes() const
{
    std::set<int> results;
    std::for_each(branch_table.begin(), branch_table.end(), 
        [&](const bt::value_type& p) {
            std::transform(p.second.begin(), p.second.end(), 
                std::inserter(results, results.begin()),
                [](int code) { return code; });
    });

    return results;
}

std::vector<int> HeadOffice::get_popular_products() const
{
    // get all products from each row after removing duplicates
    // from this list, get a list of codes that appear more than thrice
    std::vector<int> results;
    std::for_each(branch_table.begin(), branch_table.end(),
        [&](const bt::value_type& p) {
            std::set<int> codes;
            std::transform(p.second.begin(), p.second.end(), 
                std::inserter(codes, codes.begin()),
                [&](int code) { return code; });
            std::copy(codes.begin(), codes.end(), std::back_inserter(results));
    });
    std::map<int,int> counter;
    for(int i : results) counter[i]++;
    std::vector<int> ans;
    for(auto p : counter)
        if(p.second >= 3)
            ans.emplace_back(p.first);
    return ans;
}

std::string HeadOffice::to_string() const
{
    std::stringstream ss;
    for(auto p : branch_table)
        ss << p.first << " -> " << join(p.second, ",") << '\n';
    return ss.str();
}

