/* 14 Write a program to print a histogram of the frequencies of different characters in its input. */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

int counts[255];

void printHistogram(void)
{
    for (int i = 0; i < 255; ++i) {
        if (counts[i] != 0) {
            printf("%c: |", (char)(i));
            for (int j = 0; j < counts[i]; ++j)
                printf("-");
            printf("\n");
        }
    }
}

void init(void)
{
    for(int i = 0; i < 255; ++i)
        counts[i] = 0;
}

int main(void)
{
    init();
    char *s;
    scanf("%m[^\n]", &s);
    for(int i = 0; i < strlen(s); ++i)
        ++counts[s[i]];

    printf("\n");
    printHistogram();
    free(s);
    return EXIT_SUCCESS;
}
