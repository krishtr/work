/* 12 Write a program that prints its input one word per line. */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

int main(void)
{
    char *s;
    scanf("%m[^\n]", &s);
    for (size_t i = 0; i < strlen(s); ++i) {
        if (isspace(s[i]))
            putchar('\n');
        else
            putchar(s[i]);
    }
    free(s);
    return EXIT_SUCCESS;
}
