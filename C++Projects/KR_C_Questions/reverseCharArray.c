/* 19 Write a function reverse(s) that reverses the character string s . Use it to write a program that reverses its
 * input a line at a time.
 */

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void reverse(char s[])
{
    size_t len = strlen(s);
    if (len <= 1) return;

    for (int i = 0, j = len - 1; i < j; ++i, --j) {
        char temp = s[j];
        s[j] = s[i];
        s[i] = temp;
    }        
}

int main(void)
{
    char *s;
    scanf("%m[^\n]", &s);
    printf("Original string: %s\n", s);
    reverse(s);
    printf("%s\n", s);
    free(s);

    return EXIT_SUCCESS;
}
