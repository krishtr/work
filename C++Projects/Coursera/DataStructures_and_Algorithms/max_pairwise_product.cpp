#include <iostream>
#include <vector>
#include <algorithm>

using std::vector;
using std::cin;
using std::cout;

int MaxPairwiseProduct(const vector<int>& numbers) {
  int result = 0;
  int n = numbers.size();
  for (int i = 0; i < n; ++i) {
    for (int j = i + 1; j < n; ++j) {
      if (numbers[i] * numbers[j] > result) {
        result = numbers[i] * numbers[j];
      }
    }
  }
  return result;
}

long long mySolution(std::vector<long long>& numbers)
{
    std::sort(numbers.begin(), numbers.end(), std::greater<long long>());
    long long result = numbers[0] * numbers[1];
    return result;
}

int main() {
    int n;
    cin >> n;
    vector<long long> numbers(n);
    for (int i = 0; i < n; ++i) {
        cin >> numbers[i];
    }

    long long result = mySolution(numbers);//MaxPairwiseProduct(numbers);
    cout << result << "\n";
    return 0;
}
