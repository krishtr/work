#include <iostream>
#include "WeightedQuickUnion.h"

WeightedQuickUnion::WeightedQuickUnion(int N) : size_(N) {
    id_.reserve(size_);
    sz_.reserve(size_);
}

void WeightedQuickUnion::Initialize() {
    for (int i = 0; i < size_; ++i) {
        id_.push_back(i);
        sz_.push_back(1); // sz is 1 'cos there is 1 element at root
    }
}

int WeightedQuickUnion::Root(int root) const {
    while (root != id_[root]) {
        root = id_[root];
    }

    return root;
}

bool WeightedQuickUnion::Connected(int p, int q) const {
    return Root(p) == Root(q);
}

void WeightedQuickUnion::Union(int p, int q) {
    int i = Root (p);
    int j = Root (q);
    if (sz_[i] < sz_[j]) {
        id_[i] = j;
        sz_[j] += sz_[i];
    } else {
        id_[j] = i;
        sz_[i] += sz_[j];
    }
}

void WeightedQuickUnion::Display() const {
    for (auto i : id_) {
        std::cout << i << ", ";
    }

    std::cout << std::endl;
}

void WeightedQuickUnion::Start() {

    WeightedQuickUnion f (10);
    f.Initialize();

    /*f.Union (6, 5);
      f.Union (3, 2);
      f.Union (0, 3);
      f.Union (9, 1);
      f.Union (6, 9);
      f.Union (7, 4);
      f.Union (4, 3);
      f.Union (5, 7);
      f.Union (4, 8);*/

    f.Union (6, 8);
    f.Union (5, 4);
    f.Union (9, 7);
    f.Union (0, 2);
    f.Union (7, 3);
    f.Union (1, 5);
    f.Union (7, 1);
    f.Union (0, 6);
    f.Union (1, 0);
    f.Display();
}
