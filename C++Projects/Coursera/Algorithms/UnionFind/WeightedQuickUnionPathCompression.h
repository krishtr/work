#ifndef WEIGHTEDQUICKUNIONPATHCOMPRESSION_H_
#define WEIGHTEDQUICKUNIONPATHCOMPRESSION_H_

//**********************************************
// Weighted Quick Union with path compression
//**********************************************        

#include <vector>

class WeightedQuickUnionPathCompression
{
public:
        explicit WeightedQuickUnionPathCompression(int N);
        void Initialize();
        bool Connected(int p, int q) const;
        void Union(int p, int q);
        void Display() const;
        static void Start();
private:
        WeightedQuickUnionPathCompression();
        WeightedQuickUnionPathCompression(const WeightedQuickUnionPathCompression &);
        WeightedQuickUnionPathCompression & operator = (const WeightedQuickUnionPathCompression &);
        int Root(int p) const;

        int size_; 
        mutable std::vector<int> id_;
        std::vector<int> sz_;
};

#endif