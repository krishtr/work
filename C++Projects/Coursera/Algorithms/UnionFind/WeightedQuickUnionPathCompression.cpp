#include <iostream>
#include "WeightedQuickUnionPathCompression.h"

WeightedQuickUnionPathCompression::WeightedQuickUnionPathCompression(int N) : size_(N) {
        id_.reserve(size_);
        sz_.reserve(size_);
}

void WeightedQuickUnionPathCompression::Initialize() {
        for (int i = 0; i < size_; ++i) {
                id_.push_back(i);
                sz_.push_back(1); // sz is 1 'cos there is 1 element at root
        }
}

int WeightedQuickUnionPathCompression::Root(int p) const {
        int root = p;
        while (root != id_[root]) {
                root = id_[root];
        }

        // self-flatening
        int x = p;
        while (x != root) {
                x = id_[p];
                id_[p] = root;
                p = x;
        }

        return root;
}

bool WeightedQuickUnionPathCompression::Connected(int p, int q) const {
        return Root(p) == Root(q);
}

void WeightedQuickUnionPathCompression::Union(int p, int q) {
        int i = Root (p);
        int j = Root (q);
        if (sz_[i] < sz_[j]) {
                id_[i] = j;
                sz_[j] += sz_[i];
        } else {
                id_[j] = i;
                sz_[i] += sz_[j];
        }
}

void WeightedQuickUnionPathCompression::Display() const {
        for (auto i : id_) {
                std::cout << i << ", ";
        }

        std::cout << std::endl;
}

void WeightedQuickUnionPathCompression::Start() {

        WeightedQuickUnionPathCompression f (13);
        f.Initialize();

        f.Union (2, 0);
        f.Union (1, 0);
        f.Union (3, 0);
        f.Union (4, 1);
        f.Union (5, 1);
        f.Union (7, 3);
        f.Union (6, 3);
        f.Union (8, 6);
        f.Union (9, 6);
        f.Union (10, 8);
        f.Union (11, 9);
        f.Union (12, 9);
        std::cout << ((f.Connected(2, 0) == true) ? "YES" : "NO") << std::endl;
        f.Display();
}