#include <iostream>
#include "QuickFind.h"

QuickFind::QuickFind(int N) : size_(N){
  id_.reserve(size_);
}

void QuickFind::Initialize()
{
  for (int i = 0; i < size_; ++i) {
    id_.push_back(i);
  }
}

bool QuickFind::Connected(int p, int q)
{
  return id_[p] == id_[q];
}

void QuickFind::Union(int p, int q)
{
  std::cout << "(" << p << "," << q << ")" << std::endl;
  if (p == q) {
    return;
  }
  int i = id_[p];
  int j = id_[q];
  for (unsigned int k = 0; k < id_.size(); ++k) {
    if (id_[k] == i) {
      id_[k] = j;
    }
  }
}

void QuickFind::Display() const
{
  for (auto i : id_) {
    std::cout << i << ", ";
  }
  std::cout << std::endl;
}

void QuickFind::Start()
{
  QuickFind f (7);
  f.Initialize();
  f.Display();
  f.Union (0, 1);
  f.Display();
  f.Union (1, 2);
  f.Display();
  f.Union (2, 3);
  f.Display();
  f.Union (1, 5);
  f.Display();
  f.Union (2, 4);
  f.Display();
  f.Union (4, 6);
  f.Display();
}
