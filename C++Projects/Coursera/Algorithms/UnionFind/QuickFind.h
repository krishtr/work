#ifndef QUICKFIND_H_
#define QUICKFIND_H_

//**********************************************
// Quick Find
//**********************************************

#include <vector>

class QuickFind
{
public:
        explicit QuickFind(int N);
        void Initialize();
        bool Connected(int p, int q);
        void Union(int p, int q);
        void Display() const;
        static void Start();
private:
        QuickFind();
        QuickFind(const QuickFind &);
        QuickFind & operator = (const QuickFind &);

        int size_; 
        std::vector<int> id_;
};

#endif