#include <time.h>
#include "Percolation.h"

#pragma warning (disable: 4244)

Percolation::Percolation(int N) : tree_(N * (N + 2)), vecOpenCollection_(N * ( N + 2), 0),
        sizeX_(N), sizeY_(N + 2), count_(0) {
                Initialise();
}

void Percolation::Initialise() {
        tree_.Initialize();

        //
        // join the first and last row with the top and bottom
        // virtual sites
        //
        for (int i = 0; i < sizeX_; ++i) {
                tree_.Union (ConvertXYTo1D (sizeX_ >> 1, 0), ConvertXYTo1D (i, 1));
        }

        for (int i = 0; i < sizeX_; ++i) {
                tree_.Union (ConvertXYTo1D (sizeX_ >> 1, sizeY_ - 1), ConvertXYTo1D (i, sizeY_ - 2));
        }
}

//
// code calling this function should check for bounds
//
bool Percolation::IsOpen(int x, int y) const {
        return vecOpenCollection_[ConvertXYTo1D (x, y)] == 1;
}

//
// full site is a site connected to top virtual site
//
bool Percolation::IsFull(int x, int y) const {
        return tree_.Connected(ConvertXYTo1D(x,y),ConvertXYTo1D(sizeX_>>1,0)); // top site
}

//
// Are the 2 virtual sites connected?
//
bool Percolation::Percolates() const {
        return tree_.Connected (ConvertXYTo1D (sizeX_ >> 1, 0), ConvertXYTo1D (sizeX_ >> 1, sizeY_ - 1));
}

void Percolation::Open(int x, int y) {
        if (OutOfRange (x,y)) {
                return;
        }

        if (IsOpen (x,y)) {
                return;
        }
        vecOpenCollection_[ConvertXYTo1D (x,y)] = 1; // khul ja sim sim
        count_++;

        //
        // Connect with nearby open sites
        //
        if (!OutOfRange(x, y - 1)) { // top
                if (IsOpen (x,y - 1) ) {
                        tree_.Union(ConvertXYTo1D(x,y), ConvertXYTo1D(x,y - 1));
                }
        }
        if (!OutOfRange(x, y + 1)) { // bottom
                if (IsOpen (x,y + 1) ) {
                        tree_.Union(ConvertXYTo1D(x,y), ConvertXYTo1D(x,y + 1));
                }
        }
        if (!OutOfRange(x - 1, y)) { // left
                if (IsOpen (x - 1,y) ) {
                        tree_.Union(ConvertXYTo1D(x,y), ConvertXYTo1D(x - 1,y));
                }
        }
        if (!OutOfRange(x + 1, y)) { // right
                if (IsOpen (x + 1,y) ) {
                        tree_.Union(ConvertXYTo1D(x,y), ConvertXYTo1D(x + 1,y));
                }
        }
        return;
}

double Percolation::Start(int N) {
        if (N <= 1) {
                return 0.0;
        }

        Percolation pObj(N);
        srand (time (NULL)); // initialise the seed
        while (!pObj.Percolates()) {
                auto x = rand() % N + 1;
                auto y = rand() % N + 1;
                pObj.Open(x, y);
        }

        return pObj.PercolationThreshold();
}