#ifndef PERCOLATIONSTATS_H_
#define PERCOLATIONSTATS_H_

#include <tchar.h>
#include <sstream>
#include <vector>

class PercolationStats {
public:
        explicit PercolationStats (int N, int T);               // Perform T experiments on a NxN grid
        double Mean() const {
                return mean_;                                   // Mean of percolation threshold
        }
        double StdDev() const {
                return stdDev_;                                 // Standard Deviation of Percolation threhold
        }

        std::string ConfidenceInterval() const {
                std::stringstream iss;
                iss << "( " << CalculateLowerConfidenceInterval() << ", " <<
                        CalculateHigherConfidenceInterval() << " )";
                return iss.str();
        }
        
        static void Start(int argC, _TCHAR* argV[]);
        void Begin();
private:
        void CalculateMean();
        void CalculateStdDev();
        double CalculateLowerConfidenceInterval() const {
                return mean_ - double((1.96 * stdDev_ / sqrt(iterations_)));
        }
        double CalculateHigherConfidenceInterval() const {
                return mean_ + double((1.96 * stdDev_ / sqrt(iterations_)));
        }
        std::vector<double> samplePercolationThresholdCollection_;
        int iterations_, gridSize_;
        double mean_, stdDev_;
};

#endif