#include <windows.h>
#include <iostream>
#include <fstream>
#include <string>
#include "Percolation.h"
#include "PercolationTester.h"

void PercolationTester::Test(const Percolation & obj, int x, int y) {
        static int index = 0;
        if (obj.IsFull(x, y)) {
                std::cout << "Site (" << x << "," << y << ") is Full. Item # " << ++index << std::endl;
        } else if (obj.IsOpen(x,y)) {
                std::cout << "Site (" << x << "," << y << ") is Open. Item # " << ++index << std::endl;
        }
}

void PercolationTester::Start() {
        std::ifstream input;
        char path[MAX_PATH];
        GetModuleFileName(NULL, path, MAX_PATH);
        auto ptr = strrchr(path, '\\');
        sprintf (ptr, "\\..\\C++Test\\Coursera\\Algorithms\\UnionFind\\Percolation Problem\\Input.txt");
        input.open (path, std::ifstream::in);
        std::string line;
        if (std::getline(input, line)) {
                int N = atoi (line.c_str());
                Percolation obj(N);
                while (std::getline(input, line)) {
                        auto pos = line.find_last_of (" ");
                        if (pos == std::string::npos) {
                                break;
                        }

                        auto y = atoi(line.substr(0, pos).c_str());
                        auto x = atoi(line.substr(pos).c_str());
                        obj.Open(x, y);
                        Test(obj, x, y);
                        if (obj.Percolates()) {
                                //
                                // Verified! 204 open sites and percolation index of 0.51 according to the expected results.
                                //
                                std::cout << "System Percolates when Site (" << x << "," << y << ") was Opened" << std::endl;
                                std::cout << obj.PercolationThreshold() << std::endl;
                                break;
                        }
                }
        }
}