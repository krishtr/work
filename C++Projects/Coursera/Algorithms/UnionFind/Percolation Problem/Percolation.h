#ifndef PERCOLATION_H_
#define PERCOLATION_H_

#include "../WeightedQuickUnion.h"
#include "../WeightedQuickUnionPathCompression.h"

class Percolation
{
public:
        explicit Percolation(int N);                                    // Create a NxN site, with all blocked
        void Open(int i, int j);                                        // open the site, if not already open
        bool IsOpen(int i, int j) const;                                // Is site open?
        bool IsFull(int i, int j) const;                                // Is site Full?
        bool Percolates() const;                                        // does system percolate?
        int TotalOpenSiteCount() const { return count_; }               // total open site count
        double PercolationThreshold() const { 
                return double(TotalOpenSiteCount()) / double((sizeX_ * sizeX_));
        }

        static double Start(int N);
private:
        void Initialise();
        int ConvertXYTo1D(int x, int y) const { 
                return y * sizeX_ + x; 
        }
        bool OutOfRange(int x, int y) const {
                if (x < 1 || y < 1) {
                        return true;
                }

                if (x > sizeX_ || y > (sizeY_ - 2)) {
                        return true;
                }

                return false;
        }
        //WeightedQuickUnion tree_;
        WeightedQuickUnionPathCompression tree_;
        std::vector<int> vecOpenCollection_;
        int sizeX_, sizeY_, count_;
};

#endif