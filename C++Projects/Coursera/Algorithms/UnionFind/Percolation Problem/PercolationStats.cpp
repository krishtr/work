#include <iostream>
#include <functional>
#include <numeric>
#include <ctime>
#include "PercolationStats.h"
#include "Percolation.h"

#pragma warning (disable: 4244)

PercolationStats::PercolationStats(int N, int T) 
        : iterations_(T), gridSize_(N), mean_(0), stdDev_(0) {
                samplePercolationThresholdCollection_.reserve(T);
}

void PercolationStats::Begin() {
        for (int i = 0; i < iterations_; ++i) {
                samplePercolationThresholdCollection_.push_back(Percolation::Start(gridSize_));
        }

        CalculateMean();
        CalculateStdDev();
}

void PercolationStats::CalculateMean() {

        mean_ = std::accumulate(std::begin(samplePercolationThresholdCollection_),
                                std::end(samplePercolationThresholdCollection_),
                                mean_) / iterations_;
}

void PercolationStats::CalculateStdDev() {
        if (iterations_ == 1) {
                stdDev_ = -1;
                return;
        }

        for (auto sample : samplePercolationThresholdCollection_) {
                auto&& diff = std::move(sample - mean_);
                stdDev_ +=  diff * diff;
        }

        stdDev_ /= (iterations_ - 1);
        stdDev_ = sqrt(stdDev_);
}

void PercolationStats::Start(int argC, _TCHAR* argV[]) {
        if (argC < 3) {
                std::cout << "Args less than 3" << std::endl;
                std::cout << "argc = " << argC << std::endl;
                for (int i = 1; i < argC; ++i) {
                        std::cout << argV[i] << ", ";
                }
                std::cout << std::endl;
                return;
        }

        int N = atoi(argV[1]);
        int T = atoi(argV[2]);
        std::cout << "Grid Size = " << N << std::endl;
        std::cout << "Number of Iterations = " << T << std::endl;
        if (N <= 1 || T <= 0) {
                std::cout << "Invalid GridSize or Iterations" << std::endl;
        }
        unsigned int start = clock();
        PercolationStats stats(N, T);
        stats.Begin();
        std::cout << "Time taken in ms: " << clock() - start << std::endl;
        std::cout << "Mean = " << stats.Mean() << std::endl;
        std::cout << "Standard Deviation = " << stats.StdDev() << std::endl;
        std::cout << "95% Confidence Interval = " << stats.ConfidenceInterval() << std::endl;        
}