#ifndef PERCOLATIONTESTER_H_
#define PERCOLATIONTESTER_H_

class Percolation;

class PercolationTester {
public:
        static void Test (const Percolation & obj, int x, int y);
        static void Start();
};

#endif