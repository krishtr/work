#ifndef QUICKUNION_H_
#define QUICKUNION_H_

//**********************************************
// Quick Union
//**********************************************

#include <vector>

class QuickUnion
{
public:
        explicit QuickUnion(int N);
        void Initialize();
        bool Connected(int p, int q) const;
        void Union(int p, int q);
        void Display() const;
        static void Start();
private:
        QuickUnion();
        QuickUnion(const QuickUnion &);
        QuickUnion & operator = (const QuickUnion &);
        int Root(int p) const;

        int size_; 
        std::vector<int> id_;
};

#endif