#ifndef WEIGHTEDQUICKUNION_H_
#define WEIGHTEDQUICKUNION_H_

//**********************************************
// Weighted Quick Union
//**********************************************

#include <vector>

class WeightedQuickUnion
{
public:
        explicit WeightedQuickUnion(int N);
        void Initialize();
        bool Connected(int p, int q) const;
        void Union(int p, int q);
        void Display() const;
        static void Start();
private:
        WeightedQuickUnion();
        WeightedQuickUnion(const WeightedQuickUnion &);
        WeightedQuickUnion & operator = (const WeightedQuickUnion &);
        int Root(int p) const;

        int size_; 
        std::vector<int> id_, sz_;
};

#endif