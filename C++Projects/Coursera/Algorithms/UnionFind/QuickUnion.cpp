#include <iostream>
#include "QuickUnion.h"

QuickUnion::QuickUnion(int N) : size_(N) {
        id_.reserve(size_);
}

void QuickUnion::Initialize() {
        for (int i = 0; i < size_; ++i) {
                id_.push_back(i);
        }
}

int QuickUnion::Root(int p) const {
        while (p != id_[p]) {
                p = id_[p];
        }

        return p;
}

bool QuickUnion::Connected(int p, int q) const {
        return Root(p) == Root(q);
}

void QuickUnion::Union(int p, int q) {
        int i = Root(p);
        int j = Root(q);
        id_[i] = j;
}

void QuickUnion::Display() const {
        for (auto i : id_) {
                std::cout << i << ", ";
        }
        std::cout << std::endl;
}

void QuickUnion::Start() {
        QuickUnion u(10);
        u.Initialize();

        u.Union(6,8);
        u.Display();
        u.Union(5,4);
        u.Display();
        u.Union(9,7);
        u.Display();
        u.Union(0,2);
        u.Display();
        u.Union(7,3);
        u.Display();
        u.Union(1,5);
        u.Display();
        u.Union(7,1);
        u.Display();
        u.Union(0,6);
        u.Display();
        u.Union(1,0);

        u.Display();
}