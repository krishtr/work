#ifndef DEQUEUE_H_
#define DEQUEUE_H_

#include <iterator>

template<class T>
struct Node {
        explicit Node(const T& t) 
                : item(t), next(nullptr), prev(nullptr) {
        }

        T& operator*() {
                return this->item;
        }
        T item;
        Node<T> *next, *prev;
};

template<class T>
class Dequeue {
public:
        class iterator;

        // construct an empty deque
        Dequeue() : first_(nullptr), last_(nullptr), count_(0) {}

        // is the deque empty?
        bool IsEmpty() const {
                return count_ == 0;
        }

        // return the number of items on the deque
        int size() const {
                return count_;
        }

        // insert the item at the front
        void AddFirst(const T& item) {
                Node<T> *oldFirst = first_;
                first_ = new Node<T>(item);
                first_->next = oldFirst;
                first_->prev = nullptr;
                if (oldFirst != nullptr) {
                        oldFirst->prev = first_;
                } else {
                        last_ = first_;
                }
                count_++;
        }

        // insert the item at the end
        void AddLast(const T& item) {
                Node<T> *oldLast = last_;
                last_ = new Node<T>(item);
                last_->next = nullptr;
                last_->prev = oldLast;
                if (oldLast != nullptr) {
                        oldLast->next = last_;
                } else {
                        first_ = last_;
                }
                count_++;
        }

        // delete and return the item at the front
        T RemoveFirst() {
                if (first_ == nullptr) {
                        return T();
                }
                T item = first_->item;
                Node<T> *oldFirst = first_;
                first_ = first_->next;
                delete oldFirst;
                count--;
                return item;
        }

        // delete and return the item at the end
        T RemoveLast() {
                if (last_ == nullptr) {
                        return T();
                }
                T item = last_->item;
                Node<T> *oldLast = last_;
                last_ = last_->prev;
                delete oldLast;
                count--;
                return item;
        }

        iterator begin();
        iterator end();

private:
        Node<T> *first_, *last_;
        int count_;
};

template<class T>
class Dequeue<T>::iterator
        : public std::iterator<std::input_iterator_tag, Node<T>> {
public:
        typedef typename Dequeue<T>::iterator self_type;
        iterator(Node<T> *ptr) : current_(ptr) {}
        iterator() : current_(nullptr) {}

        self_type & operator++() {
                current_ = current_->next;
                return *this;
        }

        self_type operator++(int) {
                self_type old(*this);
                operator++();
                return old;
        }

        T & operator*() {
                return current_->operator*();
        }

        const self_type* operator->() const {
                return this;
        }

        bool operator==(const self_type & other) const {
                return this->current_ == other->current_;
        }

        bool operator!=(const self_type & other) const {
                return this->current_ != other->current_;
        }
        
private:
        Node<T> *current_;
};

template<class T>
typename Dequeue<T>::iterator Dequeue<T>::begin() {
        return iterator(first_);
};

template<class T>
typename Dequeue<T>::iterator Dequeue<T>::end() {
        return iterator(nullptr);
};

#endif