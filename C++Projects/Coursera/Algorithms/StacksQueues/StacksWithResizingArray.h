#ifndef STACKSWITHRESIZINGARRAY_H_
#define STACKSWITHRESIZINGARRAY_H_

#include <memory>

template<class T>
class StacksWithResizingArray {
public:
        StacksWithResizingArray() : capacity_(2), size_(0), ptrData_(new T[capacity_]) {}
        bool empty() const {
                return size_ == 0;
        }
        void push(const T& item) {
                if (size_ == capacity_) {
                        resize(capacity_ << 1);
                }
                ptrData_[size_++] = item;
        }
        T top() const {
                if (empty()) {
                        return T();
                }
                return ptrData_[size_ - 1];
        }
        void pop() {
                if (empty()) {
                        return;
                }
                if (size_ == capacity_ >> 2) {
                        resize(capacity_ >> 1);
                }
                size_--;
        }
private:
        struct Node {
                T item;
                Node* next;
        };
        void resize(int size) {
                T* ptrTemp(new T[size]);
                std::copy (ptrData_, ptrData_ + size_, ptrTemp);
                T * ptrOldData = ptrData_;
                ptrData_ = ptrTemp;
                delete [] ptrOldData;
                capacity_ = size;
        }
        int capacity_, size_;
        T* ptrData_;
};

#endif