#ifndef STACKSWITHLINKLIST_H_
#define STACKSWITHLINKLIST_H_

template<class T>
class StacksWithLinkList {
public:
        struct Node {
                T item;
                Node* next;
        };
        StacksWithLinkList() : first_(nullptr), size_(0) {}
        bool empty() const { 
                return first_ == nullptr; 
        }
        void push(const T& item) {
                Node *oldFirst = first_;
                first_ = new Node();
                first_->item = item;
                first_->next = oldFirst;
                size_++;
        }
        T top() const {
                if (empty()) {
                        return T();
                }
                return first_->item;
        }
        void pop() {
                if (empty()) {
                        return;
                }
                Node* oldFirst = first_;
                first_ = first_->next;
                delete oldFirst;
                size_--;
        }
        int size() const {
                return size_;
        }
private:
        Node* first_;
        int size_;
};
#endif