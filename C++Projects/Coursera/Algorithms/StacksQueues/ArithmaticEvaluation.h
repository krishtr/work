#ifndef ARITHMATICEVALUATION_H_
#define ARITHMATICEVALUATION_H_

//*****************************************************************************
// Evaluating arithmatic expressions. Algorithm as follows
// 1. If there is a left paranthesis, Ignore it.
// 2. If you encounter a number, put it in value stack
// 3. If you encounter an operator, put it in operator stack
// 4. If you encounter a right parantheses - 
//      a. Pop top 2 elements from value stack.
//      b. Pop top element from operator stack.
//      c. Perform the operation defined by operator on 2 elements.
//      d. Put the result back in value stack.
//*****************************************************************************

#include <tchar.h>
#include <string>
#include "StacksWithLinkList.h"

class ArithmaticEvaluation {
public:
        static void Start(int argC, _TCHAR* argV[]);
        void AddToValueStack(double item) {
                valueStack_.push(item);
        }
        void AddToOperatorStack(char item) {
                operatorStack_.push(item);
        }
        void Evaluate();
        double Result() {
                double d = valueStack_.top();
                valueStack_.pop();
                return d;
        }
private:
        double PerformOperation(double p, double q, char op) const;
        StacksWithLinkList<double> valueStack_;
        StacksWithLinkList<char> operatorStack_;
};

#endif