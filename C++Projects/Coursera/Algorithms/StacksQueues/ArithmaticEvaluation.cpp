#include <Windows.h>
#include <iostream>
#include <fstream>
#include "ArithmaticEvaluation.h"

void ArithmaticEvaluation::Start(int argC, _TCHAR* argV[]) {
        if (argC < 2) {
                std::cout << "Invalid set of inputs" << std::endl;
                return;
        }

        char path[MAX_PATH];
        GetModuleFileName(NULL, path, MAX_PATH);
        auto ptr = strrchr(path, '\\');
        sprintf (ptr, "\\Input.txt");
        std::ifstream input(path);
        if (!input.is_open()) {
                std::cout << "Input file " << argV[1] << " not open" << std::endl;
                return;
        }

        ArithmaticEvaluation evaluator;
        std::string word;
        while (std::getline(input, word, ' ')) {
                auto num = atof(word.c_str());
                if (num != 0.0) {
                        evaluator.AddToValueStack(num);
                }
                if (word == "+" || word == "-" || word == "*" || word == "/") {
                        evaluator.AddToOperatorStack(word[0]);
                }
                if (word == ")") {
                        evaluator.Evaluate();
                }
                if (word[0] != ' ') {
                        std::cout << word;
                }
        }

        std::cout << std::endl << "Result: " << evaluator.Result() << std::endl;
}

void ArithmaticEvaluation::Evaluate() {
        // only evaluate if there are atleast 2 values to operate on
        if (valueStack_.empty()
        || valueStack_.size() < 2
        || operatorStack_.empty()) {
                return;
        }

        auto q = valueStack_.top();
        valueStack_.pop();
        auto p = valueStack_.top();
        valueStack_.pop();
        auto op = operatorStack_.top();
        operatorStack_.pop();
        AddToValueStack(PerformOperation(p, q, op));
}

double ArithmaticEvaluation::PerformOperation(double p, double q, char op) const {
        switch (op) {
        case '+':
                return p + q;
        case '-':
                return p - q;
        case '*':
                return p * q;
        case '/':
                if (q == 0.0) {
                        return 0.0;
                }
                return p / q;
        }

        return 0.0;
}