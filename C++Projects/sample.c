#include <iostream>
#include <algorithm>
#include <future>
#include <list>
#include <iterator>

template <unsigned long N>
struct binary
{
    static unsigned const value
        = binary<N/10>::value * 2   // prepend higher bits
        + N%10;                    // to lowest bit
};

template <>                           // specialization
struct binary<0>                      // terminates recursion
{
    static unsigned const value = 0;
};

template<unsigned int N>
struct factorial
{
    static unsigned const value = N * factorial<N-1>::value;
};
template<>
struct factorial<0>
{
    static unsigned const value = 1;
};

template<unsigned int N>
struct Fibo {
    static unsigned const value = Fibo<N-1>::value + Fibo<N-2>::value;
};
template<>
struct Fibo<0> { static unsigned const value = 0; };
template<>
struct Fibo<1> { static unsigned const value = 1; };

    template<typename T>
std::list<T> parallelQSort(std::list<T> input)
{
    if(input.empty()) return input;
    std::list<T> result;
    result.splice(result.end(), input, input.begin());
    const T& pivot = *result.begin();

    auto divide_point = std::partition(input.begin(), input.end(), [&](const T& t) { return t < pivot; });
    std::list<T> new_lower;
    new_lower.splice(new_lower.begin(), input, input.begin(), divide_point);

    // sort the new lower
    auto lower(std::async(&parallelQSort<T>, std::move(new_lower)));
    auto new_higher(parallelQSort(std::move(input)));

    // collect the results
    result.splice(result.end(), new_higher);
    result.splice(result.begin(), lower.get());
    return result;
}

void maxSubArray(int *arr, int len)
{
    if(len == 0) return;
    auto max = arr[0], max_here = arr[0];
    for(int i = 1; i < len; ++i) {
        max_here = std::max(arr[i] + max_here, arr[i]);
        max = std::max(max, max_here);
    }
    std::cout << max << '\n';
}

int main() {
    int arr[] = {-10,2,3,-2,0,5,-15};
    size_t len = sizeof(arr) / sizeof(arr[0]);
    maxSubArray(arr, len);
    return 0;
}
