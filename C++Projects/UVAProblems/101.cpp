#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <stdexcept>

typedef std::vector<std::vector<int>> Vectype;

void display(Vectype& vec)
{
    Vectype::iterator iter (vec.begin());
    Vectype::iterator end (vec.end());
    for (; iter != end; ++iter) {
        std::cout << iter - vec.begin() << ":";
        Vectype::value_type::iterator ptr((*iter).begin());
        Vectype::value_type::iterator vecend((*iter).end());
        for (; ptr != vecend; ++ptr)
            std::cout << " " << *ptr;

        std::cout << std::endl;
    }
}

// unstack iter to end from index
void unstack( Vectype& parent, unsigned int index,
        Vectype::value_type::iterator iter)
{
    // sanity checks
    if (index > parent.size() || index < 0)
        throw std::invalid_argument("Invalid index");

    if (iter == parent[index].end())
        return;

    Vectype::value_type::iterator ptr(iter);
    Vectype::value_type::iterator end(parent[index].end());
    for (; ptr != end; ++ptr)
        parent[*ptr].push_back(*ptr);

    parent[index].erase(iter, end);
}

void moveonto(Vectype& parent, int a, int b)
{
    // validations
    if (a == b)
        return;

    Vectype::iterator iter (parent.begin());
    Vectype::iterator end (parent.end());
    Vectype::value_type::iterator a_iter, b_iter;
    int a_index = 0, b_index = 0;
    for (; iter != end; ++iter) {
        // find a
        a_iter = std::find((*iter).begin(), (*iter).end(), a);
        if (a_iter != (*iter).end()) {
            a_index = iter - parent.begin();
            break;
        }
    }
    iter = parent.begin();
    end = parent.end();
    for (; iter != end; ++iter) {
        // find b
        b_iter = std::find((*iter).begin(), (*iter).end(), b);
        if (b_iter != (*iter).end()) {
            b_index = iter - parent.begin();
            break;
        }
    }

    // validate
    if (a_index == b_index)
        return;

    // unstack both
    unstack(parent, a_index, ++a_iter);
    unstack(parent, b_index, ++b_iter);
    parent[b_index].insert( parent[b_index].end(), 
            --a_iter, 
            parent[a_index].end());
    parent[a_index].erase(a_iter, parent[a_index].end());  
}

void moveover(Vectype& parent, int a, int b)
{
    // validations
    if (a == b)
        return;

    Vectype::iterator iter (parent.begin());
    Vectype::iterator end (parent.end());
    Vectype::value_type::iterator a_iter, b_iter;
    int a_index = 0, b_index = 0;
    for (; iter != end; ++iter) {
        // find a
        a_iter = std::find((*iter).begin(), (*iter).end(), a);
        if (a_iter != (*iter).end()) {
            a_index = iter - parent.begin();
            break;
        }
    }
    iter = parent.begin();
    end = parent.end();
    for (; iter != end; ++iter) {
        // find b
        b_iter = std::find((*iter).begin(), (*iter).end(), b);
        if (b_iter != (*iter).end()) {
            b_index = iter - parent.begin();
            break;
        }
    }

    // validate
    if (a_index == b_index)
        return;

    // unstack a
    unstack(parent, a_index, ++a_iter);
    parent[b_index].insert( parent[b_index].end(), 
            --a_iter, 
            parent[a_index].end());
    parent[a_index].erase(a_iter, parent[a_index].end());  
}

void pileonto(Vectype& parent, int a, int b)
{
    // validations
    if (a == b)
        return;

    Vectype::iterator iter (parent.begin());
    Vectype::iterator end (parent.end());
    Vectype::value_type::iterator a_iter, b_iter;
    int a_index = 0, b_index = 0;
    for (; iter != end; ++iter) {
        // find a
        a_iter = std::find((*iter).begin(), (*iter).end(), a);
        if (a_iter != (*iter).end()) {
            a_index = iter - parent.begin();
            break;
        }
    }
    iter = parent.begin();
    end = parent.end();
    for (; iter != end; ++iter) {
        // find b
        b_iter = std::find((*iter).begin(), (*iter).end(), b);
        if (b_iter != (*iter).end()) {
            b_index = iter - parent.begin();
            break;
        }
    }

    // validate
    if (a_index == b_index)
        return;

    // unstack b
    unstack(parent, b_index, ++b_iter);
    parent[b_index].insert( parent[b_index].end(), 
            a_iter, 
            parent[a_index].end());
    parent[a_index].erase(a_iter, parent[a_index].end());  
}

void pileover(Vectype& parent, int a, int b)
{
    // validations
    if (a == b)
        return;

    Vectype::iterator iter (parent.begin());
    Vectype::iterator end (parent.end());
    Vectype::value_type::iterator a_iter, b_iter;
    int a_index = 0, b_index = 0;
    for (; iter != end; ++iter) {
        // find a
        a_iter = std::find((*iter).begin(), (*iter).end(), a);
        if (a_iter != (*iter).end()) {
            a_index = iter - parent.begin();
            break;
        }
    }
    iter = parent.begin();
    end = parent.end();
    for (; iter != end; ++iter) {
        // find b
        b_iter = std::find((*iter).begin(), (*iter).end(), b);
        if (b_iter != (*iter).end()) {
            b_index = iter - parent.begin();
            break;
        }
    }

    // validate
    if (a_index == b_index)
        return;

    // no unstack
    parent[b_index].insert( parent[b_index].end(),
            a_iter, 
            parent[a_index].end());
    parent[a_index].erase(a_iter, parent[a_index].end());  
}

int main() 
{
    int n = 0;
    std::cin >> n; // number of blocks
    Vectype vec;
    vec.reserve(n);
    for (int i = 0; i < n; ++i)
        vec.push_back(std::vector<int>(1,i));

    int a = 0, b = 0;
    std::string command, operation;
    while (std::cin >> command) {
        switch(command[0]) {
            case 'm':
                std::cin >> a >> operation >> b;
                if (operation == "onto")
                    moveonto(vec, a, b);
                else if (operation == "over")
                    moveover(vec, a, b);
                break;
            case 'p':
                std::cin >> a >> operation >> b;
                if (operation == "onto")
                    pileonto(vec, a, b);
                else if (operation == "over")
                    pileover(vec, a, b);
                break;
            case 'q':
                goto gameover;
        }
    }
gameover:
    display(vec);
    return 0;
}
