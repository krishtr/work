#include <iostream>
#include <string>

int main()
{
    std::string line;
    while(std::getline(std::cin, line, '\n')) {
        for(char c : line)
            std::cout << char(c - 7);
        std::cout << std::endl;
    }
    return 0;
}
