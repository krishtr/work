#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

int main()
{
    while(true) {
        int T;
        std::cin >> T;
        if(T == 0) break;
        std::vector<int> spaces;
        int min = 30; // spaces cannot exceed 25, so just keep this to be 30
        std::string line;
        std::getline(std::cin, line, '\n');
        while(T--) {
            std::getline(std::cin, line, '\n');
            int num = std::count(line.begin(), line.end(), ' ');
            spaces.push_back(num);
            min = std::min(min, num);
        }
        int total = 0;
        for(auto i : spaces)
            total += i - min;
        std::cout << total << std::endl;
    }
    return 0;
}
