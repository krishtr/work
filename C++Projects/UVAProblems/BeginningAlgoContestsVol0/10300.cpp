#include <iostream>

int main()
{
    int T;
    std::cin >> T;
    while(T--) {
        int farmers;
        std::cin >> farmers;
        int result = 0;
        while(farmers--) {
            int s, num, param;
            std::cin >> s >> num >> param;
            result += param * s;
        }
        std::cout << result << std::endl;
    }
    return 0;
}
