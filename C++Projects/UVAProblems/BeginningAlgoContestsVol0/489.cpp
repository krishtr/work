#include <iostream>
#include <string>
#include <algorithm>
#include <set>

int main()
{
    int N;
    std::set<char> wrong_guesses, right_guesses;
    while(std::cin >> N) {
        if(N == -1) break;
        wrong_guesses.clear();
        right_guesses.clear();
        std::string answer, guess;
        std::cin >> answer >> guess;
        std::sort(answer.begin(), answer.end());
        answer.erase(std::unique(answer.begin(), answer.end()), answer.end());
        for(char c : guess) {
            if(std::binary_search(answer.begin(), answer.end(), c))
                right_guesses.insert(c);
            else
                wrong_guesses.insert(c);
            if(right_guesses.size() >= answer.size())
                break;
        }
        std::cout << "Round " << N << std::endl;
        if(wrong_guesses.size() >= 7)
            std::cout << "You lose." << std::endl;
        else if(right_guesses.size() >= answer.size())
            std::cout << "You win." << std::endl;
        else if(wrong_guesses.size() < 7)
            std::cout << "You chickened out." << std::endl;
    }
    return 0;
}
