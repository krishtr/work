#include <iostream>
#include <string>

int main()
{
    std::string line;
    while(std::getline(std::cin, line, '\n')) {
        if(line.empty()) {
            std::cout << std::endl; // maze separation
            continue;
        }
        int num = 0;
        for(int i = 0; i < (int)line.size(); ++i) {
            const char c = line[i];
            if(isdigit(c)) {
                num += c - '0';
            } else if(c == 'b') {
                while(num--) std::cout << " ";
                num = 0;
            } else if(c == '!') {
                std::cout << std::endl;
            } else {
                while(num--) std::cout << c;
                num = 0;
            }
        }
        std::cout << std::endl;
    }
    return 0;
}
