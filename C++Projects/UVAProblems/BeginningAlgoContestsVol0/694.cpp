#include <iostream>

int main()
{
    int A,L,count = 0;
    while(std::cin >> A >> L) {
        if(A < 0 && L < 0) break;
        ++count;
        int sum = 0;
        long long int copy = A;
        while(true) {
            ++sum;
            if(copy == 1) break;
            copy = (copy % 2 == 0) ? (copy >> 1) : (3 * copy + 1);
            if(copy > L) break;
        }
        std::cout << "Case " << count << ": "
                  << "A = " << A << ", "
                  << "limit = " << L << ", "
                  << "number of terms = " << sum
                  << std::endl;
    }
    return 0;
}
