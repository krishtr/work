#include <iostream>
#include <string>
#include <sstream>
#include <regex>

int main()
{
    std::string line;
    std::regex pattern("[^a-zA-Z]");
    while(std::getline(std::cin, line, '\n')) {
        line = std::regex_replace(line, pattern, " ");
        std::stringstream ss(line);
        std::string word;
        int count = 0;
        while(ss >> word && !word.empty())
                ++count;
        std::cout << count << std::endl;
    }
    return 0;
}
