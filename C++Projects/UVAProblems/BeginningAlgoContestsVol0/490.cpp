#include <iostream>
#include <string>
#include <vector>

int main()
{
    std::string line;
    std::vector<std::string> lines;
    int max = -1;
    while(std::getline(std::cin, line,'\n')) {
        lines.push_back(line);
        max = std::max(max,(int)line.size());
    }

    for(int i = 0; i < max; ++i) {
        for(int j = (int)lines.size() - 1; j >= 0; --j) {
            if(i >= (int)(lines[j]).size()) {
                std::cout << " ";
                continue;
            }
            const char c = (lines[j])[i];
            if(c > 31 && c < 128 && c != '\t')
                std::cout << c;
        }
        std::cout << std::endl;
    }

    return 0;
}
