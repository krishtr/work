#include <iostream>
#include <vector>
#include <iterator>

void display(const std::vector<char>& input)
{
    std::copy(input.begin(), input.end(), std::ostream_iterator<char>(std::cout));
    std::cout << std::endl;
}

int main()
{
    int T;
    std::cin >> T;
    std::vector<char> lookup {' ', '.', 'x', 'W'};
    std::vector<int> index_lookup(256,0);
    index_lookup[' '] = 0;
    index_lookup['.'] = 1;
    index_lookup['x'] = 2;
    index_lookup['W'] = 3;
    while(T--) {
        std::string line;
        std::getline(std::cin, line); // ignore empty line
        std::vector<int> input(10,0);
        for(int i = 0; i < 10; ++i)
            std::cin >> input[i];
        int counter = 50;
        std::vector<char> output(40, ' ');
        output[20-1] = '.';
        while(counter--) {
            display(output);
            std::vector<char> temp(output.begin(), output.end());
            for(int i = 0; i < (int)output.size(); ++i) {
                int K;
                if(i == 0) 
                    K = index_lookup[temp[i]] + index_lookup[temp[i+1]];
                else if(i == (int)temp.size() - 1) 
                    K = index_lookup[temp[i-1]] + index_lookup[temp[i]];
                else
                    K = index_lookup[temp[i-1]] + index_lookup[temp[i]] + index_lookup[temp[i+1]];
                output[i] = lookup[input[K]];
            }
        }
        if(T != 0)
            std::cout << std::endl;
    }
    return 0;
}
