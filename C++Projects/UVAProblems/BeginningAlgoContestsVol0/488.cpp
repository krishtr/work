#include <iostream>
#include <sstream>

int main()
{
    int T;
    std::cin >> T;
    while(T--) {
        // accept input
        std::string empty;
        std::getline(std::cin, empty); // empty line after T, ignore it
        int A, F;
        std::cin >> A >> F;

        // generate output
        std::ostringstream ss;
        for(int i = 1; i <= A; ++i) {
            int count = i;
            while(count--) ss << i;
            ss << std::endl;
        }
        for(int j = A-1; j >= 1; --j) {
            int count = j;
            while(count--) ss << j;
            ss << std::endl;
        }

        // print this template F times
        while(F--) {
            std::cout << ss.str();
            if(F != 0) std::cout << std::endl;
        }
        if(T != 0)
            std::cout << std::endl;
    }
    return 0;
}
