#include <iostream>
#include <sstream>
#include <algorithm>
#include <vector>
#include <iterator>
//#include <fstream>
//std::ofstream file("./out");

template<class FwdIter>
void flip(FwdIter begin, FwdIter end)
{
    std::reverse(begin, end);
}

int main()
{
    std::string line;
    while(std::getline(std::cin, line)) {
        std::stringstream ss(line);
        std::vector<int> stack;
        int i;
        while(ss >> i)
            stack.push_back(i);
        std::vector<int> sort(stack);
        std::sort(sort.begin(), sort.end());
        auto end = stack.end();
        for(int i = 0; i < (int)stack.size(); ++i) {
           std::cout << stack[i];
           if(i != (int)stack.size() - 1)
               std::cout << " ";
        }
        std::cout << std::endl;
        while(!std::is_sorted(stack.begin(), stack.end()) 
        && end > stack.begin()) {
            auto it = std::max_element(stack.begin(), end);
            if(it != end - 1) {
                flip(stack.begin(), it+1);
                flip(stack.begin(), end);
            }
            std::cout << (stack.end() - it) << " ";
            std::cout << (stack.end() - end + 1) << " ";
            --end;
        }
        std::cout << '0' << std::endl;
    }
    return 0;
}
