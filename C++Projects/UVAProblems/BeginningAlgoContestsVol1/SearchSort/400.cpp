#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <iterator>
#include <math.h>
//#include <fstream>
//std::ofstream file("./out");

int main()
{
    int N;
    while(std::cin >> N) {
        std::vector<std::string> filenames(N);
        for(int i = 0; i < N; ++i) {
            std::cin >> filenames[i];
        }
        std::sort(filenames.begin(), filenames.end());
        std::cout << "------------------------------------------------------------" << std::endl;
        // what is the number of columns?
        // what is the length of the longest string?
        auto it = std::max_element(filenames.begin(), filenames.end(),
                [](const std::string& lhs, const std::string& rhs)
                { return lhs.size() < rhs.size(); });
        const int longest = (*it).size();
        const int colNum = int((60-longest)/(longest+2)) + 1;
        const int R = ceil((double)N/(double)colNum);
        for(int i = 0; i < R; ++i) {
            for(int j = 0; j < colNum; ++j) {
                if(i+j*R < (int)filenames.size()) {
                    if(i+(j+1)*R >= (int)filenames.size())
                        std::cout.width(longest);
                    else
                        std::cout.width(longest+2);
                    std::cout << std::left << filenames[i+j*R];
                }
            }
            std::cout << std::endl;
        }
    }
    return 0;
}    

