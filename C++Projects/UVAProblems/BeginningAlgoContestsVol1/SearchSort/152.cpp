#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <limits>
#include <math.h>

int main()
{
    int hist[10] = {0};
    int x,y,z;
    std::vector<std::vector<int>> input;
    while(std::cin >> x >> y >> z) {
        if(x == 0 && y == 0 && z == 0)
            break;
        std::vector<int> vec {x,y,z};
        input.push_back(vec);
    }
    for(int i = 0; i < (int)input.size(); ++i) {
        double min_d = std::numeric_limits<double>::max();
        for(int j = 0; j < (int)input.size(); ++j) {
            if(i == j) continue;
            x = input[i][0] - input[j][0];
            y = input[i][1] - input[j][1];
            z = input[i][2] - input[j][2];
            double dist = sqrt(x*x + y*y + z*z);
            min_d = std::min(min_d, dist);
        }
        if(min_d < 10) hist[int(min_d)]++;
    }
    for(int i = 0; i < 10; ++i)
        printf("%4d", hist[i]);
    std::cout << std::endl;
    return 0;
}
