#include <iostream>
#include <algorithm>
#include <vector>
//#include <fstream>

//std::ofstream file("./out");

int main()
{
    int N,Q;
    int count = 0;
    while(std::cin >> N >> Q) {
        if(N == 0 && Q == 0) break;
        std::vector<int> input(N,0);
        for(int i = 0; i < N; ++i)
            std::cin >> input[i];
        std::sort(input.begin(), input.end());
        std::cout << "CASE# " << ++count << ":" << std::endl;
        for(int i = 0; i < Q; ++i) {
            int g = -1;
            std::cin >> g;
            auto it = std::find(input.begin(), input.end(), g);
            if(it == input.end())
                std::cout << g << " not found" << std::endl;
            else
                std::cout << g << " found at " 
                << std::distance(input.begin(), it) + 1 << std::endl;
        }
    }
    return 0;
}
