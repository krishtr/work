#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <unordered_map>
#include <iterator>
#include <regex>
//#include <fstream>
//std::ofstream file("./out");

//
// Team A#3@1#Team B
void parse(const std::string in, std::vector<std::string>& out)
{
    std::regex r("[#@]+");
    std::copy(std::sregex_token_iterator(in.begin(),in.end(),r,-1),
            std::sregex_token_iterator(),
            std::back_inserter(out));
}


struct data
{
    int rank, totalPoints,gamesPlayed;
    int wins,ties,losses;
    int goalDiff,goalsFor,goalsAgainst;
    std::string name;
    data() : rank(0), totalPoints(0), gamesPlayed(0),
    wins(0),ties(0),losses(0),goalDiff(0),goalsFor(0), goalsAgainst(0) {}
    friend std::ostream& operator<<(std::ostream& os, const data& d);
};


std::ostream& operator<<(std::ostream& os, const data& d)
{
    os << d.rank << ") " << d.name << " " << d.totalPoints << "p, "
       << d.gamesPlayed << "g (" << d.wins << "-"
       << d.ties << "-" << d.losses << "), " << d.goalDiff
       << "gd (" << d.goalsFor << "-" << d.goalsAgainst << ")";
    return os;
}


//
// team rank, total points earned, games played
// wins-ties-losses, goal diff,
// goals scored, goals against
//
int main()
{
    int N;
    std::string temp;
    std::getline(std::cin, temp);
    N = stoi(temp);
    bool flag = true;
    while(N--) {
        std::string title;
        std::getline(std::cin, title);
        int T;
        std::getline(std::cin, temp);
        T = stoi(temp);
        std::vector<data> output(T);
        std::unordered_map<std::string,int> idxLookup;
        for(int i = 0; i < T; ++i) {
            std::string team;
            std::getline(std::cin, team);
            data d;
            d.name = team;
            output[i] = d;
            idxLookup[team] = i;
        }
        int m;
        std::getline(std::cin, temp);
        m = stoi(temp);
        for(int i = 0; i < m; ++i) {
            std::string teamA, teamB;
            int a, b;
            std::string line;
            std::getline(std::cin,line);
            std::vector<std::string> tokens;
            parse(line, tokens);
            teamA = tokens[0];
            a = std::stoi(tokens[1]);
            b = std::stoi(tokens[2]);
            teamB = tokens[3];
            auto iter = idxLookup.find(teamA);
            data d;
            int index = output.size();
            if(iter != idxLookup.end()) {
                d = output[iter->second];
                index = iter->second;
            } else {
                output.resize(index+1);
            }
            d.name = teamA;
            d.totalPoints += (a > b) 
                ? 3 
                : (a == b) ? 1 : 0;
            d.gamesPlayed += 1;
            d.wins += (a > b) ? 1 : 0;
            d.losses += (a < b) ? 1 : 0;
            d.ties += (a == b) ? 1 : 0;
            d.goalDiff += (a-b);
            d.goalsFor += a;
            d.goalsAgainst += b;
            output[index] = d;
            idxLookup[teamA] = index;
            auto it = idxLookup.find(teamB);
            d = data();
            index = output.size();
            if(it != idxLookup.end()) {
                d = output[it->second];
                index = it->second;
            } else {
                output.resize(index+1);
            }
            d.name = teamB;
            d.totalPoints += (b > a)
                ? 3
                : (b == a) ? 1 : 0;
            d.gamesPlayed += 1;
            d.wins += (b > a) ? 1 : 0;
            d.losses += (b < a) ? 1 : 0;
            d.ties += (b == a) ? 1 : 0;
            d.goalDiff += (b-a);
            d.goalsFor += b;
            d.goalsAgainst += a;
            output[index] = d;
            idxLookup[teamB] = index;     
        }
        std::sort(output.begin(), output.end(), 
                [](const data& lhs, const data& rhs) 
                { 
                    std::string lname(lhs.name), rname(rhs.name);
                    std::transform(lhs.name.begin(), lhs.name.end(),lname.begin(), toupper);
                    std::transform(rhs.name.begin(), rhs.name.end(),rname.begin(), toupper);
                    bool flag = (lhs.totalPoints != rhs.totalPoints)
                                ? lhs.totalPoints > rhs.totalPoints
                                : (lhs.wins != rhs.wins)
                                    ? lhs.wins > rhs.wins
                                    : (lhs.goalDiff != rhs.goalDiff)
                                        ? lhs.goalDiff > rhs.goalDiff
                                        : (lhs.goalsFor != rhs.goalsFor)
                                            ? lhs.goalsFor > rhs.goalsFor
                                            : (lhs.gamesPlayed != rhs.gamesPlayed)
                                                ? lhs.gamesPlayed < rhs.gamesPlayed
                                                : lname < rname;
                    return flag;
                }
        );
        int i = 1;
        for(auto& d : output)
            d.rank = i++;
        if(!flag) std::cout << std::endl;
        if(flag) flag = false;
        std::cout << title << std::endl;
        std::copy(output.begin(), output.end(), 
                std::ostream_iterator<data>(std::cout, "\n"));
    }
    return 0;
}
