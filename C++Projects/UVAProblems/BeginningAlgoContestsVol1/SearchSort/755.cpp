#include <iostream>
#include <algorithm>
#include <string>
#include <unordered_map>
#include <map>
#include <iterator>
//#include <fstream>
//std::ofstream file("./out");

std::unordered_map<char,int> lookup
{
    {'A', 2},{'B', 2},{'C', 2},{'D', 3},{'E', 3},
    {'F', 3},{'G', 4},{'H', 4},{'I', 4},{'J', 5},
    {'K', 5},{'L', 5},{'M', 6},{'N', 6},{'O', 6},
    {'P', 7},{'R', 7},{'S', 7},{'T', 8},{'U', 8},
    {'V', 8},{'X', 9},{'Y', 9},{'W', 9}
};

std::string parse(const std::string& line)
{
    std::string ret(line);
    for(auto& c : ret)
        if(isalpha(c))
            c = lookup[c] + '0';
    return ret;
}

int main()
{
    int N;
    std::string line;
    std::getline(std::cin, line);
    N = stoi(line);
    bool flag = true;
    while(N--) {
        if(!flag) std::cout << std::endl;
        if(flag) flag = false;
        std::getline(std::cin, line);
        std::getline(std::cin, line);
        int T = stoi(line);
        typedef std::map<std::string, int> CounterMap;
        CounterMap counter;
        while(T--) {
            std::getline(std::cin, line);
            line.erase(std::remove_if(line.begin(), line.end(),
                        [](char c) { return !isalnum(c); }), line.end());
            line.insert(line.begin()+3, '-');
            counter[parse(line)]++;
        }
        bool no_duplicate = false;
        for(auto p : counter) {
            if(p.second > 1) {
                std::cout << p.first << " " << p.second << std::endl;
                no_duplicate = true;
            }
        }
        if(!no_duplicate)
            std::cout << "No duplicates." << std::endl;
    }
    return 0;
}
