#include <iostream>
#include <string>
#include <sstream>
#include <unordered_set>
#include <map>
#include <algorithm>
//#include <fstream>
//std::ofstream file("./out");
struct data
{
    int count = 0;
    std::unordered_set<std::string> perms;
};
std::string to_lower(const std::string& word)
{
    std::string r(word);
    std::transform(word.begin(), word.end(), r.begin(), tolower);
    return r;
}
std::map<std::string, data> perms;
bool doesWordExistInPermList(const std::string& word)
{
    for(auto& p : perms) {
        std::string w = to_lower(word);
        std::sort(w.begin(), w.end());
        if( p.first == word 
        ||  p.second.perms.find(w) != p.second.perms.end()) {
            ++p.second.count;
            return true;
        }
    }
    return false;
}
template<class C>
void addPerms(C& set,const std::string& word)
{
    std::string w(word);
    std::transform(w.begin(), w.end(), w.begin(), tolower);
    std::sort(w.begin(), w.end());
    set.insert(w);
}

int main()
{
    std::string line;
    std::map<std::string,int> map;
    while(std::getline(std::cin, line)) {
        if(line == "#") break;
        std::stringstream ss(line);
        std::string word;
        while(ss >> word) {
            if(!doesWordExistInPermList(word)) {
                data d;
                d.count = 0;
                addPerms(d.perms,word);
                perms.insert(std::make_pair(word,d));
            }
        }
    }
    for(auto p : perms)
        if(p.second.count == 0)
            std::cout << p.first << std::endl;
    return 0;
}
