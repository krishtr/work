#include <iostream>
#include <vector>
#include <algorithm>
#include <stdexcept>
//#include <fstream>
//std::ofstream file("./out");

int getSwaps(std::vector<int>& vec, const std::vector<int>& sorted)
{
    int swaps = 0;
    for(size_t i = 0; i < vec.size(); ++i) {
        if(vec[i] != sorted[i]) {
            auto it = std::find(vec.begin()+i, vec.end(), sorted[i]);
            if(it == vec.end())
                throw std::runtime_error("element not found. Check your logic");
            for(; it > vec.begin()+i; --it) {
                std::swap(*it, *(it-1));
                ++swaps;
            }
        }
    }
    return swaps;
}

int main()
{
    int N;
    std::cin >> N;
    while(N--) {
        int n;
        std::cin >> n;
        std::vector<int> vec(n,0);
        for(int i = 0; i < n; ++i)
            std::cin >> vec[i];
        std::vector<int> sorted(vec.begin(), vec.end());
        std::sort(sorted.begin(), sorted.end());
        std::cout << "Optimal train swapping takes " 
            << getSwaps(vec, sorted) << " swaps." << std::endl;
    }
    return 0;
}
