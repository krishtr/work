#include <iostream>
#include <vector>
#include <string>
#include <unordered_set>
#include <algorithm>
//#include <fstream>

//std::ofstream file("./out");

// input and comp is assumed to be the same
std::pair<int,int> getCount(const std::string& input, std::string& comp)
{
    int count = 0;
    std::string copy(input);
    for(size_t i = 0; i < comp.size(); ++i) {
        if(comp[i] == input[i]) {
            ++count;
            copy[i] = comp[i] = '-';
        } 
    }
    comp.erase(std::remove(comp.begin(), comp.end(), '-'), comp.end());
    //std::sort(comp.begin(), comp.end());
    //comp.erase(std::unique(comp.begin(), comp.end()), comp.end());
    int weak = 0;
    // weak count = exists in input && !strong
    for(size_t i = 0; i < comp.size(); ++i) {
        auto pos = copy.find(comp[i]);
        if(pos != std::string::npos) {
            ++weak;
            copy[pos] = '-';
        }
    }
    return std::make_pair(count, weak);
}

// (i,j)
// i = # of strong guess
// j = # of weak guess
int main()
{
    int N;
    int index = 1;
    while(std::cin >> N) {
        if(N == 0) break;
        std::string input;
        std::getline(std::cin, input);
        std::getline(std::cin, input);
        input.erase(std::remove_if(input.begin(), input.end(), isspace), input.end());
        std::string line;
        const std::string end(N,'0');
        std::cout << "Game " << index << ":" << std::endl;
        while(std::getline(std::cin, line)) {
            line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
            if(line == end) break;
            auto p = getCount(input, line);
            std::cout << "    (" << p.first << "," << p.second << ")" << std::endl;
        }
        ++index;
    }
    return 0;
}
