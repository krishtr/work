#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <iterator>
//#include <fstream>
//std::ofstream file("./out");
std::string v("AAAAAAAAAAAAAAAAAAAAAUUUUUUUUUUUUUUUUUUUUUEEEEEEEEEEEEEEEEEEEEEOOOOOOOOOOOOOOOOOOOOOIIIIIIIIIIIIIIIIIIIII");
std::string c("JJJJJSSSSSBBBBBKKKKKTTTTTCCCCCLLLLLDDDDDMMMMMVVVVVNNNNNWWWWWFFFFFXXXXXGGGGGPPPPPYYYYYHHHHHQQQQQZZZZZRRRRR");

int main()
{
    int T;
    std::cin >> T;
    int i = 1;
    while(T--) {
        int n;
        std::cin >> n;
        std::vector<char> res, vo, co;
        for(int i = 0; 2*i < n; ++i)
            vo.push_back(v[i]);
        std::sort(vo.begin(), vo.end());
        const int m = (n%2 == 0) ? n : n-1;
        for(int i = 0; 2*i < m; ++i)
            co.push_back(c[i]);
        std::sort(co.begin(), co.end());
        for(int i = 0,j = 0; i + j < n;) {
            if(res.size() == 0 || (res.size() + 1) % 2)
                res.push_back(vo[i++]);
            else
                res.push_back(co[j++]);
        }
        std::cout << "Case " << i++ << ": ";
        std::copy(res.begin(),res.end(),std::ostream_iterator<char>(std::cout));
        std::cout << std::endl;
    }
    return 0;
}
