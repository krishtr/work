#include <iostream>
#include <regex>
#include <sstream>
#include <string>
#include <algorithm>
#include <vector>
#include <unordered_set>
#include <map>
#include <iterator>
//#include <fstream>
//std::ofstream file("./out");

struct pack
{
    std::string original;
    std::vector<std::string> vec;
};

int main()
{
    std::unordered_set<std::string> ignore_set;
    std::vector<std::string> input;;
    bool ignore = false;
    std::string line;
    while(std::getline(std::cin, line)) {
        if(line == "::") {
            ignore = true;
            continue;
        }
        if(ignore)
            input.push_back(line);
        else {
            std::transform(line.begin(), line.end(), line.begin(), toupper);
            ignore_set.insert(line);
        }
    }
    // contruct a map of keywords and strings
    std::map<std::string, pack> keys;
    for(int i = 0; i < (int)input.size(); ++i) {
        std::stringstream ss(input[i]);
        std::string word;
        while(ss >> word) {
            std::string c(word);
            std::transform(c.begin(), c.end(), c.begin(), toupper);
            keys[c].original = word;
            if(ignore_set.find(c) == ignore_set.end()) {
                keys[c].vec.push_back(input[i]);
            }
        }
    }
    std::unordered_set<std::string> unique_set;
    for(auto p : keys) {
        std::string up(p.first);
        std::transform(p.first.begin(), p.first.end(), up.begin(), toupper);
        for(auto s : p.second.vec) {
            std::string str(s);
            std::transform(str.begin(),str.end(),str.begin(),tolower);
            if(unique_set.find(s) == unique_set.end()) {
                unique_set.insert(s);
                std::regex r("\\b" + p.second.original + "\\b");
                std::smatch m;
                size_t pos1 = 0;
                std::string strc(str);
                while(std::regex_search(s,m,r)) {
                    for(int i = 0; i < (int)m.size(); ++i) {
                        size_t pos = m.position(i) + pos1;
                        std::cout << strc.replace(pos,p.first.size(),up) << std::endl;
                    }
                    strc = str;
                    s = m.suffix().str();
                    pos1 += m.prefix().str().size() + p.first.size();
                }
            }
        }
        unique_set.clear();
    }
    return 0;
}
