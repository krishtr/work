#include <iostream>
#include <math.h>
#include <regex>
//#include <fstream>
//std::ofstream file("./out");

const char POS = '+';
const char NEG = '-';
const char FAIL= '*';
const char NC  = '?';

int main()
{
    int N;
    std::string str;
    std::getline(std::cin, str);
    N = stoi(str);
    while(N--) {
        std::getline(std::cin, str);
        if(str == "1" || str == "4" || str == "78")
            std::cout << POS << std::endl;
        else {
            std::regex re(".*35$");
            if(std::regex_match(str, re))
                std::cout << NEG << std::endl;
            else {
                re = "^9.*4$";
                if(std::regex_match(str,re))
                    std::cout << FAIL << std::endl;
                else
                    std::cout << NC << std::endl;
            }
        }
    }
    return 0;
}
