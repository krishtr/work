#include <iostream>
#include <math.h>
//#include <fstream>
//std::ofstream file("./out");

bool isPerfectSquare(long double N, long double& s)
{
    s = sqrt(N);
    return (s - int(s) == 0.0);
}

bool shouldSwap(long double d)
{
    long double start = 7;
    if(d == start) return true;
    long double diff = 6;
    bool flag = false;
    for(;;) {
        start += diff;
        diff += 2;
        if(start == d)
            return flag;
        flag = !flag;
    }
}

int main()
{
    long double n;
    long double r = 0, c = 0;
    while(std::cin >> n) {
        if(n == 0) break;
        if(isPerfectSquare(n, c)) {
            if(int(fmod(n,2)) != 0) {
                r = 1;
                c = int(c);                
            } else {
                r = int(c);
                c = 1;
            }
        } else {
            // find the upper and lower range
            long double l = floor(sqrt(n));
            long double u = ceil(sqrt(n));
            long double d = (l*l + u*u + 1) / 2;
            long double s = ceil(sqrt(d));
            if(n > d) {
                r = s;
                c = s - (n - d);
            } else if(n < d) {
                r = s - (d - n);
                c = s;
            } else {
                r = c = s;
            }
            if(d >= 7 && shouldSwap(d)) 
                std::swap(r,c);
        }
        std::cout << r << " " << c << std::endl;
    }
    return 0;
}
