#include <iostream>
#include <math.h>
#include <limits>
//#include <fstream>
//std::ofstream file("./out");

const double threshold = 0.000000000001;
bool isZero(double d)
{
    return d < threshold;
}

int main()
{
    int h, n;
    while(std::cin >> h >> n) {
        if(h == 0 && n == 0)
            break;
        //
        // (m+1)^a = h
        // m^a = n
        // m = constant number of cats
        // a = height of the tree of cats where
        // the bottom row of the tree is the 
        // worker cats
        //

        // use brute force to solve the above equation
        double a = 2; // log 1 = 0
        double height = 0;
        if(n == 1) {
            a = 1;
            height = log(h) / log(2);
        } else {
            const double ratio = double(log(h)) / double(log(n));
            for(;;) {
                const double r = double(log(a+1)) / double(log(a));
                const double diff = r - ratio;
                if(isZero(diff)) {
                    height = log(n) / log(a);
                    break;
                }
                ++a;
            }
        }
        height = int(height + 0.5); // round it off correctly
        // number of useless cats:
        // 1 + n + n^2 + n^3 + n^4 + ....n^(h-1)
        // n = const number of cats, h height of tree
        int uselesscats = 0; 
        for(int i = 0; i <= height - 1; ++i)
            uselesscats += pow(a,i);
        // total height
        // H + n * H * 1/(n+1) + n^2 * H * 1/(n+1)^2 + ....(h-1) + n^h
        // H: initial height of cat
        int totalheight = 0;
        for(int i = 0; i <= height - 1; ++i)
            totalheight += h * pow(a,i) / pow(a+1,i);
        totalheight += n;
        std::cout << uselesscats << " " << totalheight << std::endl;
    }
    return 0;
}
