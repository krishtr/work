#include <iostream>
#include <iomanip>
#include <algorithm>
#include <math.h>

int main()
{
    long double n,p;
    while(std::cin >> n >> p) {
        std::cout << std::setprecision(0);
        std::cout << std::fixed;
        std::cout << (long double)std::exp(std::log(p) / n) << std::endl;
    }
    return 0;
}
