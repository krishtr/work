#include <iostream>
#include <limits>
#include <algorithm>
#include <vector>

long long pow10(int n)
{
    static unsigned long long pow[] = {1, 10, 100, 1000, 10000, 100000, 1000000, 10000000, 100000000, 1000000000, 10000000000};
    return pow[n];
}

struct Nums
{
    std::string first, second, input;
    char op;
    explicit Nums(const std::string& l) : input(l) {}
};

std::pair<bool,unsigned long long> parse(const std::string& num)
{
    auto iter = num.begin();
    while(*iter == '0') ++iter;
    unsigned long long i = 0;
    int n = std::distance(iter, num.end()) - 1;
    for(; iter != num.end(); ++iter) {
        i += (*iter - '0') * pow10(n--);
        if(i > std::numeric_limits<int>::max())
            return std::make_pair(true, i);
    }
    return std::make_pair(false, i);
}

int main()
{
    std::vector<Nums> input;
    std::string line;
    while(std::getline(std::cin, line)) {
        Nums n(line);
        line.erase(std::remove_if(line.begin(), line.end(), isspace), line.end());
        size_t pos;
        if((pos = line.find('+')) != std::string::npos)
            n.op = '+';
        else if((pos = line.find('*')) != std::string::npos)
            n.op = '*';
        n.first = line.substr(0,pos);
        n.second = line.substr(pos+1);
        input.push_back(n);
    }
    for(auto o : input) {
        std::cout << o.input << std::endl;
        auto first = parse(o.first);
        if(first.first) std::cout << "first number too big\n";
        auto second = parse(o.second);
        if(second.first) std::cout << "second number too big\n";
        if(first.first && second.first) std::cout << "result too big\n";
        else {
            unsigned long long result = 0;
            switch(o.op) {
                case '+': result = first.second + second.second;
                          if(result > std::numeric_limits<int>::max())
                              std::cout << "result too big\n";
                          break;

                case '*':
                          result = (first.first || second.first) 
                              ? ((first.second == 0 || second.second == 0) ? 0 : std::numeric_limits<long long>::max()) 
                              : (first.second * second.second);
                          if(result > std::numeric_limits<int>::max())
                              std::cout << "result too big\n";
                          break;
            }
        }
    }
    return 0;
}

