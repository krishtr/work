#include <iostream>
#include <sstream>
#include <stdexcept>
#include "../../Common.hpp"
//#include <fstream>

//std::ofstream file("./out");

void normalise(std::string& str)
{
    // drop the suffix zeros
    auto iter = str.rbegin();
    auto end = str.rend();
    while(iter != end && *iter == '0') {
        str.pop_back();
        ++iter;
    }

    // drop the prefix zeros
    auto it = std::find_if(str.begin(), str.end(), [](char c) { return c != '0'; });
    str.erase(str.begin(),it);
}

std::string power(const std::string& str, int n)
{
    if(n == 0) return std::string("1");
    if(n == 1) {
        std::string c(str);
        normalise(c);;
        return c;
    }

    // find the decimal point
    int precision = 0;
    size_t pos = str.find(".");
    if(pos != std::string::npos)
        precision = int(str.size() - pos - 1);
    std::string copy(str), result;
    copy.erase(std::remove(copy.begin(), copy.end(), '.'), copy.end());
    for(int i = 0; i < n; ++i)
        result = multiply(result, copy);
    if(precision != 0) {
        const int dec = n * precision;
        std::stringstream ss;
        if(int(result.size()) < dec) {
            ss << ".";
            for(int i = 0; i < dec - (int)result.size(); ++i)
                ss << "0";
            ss << result;
            result = ss.str();
        } else if(int(result.size()) == dec) {
            ss << "." << result;
            result = ss.str();
        } else
            result.insert(int(result.size() - dec), ".");
    }
    normalise(result);
    return result;
}

int main()
{
    std::string line;
    while(std::getline(std::cin, line)) {
        std::stringstream ss(line);
        std::string R;
        int n;
        ss >> R >> n;
        std::cout << power(R, n) << std::endl;
    }
    return 0;
}
