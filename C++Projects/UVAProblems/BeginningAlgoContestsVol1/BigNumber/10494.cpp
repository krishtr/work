#include <iostream>
//#include <fstream>
#include "../../Common.hpp"

//std::ofstream file("./out");

int main()
{
    std::string line;
    while(std::getline(std::cin, line)) {
        std::stringstream ss(line);
        std::string num1, op; 
        long long num2;
        ss >> num1 >> op >> num2;
        auto p = getQuotientRemainder(num1, num2);
        switch(op[0]) {
            case '/':
                std::cout << p.first << std::endl;
                break;
            case '%':
                std::cout << p.second << std::endl;
                break;
        }
    }
    return 0;
}
