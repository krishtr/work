#include <iostream>
#include "../../Common.hpp"
//#include <fstream>

//std::ofstream file("./out");

int main()
{
    std::vector<std::string> input;
    std::string line;
    while(std::getline(std::cin, line)) {
        input.push_back(line);
    }
    if(input.size() % 2) return -1;
    
    for(int i = 0; i < (int)input.size(); i+=2) {
        std::string& first = input[i];
        std::string& second = input[i+1];
        if(int(first.size()) < int(second.size()))
            first.swap(second);
        std::string result, temp;
        auto iter = second.rbegin();
        auto end = second.rend();
        for(; iter != end; ++iter) {
            temp = multiplyInt(first, *iter - '0');
            result = add(result, temp);
            first.push_back('0');
        }
        std::cout << result << std::endl;
    }
    return 0;
}
