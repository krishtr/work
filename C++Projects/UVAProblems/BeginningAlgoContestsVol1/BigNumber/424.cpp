#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

int main()
{
    std::vector<std::string> input;
    std::string line;
    int max = -1;
    while(std::getline(std::cin, line)) {
        if(line == "0") break;
        input.push_back(line);
        max = std::max(max, (int)line.size());
    }

    // sum
    std::string result;
    int carry = 0, i = 0;
    while(max--) {
        int temp = 0;
        for(auto line : input) {
            const int index = line.size() - 1 - i;
            temp += (index >= 0) ? line[index] - '0' : 0;
        }
        temp += carry;
        ++i;
        result.push_back(temp % 10 + '0');
        carry = temp / 10;
    }
    while(carry > 0) {
        result.push_back(carry % 10 + '0');
        carry /= 10;
    }
    std::reverse(result.begin(), result.end());
    std::cout << result << std::endl;
    return 0;
}
