#include <iostream>
#include <vector>
#include <string>
#include <algorithm>

int main()
{
    int T;
    while(std::cin >> T) {
        if(T == 0) break;
        std::vector<std::string> lookup(2*T);
        std::getline(std::cin, lookup[0]);
        for(int i = 0; i < 2*T; ++i)
            std::getline(std::cin, lookup[i], '\n');
        std::string str;
        std::getline(std::cin, str, '\n');

        // process
        for(int i = 0; i < (int)lookup.size() - 1; i += 2) {
            const std::string& findStr = lookup[i];
            const std::string& replaceStr = lookup[i+1];
            size_t pos;
            while((pos = str.find(findStr)) != std::string::npos)
                str.replace(pos, findStr.size(), replaceStr);
        }
        std::cout << str << std::endl;
    }

    return 0;
}
