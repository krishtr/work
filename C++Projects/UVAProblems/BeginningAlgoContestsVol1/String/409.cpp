#include <iostream>
#include <string>
#include <unordered_set>
#include <unordered_map>
#include <algorithm>
#include <sstream>
#include <vector>

int main()
{
    int m,n;
    int num = 1;
    while(std::cin >> m >> n) {
        std::unordered_set<std::string> lookup;
        while(m--) {
            std::string temp;
            std::cin >> temp;
            lookup.insert(temp);
        }
        std::vector<std::string> excuses;
        std::string line;
        std::getline(std::cin, line);
        while(n--) {
            std::getline(std::cin, line);
            excuses.push_back(line);
        }

        int max = -1;
        std::unordered_map<int, std::vector<std::string>> cache;
        for(auto line : excuses) {
            std::string word;
            std::string copy;
            std::replace_copy_if(line.begin(), line.end(), std::back_inserter(copy), [](char c) { return !isalpha(c); }, ' ');
            std::stringstream ss(copy);
            int count = 0;
            while(std::getline(ss, word, ' ')) {
                std::transform(word.begin(), word.end(), word.begin(), tolower);
                if(lookup.find(word) != lookup.end())
                    ++count;
            }
            if(count >= max) {
                max = count;
                cache[max].push_back(line);
            }
        }

        std::cout << "Excuse Set #" << num++ << std::endl;
        for(auto l : cache[max])
            std::cout << l << std::endl;
        std::cout << std::endl;
    }
    return 0;
}
