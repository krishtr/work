#include <iostream>
#include <string>
#include <vector>
#include <algorithm>

bool isDecodable(const std::vector<std::string>& vec)
{
    for(int i = 0; i < (int)vec.size(); ++i) {
        for(int j = i+1; j < (int)vec.size(); ++j) {
            if(vec[j].find(vec[i]) == 0)
                return false;
            else if(vec[i].find(vec[j]) == 0)
                return false;
        }
    }
    return true;
}

int main()
{
    std::string str;
    std::vector<std::string> vec;
    vec.reserve(10);
    int i = 0;
    while(std::getline(std::cin, str, '\n')) {
        if(str == "9") {
            std::cout << "Set " << ++i << " is " << (isDecodable(vec) ? std::string() : "not ") 
                << "immediately decodable" << std::endl;
            vec.clear();
        } else {
            vec.push_back(str);
        }
    }
    return 0;
}
