#include <iostream>

int main()
{
    std::string line;
    std::getline(std::cin, line); // ignore first line
    while(std::getline(std::cin, line)) {
        if(line[0] == '-') break;
        unsigned int bit = 1;
        unsigned sum = 0;
        auto iter = line.rbegin();
        auto end = line.rend();
        for(; iter != end; ++iter) {
            if(*iter == '|' || *iter == '.')
                continue;
            if(*iter == ' ')
                bit <<= 1;
            if(*iter == 'o') {
                sum += bit;
                bit <<= 1;
            }
        }
        printf("%c",sum);
    }
    return 0;
}
