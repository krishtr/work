#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <algorithm>

template<class FwdIter>
void split(const std::string& str, char d, FwdIter iter)
{
    std::string line;
    std::istringstream is(str);
    while(std::getline(is, line, d))
        (*iter)++ = line;
}

int main()
{
    int T;
    std::cin >> T;
    std::string tmp;
    std::getline(std::cin, tmp);
    while(T--) {
        std::string str1, str2;
        std::getline(std::cin, str1);
        std::getline(std::cin, str2);
        std::vector<std::string> strSplit;
        split(str1, '<', std::back_inserter(strSplit));
        split(strSplit[1], '>', std::back_inserter(strSplit));
        split(strSplit[2], '>', std::back_inserter(strSplit));
        strSplit.push_back(std::string());
        std::string s2(strSplit[3]), s3(strSplit[4]), s4(strSplit[5]), s5(strSplit[6]);
        str1.erase(std::remove(str1.begin(),str1.end(),'<'), str1.end());
        str1.erase(std::remove(str1.begin(),str1.end(),'>'), str1.end());
        std::cout << str1 << std::endl;
        std::cout << str2.replace(str2.find("..."), 3, std::string(s4+s3+s2+s5).c_str()) << std::endl;
    }
    return 0;
}
