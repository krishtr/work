#include <iostream>
#include <sstream>
#include <algorithm>
#include <set>

int main()
{
    std::string str;
    std::set<std::string> cache;
    while(std::getline(std::cin, str, '\n')) {
        std::replace_if(str.begin(), str.end(), [](char c) { return !isalpha(c); }, ' ');
        std::stringstream is(str);
        std::string word;
        while(is >> word) {
            std::transform(word.begin(), word.end(), word.begin(), tolower);
            cache.insert(word);
        }
    }
    for(auto word : cache)
        std::cout << word << std::endl;
    return 0;
}
