#include <iostream>
#include <iomanip>
#include <string>
#include <regex>
//#include <fstream>

//std::ofstream std::cout("/home/krish/Projects/Work/C++Projects/UVAProblems/out");

bool u_flag = false, p_flag = false, i_flag = false;
void convert(const std::string& str, double& val)
{
    switch(str[str.size() - 2]) {
        case 'M': 
            val *= 1000000;
            break;
        case 'm': 
            val /= 1000;
            break;
        case 'k': 
            val *= 1000;
            break;
    }
}
void parse(const std::string& str, double val, double& u, double& p, double& i)
{
    switch(str[0]) {
        case 'U': 
            u = val;
            u_flag = true;
            break;
        case 'P': 
            p = val;
            p_flag = true;
            break;
        case 'I': 
            i = val;
            i_flag = true;
            break;
    }
}

int main()
{
    int T;
    std::cin >> T;
    std::string temp;
    std::getline(std::cin, temp, '\n');
    bool flag = true;
    const std::regex expr("(U|I|P)=[-.0-9,m,M,k]+(A|V|W)", std::regex_constants::egrep);
    for(int j = 1; j <= T; ++j) {
        std::string str;
        std::getline(std::cin, str, '\n');
        auto start = std::sregex_iterator(str.begin(), str.end(), expr);
        auto end = std::sregex_iterator();
        std::string expr1((*start++).str()), expr2((*start).str());
        double u = 0.0,p = 0.0,i = 0.0;
        double val1 = 0.0, val2 = 0.0;
        if(!expr1.empty())
            val1 = std::stod(std::string(expr1.begin() + 2, expr1.end()));
        if(!expr2.empty())
            val2 = std::stod(std::string(expr2.begin() + 2, expr2.end()));
        convert(expr1, val1);
        convert(expr2, val2);
        parse(expr1, val1, u,p,i);
        parse(expr2, val2, u,p,i);
        if(!flag) std::cout << std::endl;
        if(flag) flag = false;
        std::cout << "Problem #" << j << std::endl;
        std::cout << std::setprecision(2) << std::fixed;
        if(u_flag && p_flag)
            std::cout << "I=" << p/u << "A" << std::endl;
        if(u_flag && i_flag)
            std::cout << "P=" << u*i << "W" << std::endl;;
        if(p_flag && i_flag)
            std::cout << "U=" << p/i << "V" << std::endl;;

        u_flag = p_flag = i_flag = false;
    }
    std::cout << std::endl;
    return 0;
}
