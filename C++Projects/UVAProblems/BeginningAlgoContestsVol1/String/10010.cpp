#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <cctype>
#include <utility>
typedef std::vector<std::string> STRVEC;

// row and col are 0-index based
bool find_right(const STRVEC& input, const std::string& word, int row, int col)
{
    return input[row].substr(col, word.size()) == word;
}
bool find_left(const STRVEC& input, const std::string& word, int row, int col)
{
    const std::string rev(input[row].rbegin(), input[row].rend());
    return rev.substr(input[row].size() - 1 - col, word.size()) == word;
}
bool find_down(const STRVEC& input, const std::string& word, int row, int col)
{
    if(row + word.size() > input.size()) return false;
    for(int i = row, j = 0; i < row + int(word.size()); ++i, ++j)
        if(input[i][col] != word[j])
            return false;
    return true;
}
bool find_ll(const STRVEC& input, const std::string& word, int row, int col) 
{
    if(int(row + word.size()) > (int)input.size()) return false;
    for(int i = row, j = 0; i < int(row + word.size()); ++i, ++j) {
        if(input[i][col--] != word[j])
            return false;
        if(col+1 < 0)
            return false;
    }
    return true;
}
bool find_lr(const STRVEC& input, const std::string& word, int row, int col) 
{
    if(int(row + word.size()) > (int)input.size()) return false;
    for(int i = row, j = 0; i < int(row + word.size()); ++i, ++j) {
        if(input[i][col++] != word[j])
            return false;
        if(col > int(input[i].size()))
            return false;
    }
    return true;
}


bool find_up(const STRVEC& input, const std::string& word, int row, int col)
{
    if(int(row - word.size() + 1) < 0) return false;
    for(int i = row, j = 0; i > int(row - word.size()); --i, ++j) {
        if(input[i][col] != word[j])
            return false;
    }
    return true;
}
bool find_ul(const STRVEC& input, const std::string& word, int row, int col) 
{
    if(int(row - word.size() + 1) < 0) return false;
    for(int i = row, j = 0; i > int(row - word.size()); --i, ++j) {
        if(input[i][col--] != word[j])
            return false;
        if(col + 1 < 0) // if it ends in 0, col will be -ve so we check for col+1
            return false;
    }
    return true;
    
}
bool find_ur(const STRVEC& input, const std::string& word, int row, int col) 
{
    if(int(row - word.size() + 1) < 0) return false;
    for(int i = row, j = 0; i > int(row - word.size()); --i, ++j) {
        if(input[i][col++] != word[j])
            return false;
        if(col > int(input[i].size()))
            return false;
    }
    return true;

}
std::pair<int,int> find(const STRVEC& input, const std::string& word)
{
    // i = row
    for(int i = 0; i < (int)input.size(); ++i) {
        int col = 0;
        while((col = (int)input[i].find(word[0], col)) != -1) {
            bool result = find_up(input, word, i, col) ||
                find_ur(input,word,i,col) || find_right(input,word,i,col) ||
                find_lr(input,word,i,col) || find_down(input,word,i,col) ||
                find_ll(input,word,i,col) || find_left(input,word,i,col) ||
                find_ul(input,word,i,col);
            if(result)
                return std::make_pair(i,col);
            ++col;
        }
    }
    return std::make_pair(-1,-1);
}

int main()
{
    int T = 0;
    std::cin >> T;
    bool start = true;
    while(T--) {
        std::string line;
        std::getline(std::cin, line);
        int m,n;
        std::cin >> m >> n;
        STRVEC input(m);
        for(int i = 0; i < m; ++i) {
            std::cin >> input[i];
            std::transform(input[i].begin(), input[i].end(), input[i].begin(), tolower);
        }
        int k;
        std::cin >> k;
        STRVEC words(k);
        for(int i = 0; i < k; ++i) {
            std::cin >> words[i];
            std::transform(words[i].begin(), words[i].end(), words[i].begin(), tolower);
        }
        if(!start) std::cout << std::endl;
        if(start) start = false;
        for(auto w : words) {
            auto p = find(input, w);
            std::cout << p.first + 1 << " " << p.second + 1 << std::endl;
        }
    }
    return 0;
}
