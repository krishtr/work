import sys

def xrange(x):
    return iter(range(x))

T = input()
for i in xrange(int(T)):
    str1 = input()
    str2 = input()
    l = str1.split("<")
    s2 = l[1].split(">")[0]
    s3 = l[1].split(">")[1]
    s4 = l[2].split(">")[0]
    s5 = l[2].split(">")[1]
    print(str1.replace("<","").replace(">",""))
    print(str2.replace("...", s4+s3+s2+s5))

