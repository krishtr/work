#include <iostream>
#include <string>
#include <unordered_map>

bool isPalindrome(const std::string& str)
{
    std::string::const_iterator front = str.begin(), back = str.end() - 1;
    for (; front < back; ++front, --back)
        if (*front != *back)
            return false;
    return true;
}

bool isMirroredString(const std::string& str)
{
    static const std::unordered_map<char, char> lookup {
        { 'A', 'A' }, { 'E', '3' }, { 'H', 'H' }, { 'I', 'I' },
        { 'J', 'L' }, { 'L', 'J' }, { 'M', 'M' }, { 'O', 'O' },
        { 'S', '2' }, { 'T', 'T' }, { 'U', 'U' }, { 'V', 'V' },
        { 'W', 'W' }, { 'X', 'X' }, { 'Y', 'Y' }, { 'Z', '5' },
        { '1', '1' }, { '2', 'S' }, { '3', 'E' }, { '5', 'Z' },
        { '8', '8' }};
    std::string::const_iterator front = str.begin(), back = str.end() - 1;
    for (; front <= back; ++front, --back) {
        auto m_f = lookup.find(*front);
        if (m_f == lookup.end())
            return false;
        if (front == back)
            return m_f->first == m_f->second;
        else if (m_f->second != *back)
            return false;
    }
    return true;
}

int main()
{
    std::string str;
    while (std::getline(std::cin, str)) {
        bool isPalin = isPalindrome(str);
        bool isMirror = isMirroredString(str);
        std::cout << str;
        if (isPalin) {
            if (isMirror) std::cout << " -- is a mirrored palindrome.";
            else std::cout << " -- is a regular palindrome.";
        } else {
            if (isMirror) std::cout << " -- is a mirrored string.";
            else std::cout << " -- is not a palindrome.";
        }
        std::cout << std::endl;
        std::cout << std::endl;
    }
    return 0;
}

