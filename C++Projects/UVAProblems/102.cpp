#include <iostream>
#include <vector>
#include <iterator>
#include <algorithm>
#include <stdexcept>

int main()
{
    int input[9] = {0};
    while (scanf("%d", &input[0]) != EOF) {
        int matrix[9] = {0,0,0,0,0,0,0,0,0};
        for (int i = 1; i < 9; ++i)
            scanf("%d", &input[i]);

        // create our sum matrix
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                int sum = 0;
                for (int k = 0; k < 3; ++k) {
                    if (k != i)
                        sum += input[3*k+j];
                }
                matrix[3*i+j] = sum;
            }
        }

        std::map<char, int> lookup;
        std::string str("BGC");

        // input is in this order =>
        //  0   1   2     <- Bin numbers
        // BGC BGC BGC    <- Colour codes of bottles in them
        lookup['B'] = 0;
        lookup['G'] = 1;
        lookup['C'] = 2;
        std::map<std::string, int> map;

        ///////////////////////////////////////////////////
        // 1. Get all combinations of arrangement of BGC //
        // 2. This gives all possible combinations in which
        //    respective coloured bottles can end up
        // 3. For a particular combination, find out the
        //    total possible sum by looking up the sum
        //    matrix. eg: GBC = ways in which all green
        //    bottles end up in 1, B bottles end up in 2 and
        //    C bottles end up in 3.
        // 4. Find the minimum of this sum. This will give
        //    which coloured bottle ends up in which container
        // 5. If the sum is the same, the combination should
        //    be selected in the alphabetical order.
        ///////////////////////////////////////////////////
        do {
            map[str] =  matrix[3*0 + lookup[str[0]]] +
                matrix[3*1 + lookup[str[1]]] +
                matrix[3*2 + lookup[str[2]]];
        } while (std::next_permutation(str.begin(), str.end()));
        map[str] =  matrix[3*0 + lookup[str[0]]] +
            matrix[3*1 + lookup[str[1]]] +
            matrix[3*2 + lookup[str[2]]];

        int min = INT_MAX; std::string result;
        std::map<std::string, int>::iterator iter (map.begin());
        std::map<std::string, int>::iterator end (map.end());
        for (; iter != end; ++iter) {
            if (iter->second < min) {
                min = iter->second;
                result = iter->first;
            } else if (iter->second == min) {
                if (iter->first < result)
                    result = iter->first;
            }
        }

        std::cout << result << " " << min << std::endl;
    }
    return 0;
}
