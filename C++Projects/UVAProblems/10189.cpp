#include <iostream>
#include <vector>
#include <iterator>

template<class T>
void display(const std::vector<std::vector<T>>& vec)
{
    for(auto v : vec) {
        std::copy(v.begin(), v.end(), std::ostream_iterator<T>(std::cout));
        std::cout << std::endl;
    }
}

int getVal(const std::vector<std::vector<char>>& A, int i, int j)
{
    return (A[i][j] == '*') ? 1 : 0;
}

//
// 1 row 1 col
// 1 row many col
// many row 1 col
// many row many col
// 
char get(const std::vector<std::vector<char>>& A, int i, int j)
{
   int val = -1;
   if(A.size() == 1 && A[0].size() == 1)
       val = getVal(A, 0, 0);
   else if(A.size() == 1 && A[0].size() > 1) { // 1 row
       if(j == 0) 
           val = getVal(A,i,j+1);
       else if(j == (int)A[0].size() - 1)
           val = getVal(A,i,j-1);
       else
           val = getVal(A,i,j+1) + getVal(A,i,j-1);
   } else if(A.size() > 1 && A[0].size() == 1) { // 1 col
       if(i == 0)
           val = getVal(A,i+1,j);
       else if(i == (int)A.size() - 1)
           val = getVal(A,i-1,j);
       else
           val = getVal(A,i-1,j) + getVal(A,i+1,j);
   } else {
       if(i == 0) {
           val = getVal(A,i+1,j);
           if(j == 0)
               val += getVal(A,i,j+1) + getVal(A,i+1,j+1);
           else if(j == (int)A[0].size() - 1)
               val += getVal(A,i,j-1) + getVal(A,i+1,j-1);
           else
               val += getVal(A,i,j+1) + getVal(A,i,j-1) + getVal(A,i+1,j-1) + getVal(A,i+1,j+1);
       } else if(i == (int)A.size() - 1) {
           val = getVal(A,i-1,j);
           if(j == 0)
               val += getVal(A,i-1,j+1) + getVal(A,i, j+1);
           else if(j == (int)A[0].size() - 1)
               val += getVal(A,i-1,j-1) + getVal(A,i, j-1);
           else
               val += getVal(A,i,j-1) + getVal(A,i,j+1) + getVal(A,i-1,j-1) + getVal(A,i-1,j+1);
       } else {
           val = getVal(A,i-1,j) + getVal(A,i+1,j);
           if(j == 0)
               val += getVal(A,i-1,j+1) + getVal(A,i,j+1) + getVal(A,i+1,j+1);
           else if(j == (int)A[0].size() - 1)
               val += getVal(A,i-1,j-1) + getVal(A,i,j-1) + getVal(A,i+1,j-1);
           else
               val += getVal(A,i-1,j-1) + getVal(A,i-1,j+1) + getVal(A,i,j-1) + getVal(A,i,j+1)
                   +  getVal(A,i+1,j-1) + getVal(A,i+1,j+1);
       }
   }
   return std::to_string(val)[0];
}

int main()
{
    int n = 0, m = 0;
    int count = 1;
    bool start = true;
    while(std::cin >> n >> m) {
        if(n == 0 && m == 0) break;
        if(!start) std::cout << std::endl;
        if(start) start = false;
        std::vector<std::vector<char>> A(n, std::vector<char>(m,'#'));
        for(int i = 0; i < n; ++i)
            for(int j = 0; j < m; ++j)
                std::cin >> A[i][j];
        std::vector<std::vector<char>> out(n, std::vector<char>(m,'#'));
        for(int i = 0; i < n; ++i) {
            for(int j = 0; j < m; ++j) {
                if(A[i][j] == '*')
                    out[i][j] = '*';
                else
                    out[i][j] = get(A,i,j);
            }
        }
        std::cout << "Field #" << count++ << ":" << std::endl;
        display(out);
    }
    return 0;
}
