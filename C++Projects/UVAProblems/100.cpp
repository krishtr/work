#include <iostream>
#include <unordered_map>
#include <unordered_set>

std::unordered_map<int,int> lookup;

// 
// NF = Not Found, F = Found
// lookup   visited     behavior
//  NF      NF          Insert in visited, insert in lookup and increment
//  NF      F           NA
//  F       NF          new iteration. found in lookup due to prev iteration so shud hv the count, return that
//  F       F           found in this iteration. return the count.
//

int cycleLength(int num)
{
    int count = 0;
    int origNum = num;
    std::unordered_map<int,int>::iterator iter;
    std::unordered_set<int> visited;
    while(true) {
        if((iter = lookup.find(num)) != lookup.end()) {
            count += iter->second;
            for(auto i : visited)
                lookup[i] += iter->second;
            break;
        }
        ++count;
        visited.insert(num);
        for(auto i : visited)
            lookup[i]++;
        if(num == 1) break;
        if (num % 2) num = 3 * num + 1;
        else num /= 2;
    }
    lookup[origNum] = count;
    return lookup[origNum];
}

int main()
{
    int i = 0,j = 0, m = 0, n = 0, max = 0;
    while(std::cin >> i >> j) {
        m = i; n = j;
        if (i > j)
            std::swap(m,n);

        for (int k = m; k <= n; ++k) {
            int cl = cycleLength(k);
            max = std::max(cl, max);
        }
        std::cout << i << " " << j << " " << max
            << std::endl;
        max = 0;
    }
    return 0;
}
