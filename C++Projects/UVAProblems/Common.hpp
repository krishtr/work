#include <vector>
#include <string>
#include <sstream>
#include <algorithm>

std::string add(const std::string& f, const std::string& s)
{
    if(f.empty() && s.empty()) return std::string("0");
    if(f.empty()) return s;
    if(s.empty()) return f;
    std::string result;
    int max = std::max((int)f.size(), (int)s.size());
    int carry = 0;
    for(int i = 0; i < max; ++i) {
        const int x = (int(f.size()) - i - 1 < 0) ? 0 : f[int(f.size())-1-i] - '0';
        const int y = (int(s.size()) - i - 1 < 0) ? 0 : s[int(s.size())-1-i] - '0';
        const int temp = x + y + carry;
        result.push_back(temp % 10 + '0');
        carry = temp / 10;
    }
    while(carry > 0) {
        result.push_back(carry % 10 + '0');
        carry /= 10;
    }
    std::reverse(result.begin(), result.end());
    return result;
}

//
// assume that n < num, if not return "-"
std::string subtract(const std::string& num, const std::string& n)
{
    if(n == "0") return num;
    if(num.empty()) return std::string("0");
    if(n.empty()) return num;
    if(int(num.size()) < int(n.size())) return std::string("-");
    if(int(num.size()) == int(n.size()))
        if(num[0] < n[0]) return std::string("-");
    int borrow = 0;
    std::string result;
    for(int i = 0; i < (int)num.size(); ++i) {
        int n1 = num[num.size() - 1 - i] - '0' + borrow;
        int n2 = (i >= (int)n.size()) ? 0 : n[n.size() - 1 - i] - '0';
        if(n1 < n2) {
            if(i == (int)num.size() - 1)
                return std::string("-"); // we cant borrow anymore since num < n
            n1 += 10;
            borrow = -1;
        } else {
            borrow = 0;
        }
        int diff = n1 - n2;
        result.push_back(diff + '0');
    }
    std::reverse(result.begin(), result.end());
    // remove leading zeros
    auto it = std::find_if(result.begin(), result.end(), [](char c){ return c != '0'; });
    result.erase(result.begin(), it);
    if(result.empty()) result = "0";
    return result;
}

std::string  multiplyInt(const std::string& str, int n)
{
    if(n == 0) return std::string("0");
    if(n == 1) return str;
    std::string result;
    auto iter = str.rbegin();
    auto end = str.rend();
    int carry = 0;
    for(; iter != end; ++iter) {
        const int temp = (*iter - '0') * n + carry;
        result.push_back(temp % 10 + '0');
        carry = temp / 10;
    }
    while(carry > 0) {
        result.push_back(carry % 10 + '0');
        carry /= 10;
    }
    std::reverse(result.begin(), result.end());
    return result;
}

std::string multiply(const std::string& lhs, const std::string& rhs)
{
    if(lhs.empty() && rhs.empty()) return std::string("0");
    if(lhs.empty()) return rhs;
    if(rhs.empty()) return lhs;

    std::string first(lhs), second(rhs);
    if(int(first.size()) < int(second.size()))
        first.swap(second);
    std::string result, temp;
    auto iter = second.rbegin();
    auto end = second.rend();
    for(; iter != end; ++iter) {
        temp = multiplyInt(first, *iter - '0');
        result = add(result, temp);
        first.push_back('0');
    }
    return result;
}

//
// Very slow method
std::string divide(const std::string& num, const std::string& n)
{
    std::string result(num), q("0");
    while((result = subtract(result, n)) != "-")
        q = add(q,"1");
    return q;
}

//
// get quotient and remainder
// first: quotient, second: remainder
// high school method
// ex: 18928 / 12
// first look at 18...12 * 1 = 12, remainder 6
// next 69, 12 * 5 = 60, r: 9
// next 92, 12 * 7 = 84, r: 8
// next 88, 12 * 7 = 84, r: 4
// so, q: 1577, r: 4
std::pair<std::string, long long> 
getQuotientRemainder(const std::string& num, long long n)
{
    std::string q;
    q.reserve(num.size());
    auto iter = num.begin(), end = num.end();
    long long r = 0;
    for(; iter != end; ++iter) {
        r = *iter - '0' + r * 10;
        q.push_back(int(r / n) + '0'); // collect quotient
        r %= n;
    }
    // r will be the remainder
    // remove leading 0's from q if any
    auto it = std::find_if(q.begin(), q.end(), [](char c) { return c != '0'; });
    q.erase(q.begin(), it);
    if(q.empty()) q = "0";
    return std::make_pair(q, r);
}

//
// 99 % 10 = 9 => 99 / 10 = 9, 99 - 9 * 10 = 9
std::string mod(const std::string& lhs, const std::string& rhs)
{
    auto q = divide(lhs, rhs);
    return subtract(lhs, multiply(rhs, q));
}

