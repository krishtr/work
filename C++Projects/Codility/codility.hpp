#include <iostream>
#include <iomanip>
#include <string>
#include <cstring>
#include <sstream>
#include <bitset>
#include <set>
#include <map>
#include <stack>
#include <algorithm>
#include <vector>
#include <ctime>

#include <thread>
#include <memory>
#include <functional>
#include <iterator>
#include <mutex>
#include <chrono>

int ReverseEachWord()
{
  std::string str("The boy ran");
  std::cout << str << std::endl;
  size_t pos = 0, newpos = 0;
  for(;;) {
    newpos = str.find(" ", pos);
    if(newpos == std::string::npos) {
      std::reverse(&str[pos], &str[str.length()]);
      break;
    }
    std::reverse(&str[pos], &str[newpos]);
    pos = newpos + 1;
  }
  std::cout << str << std::endl;
  return 0;
}

int BinaryAddition(std::string& num1, std::string& num2)
{
  int one = 0, two = 0;
  std::string::reverse_iterator riter(num1.rbegin());
  std::string::reverse_iterator rend(num1.rend());
  for(int i = 0; riter != rend; ++riter, ++i)
    one += std::pow(2,i) * (*riter - '0');
  riter = num2.rbegin();
  rend = num2.rend();
  for(int i = 0; riter != rend; ++riter, ++i)
    two += std::pow(2,i) * (*riter - '0');

  std::cout << "One: " << one << ", Two: " << two << std::endl;
  int result = one + two;
  std::stringstream ss;
  while(result) {
    ss << result % 2;
    result /= 2;
  }
  ss << result;
  std::string temp(ss.str());
  std::reverse(temp.begin(), temp.end());
  std::cout << "Result: " << temp << std::endl;
  return 0;
}

int BinaryAdditionUsingBitset(std::string& one, std::string& two)
{
  std::bitset<13> num1(one);
  std::bitset<7> num2(two);
  ulong res = num1.to_ulong() + num2.to_ulong();
  std::cout << std::bitset<16>(res).to_string() << std::endl;

  std::cout << std::endl;
  return BinaryAddition(one, two);
}

void myFunc(const char* str)
{
  if(!str) return;
  if(strlen(str) == 0) return;

  const char* iter = &str[0];
  const char* end = &str[strlen(str)];
  char curr_char = str[0], result_char = str[0];
  int curr_count = 0, max_count = 0;

  while(iter != end) {

    if(*iter == curr_char) {
      ++curr_count;
      if(curr_count > max_count) {
        max_count = curr_count;
        result_char = curr_char;
      }
    } else {
      curr_char = *iter;
      curr_count = 1;
    }

    ++iter;
  }

  std::cout << result_char << max_count << std::endl;
}

int Fibonacci(int n)
{
  if(n == 1 || n == 2)
    return 1;
  int grandparent = 1, parent = 1, me = 0;
  n -= 2;
  while(n) {
    me = grandparent + parent;
    grandparent = parent;
    parent = me;
    --n;
  }
  return me;
}

template<class fwdIter, class backIter>
void process(fwdIter iter, fwdIter begin, fwdIter end, backIter place)
{
  while(iter != end) {
    int j = (iter == begin) ? 1 : (*iter + *(iter - 1));
    std::cout << j << " ";
    *place++ = j;
    ++iter;
  }
  if(iter == end) {
    std::cout << 1 << std::endl;
    *place++ = 1;
  }
}

void pascalTriangle(int n)
{
  if(n < 0)
    return;
  std::vector<int> prev, curr;
  prev.resize(n+1);
  curr.resize(n+1);
  prev[0] = 1;
  std::cout << 1 << std::endl;
  for(int i = 1; i <= n; ++i) {
    curr[i] = curr[0] = 1;
    std::cout << 1;
    for(int j = 1; j < i; ++j) {
      curr[j] = prev[j-1] + prev[j];
      std::cout << " " << curr[j];
    }
    std::cout << " " << 1 << std::endl;
    curr.swap(prev);
  }
}

void pTriangle(int n)
{
  if(n < 0) return;
  std::vector<int> odd, even;
  std::cout << 1 << std::endl;
  even.push_back(1);
  std::vector<int>::iterator iter, end;
  for(int i = 1; i <= n; ++i) {
    for(int k = 1; k <= (n-i); ++k)
      std::cout << " ";
    if(i % 2 == 0) {
      iter = odd.begin();
      end = odd.end();
      even.clear();
      even.reserve(i);
      process(iter,odd.begin(),end,std::back_inserter(even));
    } else {
      iter = even.begin();
      end = even.end();
      odd.clear();
      odd.reserve(i);
      process(iter,even.begin(),end,std::back_inserter(odd));
    }
  }
  return;
}

std::vector<int> solution(std::string& s,
                          std::vector<int>& P,
                          std::vector<int>& Q)
{
  char lookup[256] = {0};
  lookup['A'] = 1;
  lookup['C'] = 2;
  lookup['G'] = 3;
  lookup['T'] = 4;
  const int M = P.size();
  std::vector<int> results(M, 0);
  for(int i = 0; i < M; ++i) {
    std::string str = s.substr(P[i], Q[i] - P[i] + 1);
    std::sort(str.begin(), str.end());
    results[i] = lookup[(int)str[0]];;
  }

  return results;
}

// Review this; A <= B
int WholeSquareCount(int A, int B)
{
  // cannot have 0 squares
  if(A <= 0 && B <= 0) return 0;
  if(A == B) return 0;

  const int N = std::sqrt(B);
  int count = 0;
  for(int i = 1; i <= N; ++i) {
    const int sq = i*i;
    if(sq >= A && sq <= B)
      ++count;
  }
  return count;
}

int ComplementaryPairs(int K, std::vector<int>& A)
{
  std::multiset<int> set;
  std::copy(A.begin(), A.end(), std::inserter(set, set.begin()));
  std::copy(set.begin(), set.end(), std::ostream_iterator<int>(std::cout, ", "));
  std::cout << std::endl;
  auto end = set.end();
  auto iter = set.begin();
  int count = 0, diff = 0;
  for(; iter != end; ++iter) {
    diff = K - *iter;
    auto pair = set.equal_range(diff);
    count += std::distance(pair.first, pair.second);
  }

  return count;
}

// Review this
int ComplementaryPairs1(int K, std::vector<int>& A)
{
  std::map<int64_t, int> map;
  const int len = A.size();
  for(int i = 0; i < len; ++i) {
    const int64_t diff = (int64_t)K - A[i];
    const int temp = (map.find(diff) != map.end()) ? map[diff] : 0;
    map[diff] = temp + 1;
  }

  int count = 0;
  for(int i = 0; i < len; ++i) {
    const int val = A[i];
    count += (map.find(val) != map.end()) ? map[val] : 0;
  }

  return count;
}

int nesting(const std::string& s)
{
  std::stack<char> stack;
  auto iter = s.begin();
  auto end = s.end();
  for(; iter != end; ++iter) {
    if(*iter == '(')
      stack.push(*iter);
    if(*iter == ')')
      stack.pop();
  }
  return stack.size() == 0;
}

std::string GetBinary(int N)
{
  if (N == 0) return std::string();
  std::bitset<32> set(N);
  std::string str = set.to_string();
  // remove the leading zeros
  // eg: 0000000000111000111000 -> 111000111000
  auto pos = str.find_first_of("1");
  return (pos == std::string::npos)
         ? std::string()
         : str.substr(pos);
}

// review - BinaryPeriod
int BinaryPeriod(int N){
  std::string binary = GetBinary(N);
  if (binary.empty()) return -1;
  int min = std::numeric_limits<int>::max();
  int Q = binary.length();
  for (int P = 1; P <= Q / 2; ++P) {
    int diff = Q - P;
    bool fail = false;
    for (int K = 0; K < diff; ++K) {
      if (binary[K] != binary[K + P]) {
        K = diff;
        fail = true;
      }
    }
    if (!fail)
      return P;
  }
  if (min == std::numeric_limits<int>::max())
    return -1;
  return min;
}

// Determine whether a triangle can be built from a given set of edges.
int Triangle(std::vector<int> &A) {
    const int N = A.size();
    if(N <= 2) return 0;
    std::sort(A.begin(), A.end());
    for(int i = 1; i < N-1; ++i) {
        uint64_t sum = A[i-1] + A[i];
        if(sum > (uint64_t)A[i+1])
            return 1;
    }
    return 0;
}

// MissingInteger
// Find the minimal positive integer not occurring in a given sequence.
int MissingInteger(std::vector<int>& A){
  std::sort(A.begin(), A.end());
  A.erase(std::unique(A.begin(), A.end()), A.end());
  const int N = A.size();
  auto iter = std::find_if(A.begin(), A.end(), [](int i) {
    return i > 0;
  });
  if (iter == A.end()) return 1;
  if (*iter > 1) return 1;
  int lookup = *iter + 1;
  for (int i = iter - A.begin() + 1; i < N; ++i) {
    if (A[i] > lookup)
      break;
    lookup += 1;
  }
  return lookup;
}

// Calculate the values of counters after applying all
// alternating operations: increase counter by 1; set value of all
// counters to current maximum.
std::vector<int> MaxCounters(int N, std::vector<int>& A){
  int max = 0;
  bool amplify = false;
  int ampfactor = 0;
  std::vector<int> results(N, 0);
  const int M = A.size();
  for (int i = 0; i < M; ++i) {
    if (A[i] == N + 1) {
      amplify = true;
      ampfactor = max;
    } else {
      if (amplify && (results[A[i] - 1] == 0 || results[A[i] - 1] < ampfactor))
        results[A[i] - 1] = ampfactor + 1;
      else
        results[A[i] - 1]++;
      max = std::max(results[A[i] - 1], max);
    }
  }
  for (int i = 0; i < N; ++i) {
    if (amplify && results[i] < ampfactor)
      results[i] = ampfactor;
  }
  return results;
}

// Given n sticks, count the number of triangles that can be constructed
// using these sticks.
// 10,2,5,1,8,12
// 1,2,5,8,10,12
int CountTriangles(std::vector<int>& A)
{
  const int N = A.size();
  std::sort(A.begin(), A.end());
  int result = 0;
  int min = std::numeric_limits<int>::max();
  for(int i = 0; i < N; ++i) {
    int z = 0;
    for(int j = i+1; j < N; ++j) {
      while(z < N && ((A[i] + A[j]) > A[z])) {
        if(i < j && j < z) {
          min = std::min(min, A[i] + A[j] + A[z]);
        }
        z += 1;
      }
      result += z - j - 1;
    }
  }
  return result;
}

// Review this
std::vector<int> CapitalCityDistance(std::vector<int>& T)
{
  const int N = T.size();
  int capital = -1;
  for (int i = 0; i < N; ++i) {
    if (i == T[i]) {
      capital = i;
      break;
    }
  }
  // sanity check
  if (capital == -1) return std::vector<int>();
  std::vector<int> result(N - 1, 0);
  for (int i = 0; i < N; ++i) {
    int curr = i;
    if (curr == capital)
      continue;

    int distance = 0;
    while (capital != T[curr]) {
      ++distance;
      curr = T[curr];
    }
    result[distance]++;
  }
  return result;
}

/*
   Write a function:
    int solution(int A, int B, int K);
that, given three integers A, B and K, returns the number of integers
within the range [A..B] that are divisible by K,
Assume:
  A and B are integers within the range [0..2,000,000,000];
  K is an integer within the range [1..2,000,000,000];
  A ≤ B.


expected worst-case time complexity is O(1);
expected worst-case space complexity is O(1).
 */

int factors(int A, int B, int K)
{
  if (K == 1) return B - A + 1;
  const int last = B / K;
  int count = 0;
  for(int i = 1; i <= last; ++i) {
    const int mult = K * i;
    if(mult >= A && mult <= B)
      ++count;
  }
  return count;
}


// check problem_statement.txt, problem statement (3)
int sumSliceToZero(std::vector<int>& A)
{
    // 2,-2,3,0,4,-7
    int count = 0, sum = 0;
    std::map<int, int> cache;
    cache[0] = 1;
    for(int i = 0; i < A.size(); ++i) {
       sum += A[i];
       count += cache[sum];
       cache[sum]++;
    }
    return count > 10000000 ? -1 : count;
}

// problem_statement.txt problem (2)
//
int rectilinear(int K, int L, int M, int N, int P, int Q, int R, int S)
{
//        (M,N)
//
// (K,L)

}




