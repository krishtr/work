#include <iostream>
#include "codility.hpp"

int main()
{
    //std::vector<int> A{2,-2,3,0,4,-7};
    std::vector<int> A {std::istream_iterator<int>(std::cin), std::istream_iterator<int>()};
    std::copy(A.begin(), A.end(), std::ostream_iterator<int>(std::cout, ","));
    std::cout << std::endl;
    std::cout << "number of slices that sum to zero: " << sumSliceToZero(A);
    std::cout << std::endl;
    return 0;
}
