x = 50

def func():
    global x
    print("x is:", x)
    x = 20
    print("global x is changed to:", x)

func()
print("x is:", x)
