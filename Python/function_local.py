x = 2

def func(x):
    print("x is:", x)
    x = 20
    print("x is changed to:", x)

func(x)
print("x is still:", x)
