import tarfile
import os

def main():
    source = "/home/krish/Projects/work/Python"
    ext = ".py"
    archive_name = "backup.tar.gz"

    print("Source: {}\nTarget: {}".format(os.path.join(source, ext), archive_name))

    tar = tarfile.TarFile.gzopen(name=archive_name, mode='w')
    sourcefiles = [f for f in os.listdir(source) if os.path.isfile(f) and os.path.splitext(f)[1] == ext]
    for f in sourcefiles:
        tar.add(os.path.join(source, f))
    tar.close()

if __name__ == "__main__":
    main()
