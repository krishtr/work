while True:
    s = input("Enter something: ")
    if s == "quit":
        break
    elif len(s) < 3:
        print("Input str is too small")
        continue
    print("Correct length of {}".format(len(s)))

print("Done")
