name = "Krishnan"

if name.startswith("Kri"):
    print("Name starts with Kri")

if 'h' in name:
    print("Name has a 'h' in it")

if name.find("ish") != -1:
    print("Name has an ish")

delimiter = '_*_'
bric = ['Brazil','Russia','India','China']
print(delimiter.join(bric))
