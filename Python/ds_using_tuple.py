zoo = ('elephant', 'python', 'lion')
print('length of zoo:', len(zoo))

new_zoo = ('monkey', 'camel', zoo)
print('length of new_zoo:', len(new_zoo))

print('zoo animals are:', zoo)
print('new_zoo animals are:', new_zoo)

print('Animals brought in from zoo to new_zoo are:', new_zoo[2])    
print('last animal to be brought into the new zoo:', new_zoo[2][2])
print('Number of animals in new zoo is:',len(new_zoo) - 1 + len(zoo))
