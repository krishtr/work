def p(str,s):
    print(str + ":" + s)

shoplist = ['apple', 'banana', 'grapes', 'oranges']
name = "Krishnan"

print(shoplist)

#indexing
p("0:",shoplist[0])
p("1:",shoplist[1])
p("2:",shoplist[2])
p("3:",shoplist[3])
p("-1:",shoplist[-1])
p("-2:",shoplist[-2])

print(name)

#string indexing
p("0:",name[0])
p("1:",name[1])
p("2:",name[2])
p("3:",name[3])
p("-1:",name[-1])
p("-2:",name[-2])

print()
print(shoplist)

#list slicing
print("1:3",shoplist[1:3])
print("1:",shoplist[1:])
print(":",shoplist[:])
print("1:-1",shoplist[1:-1])
print("-1:1",shoplist[-1:1])
print("-1:0",shoplist[-1:0])

print()
print(name)

#name slicing
print("1:3",name[1:3])
print("1:",name[1:])
print(":",name[:])
print("1:-1",name[1:-1])
print("-1:1",name[-1:1])
print("-1:0",name[-1:0])
