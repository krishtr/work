def print_max(x, y):
    '''
        Prints the max of the 2 given numbers
        Input expected are 2 integers.    
    '''
    x = int(x)
    y = int(y)

    return "max({},{}): {}".format(x,y,max(x,y))

x = int(input("Enter 1st number: "))
y = int(input("Enter 2nd number: "))

print(print_max.__doc__)
print(print_max(x,y))

print()
print(help(print_max))
