#!/bin/python

# 1 - find last element in the List
def findLastElement(List):
    return findKthElement(List, len(List))

# 2 - find last but one element in the List
def findPenultimateElement(List):
    return findLastElement(List[:-1])

# 3 - find Kth element in the List
def findKthElement(List, k):
    length = len(List)
    if length == 0 or k > length or k <= 0:
        return None
    count = 0
    for i in List:
        count += 1
        if count == k:
            return i

# 4 - find the number of elements in the List
def findLength(List):
    count = 0
    for i in List:
        count += 1
    return count

# 5 - reverse a List
def reverse(List):
    length = len(List)
    if length == 0 or length == 1:
        return List
    r = []
    i = length - 1
    while i >= 0:
        r.append(List[i])
        i -= 1
    return r

# 6 - check if its a palindrome
def palindrome(List):
    length = len(List)
    if length == 0 or length == 1:
        return True

    last = length - 1
    first = 0
    while first < last:
        if List[first] != List[last]:
            return False
        first += 1
        last -= 1
    return True

# helper functions
def display(List):
    for i in List: print i,
    print ""

def main():
    List = [1,2,3,4,5,4,3,2,1]
    display(List)
    print "%s" % findLastElement(List)
    print "%s" % findPenultimateElement(List)
    print "%s" % findKthElement(List, 5)
    print "%s" % findLength(List)
    r = reverse(List)
    display(r)
    print "%s" % palindrome(list("malayalam"))

if __name__ == "__main__":
    main()
