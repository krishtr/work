shoplist = ['apple','banana','carrots','orange']
print("I have", len(shoplist), "items to purchase")

print("The items are:", end = " ")
for item in shoplist:
    print(item, end = " ")

print("\nI also have to buy rice")
shoplist.append("rice")
print("Shopping list is now: ", shoplist)

print("I will sort my list now")
shoplist.sort()
print("Sorted list is:", shoplist)

print("The first item I'm going to buy:", shoplist[0])
olditem = shoplist[0]
del shoplist[0]
print("I have already bought:", olditem)

print("Shopping list is now:", shoplist)
