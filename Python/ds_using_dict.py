def list(ab):
    print("There are {} items in the address-book".format(len(ab)))
    for first,second in ab.items():
        print("{}: {}".format(first,second))
    print()

ab = {
    "Krish" : 'krishtr@gmail.com',
    "Aarthi": 'rtkrish@gmail.com',
    "KWork" : 'krishnan.ramaseshan@barclays.com',
    "AWork" : 'aarthi.iyer@sita.aero'
}

list(ab)

#add
print("Adding hotmail address")
ab["KHotmail"] = 'krishtr@hotmail.com'

list(ab)

#delete
print("Deleting KWork")
del ab["KWork"]

list(ab)

#search
if "Kwork" in ab:
    print("KWork email address found:", ab["KWork"])
if "Krish" in ab:
    print("Krish's email address found:", ab["Krish"])
