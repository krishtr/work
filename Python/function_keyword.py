def say(a, b=10, c=20):
    print("a:{}, b:{}, c:{}".format(a,b,c))

say(1)
say(1, 2)
say(c=50, a=100, b=900)
say(15, c=14, b=13)
