print("Simple Assignment")
shoplist = ['apple','mango','banana','orange']
print("Taking a reference")
mylist = shoplist

print(shoplist)
print(mylist)

print("removing first element from the shoplist")
del shoplist[0]
print("mylist:")
print(mylist)

print("taking a copy of the shoplist")
copylist = shoplist[:]
print(shoplist)
print(copylist)

print("removing first element of the shoplist")
del shoplist[0]
print(shoplist)
print(copylist)

