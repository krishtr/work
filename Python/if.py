num = 42
guess = int(input("Enter a number:"))

if num == guess:
    print("Congratulations! You guessed the right number!")
    print("(But you do not win any prizes)")
elif guess < num:
    print("The answer is a little higher than that")
else:
    print("The answer is a little lower than that")

print("Done")
