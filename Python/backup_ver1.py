import os
import time
import sys

def main():
    src = "~/Projects/work/Python"
    files = "*.py"
    backup = "{}_{}.tar.gz".format("backup", time.strftime("%Y%m%d"))
    print("source path: {}\nbackup path: {}".format(os.path.join(src,files), backup))

    cmd = "cd {} && tar cfz {} {}".format(src, backup, files)
    ret = os.system(cmd)
    if ret == 0:
        print("backup succeded")
    else:
        print("backup failed. return code: {}".format(ret))

if __name__ == "__main__":
    main()
