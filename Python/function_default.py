def display(message, times=1):
    print(message * times)

display("Hello")
display("World", 5)
