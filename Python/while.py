num = 42
running = True

while running:
    guess = int(input("Enter a number: "))
    if guess == num:
        print("Congratulations! You guessed the number right!")
        print("(But you dont get any prizes!)")
        running = False
    elif guess < num:
        print("Your guess is a little too low, guess higher")
    else:
        print("Your guess is a little too high, guess lower")

print("Done")
