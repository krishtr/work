age = 37
name = 'Krishnan'

print ('{} is {} years old when he is learning this language'.format(name, age))
print ('Why is {} playing with that python?'.format(name))
print ('{0:.5f}'.format(1000.0 * 87.3456))
print ('{} is learning {}'.format('Krishnan', 'python'))
print ('{0:_^12}'.format('hello'))
print ('a', end='')
print ('b')
print ('''multi-string testing
          can you check if the multi-string appears?
          is this python 2 or python 3?''')
