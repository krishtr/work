def total(a=5, *numbers, **phonebook):
    print("a:", a)

    for item in numbers:
        print(item)
    
    for first, second in phonebook.items():
        print("{}: {}".format(first, second))

print(total(10,1,2,3,Aarthi=34,Krishnan=37,Meera=5))
