def max(x, y):
    if x > y:
        return x
    elif x == y:
        return "2 given values are the same"
    else:
        return y

x = int(input("Enter 1st number: "))
y = int(input("Enter 2nd number: "))

print("Max of ({},{}): {}".format(x, y, max(x, y)))
