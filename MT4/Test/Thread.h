#ifndef THREAD_H_
#define THREAD_H_

#include <thread>
#include "LockedQueue.h"

enum CALLBACK_TYPE
{
    TYPE_PENDINGS	= 1,
    TYPE_STOP		= 2
};

struct WorkItem {
    const MT::TradeRecord* trade;
    CALLBACK_TYPE type;
};

class Thread {
    public:
        Thread();
        virtual ~Thread();

        void start();
        void join();
        virtual void run() = 0;

        std::thread::id self() const { return id_; }
        void SetId(std::thread::id id) { id_ = id; }

        void SetRunning(bool flag) { isRunning_ = flag; }
        bool GetRunning() const { return isRunning_; }

    private:
        Thread& operator=(const Thread&);
        std::thread* thread_;
        std::thread::id id_;
        bool isRunning_;
};

class ConsumerThread : public Thread {
    public:
        explicit ConsumerThread(LockedQueue<WorkItem*>& q);
        void run();
    private:
        LockedQueue<WorkItem*>& queue_;
        ConsumerThread& operator=(const ConsumerThread&);
};
#endif
