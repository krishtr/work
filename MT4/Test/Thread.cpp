#include "stdafx.h"
#include <iostream>
#include <string>
#include "Thread.h"

Thread::Thread()
    : thread_(nullptr)
      , isRunning_(false)
{}

Thread::~Thread() {
    if (thread_ && isRunning_) {
        SetRunning(false);
        thread_->join();
    }

    if (thread_) {
        delete thread_;
        thread_ = nullptr;
    }
}

static void runThread(void * args) {
    auto ptr = (static_cast<Thread*>(args));
    ptr->SetId(std::this_thread::get_id());
    ptr->SetRunning(true);
    ptr->run();
}

void Thread::start() {
    thread_ = new std::thread(runThread, this);
}

ConsumerThread::ConsumerThread(LockedQueue<WorkItem*>& q) : queue_(q) {}
void ConsumerThread::run() {
    ptrProcessor->LogInfo("ConsumerThread::run()");
    for (;;) {
        auto item = queue_.RemoveItem();
        if (item == nullptr) break;
        if (item) {
            ptrProcessor->ProcessWorkItem(item);
            delete item;
        }
    }
    ptrProcessor->LogInfo("Stopping ConsumerThread");
}
