#ifndef LOCKEDQUEUE_H_
#define LOCKEDQUEUE_H_

#include <list>
#include <mutex>
#include <condition_variable>

template<class T>
class LockedQueue {
    public:
        void AddItem(T item);
        T RemoveItem();
        size_t size();
    private:
        std::list<T> list_;
        std::mutex mutex_;
        std::condition_variable cond_var_;
};

template<class T>
void LockedQueue<T>::AddItem(T item) {
    std::lock_guard<std::mutex> gate(mutex_);
    list_.push_back(item);
    cond_var_.notify_one();
}

template<class T>
T LockedQueue<T>::RemoveItem() {
    std::unique_lock<std::mutex> lock_(mutex_);
    cond_var_.wait(lock_, [&](){ return !list_.empty(); });
    T item = list_.front();
    list_.pop_front();
    lock_.unlock();

    return item;
}

template<class T>
size_t LockedQueue<T>::size() {
    std::lock_guard<std::mutex> lock_(mutex_);
    return list_.size();
}
#endif
