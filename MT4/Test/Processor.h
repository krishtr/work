#ifndef PROCESSOR_H_
#define PROCESSOR_H_

#include <fstream>
#include <sstream>
#include <sys\timeb.h>
#include <set>

#include "Thread.h"

class Logger
{
    public:
        Logger() : out_(setPath("Test"), std::ios_base::out | std::ios_base::app) {}
        std::string setPath(const std::string& filename);
        void LogInfo(const std::string& str)    { Log(str, level::LOG_INFO); }
        void LogError(const std::string& str)   { Log(str, level::LOG_ERROR); }
        void LogDebug(const std::string& str)   { Log(str, level::LOG_DEBUG); }
    private:
        enum level { LOG_INFO, LOG_ERROR, LOG_DEBUG };
        std::ofstream out_;
        std::mutex mutex_;
        void Log(const std::string& entry, int Level);
        std::string GetToday() const;
};

class Processor
{
    public:
        Processor();
        bool start();
        bool stop();
        void LogInfo(const std::string& str);
        void LogError(const std::string& str);
        void LogDebug(const std::string& str);

        static int UserInfoGet(const int login, MT::UserInfo *info);
        void AddWorkItem(WorkItem* item);
        void ProcessWorkItem(WorkItem* item);
        bool IsPendingTradePresent(int order) const;
        void AddPendingTrade(int order);
        void EraseGateEntry(int order);

        void TradesUpdate(MT::TradeRecord *trade, MT::UserInfo *user, const int mode);
        void TradesAdd(MT::TradeRecord *trade, const MT::UserInfo *user, 
                const MT::ConSymbol *symb, int mode);
    private:
        Logger log_;
        std::set<int> gate_;
        LockedQueue<WorkItem*> queue_;
        ConsumerThread consumer_;
        mutable std::mutex mutex_;

        void TriggerOrder(const MT::TradeRecord* trade);
        void ProcessStopLossOrder(const MT::TradeRecord* trade);
};

inline void Processor::LogInfo(const std::string& str) { return log_.LogInfo(str); }
inline void Processor::LogError(const std::string& str) { return log_.LogError(str); }
inline void Processor::LogDebug(const std::string& str) { return log_.LogDebug(str); }

#endif
