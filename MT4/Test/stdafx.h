#ifndef STDAFX_H_
#define STDAFX_H_

#pragma once;

#include <Windows.h>
#include <time.h>
#include <stdlib.h>

namespace MT {
        #include "MT4ServerAPI.h"
}

#include "Processor.h"

extern MT::CServerInterface *g_pServer;
extern Processor* ptrProcessor;

#endif