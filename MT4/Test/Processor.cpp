#include "stdafx.h"
#include <memory>
#include "Processor.h"

void Logger::Log(const std::string& entry, int level)
{
    time_t now;
    time(&now);
    _timeb nw;
    _ftime(&nw);
    tm* time = gmtime(&now);
    char text[128];

    std::lock_guard<std::mutex> lock(mutex_);
    sprintf (text, "%02d:%02d:%02d:%03hu%c", 
            time->tm_hour, time->tm_min, time->tm_sec, nw.millitm, ' ');
    out_ << text << "(@" << GetCurrentThreadId() << ") ";

    switch(level) {
        case level::LOG_INFO:
            {
                out_ << "INFO: ";
            }
            break;
        case level::LOG_ERROR:
            {
                out_ << "ERROR: ";
            }
            break;
        case level::LOG_DEBUG:
            {
                out_ << "DEBUG: ";
            }
            break;
    }

    out_ << entry << std::endl;
}

std::string Logger::GetToday() const
{
    time_t now;
    time(&now);
    tm* time = gmtime(&now);
    char text[128];
    strftime(text, 127, "%Y-%m-%d", time);
    return text;
}

std::string Logger::setPath(const std::string& filename)
{
    char path[MAX_PATH];
    GetModuleFileName(NULL, path, MAX_PATH);
    char *pos=strrchr(path, '\\');
    if (!pos) return std::string();

    sprintf(pos, "\\Logs\\Logs\\");
    std::string filename_;
    filename_.append(std::string(path));
    if (GetFileAttributes(filename_.c_str()) == INVALID_FILE_ATTRIBUTES) {
        CreateDirectory(filename_.c_str(), 0);
    }

    filename_.append(filename);
    std::stringstream ss;
    ss << "_" << GetToday() << ".log";
    filename_.append(ss.str());
    return filename_;
}

Processor::Processor()
    : consumer_(queue_)
{}

bool Processor::start()
{
    consumer_.start();
    return true;
}

bool Processor::stop()
{
    ptrProcessor->LogInfo("Processor::Stop");
    AddWorkItem(nullptr); // force a quit
    return true;
}

void Processor::AddWorkItem(WorkItem* item)
{
    queue_.AddItem(item);
}

void Processor::ProcessWorkItem(WorkItem* item)
{
    if (!item) return;

    switch(item->type) {
        case CALLBACK_TYPE::TYPE_PENDINGS:
            {
                TriggerOrder(item->trade);
                EraseGateEntry(item->trade->order);
            }
            break;
        case CALLBACK_TYPE::TYPE_STOP:
            {
                ProcessStopLossOrder(item->trade);
                EraseGateEntry(item->trade->order);
            }
            break;
    }
}
void Processor::TriggerOrder(const MT::TradeRecord* trade)
{
    ptrProcessor->LogInfo("TriggerOrder");

    //
    // populate our record
    //

    // first get our trade record
    char text[128] = {0};
    std::unique_ptr<MT::TradeRecord> ptrTrade (new MT::TradeRecord());
    if (!g_pServer->OrdersGet(trade->order, ptrTrade.get())) {
        sprintf(text, "TriggerOrder: OrdersGet failed for %d", trade->order);
        ptrProcessor->LogError(text);
        return;
    }

    // is it still a limit order?
    bool limit = (ptrTrade->cmd > MT::OP_SELL) && (ptrTrade->cmd < MT::OP_BALANCE);
    if (!limit) {
        sprintf(text, "TriggerOrder: Order %d is not a limit order", ptrTrade->order);
        ptrProcessor->LogError(text);
        return;
    }

    // was it cancelled for some reason?
    if (ptrTrade->close_time != 0) {
        sprintf(text, "TriggerOrder: Order %d is already cancelled", ptrTrade->order);
        ptrProcessor->LogError(text);
        return;
    }

    // populate the open_price => Use the fill price from the FIX Execution Report
    ptrTrade->open_price = trade->close_price;

    // change the command because now we are converting it into an open position
    if ((trade->cmd == MT::OP_BUY_LIMIT) || (trade->cmd == MT::OP_BUY_STOP)) {
        ptrTrade->cmd = MT::OP_BUY;
    } else if ((trade->cmd == MT::OP_SELL_LIMIT) || (trade->cmd == MT::OP_SELL_STOP)) {
        ptrTrade->cmd = MT::OP_SELL;
    }

    // populate volume => use the FILL volume from FIX Execution Report
    ptrTrade->volume = trade->volume;

    // get User information
    std::unique_ptr<MT::UserInfo> ptrUser(new MT::UserInfo());
    UserInfoGet(trade->login, ptrUser.get());
    if (ptrUser->login == 0) {
        sprintf(text, "TriggerOrder: UserInfoGet failed for %d while triggering order %d", 
                trade->login, ptrTrade->order);
        ptrProcessor->LogError(text);
        return;
    }

    // zero out these fields
    ptrTrade->profit		= 0;
    ptrTrade->open_time		= g_pServer->TradeTime();
    ptrTrade->close_time	= 0;
    ptrTrade->storage		= 0;
    ptrTrade->taxes			= 0;
    ptrTrade->expiration	= 0;

    // populate profit
    if (!g_pServer->TradesCalcProfit(ptrUser->group, ptrTrade.get())) {
        sprintf(text, "TriggerOrder: Unable to calculate profit for %d", 
                ptrTrade->order);
        ptrProcessor->LogError(text);
        ptrTrade->profit = 0.0;
    }

    // get the symbol info
    std::unique_ptr<MT::ConSymbol> symbol(new MT::ConSymbol);
    if (!g_pServer->SymbolsGet(trade->symbol, symbol.get())) {
        sprintf(text, "TriggerOrder: Unable to get symbol info for %s",
                trade->symbol);
        ptrProcessor->LogError(text);
        return;
    }

    // populate the rate fields
    ptrTrade->conv_rates[0] = g_pServer->TradesCalcConvertation(ptrUser->group, FALSE, 
            ptrTrade->open_price, symbol.get());
    ptrTrade->margin_rate = g_pServer->TradesCalcConvertation(ptrUser->group, TRUE, 
            ptrTrade->open_price, symbol.get());

    // client commission if any
    g_pServer->TradesCommission(ptrTrade.get(), ptrUser->group, symbol.get());

    sprintf(text, "TriggerOrder: Activating order %d", ptrTrade->order);
    ptrProcessor->LogInfo(text);

    if (!g_pServer->OrdersUpdate(ptrTrade.get(), ptrUser.get(), MT::UPDATE_ACTIVATE)) {
        sprintf(text, "TriggerOrder: Unable to activate order %d", ptrTrade->order);
        ptrProcessor->LogError(text);
        return;
    }
}
void Processor::ProcessStopLossOrder(const MT::TradeRecord* trade)
{
    //
    // We need to close the order in response to a SL/TP trigger
    //
    char text[128];
    sprintf(text, "ProcessStopLossOrder: Processing SL/TP close for order %d", trade->order);
    ptrProcessor->LogInfo(text);

    std::unique_ptr<MT::TradeTransInfo> ptrTrans(new MT::TradeTransInfo());
    ptrTrans->cmd           = static_cast<short>(trade->cmd);
    ptrTrans->order         = trade->order;

    // volume to be closed => This will be the filled volume from the Execution Report
    ptrTrans->volume        = trade->volume;

    //
    // price to close the trade => Executed price in the Execution report.
    // I'm using the close_price field of trade record for illustration only
    //
    ptrTrans->price         = trade->close_price;
    ptrTrans->sl            = trade->sl;
    ptrTrans->tp            = trade->tp;
    ptrTrans->type          = MT::TT_BR_ORDER_CLOSE;

    strncpy(ptrTrans->symbol, trade->symbol, sizeof(trade->symbol)/sizeof(trade->symbol[0]));

    //
    // the comment needs to accurately show whether its closed due to sl or tp
    // trade->close_price here is equivalent to the filled price
    // In the following cases, we are sure of the comment that needs to be put
    // For other extraneous cases, leave the comment field empty instead of
    // making wrong guesses
    // The right way to do this would be to remember this information when the
    // sl/tp was triggered (MtSrvTradeStopsApply) and use that information here
    //
    if (trade->cmd == MT::OP_BUY) {
        if (trade->close_price < trade->open_price) {
            if (trade->sl > 0 && (trade->close_price <= trade->sl))
                sprintf(ptrTrans->comment, "[sl]");
        } 
        else if (trade->close_price > trade->open_price) {
            if (trade->tp > 0 && (trade->close_price >= trade->tp))
                sprintf(ptrTrans->comment, "[tp]");
        }
    }

    if (trade->cmd == MT::OP_SELL) {
        if (trade->close_price > trade->open_price) {
            if (trade->sl > 0 && (trade->close_price >= trade->sl))
                sprintf(ptrTrans->comment, "[sl]");
        } 
        else if (trade->close_price < trade->open_price) {
            if (trade->tp > 0 && (trade->close_price <= trade->tp))
                sprintf(ptrTrans->comment, "[tp]");
        }
    }

    std::unique_ptr<MT::UserInfo> ptrUser(new MT::UserInfo());
    UserInfoGet(trade->login, ptrUser.get());

    // -------------close the trade-------------//
    if (!g_pServer->OrdersClose(ptrTrans.get(), ptrUser.get())) {
        sprintf(text, "ProcessStopLossOrder: Error closing order %d", ptrTrans->order);
        ptrProcessor->LogError(text);
        return;
    }

    sprintf(text, "ProcessStopLossOrder: order %d closed sucessfully", ptrTrans->order);
    ptrProcessor->LogInfo(text);
}
void Processor::TradesUpdate(MT::TradeRecord *trade, MT::UserInfo *user, const int mode)
{
    // get the symbol info
    char text[128];
    std::unique_ptr<MT::ConSymbol> symbol(new MT::ConSymbol);
    if (!g_pServer->SymbolsGet(trade->symbol, symbol.get())) {
        sprintf(text, "TradesUpdate: Unable to get symbol info for %s",
                trade->symbol);
        ptrProcessor->LogError(text);
        return;
    }

    switch(trade->cmd) {
        case MT::OP_BUY:
        case MT::OP_SELL:
            {
                switch(mode) {
                    case MT::UPDATE_ACTIVATE:
                        {
                            sprintf(text, "TradesUpdate: UPDATE_ACTIVATE for order %d", trade->order);
                            ptrProcessor->LogInfo(text);
                            TradesAdd(trade, user, symbol.get(), mode);
                        }
                        break;
                }
            }
            break;
    }
}
void Processor::TradesAdd(MT::TradeRecord *trade, const MT::UserInfo *, const MT::ConSymbol *, int )
{
    char text[128] = {0};
    sprintf(text, "TradesAdd: Trade %d opened", trade->order);
    ptrProcessor->LogInfo(text);
}

bool Processor::IsPendingTradePresent(int order) const
{
    std::lock_guard<std::mutex> lock(mutex_);
    return gate_.find(order) != gate_.end();
}

void Processor::AddPendingTrade(int order)
{
    std::lock_guard<std::mutex> lock(mutex_);
    gate_.insert(order);
}

void Processor::EraseGateEntry(int order)
{
    std::lock_guard<std::mutex> lock(mutex_);
    auto iter = gate_.find(order);
    if (iter != gate_.end())
        gate_.erase(iter);
}

int Processor::UserInfoGet(const int login, MT::UserInfo *info)
{
    MT::UserRecord user={0};
    //---- clear info
    std::memset(info,0,sizeof(MT::UserInfo));

    //---- get user record
    if (g_pServer->ClientsUserInfo(login,&user) == FALSE) return(FALSE);
    //---- fill login
    info->login=user.login;
    //---- fill permissions
    info->enable=user.enable;
    info->grp.enable=user.enable;
    info->enable_read_only=user.enable_read_only;
    info->enable_change_password=user.enable_change_password;
    //---- fill trade data
    info->leverage     =user.leverage;
    info->agent_account=user.agent_account;
    info->credit       =user.credit;
    info->balance      =user.balance;
    info->prevbalance  =user.prevbalance;
    //---- fill group
    strcpy(info->group,user.group);
    strcpy(info->grp.group,user.group);

    strcpy(info->password,user.password);
    strcpy(info->name,user.name);
    //---- ok
    return(TRUE);
}
