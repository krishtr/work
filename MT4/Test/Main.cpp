#include "stdafx.h"
#include <memory>

MT::PluginInfo g_PluginInfo = {		"Test",			//	char[128]
    2,
    "Test",
    NULL
};

HMODULE g_hModule = NULL;
DWORD GetModuleFileName_error_code;
char configuration_file_buffer[MAX_PATH];
const char default_configuration_file[] = "Test.ini";
extern const char* configuration_file = default_configuration_file;
MT::CServerInterface *g_pServer = NULL;

extern Processor* ptrProcessor = 0;

BOOL APIENTRY DllMain(HMODULE hModule, DWORD ul_reason_for_call, LPVOID)
{
    switch (ul_reason_for_call)
    {
        case DLL_PROCESS_ATTACH:
            if	(const DWORD len = GetModuleFileName(g_hModule = hModule, configuration_file_buffer, MAX_PATH)) 
            {
                if	(GetLastError() != ERROR_INSUFFICIENT_BUFFER) 
                {
                    DWORD dot_pos = len;
                    while (--dot_pos > 0 && configuration_file_buffer[dot_pos] != '.');
                    if	(dot_pos > 0 && dot_pos + 4 < MAX_PATH) 
                    {
                        configuration_file_buffer[dot_pos + 1] = 'i';
                        configuration_file_buffer[dot_pos + 2] = 'n';
                        configuration_file_buffer[dot_pos + 3] = 'i';
                        configuration_file_buffer[dot_pos + 4] = '\0';

                        configuration_file = configuration_file_buffer;

                        DWORD bs_pos = dot_pos;
                        while (--bs_pos > 0 && configuration_file_buffer[bs_pos] != '\\');
                        DWORD i = bs_pos;
                        if	(dot_pos - bs_pos - 1 < sizeof g_PluginInfo.name)
                        {
                            while (++i < dot_pos)
                            {
                                //	Don't add the spaces
                                if	(configuration_file_buffer[i] == ' ')
                                    g_PluginInfo.name[i - bs_pos - 1] = '_';
                                else
                                    g_PluginInfo.name[i - bs_pos - 1] = configuration_file_buffer[i];
                            }

                            g_PluginInfo.name[i - bs_pos] = '\0';
                        }
                    }
                }
            }
            else
                GetModuleFileName_error_code = len;
            break;
        case DLL_THREAD_ATTACH:
        case DLL_THREAD_DETACH:
        case DLL_PROCESS_DETACH:
            break;
    }
    return TRUE;
}

void APIENTRY MtSrvAbout(MT::PluginInfo *info) 
{
    if (info) {
        strcpy(g_PluginInfo.copyright, "CopyRight, Test");
        memcpy(info, &g_PluginInfo, sizeof(MT::PluginInfo));
    }
}
int APIENTRY MtSrvStartup(MT::CServerInterface *server) 
{
    if (!server) return FALSE;

    ptrProcessor = new Processor();

    // keep a copy
    g_pServer = server;

    ptrProcessor->LogInfo("***********************************************************************");
    ptrProcessor->LogInfo("MtSrvStartup");
    bool ret = ptrProcessor->start();
    return ret ? TRUE : FALSE;
}
void APIENTRY MtSrvCleanup(void) 
{
    ptrProcessor->LogInfo("MtSrvCleanup");
    ptrProcessor->LogInfo("***********************************************************************");
    ptrProcessor->stop();
    delete ptrProcessor;
    ptrProcessor = 0;
}
int APIENTRY MtSrvDealerConfirm(const int,const MT::UserInfo *,double *) 
{
    ptrProcessor->LogInfo("MtSrvDealerConfirm");
    return 1;
}
int APIENTRY MtSrvTradeTransaction(MT::TradeTransInfo*, const MT::UserInfo *, int *) 
{
    ptrProcessor->LogInfo("MtSrvTradeTransaction");
    return MT::RET_OK;

}
void APIENTRY MtSrvTradeRequestApply(MT::RequestInfo *request, const int) 
{
    ptrProcessor->LogInfo("MtSrvTradeRequestApply");
    const UCHAR l_type = request->trade.type;

    MT::UserInfo info = {0};
    Processor::UserInfoGet(request->login, &info);

    switch(l_type)
    {
        case MT::TT_ORDER_MK_OPEN:  
        case MT::TT_ORDER_MK_CLOSE:
            {
                if(l_type == MT::TT_ORDER_MK_CLOSE) {				
                    request->trade.cmd = static_cast<short>((request->trade.cmd == MT::OP_BUY) 
                            ? MT::OP_SELL 
                            : MT::OP_BUY);
                }

                g_pServer->RequestsConfirm(request->id, &info, request->prices);
            }
            break;

        case MT::TT_ORDER_PENDING_OPEN:
            {			
                double l_prices[2];	
                l_prices[0] = 0;
                l_prices[1] = 0;

                g_pServer->RequestsConfirm(request->id, &info, l_prices);
            }
            break;

        case MT::TT_ORDER_MODIFY:
            {						
                g_pServer->RequestsConfirm(request->id, &info, request->prices);
            }
            break;

        case MT::TT_ORDER_DELETE:
            {											
                g_pServer->RequestsConfirm(request->id, &info, request->prices);
            }
            break;

        default:
            char text[128];
            sprintf(text, "MtSrvTradeRequestApply: request->trade.type is %d", l_type);
            ptrProcessor->LogInfo(text);
            return;
    }
}
void APIENTRY MtSrvTradesAddExt(MT::TradeRecord *trade,const MT::UserInfo *user,const MT::ConSymbol *symb,const int mode) 
{
    ptrProcessor->TradesAdd(trade, user, symb, mode);
}
void APIENTRY MtSrvTradesUpdate(MT::TradeRecord *trade,MT::UserInfo *user,const int mode) 
{
    ptrProcessor->LogInfo("MtSrvTradesUpdate");
    ptrProcessor->TradesUpdate(trade, user, mode);
}
int  APIENTRY MtSrvTradePendingsFilter(const MT::ConGroup *group, const MT::ConSymbol *symbol, const MT::TradeRecord *trade)
{
    //	Is it ready to go?
    bool trigger = false;
    double prices[2] = {0.0};
    if (g_pServer->HistoryPricesGroup(symbol->symbol, group, prices) == MT::RET_OK)
    {
        switch (trade->cmd)
        {
            case MT::OP_BUY_LIMIT:
                if	(prices[1] <= trade->open_price)
                    trigger = true;
                break;
            case MT::OP_SELL_LIMIT:
                if	(prices[0] >= trade->open_price)
                    trigger = true;
                break;
            case MT::OP_BUY_STOP:
                if	(prices[1] >= trade->open_price)
                    trigger = true;
                break;
            case MT::OP_SELL_STOP:
                if	(prices[0] <= trade->open_price)
                    trigger = true;
                break;
        }
    }

    if (trigger) {
        if (!ptrProcessor->IsPendingTradePresent(trade->order)) {
            ptrProcessor->AddPendingTrade(trade->order);
            char text[128];
            sprintf(text, "MtSrvTradePendingsFilter: Trigger for order %d received", trade->order);
            ptrProcessor->LogInfo(text);
            WorkItem *item = new WorkItem;
            item->trade = trade;
            item->type = CALLBACK_TYPE::TYPE_PENDINGS;
            ptrProcessor->AddWorkItem(item);
        }
    }
    return MT::RET_OK_NONE;
}
int APIENTRY MtSrvTradeStopsFilter(const MT::ConGroup *,const MT::ConSymbol *,const MT::TradeRecord *trade)
{
    char text[128];
    sprintf(text, "MtSrvTradeStopsFilter: SL/TP for order %d received", trade->order);
    ptrProcessor->LogInfo(text);
    if (!ptrProcessor->IsPendingTradePresent(trade->order)) {
        WorkItem *item = new WorkItem;
        item->trade = trade;
        item->type = CALLBACK_TYPE::TYPE_STOP;
        ptrProcessor->AddWorkItem(item);
    }
    return MT::RET_OK_NONE;
}
