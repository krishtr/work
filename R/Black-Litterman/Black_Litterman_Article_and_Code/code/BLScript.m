% The script BLScript.m is to support the Black-Litterman implementation
% example described in the article "Building and Extending Portfolio
% Optimization Models with MATLAB" that appears in the Wilmott magazine May
% 2013 edition
%   --------------------------------------------------------------------------
%   Author: Sri Krishnamurthy,CFA
%   Contact: skrishna@mathworks.com
%   Copyright 1984-2013 The MathWorks, Inc.

clear all;close all;

%% Load Ticker Data for the Dow Jones index
load djia.mat ;

StartDate = '01-01-2003';
EndDate = '12-01-2012';

%% Get monthly return data for DOW Jones 30 tickers from Yahoo
load djiadata.mat;
tickData = fints(flipud(djiaData(:,1,1)),flipud(djiaData(:,:,7)),tickers,'m');
returnData = tick2ret(tickData);

%% Get monthly return data for DOW Jones 30 index
% Note: Yahoo doesn't provide direct download for the ^DJIA ticker.
% Manually downloaded data is in djiaData.xlsx
[nos, txt] = xlsread('djiaData.xlsx');
marketData = flipud(nos(:,end));
adjCloseMarketData = fints(flipud(txt),marketData,'DJIA','m');
returnMarketData = tick2ret(adjCloseMarketData);

%% Get 4-Week TBill data from the Federal reserve website
% We will use the 4-Week TBill rates as a proxy for Risk-free rate
% f = fred;
% tbillData = fetch(f,'tb4wk', StartDate, EndDate);
% returnTBillData = fints(tbillData.Data(2:end-1,1),tbillData.Data(2:end-1,2),'TBILL','m')/1200;
load fredData.mat

% Compute Variance-Covariance matrix
sigma = cov(returnData);

%% Extract market,tbill and portfolio data
portfolio = fts2mat(returnData);
market = fts2mat(returnMarketData);
rfAsset = fts2mat(returnTBillData);
meanRiskFreeRate = mean(rfAsset);

%% Instantiate a new PortfolioBL Object
obj = PortfolioBL('AssetList', tickers);
obj = PortfolioBL(obj,'AssetMean', mean(portfolio), 'AssetCovar', sigma,'RiskFreeRate',meanRiskFreeRate );
obj = obj.setDefaultConstraints;
obj = obj.setIndexData(market);
obj.plotFrontier;

%% Black-Litterman Approach
% Step 1 : Compute and review Excess Returns
% Approach 1: Compute PI : Historical Eqm Excess returns
obj = obj.computeExcessHistoricalReturns(portfolio,rfAsset);

% Approach 2: Compute PI : Implied Eqm Excess returns
mktCaps = zeros(length(tickers),1);
for i = 1:length(tickers)
    mktCaps(i) = tickersInfo(i).marketCap;
end
obj = obj.computeExcessImpliedReturns(portfolio,market,rfAsset,mktCaps );

% Compare excess returns and weights implied by historical, Implied and current market weights
compareWeights( obj.ExcessHistoricalReturns, obj.ExcessImpliedReturns, sigma,mktCaps )

% We will use Excess Implied Returns for our BlackLitterman calculation
obj =obj.setPI(obj.ExcessImpliedReturns);

% Step 2 : Input views and compute Confidence matrix
obj = obj.inputExpectedReturnViews;
obj.reviewViewsMatrix;

% We will use Tau to be 1
obj = obj.setTau(1);

% Omega
% You can input the Omega matrix or compute it.
obj = obj.computeOmega;

% Step 3 : Combine Implied Excess Returns with views to derive new Expected
% Return vectors
obj = obj.computeBlackLitterman;
figure;
obj.plotFrontier;


