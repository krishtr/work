function compareWeights( ExcessHistoricalReturns, ExcessImpliedReturns, sigma, mktCaps )
%compareWeights Helper function to compute and plot allocations

%   --------------------------------------------------------------------------
%   Author: Sri Krishnamurthy,CFA
%   Contact: skrishna@mathworks.com
%   Copyright 1984-2013 The MathWorks, Inc.

excessReturns = [ExcessHistoricalReturns ExcessImpliedReturns];
z = [sigma\ExcessHistoricalReturns sigma\ExcessImpliedReturns];
weights = [z./(ones(size(z,1))*z) mktCaps/(sum(mktCaps))];

% Note historical weights are volatile
figure;
subplot(2,1,1)
bar(excessReturns);
axis tight
% Add title and axis labels
title('Excess Returns by Asset');
xlabel('Asset');
ylabel('Excess Returns');

% Add a legend
legend('Historical', 'Implied');

subplot(2,1,2)
bar(1:length(mktCaps),weights, 1);

% Set the axis limits
axis([0 length(mktCaps)+.5 min(min(weights)) max(max(weights))+.0001]);

% Add title and axis labels
title('Weights by Asset');
xlabel('Asset');
ylabel('Weights');

% Add a legend
legend('Historical', 'Implied','Observed Market Wts');

end

