package Problems

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import Arithmatic._

/**
  * Created by krish on 28/12/16.
  */
@RunWith(classOf[JUnitRunner])
class ArithmaticSuite extends FunSuite {

  // 31- prime test
  test("isPrime: test 1") {
    intercept[IllegalArgumentException] {
      isPrime(1)
    }
  }
  test("isPrime: test -ve") {
    intercept[IllegalArgumentException] {
      isPrime(-2)
    }
  }
  test("isPrime: test 0") {
    intercept[IllegalArgumentException] {
      isPrime(0)
    }
  }
  test("isPrime: test non-prime") {
    assert(isPrime(8) === false)
  }
  test("isPrime: test prime") {
    assert(isPrime(53) === true)
  }

  // 32- gcd
  test("gcd: rhs is 0") {
    intercept[IllegalArgumentException] {
      gcd(5, 0)
    }
  }
  test("gcd: lhs is 0") {
    intercept[IllegalArgumentException] {
      gcd(0, 9)
    }
  }
  test("gcd: 2 +ve numbers") {
    assert(gcd(1071, 462) === 21)
  }

  // 33- coprimes
  test("isCoPrime: rhs is 0") {
    intercept[IllegalArgumentException] {
      isCoPrime(89, 0)
    }
  }
  test("isCoPrime: lhs is 0") {
    intercept[IllegalArgumentException] {
      isCoPrime(0, 89)
    }
  }
  test("isCoPrime: 2 +ve numbers") {
    assert(isCoPrime(1071, 462) === false)
  }
  test("isCoPrime: 2 primes") {
    assert(isCoPrime(29, 53) === true)
  }

  // 34- totient
  test("phi: test for -ve") {
    intercept[IllegalArgumentException] {
      phi(-2)
    }
  }
  test("phi: test for 0") {
    intercept[IllegalArgumentException] {
      phi(0)
    }
  }
  test("phi: test for +ve") {
      assert(phi(10) === 4)
  }
  test("phi: test another for +ve") {
    assert(phi(9) === 6)
  }
}
