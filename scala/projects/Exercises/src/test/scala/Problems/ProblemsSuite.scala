package Problems

import java.util.NoSuchElementException

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import Lists._

/**
  * Created by krish on 28/12/16.
  */
@RunWith(classOf[JUnitRunner])
class ProblemsSuite extends FunSuite {

  test("last: abcd") {
    assert(last("abcd".toList) === 'd')
  }
  test("last: 1234") {
    assert(last(List(1,2,3,4)) === 4)
  }
  test("last: list of strings") {
    assert(last(List("Hello","this","is","a","test")) === "test")
  }
  test("last: empty list") {
    intercept[NoSuchElementException] {
      last(List())
    }
  }
  // last with pattern matching
  test("lastPattern: abcd") {
    assert(lastUsingPatternMatching("abcd".toList) === 'd')
  }
  test("lastPattern: 1234") {
    assert(lastUsingPatternMatching(List(1,2,3,4)) === 4)
  }
  test("lastPattern: list of strings") {
    assert(lastUsingPatternMatching(List("Hello","this","is","a","test")) === "test")
  }
  test("lastPattern: empty list") {
    intercept[NoSuchElementException] {
      lastUsingPatternMatching(List())
    }
  }

  // last but one
  test("lastButOne: abcd") {
    assert(lastButOne("abcd".toList) === 'c')
  }
  test("lastButOne: 1234") {
    assert(lastButOne(List(1,2,3,4)) === 3)
  }
  test("lastButOne: list of strings") {
    assert(lastButOne(List("Hello","this","is","a","test")) === "a")
  }
  test("lastButOne: empty list") {
    intercept[NoSuchElementException] {
      lastButOne(List())
    }
  }
  test("lastButOne: 1 element list") {
    intercept[NoSuchElementException] {
      lastButOne(List(1))
    }
  }

  // penultimate
  test("penultimate: abcd") {
    assert(penultimate("abcd".toList) === 'c')
  }
  test("penultimate: 1234") {
    assert(penultimate(List(1,2,3,4)) === 3)
  }
  test("penultimate: list of strings") {
    assert(penultimate(List("Hello","this","is","a","test")) === "a")
  }
  test("penultimate: empty list") {
    intercept[NoSuchElementException] {
      penultimate(List())
    }
  }
  test("penultimate: 1 element list") {
    intercept[NoSuchElementException] {
      penultimate(List(1))
    }
  }

  // list Kth
  test("listKth: abcd") {
    assert(listKth(3,"abcd".toList) === 'd')
  }
  test("listKth: 1234") {
    assert(listKth(0,List(1,2,3,4)) === 1)
  }
  test("listKth: second element in 1234") {
    assert(listKth(1,List(1,2,3,4)) === 2)
  }
  test("listKth: list of strings") {
    assert(listKth(2,List("Hello","this","is","a","test")) === "is")
  }
  test("listKth: empty list") {
    intercept[NoSuchElementException] {
      listKth(0,List())
    }
  }
  test("listKth: -ve K") {
    intercept[NoSuchElementException] {
      listKth(-1,List(1))
    }
  }
  test("listKth: K more than length") {
    intercept[NoSuchElementException] {
      listKth(2, List(1))
    }
  }

  // findKth version
  test("findKth: abcd") {
    assert(findKth(3,"abcd".toList) === 'd')
  }
  test("findKth: 1234") {
    assert(findKth(0,List(1,2,3,4)) === 1)
  }
  test("findKth: second element in 1234") {
    assert(findKth(1,List(1,2,3,4)) === 2)
  }
  test("findKth: list of strings") {
    assert(findKth(2,List("Hello","this","is","a","test")) === "is")
  }
  test("findKth: empty list") {
    intercept[NoSuchElementException] {
      findKth(0,List())
    }
  }
  test("findKth: -ve K") {
    intercept[NoSuchElementException] {
      findKth(-1,List(1))
    }
  }
  test("findKth: K more than length") {
    intercept[NoSuchElementException] {
      findKth(2, List(1))
    }
  }

  // size
  test("size: empty list") {
    assert(size(List()) === 0)
  }
  test("size: 1 element list") {
    assert(size(List(1)) === 1)
  }
  test("size: N element list") {
    assert(size(List(1,2,3,4,5,6,7)) === 7)
  }

  // size using fold
  test("sizeFold: empty list") {
    assert(sizeFold(List()) === 0)
  }
  test("sizeFold: 1 element list") {
    assert(sizeFold(List(1)) === 1)
  }
  test("sizeFold: N element list") {
    assert(sizeFold(List(1,2,3,4,5,6,7)) === 7)
  }

  // reverse
  test("reverse: empty list") {
    assert(reverse(List()) === List())
  }
  test("reverse: 1 element list") {
    assert(reverse(List(1)) === List(1))
  }
  test("reverse: N element INT list") {
    assert(reverse(List(1, 1, 2, 3, 5, 8)) === List(8, 5, 3, 2, 1, 1))
  }
  test("reverse: N element string list") {
    assert(reverse(List("Hello","this","is","a","test")) === List("test","a","is","this","Hello"))
  }

  // reverse using fold
  test("reverseFold: empty list") {
    assert(reverseFold(List()) === List())
  }
  test("reverseFold: 1 element list") {
    assert(reverseFold(List(1)) === List(1))
  }
  test("reverseFold: N element INT list") {
    assert(reverseFold(List(1, 1, 2, 3, 5, 8)) === List(8, 5, 3, 2, 1, 1))
  }
  test("reverseFold: N element string list") {
    assert(reverseFold(List("Hello","this","is","a","test")) === List("test","a","is","this","Hello"))
  }

  // palindrome
  test("isPalindrome: empty list") {
    assert(isPalindrome(List()) === true)
  }
  test("isPalindrome: 1 element list") {
    assert(isPalindrome(List(1)) === true)
  }
  test("isPalindrome: N element INT list -ve case") {
    assert(isPalindrome(List(1, 1, 2, 3, 5, 8)) === false)
  }
  test("isPalindrome: N element INT list +ve case") {
    assert(isPalindrome(List(1, 2, 3, 2, 1)) === true)
  }
  test("isPalindrome: N element string list +ve case") {
    assert(isPalindrome("malayalam".toList) === true)
  }

  // isPalin (pattern matching solution)
  test("isPalin: empty list") {
    assert(isPalin(List()) === true)
  }
  test("isPalin: 1 element list") {
    assert(isPalin(List(1)) === true)
  }
  test("isPalin: N element INT list -ve case") {
    assert(isPalin(List(1, 1, 2, 3, 5, 8)) === false)
  }
  test("isPalin: N element INT list +ve case") {
    assert(isPalin(List(1, 2, 3, 2, 1)) === true)
  }
  test("isPalin: N element string list +ve case") {
    assert(isPalin("malayalam".toList) === true)
  }

  // flatten
  test("flatten: empty list") {
    assert(flatten(List()) === List())
  }
  test("flatten: non-empty list") {
    assert(flatten(List(List(1, 1), 2, List(3, List(5, 8)))) === List(1, 1, 2, 3, 5, 8))
  }
  // flatten using flatMap
  test("makeFlat: empty list") {
    assert(makeFlat(List()) === List())
  }
  test("makeFlat: non-empty list") {
    assert(makeFlat(List(List(1, 1), 2, List(3, List(5, 8)))) === List(1, 1, 2, 3, 5, 8))
  }

  // compress - remove consecutive dupes
  test("compress: empty list") {
    assert(compress(List()) === List())
  }
  test("compress - single element dupe list") {
    assert(compress(List(1,1)) === List(1))
  }
  test("compress - multiple element non-dupe list") {
    assert(compress(List(1,2,3)) === List(1,2,3))
  }
  test("compress - multiple element dupe list") {
    assert(compress(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)) === List('a, 'b, 'c, 'a, 'd, 'e))
  }
  // compress using Fold
  test("compressF: empty list") {
    assert(compressF(List()) === List())
  }
  test("compressF - single element dupe list") {
    assert(compressF(List(1,1)) === List(1))
  }
  test("compressF - multiple element non-dupe list") {
    assert(compressF(List(1,2,3)) === List(1,2,3))
  }
  test("compressF - multiple element dupe list") {
    assert(compressF(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)) === List('a, 'b, 'c, 'a, 'd, 'e))
  }

  // pack consecutive duplicates of list elements into sublists
  test("pack: empty list") {
    assert(pack(List()) === List())
  }
  test("pack - single element dupe list") {
    assert(pack(List(1,1)) === List(List(1,1)))
  }
  test("pack - multiple element non-dupe list") {
    assert(pack(List(1,2,3)) === List(List(1),List(2),List(3)))
  }
  test("pack - multiple element dupe list") {
    assert(pack(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)) ===
      List(List('a, 'a, 'a, 'a), List('b), List('c, 'c), List('a, 'a), List('d), List('e, 'e, 'e, 'e)))
  }

  // pack using fold
  test("packF: empty list") {
    assert(packF(List()) === List())
  }
  test("packF - single element dupe list") {
    assert(packF(List(1,1)) === List(List(1,1)))
  }
  test("packF - multiple element non-dupe list") {
    assert(packF(List(1,2,3)) === List(List(1),List(2),List(3)))
  }
  test("packF - multiple element dupe list") {
    assert(packF(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)) ===
      List(List('a, 'a, 'a, 'a), List('b), List('c, 'c), List('a, 'a), List('d), List('e, 'e, 'e, 'e)))
  }

  // run length encoding
  test("encode: empty list") {
    assert(encode(List()) === List())
  }
  test("encode - single element dupe list") {
    assert(encode(List(1,1)) === List((2,1)))
  }
  test("encode - multiple element non-dupe list") {
    assert(encode(List(1,2,3)) === List((1,1),(1,2),(1,3)))
  }
  test("encode - multiple element dupe list") {
    assert(encode(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)) ===
      List((4,'a), (1,'b), (2,'c), (2,'a), (1,'d), (4,'e)))
  }

  // modified run length encoding
  test("encodeModified: empty list") {
    assert(encodeModified(List()) === List())
  }
  test("encodeModified - single element dupe list") {
    assert(encodeModified(List(1,1)) === List((2,1)))
  }
  test("encodeModified - multiple element non-dupe list") {
    assert(encodeModified(List(1,2,3)) === List(1,2,3))
  }
  test("encodeModified - multiple element dupe list") {
    assert(encodeModified(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)) ===
      List((4,'a), 'b, (2,'c), (2,'a), 'd, (4,'e)))
  }

  // decode
  test("decode: Empty list") {
    assert(decode(List()) === List())
  }
  test("decode: 1 element list") {
    assert(decode(List((4,1))) === List(1,1,1,1))
  }
  test("decode: check 0 count decode") {
    assert(decode(List((0,'r))) === List())
  }
  test("decode: multi element decode") {
    assert(decode(List((4, 'a), (1, 'b), (2, 'c), (2, 'a), (1, 'd), (4, 'e))) ===
      List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))
  }

  // run length encoding direct method
  test("encodeDirect: empty list") {
    assert(encodeDirect(List()) === List())
  }
  test("encodeDirect - single element dupe list") {
    assert(encodeDirect(List(1,1)) === List((2,1)))
  }
  test("encodeDirect - multiple element non-dupe list") {
    assert(encodeDirect(List(1,2,3)) === List((1,1),(1,2),(1,3)))
  }
  test("encodeDirect - multiple element dupe list") {
    assert(encodeDirect(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)) ===
      List((4,'a), (1,'b), (2,'c), (2,'a), (1,'d), (4,'e)))
  }

  // duplicate the elements of a list
  test("duplicate: empty list") {
    assert(duplicate(List()) === List())
  }
  test("duplicate - single element dupe list") {
    assert(duplicate(List(1)) === List(1,1))
  }
  test("duplicate - multiple element non-dupe list") {
    assert(duplicate(List(1,2,3)) === List(1,1,2,2,3,3))
  }
  test("duplicate - multiple element dupe list") {
    assert(duplicate(List('a, 'b, 'c, 'c, 'd)) === List('a, 'a, 'b, 'b, 'c, 'c, 'c, 'c, 'd, 'd))
  }

  // duplicate N times
  test("duplicateN: empty list") {
    assert(duplicateN(1, List()) === List())
  }
  test("duplicateN: any list 0 times") {
    assert(duplicateN(0, List(1,2)) === List(1,2))
  }
  test("duplicateN: -ve N") {
    intercept[IllegalArgumentException] {
      duplicateN(-4, List(1,2))
    }
  }
  test("duplicateN: +ve N multiple elements") {
    assert(duplicateN(3, List('a, 'b, 'c, 'c, 'd)) ===
      List('a, 'a, 'a, 'b, 'b, 'b, 'c, 'c, 'c, 'c, 'c, 'c, 'd, 'd, 'd))
  }

  // drop
  test("drop: empty list") {
    intercept[IllegalArgumentException] {
      drop(1, List())
    }
  }
  test("drop: N <= 0 list") {
    intercept[IllegalArgumentException] {
      drop(0, List(1,2))
    }
  }
  test("drop: -ve N list") {
    intercept[IllegalArgumentException] {
      drop(-1, List(1,2))
    }
  }
  test("drop: N >= list size") {
    intercept[IllegalArgumentException] {
      drop(5, List(1,2))
    }
  }
  test("drop: normal op") {
    assert(drop(3, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)) ===
      List('a, 'b, 'd, 'e, 'g, 'h, 'j, 'k))
  }

  // drop Alternate approach
  test("dropAlt: empty list") {
    assert(dropAlt(1, List()) === List())
  }
  test("dropAlt: N <= 0 list") {
    intercept[ArithmeticException] {
      dropAlt(0, List(1,2))
    }
  }
  test("dropAlt: -ve N list") {
    assert(dropAlt(-1, List(1,2)) === List())
  }
  test("dropAlt: N >= list size") {
    assert(dropAlt(5, List(1,2)) === List(1,2))
  }
  test("dropAlt: normal op") {
    assert(dropAlt(3, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)) ===
      List('a, 'b, 'd, 'e, 'g, 'h, 'j, 'k))
  }

  // splitTRecursive
  test("splitTRecursive: empty list") {
    val l = List()
    assert(splitTRecursive(2, l) === l.splitAt(2))
  }
  test("splitTRecursive: empty -ve index list") {
    val l = List()
    assert(splitTRecursive(-2, l) === l.splitAt(-2))
  }
  test("splitTRecursive: 1 element list") {
    val l = List(1)
    assert(splitTRecursive(1, l) === l.splitAt(1))
  }
  test("splitTRecursive: 1 element -ve index list") {
    val l = List(1)
    assert(splitTRecursive(-1, l) === l.splitAt(-1))
  }
  test("splitTRecursive: -ve index") {
    val l = List(1,2,3,4,5)
    assert(splitTRecursive(-5, l) === l.splitAt(-5))
  }
  test("splitTRecursive: index greater than size") {
    val l = List(1,2)
    assert(splitTRecursive(5, l) === l.splitAt(5))
  }
  test("splitTRecursive: Normal list splitTRecursive") {
    val l = List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)
    assert(splitTRecursive(3, l) === l.splitAt(3))
  }

  // 18 - slice
  test("slice: Empty list") {
    assert(sliceTRecursive(1,2,List()) === List())
  }
  test("slice: start index greater than end index") {
    assert(sliceTRecursive(3,2,List(1,2,3,4,5)) === List())
  }
  test("slice: -ve start and end index") {
    assert(sliceTRecursive(-1,-2,List(1,2,3,4)) === List())
  }
  test("slice: -ve start index") {
    assert(sliceTRecursive(-1,2,List(1,2,3,4)) === List(1,2))
  }
  test("slice: bounded start and end index") {
    assert(sliceTRecursive(1,2,List(1,2,3,4)) === List(2))
  }
  test("slice: end index greater than size") {
    assert(sliceTRecursive(2,10,List(1,2,3,4)) === List(3,4))
  }
  test("slice: both indexes greater than size") {
    assert(sliceTRecursive(5,5,List(1,2,3,4)) === List())
  }
  test("slice: start index -ve and end index greater than size") {
    assert(sliceTRecursive(-1,12,List(1,2,3,4)) === List(1,2,3,4))
  }

  // 19 - rotate
  test("rotate: empty list") {
    assert(rotate(1, List()) === List())
  }
  test("rotate: -ve factor") {
    assert(rotate(-2, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)) ===
      List('j, 'k, 'a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i))
  }
  test("rotate: +ve bounded factor") {
    assert(rotate(3, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)) ===
      List('d, 'e, 'f, 'g, 'h, 'i, 'j, 'k, 'a, 'b, 'c))
  }
  test("rotate: +ve factor greater than size") {
    assert(rotate(14, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k)) ===
      List('d, 'e, 'f, 'g, 'h, 'i, 'j, 'k, 'a, 'b, 'c))
  }

  // 20 - removeAt
  test("removeAt: Empty list") {
    intercept[IllegalArgumentException] {
      removeAt(1, List())
    }
  }
  test("removeAt: -ve index") {
    intercept[NoSuchElementException] {
      removeAt(-1, List(1,2,3))
    }
  }
  test("removeAt: index greater than size") {
    intercept[NoSuchElementException] {
      removeAt(5, List(1,2,3))
    }
  }
  test("removeAt: bounded index") {
    assert(removeAt(1, List('a, 'b, 'c, 'd)) === (List('a, 'c, 'd),'b))
  }

  // 21 - InsertAt.
  // Assumption: -ve index - insert at the start, larger index - insert at the end
  test("insertAt: empty list") {
    assert(insertAt('new, 2, List()) === List('new))
  }
  test("insertAt: -ve index") {
    assert(insertAt('new, -2, List('a, 'b, 'c, 'd)) === List('new, 'a, 'b, 'c, 'd))
  }
  test("insertAt: index greater than size") {
    assert(insertAt('new, 6, List('a, 'b, 'c, 'd)) === List('a, 'b, 'c, 'd, 'new))
  }
  test("insertAt: index within bounds") {
    assert(insertAt('new, 2, List('a, 'b, 'c, 'd)) === List('a, 'b, 'new, 'c, 'd))
  }

  // 22 - range
  test("range: end index less than start index") {
    assert(rangeTRecursive(3, 1) === List())
  }
  test("range: -ve start index") {
    assert(rangeTRecursive(-1,1) === List(-1,0,1))
  }
  test("range: -ve indexes") {
    assert(rangeTRecursive(-3,-1) === List(-3,-2,-1))
  }
  test("range: +ve indexes") {
    assert(rangeTRecursive(1,3) === List(1,2,3))
  }
  test("range: same start and end indexes") {
    assert(rangeTRecursive(1,1) === List(1))
  }

  // 23 - randomSelect
  test("randomSelect: empty list") {
    assert(randomSelect(3, List()) === List())
  }
  test("randomSelect: -ve index") {
    assert(randomSelect(-2, List(1,2,3,4)) === List())
  }
  test("randomSelect: 0 index") {
    assert(randomSelect(0, List(1,2,3,4)) === List())
  }
  test("randomSelect: index larger than size") {
    val l = List(1,2,3,4)
    assert(randomSelect(10, l).size === l.size)
  }
  test("randomSelect: index within bounds") {
    val l = List(1,2,3,4)
    assert(randomSelect(2, l).size === 2)
    assert((randomSelect(2,l).toSet subsetOf l.toSet) === true)
  }

  // 24 - lotto
  test("lotto: 0 index") {
    assert(lotto(0,49) === List())
  }
  test("lotto: -ve index") {
    assert(lotto(-2,49) === List())
  }
  test("lotto: +ve index") {
    val l = lotto(5,49)
    assert(l.size === 5)
    assert(l.count(_ < 1) === 0)
    assert(l.count(_ > 49) === 0)
  }

  // 25 random permute
  test("randomPermute: empty list") {
    assert(randomPermute(List()) === List())
  }
  test("randomPermute: normal list") {
    val l = List('a, 'b, 'c, 'd, 'e, 'f)
    assert(randomPermute(l).size === l.size)
    assert(randomPermute(l).toSet === l.toSet)
  }

  // 26 combinations
  test("combinations: empty list") {
    val e = List()
    assert(combinations(3,e) === e.combinations(3).toList)
  }
  test("combinations: 6 choose 3") {
    val e = List(1,2,3,4,5,6)
    assert(combinations(3,e) === e.combinations(3).toList)
  }
  test("combinations: 6 choose 1") {
    val e = List(1,2,3,4,5,6)
    assert(combinations(1,e) === e.combinations(1).toList)
  }
  test("combinations: 6 choose 6") {
    val e = List(1,2,3,4,5,6)
    assert(combinations(6,e) === e.combinations(6).toList)
  }
}
