/**
  * Created by krish on 07/01/17.
  */

package Problems

object Arithmatic {

  // 31- Determine if the given number is prime
  def isPrime(start: Int): Boolean = {
    val sqrt = math.sqrt(start).toInt
    @scala.annotation.tailrec
    def _prime(list: List[Int]): Boolean = list match {
      case Nil => true
      case h :: _ if start % h == 0 => false
      case _ :: tail => _prime(tail)
    }
    if(start <= 1) throw new IllegalArgumentException("primes are +ve and greater than one")
    else _prime((2 to sqrt).toList)
    // one liner: (2 to sqrt).toList forall (start % _ != 0)
  }

  // 32- Determine the greatest common divisor of two positive integer numbers.
  // Use Euclid's algorithm
  def gcd(lhs: Int, rhs: Int): Int = {
    @scala.annotation.tailrec
    def _gcd(lhs: Int, rhs: Int): Int = {
      if (rhs == 0) lhs
      else _gcd(rhs, lhs % rhs)
    }
    if(lhs == 0 || rhs == 0) throw new IllegalArgumentException("Either of the inputs cannot be 0")
    else _gcd(lhs, rhs)
  }

  // 33- Determine whether two positive integer numbers are coprime.
  // Two numbers are coprime if their greatest common divisor equals 1.
  def isCoPrime(lhs: Int, rhs: Int): Boolean =
    gcd(lhs, rhs) == 1

  // 34- Calculate Euler's totient function phi(m).
  // Euler's so-called totient function phi(m) is defined as the number of
  // positive integers r (1 <= r <= m) that are coprime to m
  def phi(N: Int): Int =
    if(N <= 0) throw new IllegalArgumentException("only +ve numbers from 1")
    else ((1 to N) filter (isCoPrime(_, N))).size

  // 35-  Determine the prime factors of a given positive integer.
  // Construct a flat list containing the prime factors in ascending order
  def primeListHelper(N: Int): List[Int] = {
    @annotation.tailrec
    def _prime(N: Int, factor: Int, acc: List[Int]): List[Int] = {
      if(N == 1) acc
      else {
        if(N % 2 == 0) _prime(N / 2, factor, acc :+ 2)
        else if(N % factor == 0) _prime(N / factor, factor, acc :+ factor)
        else _prime(N, factor + 2, acc)
      }
    }
    _prime(N, 3, Nil)
  }

  def primeFactors(N: Int): List[Int] =
    primeListHelper(N)

  // 36- prime factors multiplicity (Should it be sorted?)
  def primeFactorMultiplicity(N: Int): Map[Int, Int] =
    (primeFactors(N) groupBy(x => x))
      .toStream
      .sortWith{ (x,y) => x._1 < y._1 }
      .toMap
      .mapValues(x => x.size)

  // 37-Calculate Euler's totient function phi(m) (improved).
  // See problem P34 for the definition of Euler's totient function.
  // If the list of the prime factors of a number m is known in the form of problem P36 then the function phi(m>)
  // can be efficiently calculated as follows: Let [[p1, m1], [p2, m2], [p3, m3], ...] be the list of prime factors
  // (and their multiplicities) of a given number m. Then phi(m) can be calculated with the following formula:

  // phi(m) = (p1-1)*p1^(m1-1) * (p2-1)*p2^(m2-1) * (p3-1)*p3^(m3-1) * ...

  // Note that a^b stands for the bth power of a.
  def phiImproved(N: Int): Int =
    primeFactorMultiplicity(N).foldLeft(1) { (r, c) => r * (c._1 - 1) * math.pow(c._1, c._2 - 1).toInt }

  // 38- phi(m) is super slow compared to phiImproved(m).
  // as an example try with N = 100000900

  // 39- A list of prime numbers.
  // Given a range of integers by its lower and upper limit, construct a list of all prime numbers in that range
  def listPrimesInRange(lo: Int, hi: Int): List[Int] = {
    @annotation.tailrec
    def _prime(lo: Int, hi: Int, acc: List[Int]): List[Int] = {
      if(lo > hi) throw new IllegalArgumentException("Invalid bounds")
      else
        if(lo == hi) if(isPrime(lo)) acc :+ lo else acc
        else if(isPrime(lo)) _prime(lo + 1, hi, acc :+ lo)
        else _prime(lo + 1, hi, acc)
    }
    _prime(lo, hi, Nil)
  }

  // simpler method
  def listPrimesInRange2(lo: Int, hi: Int): List[Int] =
    (lo to hi).toList filter isPrime

  // 40- Goldbach's conjecture.
  // Goldbach's conjecture says that every positive even number greater than 2 is the sum of two prime numbers.
  // E.g. 28 = 5 + 23. It is one of the most famous facts in number theory that has not been proved to be correct
  // in the general case. It has been numerically confirmed up to very large numbers (much larger than Scala's Int
  // can represent). Write a function to find the two prime numbers that sum up to a given even integer.
  // Krish: Is 4 a valid case? 4 = 3 + 1 or 2 + 2 but 1 is not a prime. We can either consider 1 as prime for this problem
  // or consider same prime numbers in the set.
  def sieve(list: Stream[Int]): Stream[Int] =
    list.head #:: sieve(list.tail filter(x => x % list.head != 0))

  def from(N: Int): Stream[Int] = N #:: from(N + 1)

  def goldbach(N: Int): (Int, Int) = {
    def _gold(i: Int, list: Stream[Int]): Stream[(Int, Int)] =
      for(j <- list if i + j == N) yield (i,j)
    @annotation.tailrec
    def _iter(list: Stream[Int], acc: List[(Int, Int)]): (Int, Int) = {
      if(list.isEmpty) acc.head
      else {
        val res = _gold(list.head, list)
        if (res.nonEmpty) res.head
        else _iter(list.tail, acc)
      }
    }
    if(N % 2 != 0) throw new IllegalArgumentException("Input is not an even number")
    else _iter(sieve(from(2)).take(N), Nil)
  }

  // 41- A list of Goldbach compositions.
  // Given a range of integers by its lower and upper limit, print a list of all
  // even numbers and their Goldbach composition.
  /**
    scala> printGoldbachList(9 to 20)
    10 = 3 + 7
    12 = 5 + 7
    14 = 3 + 11
    16 = 3 + 13
    18 = 5 + 13
    20 = 3 + 17
  */
  def printGoldbachList(lo: Int, hi: Int): List[(Int, Int)] = {
    (lo to hi).filter(_ % 2 == 0).toList map goldbach
  }

  /**
    * In most cases, if an even number is written as the sum of two prime numbers, one of them is very small.
    * Very rarely, the primes are both bigger than, say, 50. Try to find out how many such cases
    * there are in the range 2..3000.
    *
      Example (minimum value of 50 for the primes):

      scala> printGoldbachListLimited(1 to 2000, 50)
      992 = 73 + 919
      1382 = 61 + 1321
      1856 = 67 + 1789
      1928 = 61 + 1867
    */
  def printGoldbachListLimited(list: List[Int], N: Int): List[(Int, Int)] = {
    printGoldbachList(list.head, list.last).filter { case (x,y) => x > N && y > N}
  }
}