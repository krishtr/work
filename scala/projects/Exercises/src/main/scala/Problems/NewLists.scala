package Problems

/**
  * Created by krish on 20/05/18.
  */
object NewLists {

  // Find the last element of a list
  def last[T](list: List[T]) : T = {
    def iter(l: List[T], elem: T) : T = {
      if(l.isEmpty) elem
      else iter(l.tail, l.head)
    }
    iter(list, list.head)
  }

  // find penultimate element of the list
  def penultimate[T](list: List[T]) : T = {
    def iter(l: List[T], elem: T) : T = {
      if(l.tail.isEmpty) elem
      else iter(l.tail, l.head)
    }
    if(list.size < 2) throw new NoSuchElementException
    else iter(list, list.head)
  }

  // find nth element of the list
  def nth[T](N: Int, list: List[T]) : T = {
    def iter(n: Int, l: List[T]) : T = {
      if(n == 0) l.head
      else iter(n - 1, l.tail)
    }
    iter(N, list)
  }

  // find size of the list
  def length[T](list: List[T]) : Int = {
    def iter(l: List[T], acc: Int) : Int = {
      if(l.isEmpty) acc
      else iter(l.tail, acc + 1)
    }
    if(list.isEmpty) 0
    else iter(list.tail, 1)
  }
}
