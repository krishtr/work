def drop[T](N: Int, list: List[T]) : List[T] = {
  def iter(n: Int, l: List[T], acc: List[T]) : List[T] = {
    if(l.isEmpty) acc
    else {
      if(n == 1) iter(N, l.tail, acc)
      else iter(n - 1, l.tail, acc :+ l.head)
    }
  }
  iter(N, list, Nil)
}

drop(5, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k))
