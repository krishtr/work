package Problems
import scala.language.postfixOps
import scala.annotation.tailrec

object Lists {
  /**
    * 1. Find the last element of a list
    */
  def last[T](list: List[T]): T = {
    @tailrec
    def iter(l: List[T], elem: T): T =
      if (l.isEmpty) elem
      else iter(l.tail, l.head)

    if(list.isEmpty) throw new java.util.NoSuchElementException("Input list is empty")
    else iter(list, list.head)
  }

  @tailrec
  def lastUsingPatternMatching[T](list: List[T]): T = list match {
    case Nil => throw new NoSuchElementException("Input list is Empty")
    case h :: Nil => h
    case h :: t => lastUsingPatternMatching(t)
  }

  // Nth element from back, last element is element 0
  @tailrec
  def lastBackNth[T](N: Int, list: List[T]): T = {
    if(N < 0 || N > list.length) throw new NoSuchElementException
    else
    if(list.length == N+1) list.head
    else lastBackNth(N, list.tail)
  }
  /**
    * 2. Find the last but one element of a list
    * */
  def penultimate[T](list: List[T]): T = lastBackNth(1, list)

  @tailrec
  def lastButOne[T](list: List[T]): T = list match {
    case h :: List(t) => h // 2 element case
    case _ :: tail => lastButOne(tail) // having some tail case
    case _ => throw new NoSuchElementException // all other cases
  }

  /**
    * 3. Find the Kth element of a list, first element in the list is element 0
    * */
  @tailrec
  def listKth[T](K: Int, list: List[T]): T =
  if(K < 0 || K >= list.length) throw new NoSuchElementException
  else
  if(K == 0) list.head
  else listKth(K - 1, list.tail)

  @tailrec
  def findKth[T](K: Int, list: List[T]): T = K match {
    case 0 => list.head
    case K if K > 0 && K < list.length => findKth(K - 1, list.tail) // Note how if is part of case (like in for-comprehension)
    case _ => throw new NoSuchElementException
  }

  /**
    * 04 - Find the number of elements of a list
    * */
  def size[T](list: List[T]): Int = {
    @tailrec
    def iter(l: List[T], acc: Int): Int = l match {
      case Nil => acc
      case _ :: tail => iter(tail, acc + 1)
    }
    iter(list, 0)
  }

  // size using fold
  def sizeFold[T](list: List[T]): Int =
    list.foldLeft(0) { (i,_) => i + 1 }

  /**
    * 05 - Reverse a list
    * */
  def reverse[T](list: List[T]): List[T] = {
    @tailrec
    def iter[T](l: List[T], acc: List[T]): List[T] = l match {
      case Nil => acc
      case h :: tail => iter(tail, h :: acc)
    }

    iter(list, Nil)
  }

  // reverse using fold
  // r -> result
  // c -> current element
  // take the current element and "add" it to the result and
  // use the result as the current element
  //
  def reverseFold[T](list: List[T]): List[T] =


  list.foldLeft(List[T]()) { (r, c) => c :: r }

  /**
    * 06 - Find out whether a list is a palindrome
    * */
  def isPalindrome[T](list: List[T]): Boolean = reverse(list) == list

  // pattern matching solution
  def isPalin[T](list: List[T]): Boolean = {
    @tailrec
    def _isPalin(l: List[T], flag: Boolean): Boolean = l match {
      case Nil => flag
      case List(x) => true
      case h :: tail => _isPalin(tail.init, flag && (h == tail.last))
    }
    _isPalin(list, true)
  }

  /**
    * 07 - Flatten a nested list structure
    * Example:
    * scala> flatten(List(List(1, 1), 2, List(3, List(5, 8))))
    * res0: List[Any] = List(1, 1, 2, 3, 5, 8)
    */
  def flatten[T](list: List[T]): List[Any] = {
    def _flatten[T](l: List[T], acc: List[Any]): List[Any] = l match {
      case Nil => acc
      case head :: tail => head match {
        case h :: t => _flatten(tail, _flatten(t, acc :+ h))
        case x => _flatten(tail, acc :+ x)
      }
    }
    _flatten(list, List())
  }

  // flatten using flatMap
  // you usually have .flatMap { x => x match { case...} }
  // instead the syntax allows us to skip the match statement and
  // have it as follows: .flatMap { case ... }
  def makeFlat(list: List[Any]): List[Any] = list.flatMap {
    case ls: List[_] => makeFlat(ls)
    case a => List(a)
  }

  /**
    * 08 - Eliminate consecutive duplicates of list elements
    * */
  def compress[T](list: List[T]): List[T] = {
    @tailrec
    def iter[T](ll: List[T], acc: List[T]): List[T] =
      if(ll.isEmpty) acc
      else {
        if(acc.isEmpty || ll.head != acc.last) iter(ll.tail, acc :+ ll.head)
        else iter(ll.tail, acc)
      }
    iter(list,List[T]())
  }
  // compress using foldLeft
  def compressF[T](list: List[T]): List[T] =
    list.foldLeft(List[T]()) { (r,c) => if(r.isEmpty || r.last != c) r :+ c else r }
  // compress using foldRight
  def compressR[T](l: List[T]): List[T] =
    l.foldRight(List[T]()) { (c,r) => if(r.isEmpty || r.head != c) c :: r else r }

  /**
    * 09 - Pack consecutive duplicates of list elements into sublists
    * */
  def pack[T](list: List[T]): List[List[T]] = {
    @tailrec
    def iter(l: List[T], acc: List[List[T]]): List[List[T]] = {
      if(l.isEmpty) acc
      else {
        val (first, rest) = l.span(_ == l.head)
        iter(rest, acc :+ first)
      }
    }
    iter(list, List[List[T]]())
  }

  // pack using fold
  def packF[T](list: List[T]) =
    list.foldLeft(List[List[T]]()) { (r,c) =>
      if(r.isEmpty || r.last.head != c) r :+ List(c) // create a new list and append it
      else r.init :+ (r.last :+ c) // last the last list, append element and add it to the rest of the list
    }

  /**
    * 10 - Run-length encoding of a list.
    * scala> encode(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))
    * res0: List[(Int, Symbol)] = List((4,'a), (1,'b), (2,'c), (2,'a), (1,'d), (4,'e))
    * */
  def encode[T](list: List[T]): List[(Int, T)] =
    for(l <- pack(list)) yield (l.size, l.head)

  /**
    * 11 - Modified run-length encoding. Only elements with duplicates are transferred as (N, E) terms
    * scala> encodeModified(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))
    * res0: List[Any] = List((4,'a), 'b, (2,'c), (2,'a), 'd, (4,'e))
    * */
  def encodeModified[T](list: List[T]) =
    pack(list) map (l => if(l.size == 1) l.head else (l.size, l.head))

  /**
    * 12 - Decode a run-length encoded list
    * scala> decode(List((4, 'a), (1, 'b), (2, 'c), (2, 'a), (1, 'd), (4, 'e)))
    * res0: List[Symbol] = List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e)
    * */
  def decode[T](ll: List[(Int, T)]): List[T] =
    if(ll.isEmpty) Nil
    else for((f,s) <- ll; j <- 1 to f) yield s

  /**
    * 13 - Run-length encoding of a list (direct solution) i.e Same as (10) but Dont use pack
    * scala> encodeDirect(List('a, 'a, 'a, 'a, 'b, 'c, 'c, 'a, 'a, 'd, 'e, 'e, 'e, 'e))
    * res0: List[(Int, Symbol)] = List((4,'a), (1,'b), (2,'c), (2,'a), (1,'d), (4,'e))
    * */
  def encodeDirect[T](list: List[T]): List[(Int, T)] = {
    @tailrec
    def _encode(l: List[T], acc: List[(Int, T)]): List[(Int, T)] =
      if(l.isEmpty) acc
      else {
        val (first, second) = l.span(_ == l.head)
        _encode(second, acc :+ (first.size, first.head))
      }
    _encode(list, Nil)
  }

  /**
    * 14 - Duplicate the elements of a list
    * scala> duplicate(List('a, 'b, 'c, 'c, 'd))
    * res0: List[Symbol] = List('a, 'a, 'b, 'b, 'c, 'c, 'c, 'c, 'd, 'd)
    * */
  def duplicate[T](list: List[T]): List[T] =
    list flatMap (x => List(x,x))

  /**
    * 15 - Duplicate the elements of a list a given number of times
    * scala> duplicateN(3, List('a, 'b, 'c, 'c, 'd))
    * res0: List[Symbol] = List('a, 'a, 'a, 'b, 'b, 'b, 'c, 'c, 'c, 'c, 'c, 'c, 'd, 'd, 'd)
    * */
  def duplicateN[T](N: Int, list: List[T]): List[T] = N match {
    case x if x < 0 => throw new IllegalArgumentException("repeat Argument cannot be negative")
    case 0 => list
    case x => list flatMap (i => List.fill(x)(i))
  }

  /**
    * 16 - Drop every Nth element from a list.
    * scala> drop(3, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k))
    * res0: List[Symbol] = List('a, 'b, 'd, 'e, 'g, 'h, 'j, 'k)
    * */
  def drop[T](N: Int, list: List[T]): List[T] = {
    @tailrec
    def _drop(l: List[T], acc: List[T]): List[T] = {
      if (l.isEmpty) acc
      else _drop(l.drop(N), acc ::: l.take(N - 1))
    }
    if(N <= 0 || N > list.size || list.isEmpty)
      throw new IllegalArgumentException("invalid drop index or empty list. index cant be -ve/0/> list size")
    _drop(list, Nil)
  }

  // alternate approach using 'zipWithIndex' built-in method
  // this method produces a warning. hence the postfixOps import statement
  // at the start
  def dropAlt[T](N: Int, list: List[T]): List[T] =
  (list zipWithIndex).filter(x => (x._2 + 1) % N != 0) map(_._1)

  /**
    * 17 - Split a list into two parts.
    * scala> split(3, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k))
    * res0: (List[Symbol], List[Symbol]) = (List('a, 'b, 'c),List('d, 'e, 'f, 'g, 'h, 'i, 'j, 'k))
    * */
  def split[T](N: Int, list: List[T]): (List[T], List[T]) =
    (list.take(N), list.drop(N))

  def splitTRecursive[T](N: Int, list: List[T]): (List[T], List[T]) = {
    @tailrec
    def _split(c: Int, l: List[T], front: List[T]): (List[T], List[T]) = {
      if(c == 0) (front, l)
      else _split(c - 1, l.tail, front :+ l.head)
    }
    N match {
      case x if x < 0 => _split(0, list, List())
      case x if x > list.size => _split(list.size, list, List())
      case _ => _split(N, list, List())
    }
  }

  /**
    * 18 - Extract a slice from a list.
    * Given two indices, I and K, the slice is the list containing the elements from and including
    * the Ith element up to but not including the Kth element of the original list.
    * Start counting the elements with 0
    * scala> slice(3, 7, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k))
    * res0: List[Symbol] = List('d, 'e, 'f, 'g)
    * */
  def slice[T](i: Int, j: Int, list: List[T]): List[T] =
    list.drop(i).take(j - i)

  def sliceTRecursive[T](i: Int, j: Int, list: List[T]): List[T] = {
    @tailrec
    def _slice(f: Int, s: Int, l: List[T], acc: List[T]): List[T] = {
      if(f == 0 && s == 0) acc
      else {
        if(f != 0) _slice(f - 1, s, l.tail, acc)
        else _slice(f, s - 1, l.tail, acc :+ l.head)
      }
    }
    val lo = scala.math.max(i, 0)
    val hi = scala.math.min(j, list.size)
    if(list.isEmpty || hi <= lo) Nil
    else _slice(lo, hi - lo, list, Nil)
  }

  /**
    * 19- Rotate a list N places to the left
    * scala> rotate(3, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k))
    * res0: List[Symbol] = List('d, 'e, 'f, 'g, 'h, 'i, 'j, 'k, 'a, 'b, 'c)
    *
    * scala> rotate(-2, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i, 'j, 'k))
    * res1: List[Symbol] = List('j, 'k, 'a, 'b, 'c, 'd, 'e, 'f, 'g, 'h, 'i)
    * */
  def rotate[T](n: Int, list:List[T]): List[T] = {
    @tailrec
    def _rotate(c: Int, left: List[T], right: List[T]): List[T] = {
      if(c == 0) left ::: right
      else _rotate(c - 1, left.tail, right :+ left.head)
    }
    if(list.isEmpty) Nil
    else {
      val N = (list.size + n) % list.size
      _rotate(N, list, Nil)
    }
  }

  /**
    * 20- Remove the Kth element from a list. Elements start from 0
    * scala> removeAt(1, List('a, 'b, 'c, 'd))
    * res0: (List[Symbol], Symbol) = (List('a, 'c, 'd),'b)
    * */
  def removeAt[T](n: Int, list: List[T]): (List[T], T) = {
    @tailrec
    def _remove(c: Int, acc: List[T], l: List[T]): (List[T], T) = {
      if(c == 0) (acc ::: l.tail, l.head)
      else _remove(c - 1, acc :+ l.head, l.tail)
    }
    if(list.isEmpty) throw new IllegalArgumentException("Empty list")
    else if(n < 0 || n > list.size) throw new NoSuchElementException("illegal index")
    else _remove(n, Nil, list)
  }

  /**
    * 21- Insert an element at a given position into a list.
    * scala> insertAt('new, 1, List('a, 'b, 'c, 'd))
    * res0: List[Symbol] = List('a, 'new, 'b, 'c, 'd)
    * */
  def insertAt[T](elem: T, n: Int, list: List[T]): List[T] = {
    @tailrec
    def _insert(c: Int, acc: List[T], rest: List[T]): List[T] = {
      if(c == 0) (acc :+ elem) ::: rest
      else _insert(c - 1, acc :+ rest.head, rest.tail)
    }
    val N = scala.math.min(scala.math.max(n, 0), list.size)
    _insert(N, Nil, list)
  }

  /**
    * 22- Create a list containing all integers within a given range.
    * scala> range(4, 9)
    * res0: List[Int] = List(4, 5, 6, 7, 8, 9)
    */
  def range(i: Int, j: Int): List[Int] =
    (for(c <- i to j) yield c).toList

  def rangeTRecursive(i: Int, j: Int): List[Int] = {
    @tailrec
    def _range(c:Int, acc: List[Int]): List[Int] = {
      if(c == j) acc :+ j
      else _range(c + 1, acc :+ c)
    }
    if(j < i) Nil
    else _range(i, Nil)
  }

  /**
    * 23- Extract a given number of randomly selected elements from a list.
    * Use solution from (20)
    * scala> randomSelect(3, List('a, 'b, 'c, 'd, 'e, 'f, 'g, 'h))
    * res0: List[Symbol] = List('e, 'd, 'a)
    * */
  def randomSelect[T](n: Int, list: List[T]): List[T] = {
    val r = scala.util.Random
    @tailrec
    def _call(c: Int, ll: List[T], acc: List[T]): List[T] = {
      if(c == 0 || ll.isEmpty) acc
      else {
        val (l,v) = removeAt(r.nextInt(ll.size), ll)
        _call(c - 1, l, acc :+ v)
      }
    }
    val N = scala.math.min(scala.math.max(n, 0), list.size)
    _call(N, list, Nil)
  }

  /**
    * 24- Lotto: Draw N different random numbers from the set 1..M
    * scala> lotto(6, 49)
    * res0: List[Int] = List(23, 1, 17, 33, 21, 37)
    * */
  def lotto(n: Int, end: Int): List[Int] = {
    val r = scala.util.Random
    @tailrec
    def _lotto(c: Int, acc: List[Int]): List[Int] = {
      if(c == 0) acc
      else _lotto(c - 1, acc :+ (1 + r.nextInt(end)))
    }
    val N = scala.math.max(n, 0)
    _lotto(N, Nil)
  }

  /**
    * 25- Generate a random permutation of the elements of a list.
    * Use (23)
    * scala> randomPermute(List('a, 'b, 'c, 'd, 'e, 'f))
    * res0: List[Symbol] = List('b, 'a, 'd, 'c, 'e, 'f)
    * */
  def randomPermute[T](list: List[T]): List[T] =
    randomSelect(list.size, list)

  /**
    * 26- Generate the combinations of K distinct objects chosen from the N elements of a list.
    * In how many ways can a committee of 3 be chosen from a group of 12 people?
    * We all know that there are C(12,3) = 220 possibilities (C(N,K) denotes the well-known binomial coefficient).
    * For pure mathematicians, this result may be great. But we want to really generate all the possibilities.
    * scala> combinations(3, List('a, 'b, 'c, 'd, 'e, 'f))
    * res0: List[List[Symbol]] = List(List('a, 'b, 'c), List('a, 'b, 'd), List('a, 'b, 'e), ...
    * */

  /*
  * The approach to solve this problem is as follows
  * Lets say we have a 6 element list and we want to find all 3 element (n) combinations
  * so we have List(a,b,c,d,e,f) and we want List(List(a,b,c), List(a,b,d)...
  * we can do this by -
  * appending the first element to all (n-1) element combinations of the rest of the list
  * PLUS
  * all n element combinations of the rest of the list
  * So - a[(b,c), (b,d), (b,e)....] -> a + 2 elem combinations in (b,c,d,e,f)
  * UNION
  * repeat this for the rest of the list (ie 3 element combinations on (b,c,d,e,f))
  * */
  // 1 line solution
  def combo[T](n: Int, list: List[T]): List[List[T]] =
    list.combinations(n).toList

  def combinations[T](n: Int, list: List[T]): List[List[T]] = list match {
    case Nil => Nil
    case h :: tail if n == 1 => list.map(List(_)) // 1 element combos (n = 1). So just convert our list to list(list)
    case h :: tail => combinations(n - 1, tail).map(h :: _) ::: combinations(n, tail)
      // append h to each of the n-1 elem combos on tail UNION (:::) n elem combos on tail
  }

  /**
    * 27- Group the elements of a set into disjoint subsets.
    a) In how many ways can a group of 9 people work in 3 disjoint subgroups of 2, 3 and 4 persons? Write a function that generates all the possibilities.

    Example:

    scala> group3(List("Aldo", "Beat", "Carla", "David", "Evi", "Flip", "Gary", "Hugo", "Ida"))
    res0: List[List[List[String]]] = List(List(List(Aldo, Beat), List(Carla, David, Evi), List(Flip, Gary, Hugo, Ida)), ...

    b) Generalize the above predicate in a way that we can specify a list of group sizes and the predicate will return a list of groups.

    Example:

    scala> group(List(2, 2, 5), List("Aldo", "Beat", "Carla", "David", "Evi", "Flip", "Gary", "Hugo", "Ida"))
    res0: List[List[List[String]]] = List(List(List(Aldo, Beat), List(Carla, David), List(Evi, Flip, Gary, Hugo, Ida)), ...

    Note that we do not want permutations of the group members; i.e. ((Aldo, Beat), ...) is the same solution as ((Beat, Aldo), ...). However, we make a difference between ((Aldo, Beat), (Carla, David), ...) and ((Carla, David), (Aldo, Beat), ...).

    You may find more about this combinatorial problem in a good book on discrete mathematics under the term "multinomial coefficients".
    */

  def group[T](occ: List[Int], list: List[T]): List[List[List[T]]] = occ match {
    case Nil => List(Nil)
    case h :: tail => list.combinations(h).toList flatMap {
      c => group(tail, list.filterNot(c.toSet)) map (c :: _)
    }
  }
  def group3[T](list: List[T]): List[List[List[T]]] = group(List(2,3,4), list)

  def group3_alt[T](list: List[T]): List[(List[T], List[T], List[T])] = {
    if(list.isEmpty) Nil
    else {
      for {
        fourelem <- combo(4, list) // List[T] <- List[List[T]]
        threeelem <- combo(3, list.filterNot(fourelem.toSet)) // List[T] <- List[List[T]]
      } yield(list.filterNot((threeelem ::: fourelem).toSet), threeelem, fourelem)
    }
  }

  /**
    * 28- Sorting a list of lists according to length of sublists.
    a) We suppose that a list contains elements that are lists themselves. The objective is to sort the elements of the list according to their length.
    E.g. short lists first, longer lists later, or vice versa.

    Example:

    scala> lsort(List(List('a, 'b, 'c), List('d, 'e), List('f, 'g, 'h), List('d, 'e), List('i, 'j, 'k, 'l), List('m, 'n), List('o)))
    res0: List[List[Symbol]] = List(List('o), List('d, 'e), List('d, 'e), List('m, 'n), List('a, 'b, 'c), List('f, 'g, 'h), List('i, 'j, 'k, 'l))

    b) Again, we suppose that a list contains elements that are lists themselves.
    But this time the objective is to sort the elements according to their length frequency; i.e. in the default,
    sorting is done ascendingly, lists with rare lengths are placed, others with a more frequent length come later.

    Example:

    scala> lsortFreq(List(List('a, 'b, 'c), List('d, 'e), List('f, 'g, 'h), List('d, 'e), List('i, 'j, 'k, 'l), List('m, 'n), List('o)))
    res1: List[List[Symbol]] = List(List('i, 'j, 'k, 'l), List('o), List('a, 'b, 'c), List('f, 'g, 'h), List('d, 'e), List('d, 'e), List('m, 'n))

    Note that in the above example, the first two lists in the result have length 4 and 1 and both lengths appear
    just once. The third and fourth lists have length 3 and there are two list of this length.
    Finally, the last three lists have length 2. This is the most frequent length.
    */
  def lsort[T](list: List[List[T]]): List[List[T]] = list.sortWith(_.size < _.size)

  def lsortFreq[T](list: List[List[T]]): List[List[T]] =
    list.groupBy(_.size)
      .values.toList
      .sortWith(_.size < _.size)
      .flatten
}