package recfun

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
  }

  /**
   * Exercise 1
   */
    def pascal(c: Int, r: Int): Int =
      if(c == 0 || c == r) 1
      else pascal(c, r - 1) + pascal(c - 1, r - 1)
  
  /**
   * Exercise 2
   */
    def balance(chars: List[Char]): Boolean = {
      def process(chars: List[Char], acc: Int) : Boolean =
        if(chars.isEmpty) acc == 0
        else {
          if(chars.head == '(') process(chars.tail, acc + 1)
          else {
            if(chars.head == ')') {
              if(acc == 0) false
              else process(chars.tail, acc - 1)
            } else process(chars.tail, acc)
          }
        }
      if(chars.isEmpty) false
      else process(chars, 0)
    }
  
  /**
   * Exercise 3
   */
    def countChange(money: Int, coins: List[Int]): Int =
      if(money == 0) 1
      else if(money > 0 && coins.nonEmpty)
        countChange(money - coins.head, coins) + countChange(money, coins.tail)
      else 0
  }
