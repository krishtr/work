package patmat

import org.scalatest.FunSuite

import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner

import patmat.Huffman._

@RunWith(classOf[JUnitRunner])
class HuffmanSuite extends FunSuite {
	trait TestTrees {
		val t1 = Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5)
		val t2 = Fork(Fork(Leaf('a',2), Leaf('b',3), List('a','b'), 5), Leaf('d',4), List('a','b','d'), 9)
    val tree = Fork(Leaf('A',8), Fork(Fork(Leaf('B',3), Fork(Leaf('C',1),Leaf('D',1),"CD".toList,2),"BCD".toList,5),Fork(Fork(Leaf('E',1),Leaf('F',1),"EF".toList,2),Fork(Leaf('G',1),Leaf('H',1),"GH".toList,2),"EFGH".toList,4),"BCDEFGH".toList,9), "ABCDEFGH".toList, 17)
    //val codeTree = List()
	}


  test("weight of a larger tree") {
    new TestTrees {
      assert(weight(t1) === 5)
    }
  }


  test("chars of a larger tree") {
    new TestTrees {
      assert(chars(t2) === List('a','b','d'))
    }
  }

  test("testing the times") {
    new TestTrees {
      def list = List('a', 'b', 'd', 'a', 'b', 'a')
      assert(times(list) === List(('d',1),('b',2),('a',3)))
    }
  }

  test("test singleton") {
    new TestTrees {
      assert(singleton(List()) === false)
      assert(singleton(List(Leaf('a', 1))) === true)
      assert(singleton(List(Leaf('a', 1), Leaf('b', 2))) === false)
    }
  }


  test("string2chars(\"hello, world\")") {
    assert(string2Chars("hello, world") === List('h', 'e', 'l', 'l', 'o', ',', ' ', 'w', 'o', 'r', 'l', 'd'))
  }


  test("makeOrderedLeafList for some frequency table") {
    assert(makeOrderedLeafList(List(('t', 2), ('e', 1), ('x', 3))) === List(Leaf('e',1), Leaf('t',2), Leaf('x',3)))
  }


  test("combine of some leaf list") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 4))
    assert(combine(leaflist) === List(Fork(Leaf('e',1),Leaf('t',2),List('e', 't'),3), Leaf('x',4)))
  }

  test("test until") {
    val leaflist = List(Leaf('e', 1), Leaf('t', 2), Leaf('x', 4))
    assert(chars(until(singleton, combine)(leaflist).head) === List('e','t','x'))
    assert(weight(until(singleton, combine)(leaflist).head) === 7)
  }

  test("test decode1") {
    new TestTrees {
      val bits1 = List(1,0,0,0,1,0,1,0)
      assert(decode(tree, bits1) === "BAC".toList)
    }
  }

  test("test decode2") {
    new TestTrees {
      val bits1 = List(1,0,1,1)
      assert(decode(tree,bits1) === "D".toList)
    }
  }

  test("decodeSecret") {
    assert(decodedSecret === "huffmanestcool".toList)
  }

  test("encode test") {
    new TestTrees {
      assert(encode(tree)("D".toList) === List(1,0,1,1))
    }
  }

  test("encode") {
    encode(frenchCode)(List('h', 'u', 'f', 'f', 'm', 'a', 'n', 'e', 's', 't', 'c', 'o', 'o', 'l')) === List(0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1)
  }

  test("convert") {
    val list = List('h', 'u', 'f', 'f', 'm', 'a', 'n', 'e', 's', 't', 'c', 'o', 'o', 'l')
    val codeTable = convert(frenchCode)
    assert(secret === (list flatMap (x => codeBits(codeTable)(x))))
  }

  test("simple encode") {
    new TestTrees {
      assert(encode(t1)("ab".toList) === List(0,1))
    }
  }

  test("decode and encode a very short text should be identity") {
    new TestTrees {
      assert(decode(t1, encode(t1)("ab".toList)) === "ab".toList)
    }
  }

  test("quickencode") {
    val text = List('h', 'u', 'f', 'f', 'm', 'a', 'n', 'e', 's', 't', 'c', 'o', 'o', 'l')
    assert(secret === quickEncode(frenchCode)(text))
  }
}
