package forcomp


object Anagrams {

  /** A word is simply a `String`. */
  type Word = String

  /** A sentence is a `List` of words. */
  type Sentence = List[Word]

  /** `Occurrences` is a `List` of pairs of characters and positive integers saying
   *  how often the character appears.
   *  This list is sorted alphabetically w.r.t. to the character in each pair.
   *  All characters in the occurrence list are lowercase.
   *
   *  Any list of pairs of lowercase characters and their frequency which is not sorted
   *  is **not** an occurrence list.
   *
   *  Note: If the frequency of some character is zero, then that character should not be
   *  in the list.
   */
  type Occurrences = List[(Char, Int)]

  /** The dictionary is simply a sequence of words.
   *  It is predefined and obtained as a sequence using the utility method `loadDictionary`.
   */
  val dictionary: List[Word] = loadDictionary

  /** Converts the word into its character occurrence list.
   *
   *  Note: the uppercase and lowercase version of the character are treated as the
   *  same character, and are represented as a lowercase character in the occurrence list.
   *
   *  Note: you must use `groupBy` to implement this method!
   */
  def wordOccurrences(w: Word): Occurrences = w.toLowerCase.groupBy(c => c).mapValues(v => v.length).toList.sorted

  /** Converts a sentence into its character occurrence list. */
  def sentenceOccurrences(s: Sentence): Occurrences = wordOccurrences(s.flatten.mkString)

  /** The `dictionaryByOccurrences` is a `Map` from different occurrences to a sequence of all
   *  the words that have that occurrence count.
   *  This map serves as an easy way to obtain all the anagrams of a word given its occurrence list.
   *
   *  For example, the word "eat" has the following character occurrence list:
   *
   *     `List(('a', 1), ('e', 1), ('t', 1))`
   *
   *  Incidentally, so do the words "ate" and "tea".
   *
   *  This means that the `dictionaryByOccurrences` map will contain an entry:
   *
   *    List(('a', 1), ('e', 1), ('t', 1)) -> Seq("ate", "eat", "tea")
   *
   */
  lazy val dictionaryByOccurrences: Map[Occurrences, List[Word]] =
    dictionary groupBy wordOccurrences withDefaultValue List()

  /** Returns all the anagrams of a given word. */
  def wordAnagrams(word: Word): List[Word] = dictionaryByOccurrences(wordOccurrences(word))

  /** Returns the list of all subsets of the occurrence list.
   *  This includes the occurrence itself, i.e. `List(('k', 1), ('o', 1))`
   *  is a subset of `List(('k', 1), ('o', 1))`.
   *  It also include the empty subset `List()`.
   *
   *  Example: the subsets of the occurrence list `List(('a', 2), ('b', 2))` are:
   *
   *    List(
   *      List(),
   *      List(('a', 1)),
   *      List(('a', 2)),
   *      List(('b', 1)),
   *      List(('a', 1), ('b', 1)),
   *      List(('a', 2), ('b', 1)),
   *      List(('b', 2)),
   *      List(('a', 1), ('b', 2)),
   *      List(('a', 2), ('b', 2))
   *    )
   *
   *  Note that the order of the occurrence list subsets does not matter -- the subsets
   *  in the example above could have been displayed in some other order.
   */
  def combinations(occurrences: Occurrences): List[Occurrences] = {
    def genPairs(p: (Char,Int)): List[(Char,Int)] = (for(i <- 1 to p._2) yield p._1 -> i).toList
    def generate(org: List[(Char,Int)], acc: List[(Char,Int)]): List[(Char,Int)] = {
      if(org.isEmpty) acc
      else {
        val l = genPairs(org.head)
        generate(org.tail, l ::: acc)
      }
    }
    // generate a whole list first
    val l = generate(occurrences, List())

    // generate the various combinations
    val x = l.toSet.subsets.toList filterNot (s => s.groupBy(p => p._1).exists(p => p._2.size > 1))

    // convert to list
    for(element <- x) yield element.toList.sorted
  }

  /** Subtracts occurrence list `y` from occurrence list `x`.
   *
   *  The precondition is that the occurrence list `y` is a subset of
   *  the occurrence list `x` -- any character appearing in `y` must
   *  appear in `x`, and its frequency in `y` must be smaller or equal
   *  than its frequency in `x`.
   *
   *  Note: the resulting value is an occurrence - meaning it is sorted
   *  and has no zero-entries.
   */
  def subtract(x: Occurrences, y: Occurrences): Occurrences = {

    // change the sign of y
    val minusY = y map { case (k,v) => (k, -v) }

    // group common occurances
    val repeatPairsMap = (x ++ minusY) groupBy (p => p._1)

    // (1) reduce the elements in the repeated list by adding their frequencies.
    // key will be the same and so doesn't matter is x._1 is used or y._1
    // (2) Convert the result set to a list
    // (3) filter out any pairs with 0 frequencies
    // (4) sort it to conform it to occurance definition
    //
    repeatPairsMap.mapValues(v => v.reduce((x,y) => (x._1, x._2 + y._2)))
      .values
      .toList
      .filter(p => p._2 != 0)
      .sorted
  }

  /** Returns a list of all anagram sentences of the given sentence.
   *
   *  An anagram of a sentence is formed by taking the occurrences of all the characters of
   *  all the words in the sentence, and producing all possible combinations of words with those characters,
   *  such that the words have to be from the dictionary.
   *
   *  The number of words in the sentence and its anagrams does not have to correspond.
   *  For example, the sentence `List("I", "love", "you")` is an anagram of the sentence `List("You", "olive")`.
   *
   *  Also, two sentences with the same words but in a different order are considered two different anagrams.
   *  For example, sentences `List("You", "olive")` and `List("olive", "you")` are different anagrams of
   *  `List("I", "love", "you")`.
   *
   *  Here is a full example of a sentence `List("Yes", "man")` and its anagrams for our dictionary:
   *
   *    List(
   *      List(en, as, my),
   *      List(en, my, as),
   *      List(man, yes),
   *      List(men, say),
   *      List(as, en, my),
   *      List(as, my, en),
   *      List(sane, my),
   *      List(Sean, my),
   *      List(my, en, as),
   *      List(my, as, en),
   *      List(my, sane),
   *      List(my, Sean),
   *      List(say, men),
   *      List(yes, man)
   *    )
   *
   *  The different sentences do not have to be output in the order shown above - any order is fine as long as
   *  all the anagrams are there. Every returned word has to exist in the dictionary.
   *
   *  Note: in case that the words of the sentence are in the dictionary, then the sentence is the anagram of itself,
   *  so it has to be returned in this list.
   *
   *  Note: There is only one anagram of an empty sentence.
   */
  def sentenceAnagrams(sentence: Sentence): List[Sentence] =
    if (sentence.isEmpty) List(List())
    else {
      // find the unique code of our input sentence
      val uniqCode = sentenceOccurrences(sentence)
      // find all combinations of occ list of our unique code
      // find their corresponding words from dictionary
      // remove empty lists (which are produced for occ code for words not in our dict)
      // get an array so that we can process the collection (element fetch esp) faster
      //
      val allwords = (combinations(uniqCode) map dictionaryByOccurrences filterNot (l => l.isEmpty)).flatten.sorted.toArray
      // store the length
      val len = sentence.flatten.mkString.length

      // not the most elegant/efficient method, but I think it does the job
      // given a big list of all the words after our dict lookup (so we know this list has words that are valid)
      // we now need to find combinations of these words that will be of the same length (else its not a anagram)
      // if length check is not given we will 2^N where N is the list array length combinations possible which
      // will not terminate
      // Complexity => N^3
      //
      def collectResults: List[Sentence] = {
        var acc = List[Sentence]()
        for(i <- 0 until allwords.length - 2) {
          for(j <- i until allwords.length - 1) {
            for(k <- j until allwords.length) {
              if((allwords(i).size + allwords(j).size + allwords(k).size) == len) {
                acc = List(allwords(i), allwords(j), allwords(k)) :: acc
              }
              if(allwords(i).size == len) {
                acc = List(allwords(i)) :: acc
              }
              if(allwords(j).size == len) {
                acc = List(allwords(j)) :: acc
              }
              if(allwords(k).size == len) {
                acc = List(allwords(k)) :: acc
              }
              if((allwords(i).size + allwords(j).size) == len) {
                acc = List(allwords(i), allwords(j)) :: acc
              }
              if((allwords(i).size + allwords(k).size) == len) {
                acc = List(allwords(i), allwords(k)) :: acc
              }
              if((allwords(k).size + allwords(j).size) == len) {
                acc = List(allwords(k), allwords(j)) :: acc
              }
            }
          }
        }
        acc
      }

      // given the list of sentences that pass the length check
      // get those sentences that match our unique code. Only words that
      // have the same unique code are anagrams
      // make it free of duplicates
      // given a list of words, we can have those words appearing in different order
      // which would also make it an anagram
      // clean it up
      //
      collectResults
        .filter(s => sentenceOccurrences(s) == uniqCode)
        .distinct
        .map(l => l.permutations.toList)
        .flatten
        .distinct
    }

  /**
    * Recursive approach to do this
    * */
  def sentenceAnagramRecursive(sentence: Sentence): List[Sentence] = {
    def sentenceAnagramHelper(occurrence: Occurrences): List[Sentence] = occurrence match {
      case Nil => List(Nil)
      case _ => {
        val perms = combinations(occurrence)
        for {
          occ <- perms if dictionaryByOccurrences.keySet(occ)
          word <- dictionaryByOccurrences(occ)
          rest <- sentenceAnagramHelper(subtract(occurrence, occ))
        } yield word :: rest
      }
    }
    sentenceAnagramHelper(sentenceOccurrences(sentence))
  }
}
