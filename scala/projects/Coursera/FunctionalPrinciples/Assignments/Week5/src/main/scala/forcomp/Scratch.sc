// flatten(List(List(1, 1), 2, List(3, List(5, 8)))) === List(1,1,2,3,5,8)
// flatten(List(List(1,1,2,3))) === List(1,1,2,3)
def flatten(list: List[Any]) : List[Any] = {
    def iter(workList: List[Any], acc: List[Any]) : List[Any] = workList match {
        case Nil => acc
        case head :: tail => head match {
            case h :: t => iter(tail, iter(t, acc :+ h))
            case x => iter(tail, acc :+ x)
        }
    }
    iter(list, Nil)
}

import math.Ordering

def msort[T](list: List[T]) (implicit ord : Ordering[T]) : List[T] = {
    val n = list.size / 2
    if(n == 0) list
    else {
        def merge(lhs: List[T], rhs: List[T]) : List[T] = {
            (lhs, rhs) match {
                case (l, Nil) => l
                case (Nil, r) => r
                case (l :: ls, r :: rs) =>
                    if(ord.lt(l, r)) l +: merge(ls, r :: rs)
                    else r +: merge(l::ls, rs)
            }
        }
        val (lhs, rhs) = list splitAt n
        merge(msort(lhs), msort(rhs))
    }
}
msort(List(4,5,2,7,1,8,3,9))