import math.Ordering
import scala.io.Source

object week5 {

  def removeAt[T](n: Int, xs: List[T]): List[T] = {
    def iter(i: Int, ys: List[T], acc: List[T]): List[T] = {
      if(i > ys.size || i < 0) ys
      else if(i == 0) {if(ys.isEmpty) acc else acc ::: ys.tail}
      else iter(i - 1, ys.tail, acc :+ ys.head)
    }
    iter(n, xs, List[T]())
  }
  removeAt(4,"abcdef".toList)

  def remove[T](n: Int, xs: List[T]): List[T] = (xs take n) ::: (xs drop n+1)
  remove(4, "abcedef".toList)

  def flatten(xs: List[Any]): List[Any] = {
    def iter(ys: List[Any], acc: List[Any]): List[Any] =
      if(ys.isEmpty) acc
      else {
        ys.head match {
          case z :: zs => iter(ys.tail, iter(zs, acc :+ z))
          case x => iter(ys.tail, acc :+ x)
        }
      }
    iter(xs, List[Any]())
  }

  // flatten(List(List(1, 1), 2, List(3, List(5, 8)))) === List(1,1,2,3,5,8)
  flatten(List(List(1, 1), 2, List(3, List(5, 8))))
  flatten(List(List(1, 1), 2, List(3, List(5, List(8,9, List(1, List(10,11,List(12,13)))))), 7,8))

  def msort[T](list: List[T])(implicit ord: Ordering[T]): List[T] = {
    val n = list.length / 2
    if(n == 0) list
    else {
      def merge(xs: List[T], ys: List[T]): List[T] =
        (xs, ys) match {
          case (Nil, ys) => ys
          case (xs, Nil) => xs
          case (a::as, b::bs) =>
            if(ord.lt(a,b)) a :: merge(as, ys)
            else b :: merge(xs, bs)
        }
      val (first, second) = list splitAt n
      merge(msort(first), msort(second))
    }
  }
  val nums = List(-100,10,0,-1,-2,-99)
  val fruits = List("kiwis","bananas","orange","apples","sapota","pears","clementines")
  msort(nums)
  msort(fruits)

  def squareList(xs: List[Int]): List[Int] = xs match {
    case Nil => Nil
    case x :: xs1 => x*x :: squareList(xs1)
  }

  def squareList1(xs: List[Int]): List[Int] = xs map (x => x * x)
  squareList(nums)
  squareList1(nums)

  def pack[T](xs: List[T]): List[List[T]] = xs match {
    case Nil => Nil
    case y :: ys =>
      val (f, rest) = xs.span(x => x == y)
      f :: pack(rest)
  }

  val letters = List("a", "a", "a", "b", "c", "c", "a","d")
  pack(letters)

  def encode[T](xs: List[T]): List[(T, Int)] = xs match {
    case Nil => Nil
    case y :: ys =>
      val (first, rest) = xs.span(x => x == y)
      (first.head, first.size) :: encode(rest)
  }
  encode(letters)

  def simplerEncode[T](xs: List[T]): List[(T, Int)] =
    pack(xs) map (list => (list.head, list.size))

  simplerEncode(letters)

  def mapFunRight[T, U](xs: List[T], f: T => U): List[U] =
    (xs foldRight List[U]())( f(_) :: _ )

  def mapFunLeft[T, U](xs: List[T], f: T => U): List[U] =
    (xs foldLeft List[U]())( (x,y) => x ::: List(f(y)) )

  def lengthFun[T](xs: List[T]): Int =
    (xs foldRight 0)( (_,x) => x + 1 )


  lengthFun(letters)
  letters.map(x => x.toUpperCase())
  mapFunRight(letters, (x: String) => x.toUpperCase())
  mapFunLeft(letters, (x: String) => x.toUpperCase())

  val lookup1 = (1 to 26) zip ("abcdefghijklmnopqrstuvwxyz".toList)
  def ntop[T](name: List[T]): List[Int] = name match {
    case Nil => Nil
    case x :: xs => lookup1.filter(i => i._2 == x).head._1 :: ntop(xs)
  }

  def isVowel(c: Char): Boolean = c.toUpper match {
    case 'A' | 'E' | 'I' | 'O' | 'U' => true
    case _ => false
  }

  ntop("meera".toList).sum
  ntop("aarthi".toList).sum
  ntop("krishnanramaseshan".toList).sum

  val hello = "hello World"
  hello flatMap (c => List('.', c))
  val x = 1 to 5
  val y = 1 to 3

  x flatMap (i => y map (j => (i,j)))
  (x zip y).map(x => x._1 * x._2).sum

  (x zip y).map{ case (x,y) => x * y }.sum

  def isPrime(n: Int): Boolean = {
    !(2 until n).map(x => n % x).contains(0)
  }
  isPrime(37)
  isPrime(193)
  isPrime(2)
  isPrime(9)

  val n = 7
  val coll = (1 until n) flatMap(i => (1 until i) map(j => (i, j)))
  for( i <- coll if isPrime(i._1 + i._2) ) yield i

  for {
    i <- 1 until n
    j <- 1 until i
    if isPrime(i + j)
  } yield (i,j)

  (for( (f, s) <- (x zip y) ) yield f * s).sum

  class Poly(terms0: Map[Int, Double]) {
    val terms = terms0 withDefaultValue 0.0
    def this(bindings: (Int, Double)*) = this(bindings.toMap)
    /*
    def + (other: Poly) = new Poly(terms ++ (other.terms map adjust))
    def adjust(t: (Int, Double)): (Int, Double) = {
      val (exp, coeff) = t
      exp -> (coeff + terms(exp))
    }
    */

    /*
    * in foldLeft, the auxiliary function (addterms) in this case, what should be the signature
    * of this function?
    * (1) Its a binary function so needs two args
    * (2) The left arg is the type of the zero term i.e the term which has the initial value (in this case its
    * terms which is a collection we are starting with. So the left arg will be the same type as terms ie a Map
    * (3) The right arg is the type of each element in the collection, in this case its a pair of (Int, Double)
    * (4) The idea is that the you take each term of the collection on which you are applying the foldleft to and then
    * apply the function between this "current term" and the zero term and the result will be "stored" in the zero
    * term and that will be the new left term for the next iteration
    * (5) Note the use of '+' to add a single element of type pair to a map
    * (6) A way to interpret the return statement is that as soon as the new term is calculated and the new pair
    * is added to the Map 't' that map is returned. So the resultSet is slowly growing as and when the new pair is
    * processed
    * (7) Also, note that we have explicitly shows the return type and args type in the function addterms.
    * This is a good practice since it explicitly states what you want the function to work on and return.
    * When you do this, the error msgs in foldleft expression will make more sense. Otherwise, changing the
    * signature of the function based on the error msgs in foldleft will cause confusion and will deviate
    * from what you want the function to do.
    *
    * How do we know what should be the zero term?
    * A: Consider the boundary case. If the collection on which you are applying the foldLeft to is an empty
    * collection, what do you want the result to be? In this case, I want it to be terms
    *
    * */
    def + (other: Poly) = new Poly((other.terms foldLeft terms)(addterms))
    def addterms(t: Map[Int, Double], p: (Int, Double)): Map[Int, Double] = {
      val (currExp, currCoeff) = p
      t + (currExp -> (currCoeff + terms(currExp)))
    }
    override def toString() =
      (for( (k,v) <- terms.toSeq.sorted.reverse ) yield v + "x^" + k) mkString " + "
  }

  val p1 = new Poly(1 -> 2.0, 3 -> 4.0, 5 -> 6.2)
  val p2 = new Poly(0 -> 3.0, 3 -> 8.0)
  p1 + p2
}