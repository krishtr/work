import scala.io.Source
//****************************

val lookup = Map('2'->"ABC",'3'->"DEF", '4'->"GHI", '5'->"JKL", '6'->"MNO",'7'->"PQRS", '8'->"TUV", '9'->"WXYZ")
val in = Source.fromURL("http://lamp.epfl.ch/files/content/sites/lamp/files/teaching/progfun/linuxwords.txt")
val words: List[String] = in.getLines.toList filter (word => word forall (ch => ch.isLetter))

// get reverse mapping - A->2, G->4 etc
val reverseLookup: Map[Char, Char] = for( (num, letters) <- lookup; letter <- letters ) yield letter -> num

// given a word (String) get the corresponding number code representing this word
def wordCode(word: String): String = word.toUpperCase map reverseLookup
wordCode("Java")

// get a list of words that are represented by the same number code
val wordsByNum: Map[String, Seq[String]] = words groupBy wordCode withDefaultValue Seq()

// given a number, get a set of all possible word-lists made up by the number
def encode(number: String): Set[List[String]] =
  if(number.isEmpty) Set(List())
  else
    (for {
      num <- 1 to number.length
      word <- wordsByNum(number take num)
      rest <- encode(number drop num)
    } yield word :: rest).toSet

val test = "7225247386"
4
encode(test)
def translate(number: String): Set[String] = encode(number) map (_ mkString " ")
translate(test)