/*/*
def sum(f: Int => Int, a: Int, b: Int) : Int = {

  def loop(a: Int, acc: Int) : Int = {
    if(a > b) acc
    else loop(a + 1, f(a) + acc)
  }

  loop(a, 0)
}

def fact(x : Int) : Int = if(x == 0) 1 else x * fact(x-1)

sum(fact, 1, 5)
sum(x => x * x, 3, 5) // sq(3) + sq(4) + sq(5)

def sum1(f: Int => Int): (Int, Int) => Int = {
  def sumF(a: Int, b: Int) : Int = {
    if(a > b) 0
    else f(a) + sumF(a + 1, b)
  }
  sumF // returning a function
} // so takes functions as params and returns functions as results

sum1(fact)(1,5) // passing functions (fact) as parameters

def sumFactorials = sum1(fact)
sum1(fact)
sumFactorials(1,5)

def sum2(f : Int => Int) (a: Int, b: Int) : Int = {
  if(a > b) 0
  else f(a) + sum2(f)(a + 1,b)
}

sum2(fact)(1,5)

/*
// calcs product of values of function on a given interval
def product1(f: Int => Int) : (Int, Int) => Int = {
  def prodF(a: Int, b: Int) : Int = {
    if(a > b) 1
    else f(a) * prodF(a + 1, b)
  }
  prodF
}
*/


def product(f: Int => Int)(a: Int, b: Int) : Int = {
  if(a > b) 1
  else f(a) * product(f)(a + 1, b)
}


//product(fact)
//product(x => x)(1,5)
//product1(fact)(1,5)

// factorials in terms of product
def factorial(y: Int) = product(x => x)(1, y)
factorial(4)

def mapReduce(initial: Int, f: Int => Int, combine: (Int, Int) => Int)(a: Int, b: Int) : Int = {
  if(a > b) initial
  else combine(f(a), mapReduce(initial, f, combine)(a + 1, b))
}

mapReduce(1, x=>x, (a,b) => a*b)(1,5)
def factorialo(y: Int) : Int = mapReduce(1, x=>x, (a,b) => a*b)(1,y)
factorialo(5)
factorialo(10)
*/

//------------------------------------------------------
import math.abs

object Week2 {

  val tolerance = 0.0000001
  def isCloseEnough(x: Double, y: Double): Boolean =
    abs((x - y) / x) / x < tolerance

  def fixedPoint(f: Double => Double)(x: Double) : Double = {
    def iterate(guess: Double) : Double = {
      val next = f(guess)
      if(isCloseEnough(next, guess)) next
      else iterate(next)
    }
    iterate(x)
  }

  def averageDamp(f: Double => Double)(x: Double) = 0.5 * (x + f(x))
  def sqrt(x: Double) = fixedPoint(averageDamp(y => x/y))(1)
  sqrt(2)
  sqrt(5)
  // Even though averageDamp expects a function and a value,
  // averageDamp(y => x/y) [without the value parameter] is a "function object"
  // which can then be treated as one and passed around like to fixedPoint in this case which
  // expects a function
  // http://stackoverflow.com/questions/37902008/calling-curried-functions-in-scala
}

*/
/*
import math.abs

class Rational(x: Int, y: Int) {
  require(y != 0, "denominator should be nonzero")

  private final def gcd(a: Int, b: Int) : Int = if(b == 0) a else gcd(b, a % b)
  private final val g = gcd(x,y) // called on construction
  val numer = x / g
  val denom = y / g

  // x and y are made to vals from defs because we want them to be evaluated only
  // once (call by value) instead of everytime they are called (call by name)

  // now everytime Rational is created, x and y are initialised to its simplest form
  // Note that Rational(...) represents a class declaration and a ctor def in the same
  // statement and hence member variables are going to be initialised as though the ctor
  // was called

  def + (that: Rational) =
    new Rational(
      this.numer * that.denom + that.numer * this.denom,
      this.denom * that.denom
    )

  def - (that: Rational) = this + -that // DRY principle (Don't repeat yourself)
  // instead of repeating the complicated formula for addition with sign changed,
  // you implement your function in terms of that complicated formula so that "you
  // don't repeat yourself"

  def unary_- = new Rational(-this.numer, this.denom)

  def < (that: Rational) : Boolean = this.numer * that.denom < that.numer * this.denom

  def max(that: Rational) = if(this < that) that else this

  override def toString = (if(this.denom < 0) -this.numer else this.numer) + "/" + abs(this.denom)
}


val x = new Rational(-1,-3)
val y = new Rational(5,6)
val z = new Rational(3,4)
y < z
y max z
x max y
x max z
x max x
z max y

// x - y - z
// x.add(y.neg).add(z.neg)
x - y - z
*/

type Set = Int => Boolean
def contains(s: Set, elem: Int) : Boolean = s(elem)
def singletonSet(elem: Int): Set = (x:Int) => x == elem
def set5 = singletonSet(5)
def set6 = singletonSet(6)

def union(s: Set, t: Set): Set = (x:Int) => contains(s, x) || contains(t, x)
def intersect(s: Set, t: Set): Set = (x:Int) => contains(s, x) && contains(t, x)
def diff(s: Set, t: Set): Set = (x:Int) => contains(s, x) && !contains(t, x)

def first100 = (x:Int) => x >= 1 && x <= 100
def next100 = (x:Int) => x >= 100 && x <= 200
def first200 = union(first100, next100)
def just100 = intersect(first100, next100)
def first99 = diff(first100, next100)

first200(2)
first200(201)
just100(1)
just100(100)
first99(100)
first99(150)
first99(99)
first99(1)
first99(0)

/**
  * Returns the subset of `s` for which `p` holds.
  */
def filter(s: Set, p: Int => Boolean): Set = (x:Int) => contains(s,x) && p(x)
def multiples5in100 = filter(first100, (x:Int) => x % 5 == 0)
multiples5in100(4)
multiples5in100(5)
multiples5in100(95)
multiples5in100(40)
multiples5in100(105)

val bound = 1000

def toSetString(s: Set): String = {
  val xs = for (i <- -bound to bound if contains(s, i)) yield i
  xs.mkString("{", ",", "}")
}

/**
  * Prints the contents of a set on the console.
  */
def printSet(s: Set) {
  println(toSetString(s))
}
/*
printSet(first100)
printSet(first99)
printSet(first200)
printSet(just100)
*/

/**
  * Returns whether all bounded integers within `s` satisfy `p`.
  */
def forall(s: Set, p: Int => Boolean): Boolean = {
  def iter(a: Int): Boolean = {
    if (a > bound) true
    else if (contains(s,a) && !p(a)) false
    else iter(a + 1)
  }
  return iter(-bound)
}

def even = (x:Int) => x % 2 == 0
def evenfirst100 = intersect(first100, even)
forall( union( evenfirst100, singletonSet(102) ), even )

/**
  * Returns whether there exists a bounded integer within `s`
  * that satisfies `p`.
  */
def exists(s: Set, p: Int => Boolean): Boolean = !forall(s, (x:Int) => !p(x))
def first9 = (x:Int) => x > 0 && x < 10
def multipleOf(n: Int) = (x:Int) => x % n == 0
forall(first9, multipleOf(5))
exists(first9, multipleOf(5))
forall(first9, multipleOf(10))
exists(first9, multipleOf(10))

/**
  * Returns a set transformed by applying `f` to each element of `s`.
  */
def map(s: Set, f: Int => Int): Set = {
  def iter(a: Int, acc: Set) : Set = {
    if(a > bound) acc
    else if(contains(s,a)) iter(a+1, union(acc, (x:Int) => x == f(a)))
    else iter(a + 1, acc)
  }
  iter(-bound, (x: Int) => false)
}
printSet(map(first9, (x:Int) => 2*x)) // {2,4,6,8,10,12,14,16,18}