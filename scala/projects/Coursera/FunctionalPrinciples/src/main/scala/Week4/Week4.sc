object Week4 {
  abstract class Nat {
    def isZero: Boolean
    def predecessor: Nat
    def successor: Nat = new Succ(this)
    def +(n: Nat): Nat
    def -(n: Nat): Nat
  }

  object Zero extends Nat {
    def isZero: Boolean = true
    def predecessor: Nat = throw new Error("no predecessor for Zero")
    def +(that: Nat): Nat = that
    def -(that: Nat): Nat =
      if(that.isZero) this
      else throw new Error("no predecessor for Zero")
  }

  class Succ(n: Nat) extends Nat {
    def isZero: Boolean = false
    def predecessor: Nat = n
    def +(that: Nat): Nat = new Succ(n + that)
    def -(that: Nat): Nat =
      if(that.isZero) this
      else n - that.predecessor
  }

  trait List[T] {
    def isEmpty: Boolean
    def head: T
    def tail: List[T]
  }

  class Nil[T] extends List[T] {
    def isEmpty: Boolean = true
    def head: Nothing = throw new NoSuchElementException("Nil.head")
    def tail: Nothing = throw new NoSuchElementException("Nil.tail")
    override def toString: String = "X"
  }

  class Cons[T](val head: T, val tail: List[T]) extends List[T] {
    def isEmpty: Boolean = false
    override def toString: String = head + "->" + tail
  }

  object List {
    def apply[T]() = new Nil
    def apply[T](n: Int) = new Cons(n, new Nil)
    def apply(l: Int, r: Int) = new Cons(l, new Cons(r, new Nil))
  }

  trait Expr
  case class Number(n: Int) extends Expr
  case class Sum(e1: Expr, e2: Expr) extends Expr
  case class Product(e1: Expr, e2: Expr) extends Expr
  case class Var(x: String) extends Expr

  def eval(e: Expr): Int = e match {
    case Number(n) => n
    case Sum(e1, e2) => eval(e1) + eval(e2)
    case Product(e1, e2) => eval(e1) * eval(e2)
  }

  def show(e: Expr): String = e match {
    case Number(n) => n.toString
    case Sum(e1, e2) => "(" + show(e1) + "+" + show(e2) + ")"
    case Product(e1, e2) => show(e1) + "*" + show(e2)
    case Var(x) => x.toString
  }

  eval(Sum(Number(1), Number(2)))
  show(Sum(Number(1), Number(2)))
  show(Product(Number(1), Sum(Number(2), Var("x")))) // 1*(2+x)
  show(Sum(Number(1), Product(Number(2), Var("x")))) // 1+2*x
  show(Sum(Product(Number(2), Var("x")), Var("y")))
  show(Product(Sum(Number(2), Var("x")), Var("y")))
}