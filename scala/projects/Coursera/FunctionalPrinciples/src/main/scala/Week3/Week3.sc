
object IntSets {

  abstract class IntSet {
    def incl(x: Int): IntSet
    def contains(x: Int): Boolean
    def union(other: IntSet): IntSet
  }

  object Empty extends IntSet {
    def incl(x: Int): IntSet = new NonEmpty(x, Empty, Empty)
    def contains(x: Int): Boolean = false
    def union(other: IntSet): IntSet = other
    override def toString = "."
  }

  class NonEmpty(elem: Int, left: IntSet, right: IntSet) extends IntSet {

    def incl(x: Int): IntSet = {
      if(x < elem) new NonEmpty(elem, left incl x, right)
      else if(x > elem) new NonEmpty(elem, left, right incl x)
      else this
    }

    def contains(x: Int): Boolean = {
      if(x < elem) left contains x
      else if(x > elem) right contains x
      else true
    }

    def union(other: IntSet): IntSet = {
      ((left union right) union other) incl elem
    }

    override def toString = "{" + left + elem + right + "}"
  }

  val t1 = new NonEmpty(7, Empty, Empty) incl 5 incl 9 incl 11 incl 3 incl 13
  val a: Array[NonEmpty] = Array(new NonEmpty(1, Empty, Empty))
  //val b: Array[IntSet] = a
}

trait List[T] {
  def isEmpty: Boolean
  def head: T
  def tail: List[T]
}

class Nil[T] extends List[T] {
  def isEmpty: Boolean = true
  def head: Nothing = throw new NoSuchElementException("Nil.head")
  def tail: Nothing = throw new NoSuchElementException("Nil.tail")
  override def toString: String = "X"
}

class Cons[T](val head: T, val tail: List[T]) extends List[T] {
  def isEmpty: Boolean = false
  override def toString: String = head + "->" + tail
}

def nth[T](n: Int, list: List[T]): T =
  if (list.isEmpty) throw new IndexOutOfBoundsException("Out of bounds")
  else if (n == 0) list.head
  else nth(n - 1, list.tail)


val l = new Cons(1, new Cons(2, new Cons(3, new Cons(4, new Cons(5, new Cons(6, new Nil))))))
l
//nth(2, l)
