object session {

  def abs(x : Double) : Double = if (x < 0) -x else x

  def mySqrt(x : Double) : Double = {

    def sqrtIter(guess: Double): Double =
      if (isGoodEnough(guess)) guess
      else sqrtIter(improve(guess))

    def isGoodEnough(guess: Double): Boolean =
      abs(guess * guess - x) / x < 0.00000000001

    def improve(guess: Double): Double =
      (guess + x / guess) * 0.5

    sqrtIter(1.0)

  }

  mySqrt(2.0)
  mySqrt(25)
  mySqrt(100)
  mySqrt(1)
  mySqrt(1e-6)
  mySqrt(1e60)
  mySqrt(1e50)

  def factorial(x: Int) = {
    def loop(acc: Int, n: Int) : Int =
      if (n == 0) acc
      else loop(acc * n, n - 1)
    loop(1, x)
  }

  factorial(4)
  factorial(5)

  def pascal(c: Int, r: Int) : Int =
    if(c == 0 || c == r) 1
    else pascal(c, r - 1) + pascal(c - 1, r - 1)

  for (row <- 0 to 10) {
    for (col <- 0 to row)
      print(pascal(col, row) + " ")
    println()
  }

  def balance(chars: List[Char]) : Boolean = {
    def process(chars: List[Char], acc: Int) : Boolean =
      if(chars.isEmpty) acc == 0
      else {
        if(chars.head == '(') process(chars.tail, acc + 1)
        else {
          if(chars.head == ')') {
            if(acc == 0) false
            else process(chars.tail, acc - 1)
          } else process(chars.tail, acc)
        }
      }
    if(chars.isEmpty) false
    else process(chars, 0)
  }

  balance("".toList)
  balance("(".toList)
  balance("()".toList)
  balance("())(".toList)
  balance(")".toList)
  balance(":-)".toList)
  balance("(if (zero? x) max (/ 1 x))".toList)
  balance("I told him (that it’s not (yet) done). (But he wasn’t listening)".toList)
  balance("((((((((((-))))))))))".toList)

  def countChange(money: Int, coins: List[Int]) : Int =
    if(money == 0) 1
    else if(money > 0 && coins.nonEmpty)
      countChange(money - coins.head, coins) + countChange(money, coins.tail)
    else 0

  countChange(4, List(1,2))

}