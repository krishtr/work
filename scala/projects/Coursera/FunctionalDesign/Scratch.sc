
trait JSON
case class JObj(bindings: Map[String, JSON]) extends JSON
case class JSeq(elems: List[JSON]) extends JSON
case class JStr(name: String) extends JSON
case class JNum(num: Int) extends JSON
case class JBool(flag: Boolean) extends JSON
case object JNull extends JSON

val json = JObj(Map(
    "firstName" -> JStr("John"),
    "lastName" -> JStr("Smith"),
    "address" -> JObj(Map(
        "stateAddress" -> JStr("21 2nd Street"),
        "state" -> JStr("NY"),
        "postalCode" -> JNum(10021)
    )),
    "phoneNumbers" -> JSeq(List(
        JObj(Map(
            "type" -> JStr("home"), "number" -> JStr("212 555-1234")
        )),
        JObj(Map(
            "type" -> JStr("fax"), "number" -> JStr("646 555-4567")
        ))
    ))
))

def show(json: JSON) : String = {
    def showlist(list: List[JSON]) : String = {
        list match {
            case h :: Nil => show(h)
            case h :: t => show(h) + "," + showlist(t)
            case Nil => ""
        }
    }
    def showmap(map: Map[String, JSON]) : String = {
        (for((l, r) <- map) yield "\"" + l + "\":" + show(r)).mkString(", ")
    }
    def showmap1(map: Map[String, JSON]): String = {
        map map { case (k,v) => "\"" + k + "\":" + show(v) } mkString ", "
    }
    json match {
        case JNull => "null"
        case JBool(x) => x.toString
        case JNum(x) => x.toString
        case JStr(x) => "\"" + x + "\""
        case JSeq(x) => "[" + x.map(show).mkString(", ") + "]\n"
        case JObj(x) => "{" + showmap(x) + "}\n"
    }
}

case class Book(title: String, authors: List[String])
val books = List(
    Book(title = "Effective Java", authors = List("Bloch Joshua")),
    Book(title = "Programming in C++", authors = List("Bjarne Stroustrup")),
    Book(title = "Effective C++", authors = List("Scott Meyers")),
    Book(title = "Effective Modern C++", authors = List("Scott Meyers"))
)

for(b <- books; a <- b.authors if a startsWith "Scott") yield b.title
books flatMap ( b =>
    b.authors.filter(a => a startsWith "Scott") map (_ => b.title)
    )

trait Generator[+T] {
    self =>
    def generate : T
    def flatMap[S](f: T => Generator[S]): Generator[S] = new Generator[S] {
        def generate = f(self.generate).generate
    }
    def map[S](f: T => S): Generator[S] = new Generator[S] {
        def generate: S = f(self.generate)
    }
}
val integers = new Generator[Int] {
    val rand = new java.util.Random
    def generate = rand.nextInt()
}
val booleans = for(x <- integers) yield x > 0
def single[T](x: T) = new Generator[T] {
    def generate = x
}
def choose(lo: Int, hi: Int): Generator[Int] =
    for(x <- integers) yield lo + x % (hi - lo)
def oneOf[T](xs: T*) = {
    for(idx <- choose(0, xs.length)) yield xs(idx)
}

def emptyList = single(Nil)
def nonEmptyList = for {
    head <- integers
    tail <- lists
} yield head :: tail

def lists: Generator[List[Int]] =  for {
    isEmpty <- booleans
    list <- if (isEmpty) emptyList else nonEmptyList
} yield list

trait Tree
case class Inner(lhs: Tree, rhs: Tree) extends Tree
case class Leaf(x: Int) extends Tree

val trees: Generator[Tree] = for {
    isLeaf <- booleans
    x <- integers
    tree <- if(isLeaf) leaf(x) else inner
} yield tree

trees.generate

def leaf(x: Int) = single(Leaf(x))
def inner = for {
    lhs <- trees
    rhs <- trees
} yield Inner(lhs, rhs)